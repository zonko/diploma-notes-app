class NoteAttachment {
  String name;
  String type;
  List<int> content;

  NoteAttachment();

  Map<String, dynamic> toDbMap(int noteId) {
    var map = <String, dynamic> {
      'Name': name,
      'Type': type,
      'Content': content,
      'NoteId': noteId
    };
    return map;
  }

  NoteAttachment.fromDbMap(Map<String, dynamic> map) {
    name = map['Name'];
    type = map['Type'];
    content = map['Content'];
  }
}