﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Notes.Service.DataAccess.Entities
{
    internal class DbNoteAttachment
    {
        [Key]
        public long Id { get; set; }
        public virtual  DbNote Note {get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public byte[] Content { get; set; }
    }
}