﻿using System;
using System.Globalization;
using System.IO;
using Xamarin.Forms;

namespace XForms.Notes.UI.Converters
{
    public class ByteArrayToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is byte[] imageBytes) || imageBytes.Length <= 0) return null;
            try
            {
                var imageSource = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                return imageSource;
            }
            catch
            {
                // ignored
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}