"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialog_service_1 = require("~/services/dialog.service");
var AppComponent = /** @class */ (function () {
    function AppComponent(_dialogService) {
        this._dialogService = _dialogService;
        this.isAlertDisplayed = false;
        this.isBusy = false;
        this.alertTitle = "Title";
        this.alertText = "Text";
        this.alertButtonText = "OK";
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._dialogService.showMessageDialog$.subscribe(function (options) {
            _this.alertTitle = options.title;
            _this.alertText = options.text;
            _this.alertButtonText = options.okButtonText;
            _this.isAlertDisplayed = true;
        });
        this._dialogService.setBusyIndicator$.subscribe(function (busy) {
            _this.isBusy = busy;
        });
    };
    AppComponent.prototype.onAlertConfirmed = function () {
        this.isAlertDisplayed = false;
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: "app-app",
            templateUrl: "./app.component.html",
        }),
        __metadata("design:paramtypes", [dialog_service_1.DialogService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBZ0Q7QUFDaEQsNERBQXdEO0FBT3hEO0lBT0ksc0JBQXFCLGNBQTZCO1FBQTdCLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBTmxELHFCQUFnQixHQUFHLEtBQUssQ0FBQztRQUN6QixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2YsZUFBVSxHQUFHLE9BQU8sQ0FBQztRQUNyQixjQUFTLEdBQUcsTUFBTSxDQUFDO1FBQ25CLG9CQUFlLEdBQUcsSUFBSSxDQUFDO0lBR3ZCLENBQUM7SUFFRCwrQkFBUSxHQUFSO1FBQUEsaUJBVUM7UUFURyxJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxVQUFBLE9BQU87WUFDcEQsS0FBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztZQUM5QixLQUFJLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUM7WUFDNUMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSTtZQUNoRCxLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFRCx1Q0FBZ0IsR0FBaEI7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO0lBQ2xDLENBQUM7SUF4QlEsWUFBWTtRQUx4QixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFNBQVM7WUFDbkIsV0FBVyxFQUFFLHNCQUFzQjtTQUN0QyxDQUFDO3lDQVN1Qyw4QkFBYTtPQVB6QyxZQUFZLENBMkJ4QjtJQUFELG1CQUFDO0NBQUEsQUEzQkQsSUEyQkM7QUEzQlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge0RpYWxvZ1NlcnZpY2V9IGZyb20gXCJ+L3NlcnZpY2VzL2RpYWxvZy5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiBcImFwcC1hcHBcIixcclxuICAgIHRlbXBsYXRlVXJsOiBcIi4vYXBwLmNvbXBvbmVudC5odG1sXCIsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQXBwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0e1xyXG4gICAgaXNBbGVydERpc3BsYXllZCA9IGZhbHNlO1xyXG4gICAgaXNCdXN5ID0gZmFsc2U7XHJcbiAgICBhbGVydFRpdGxlID0gXCJUaXRsZVwiO1xyXG4gICAgYWxlcnRUZXh0ID0gXCJUZXh0XCI7XHJcbiAgICBhbGVydEJ1dHRvblRleHQgPSBcIk9LXCI7XHJcblxyXG4gICAgY29uc3RydWN0b3IgKHByaXZhdGUgX2RpYWxvZ1NlcnZpY2U6IERpYWxvZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nJC5zdWJzY3JpYmUob3B0aW9ucyA9PntcclxuICAgICAgICAgICAgdGhpcy5hbGVydFRpdGxlID0gb3B0aW9ucy50aXRsZTtcclxuICAgICAgICAgICAgdGhpcy5hbGVydFRleHQgPSBvcHRpb25zLnRleHQ7XHJcbiAgICAgICAgICAgIHRoaXMuYWxlcnRCdXR0b25UZXh0ID0gb3B0aW9ucy5va0J1dHRvblRleHQ7XHJcbiAgICAgICAgICAgIHRoaXMuaXNBbGVydERpc3BsYXllZCA9IHRydWU7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zZXRCdXN5SW5kaWNhdG9yJC5zdWJzY3JpYmUoYnVzeSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNCdXN5ID0gYnVzeTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIG9uQWxlcnRDb25maXJtZWQoKSB7XHJcbiAgICAgICAgdGhpcy5pc0FsZXJ0RGlzcGxheWVkID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG5cclxufVxyXG4iXX0=