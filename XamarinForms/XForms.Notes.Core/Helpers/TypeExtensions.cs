﻿using System;
using XForms.Notes.Core.WebService.Models.Responses;

namespace XForms.Notes.Core.Helpers
{
    public static class TypeExtensions
    {
        public static string ToErrorMessage(this WsErrorType errorType)
        {
            switch (errorType)
            {
                case WsErrorType.Ok: return "";
                case WsErrorType.AuthorizationError: return "Avtentikacija s spletnim srežnikom ni uspela.";
                case WsErrorType.BadRequest: return "Neznana zaheteva.";
                case WsErrorType.InternalServerError: return "Notranja napaka na strežniku.";
                case WsErrorType.UsernameIsTaken: return "Uporabniško ime je zasedeno.";
                case WsErrorType.WrongUsernameOrPassword: return "Napačno uporabniško ime ali geslo.";
                case WsErrorType.ServerNotReachable: return "Strežnik ni dosegljiv.";
                default: return "Nepričakovana napaka";
            }
        }
    }
}