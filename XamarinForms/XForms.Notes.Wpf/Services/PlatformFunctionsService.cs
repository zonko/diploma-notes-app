﻿using SQLite;
using XForms.Notes.Core.Models.IocInterfaces;

namespace XForms.Notes.Wpf.Services
{
    public class PlatformFunctionsService : IPlatformFunctionsService
    {
        public SQLiteConnection GetDatabaseConnection(string filename)
        {
            var conn = new SQLiteConnection(filename);

            conn.Query<int>("PRAGMA key=geslo");          // ta vrstica samo nastavi geslo, ki bo uporabljeno
            conn.Query<int>("SELECT count(*) FROM sqlite_master");    // v tej pa bo padel exception, če geslo ni pravo

            return conn;
        }
    }
}