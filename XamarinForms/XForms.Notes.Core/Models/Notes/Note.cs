﻿using System;
using System.Collections.Generic;

namespace XForms.Notes.Core.Models.Notes
{
    public class Note
    {
        public int? Id { get; set; }
        public int? Cid { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime DateTimeChanged { get; set; }
        public bool Modified { get; set; }
        public bool Active { get; set; }
        public List<NoteAttachment> Attachments { get; set; }
    }
}