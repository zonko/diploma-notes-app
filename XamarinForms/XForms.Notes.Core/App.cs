﻿using MvvmCross;
using MvvmCross.ViewModels;
using XForms.Notes.Core.Database;
using XForms.Notes.Core.Logic;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.Services;
using XForms.Notes.Core.ViewModels;
using XForms.Notes.Core.WebService;

namespace XForms.Notes.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.ConstructAndRegisterSingleton<IDialogService, DialogService>();

            if (!Mvx.CanResolve<ISecureStorageService>())
            {
                Mvx.RegisterSingleton<ISecureStorageService>(() => new SecureStorageService());
            }
            Mvx.RegisterType<DataAccess>();
            Mvx.RegisterType<NotificationsLogic>();
            Mvx.RegisterType<NotesServiceManager>();
            Mvx.RegisterType<NotesLogic>();
            Mvx.RegisterType<UserLogic>();

            // register the appstart object
            RegisterAppStart<StartupPageViewModel>();
        }
    }
}