﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Notes.Service.DataAccess.Entities
{
    internal class DbNote
    {
        [Key]
        public long Id { get; set; }
        public bool Active { get; set; }
        public virtual DbUser User { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime DateTimeChanged { get; set; }

        public ICollection<DbNoteAttachment> Attachments { get; set; }
    }
}