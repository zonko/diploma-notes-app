"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("~/services/user.service");
var helpers_1 = require("~/helpers/helpers");
var router_1 = require("@angular/router");
var dialog_service_1 = require("~/services/dialog.service");
var page_1 = require("tns-core-modules/ui/page");
var StartupComponent = /** @class */ (function () {
    function StartupComponent(_userService, _router, _dialogService, _page) {
        this._userService = _userService;
        this._router = _router;
        this._dialogService = _dialogService;
        this._page = _page;
    }
    StartupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._page.actionBarHidden = true;
        this._userService.checkLogin().then(function (res) {
            if (res.result) {
                if (!res.success) {
                    _this._dialogService.showMessageDialog("Opozorilo", "Sinhronizacija ni uspela: " + helpers_1.Helpers.getErrorMessage(res.errorType));
                }
                _this._router.navigateByUrl("notes");
                return;
            }
            else if (!res.success) {
                var errorMessage = "Preverjanje prijave ni uspelo: " + helpers_1.Helpers.getErrorMessage(res.errorType);
                _this._dialogService.showMessageDialog("Napaka", errorMessage);
            }
            _this._router.navigateByUrl("login");
        }).catch(function (err) {
            _this._dialogService.showMessageDialog("Napaka", "Preverjanje prijave ni uspelo: Prišlo je do nepričakovane napake: " + err);
            _this._router.navigateByUrl("login");
        });
    };
    StartupComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-startup',
            templateUrl: './startup.component.html',
            styleUrls: ['./startup.component.css']
        }),
        __metadata("design:paramtypes", [user_service_1.UserService,
            router_1.Router,
            dialog_service_1.DialogService,
            page_1.Page])
    ], StartupComponent);
    return StartupComponent;
}());
exports.StartupComponent = StartupComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhcnR1cC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzdGFydHVwLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFrRDtBQUNsRCx3REFBc0Q7QUFDdEQsNkNBQTBDO0FBQzFDLDBDQUF1QztBQUN2Qyw0REFBd0Q7QUFDeEQsaURBQThDO0FBVTlDO0lBRUksMEJBQW9CLFlBQXlCLEVBQ3pCLE9BQWUsRUFDZixjQUE2QixFQUM3QixLQUFXO1FBSFgsaUJBQVksR0FBWixZQUFZLENBQWE7UUFDekIsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUNmLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQzdCLFVBQUssR0FBTCxLQUFLLENBQU07SUFBSSxDQUFDO0lBRXBDLG1DQUFRLEdBQVI7UUFBQSxpQkFrQkM7UUFqQkcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRztZQUNuQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDYixFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUNmLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLDRCQUE0QixHQUFFLGlCQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUM3SCxDQUFDO2dCQUNELEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNwQyxNQUFNLENBQUM7WUFDWCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLElBQU0sWUFBWSxHQUFHLGlDQUFpQyxHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDaEcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUM7WUFDbEUsQ0FBQztZQUNELEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3hDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7WUFDUixLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxvRUFBb0UsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUM1SCxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN4QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUF6QlEsZ0JBQWdCO1FBTjVCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGFBQWE7WUFDdkIsV0FBVyxFQUFFLDBCQUEwQjtZQUN2QyxTQUFTLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQztTQUN6QyxDQUFDO3lDQUdvQywwQkFBVztZQUNoQixlQUFNO1lBQ0MsOEJBQWE7WUFDdEIsV0FBSTtPQUx0QixnQkFBZ0IsQ0EyQjVCO0lBQUQsdUJBQUM7Q0FBQSxBQTNCRCxJQTJCQztBQTNCWSw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJ34vc2VydmljZXMvdXNlci5zZXJ2aWNlJztcclxuaW1wb3J0IHtIZWxwZXJzfSBmcm9tIFwifi9oZWxwZXJzL2hlbHBlcnNcIjtcclxuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtEaWFsb2dTZXJ2aWNlfSBmcm9tIFwifi9zZXJ2aWNlcy9kaWFsb2cuc2VydmljZVwiO1xyXG5pbXBvcnQge1BhZ2V9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2VcIjtcclxuaW1wb3J0IHtOb3RpZmljYXRpb25TZXJ2aWNlfSBmcm9tIFwifi9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZVwiO1xyXG5pbXBvcnQgKiBhcyBMb2NhbE5vdGlmaWNhdGlvbnMgZnJvbSBcIm5hdGl2ZXNjcmlwdC1sb2NhbC1ub3RpZmljYXRpb25zXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ2FwcC1zdGFydHVwJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zdGFydHVwLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3N0YXJ0dXAuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTdGFydHVwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF91c2VyU2VydmljZTogVXNlclNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2RpYWxvZ1NlcnZpY2U6IERpYWxvZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9wYWdlOiBQYWdlKSB7IH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLl9wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5fdXNlclNlcnZpY2UuY2hlY2tMb2dpbigpLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlcy5yZXN1bHQpIHtcclxuICAgICAgICAgICAgICAgIGlmICghcmVzLnN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiT3Bvem9yaWxvXCIsIFwiU2luaHJvbml6YWNpamEgbmkgdXNwZWxhOiBcIiArSGVscGVycy5nZXRFcnJvck1lc3NhZ2UocmVzLmVycm9yVHlwZSkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlQnlVcmwoXCJub3Rlc1wiKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICghcmVzLnN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGVycm9yTWVzc2FnZSA9IFwiUHJldmVyamFuamUgcHJpamF2ZSBuaSB1c3BlbG86IFwiICsgSGVscGVycy5nZXRFcnJvck1lc3NhZ2UocmVzLmVycm9yVHlwZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiTmFwYWthXCIsIGVycm9yTWVzc2FnZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlQnlVcmwoXCJsb2dpblwiKTtcclxuICAgICAgICB9KS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiTmFwYWthXCIsIFwiUHJldmVyamFuamUgcHJpamF2ZSBuaSB1c3BlbG86IFByacWhbG8gamUgZG8gbmVwcmnEjWFrb3ZhbmUgbmFwYWtlOiBcIiArIGVycik7XHJcbiAgICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZUJ5VXJsKFwibG9naW5cIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==