import 'dart:async';

import 'package:flutter/widgets.dart';

class Globals {
  static final navigatorKey = new GlobalKey<NavigatorState>();

  static final doRefreshController = new StreamController.broadcast();
  static Stream get doRefresh => doRefreshController.stream;
}