﻿using System;
using MvvmCross.ViewModels;
using XForms.Notes.Core.Helpers;
using XForms.Notes.Core.Logic;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.ViewModels.Main;
using XForms.Notes.Core.ViewModels.Main.Detail;

namespace XForms.Notes.Core.ViewModels
{
    public class StartupPageViewModel : MvxViewModel
    {
        private readonly IDialogService _dialogService;
        private readonly UserLogic _userLogic;
        private readonly NotesLogic _notesLogic;

        public StartupPageViewModel(IDialogService dialogService, UserLogic userLogic, NotesLogic notesLogic)
        {
            _userLogic = userLogic;
            _notesLogic = notesLogic;
            _dialogService = dialogService;
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            Init();
        }

        private async void Init()
        {
            try
            {
                if (await _userLogic.CheckLogin())
                {
                    try
                    {
                        await _notesLogic.Synchronize();
                    }
                    catch (Exception exception)
                    {
                        _dialogService.ShowAlert("Napaka", "Sinhronizacija ni uspela. " + ExceptionHelper.GetErrorMessage(exception));
                    }

                    await NavigationService.Navigate<MasterPageViewModel>();
                    await NavigationService.Navigate<NotesPageViewModel>();
                }
                else
                {
                    await NavigationService.Navigate<LoginPageViewModel>();
                }
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", ExceptionHelper.GetErrorMessage(exception));
                await NavigationService.Navigate<LoginPageViewModel>();
            }
        }
    }
}