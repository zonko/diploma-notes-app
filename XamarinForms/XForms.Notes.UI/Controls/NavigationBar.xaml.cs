﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XForms.Notes.UI.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NavigationBar : Grid
	{
		public NavigationBar ()
		{
			InitializeComponent ();
		}

	    public static readonly BindableProperty NavigationCommandProperty =
	        BindableProperty.Create(nameof(NavigationCommand), typeof(ICommand), typeof(ClickFrame), default(ICommand));

	    public ICommand NavigationCommand
        {
	        get => GetValue(NavigationCommandProperty) as ICommand;
	        set => SetValue(NavigationCommandProperty, value);
	    }

	    public static readonly BindableProperty ActionCommandProperty =
	        BindableProperty.Create(nameof(ActionCommand), typeof(ICommand), typeof(ClickFrame), default(ICommand));

	    public ICommand ActionCommand
        {
	        get => GetValue(ActionCommandProperty) as ICommand;
	        set => SetValue(ActionCommandProperty, value);
	    }

	    public string Title
	    {
	        get => TitleLabel.Text;
	        set => TitleLabel.Text = value;
	    }

	    public ImageSource NavigationButtonIconSource
	    {
	        get => NavigationButtonIcon.Source;
	        set => NavigationButtonIcon.Source = value;
	    }

	    public ImageSource ActionButtonIconSource
        {
	        get => ActionButtonIcon.Source;
	        set => ActionButtonIcon.Source = value;
	    }

        private void OnNavigationButtonClicked()
	    {
	        NavigationCommand?.Execute(EventArgs.Empty);
	    }

	    private void OnActionButtonClicked()
	    {
	        ActionCommand?.Execute(EventArgs.Empty);
	    }
	}
}