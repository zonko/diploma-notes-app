// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ws_sync_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WsSyncResponse _$WsSyncResponseFromJson(Map<String, dynamic> json) {
  return WsSyncResponse()
    ..errorType = json['errorType'] as int
    ..responseMessage = json['responseMessage'] as String
    ..errorMessage = json['errorMessage'] as String
    ..result = json['result'] == null
        ? null
        : WsSynchronization.fromJson(json['result'] as Map<String, dynamic>);
}

Map<String, dynamic> _$WsSyncResponseToJson(WsSyncResponse instance) =>
    <String, dynamic>{
      'errorType': instance.errorType,
      'responseMessage': instance.responseMessage,
      'errorMessage': instance.errorMessage,
      'result': instance.result
    };
