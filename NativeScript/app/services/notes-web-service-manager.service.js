"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var secure_storage_service_1 = require("~/services/secure-storage.service");
var ws_user_1 = require("~/models/web-service/ws-user");
var ws_synchronization_1 = require("~/models/web-service/ws-synchronization");
var operation_result_1 = require("~/models/core/operation-result");
var ws_error_type_1 = require("~/models/web-service/ws-error-type");
var operators_1 = require("rxjs/operators");
var NotesWebServiceManagerService = /** @class */ (function () {
    function NotesWebServiceManagerService(_http, _secureStorageService) {
        this._http = _http;
        this._secureStorageService = _secureStorageService;
        this._baseServiceUrl = "http://notesapp2-001-site1.1tempurl.com/NotesService.svc/";
        this._pingMethodName = "Ping";
        this._loginMethodName = "Login";
        this._registerNewUserMethodName = "RegisterNewUser";
        this._synchronizeMethodName = "Synchronize";
    }
    NotesWebServiceManagerService.prototype.ping = function () {
        return __awaiter(this, void 0, void 0, function () {
            var timeoutPromise, pingResult, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        timeoutPromise = new Promise(function (resolve, reject) {
                            var wait = setTimeout(function () {
                                clearTimeout(wait);
                                resolve(ws_error_type_1.WsErrorType.serverNotReachable);
                            }, 2000);
                        });
                        return [4 /*yield*/, Promise.race([
                                timeoutPromise,
                                this.doPing().toPromise()
                            ])];
                    case 1:
                        pingResult = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(pingResult === ws_error_type_1.WsErrorType.ok)];
                    case 2:
                        e_1 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    NotesWebServiceManagerService.prototype.doPing = function () {
        var fullUrl = this._baseServiceUrl + this._pingMethodName;
        return this._http.get(fullUrl).pipe(operators_1.map(function (r) { return r.errorType; }));
    };
    NotesWebServiceManagerService.prototype.login = function (username, passwordHash) {
        var fullUrl = this._baseServiceUrl + this._loginMethodName;
        var requestData = new ws_user_1.WsUser(username, passwordHash);
        return this._http.post(fullUrl, requestData, this.getHttpOptions());
    };
    NotesWebServiceManagerService.prototype.registerNewUser = function (username, passwordHash) {
        var fullUrl = this._baseServiceUrl + this._registerNewUserMethodName;
        var requestData = new ws_user_1.WsUser(username, passwordHash);
        return this._http.post(fullUrl, requestData, this.getHttpOptions());
    };
    NotesWebServiceManagerService.prototype.synchronize = function (notes, lastSyncDateTime) {
        var fullUrl = this._baseServiceUrl + this._synchronizeMethodName;
        var requestData = new ws_synchronization_1.WsSynchronization();
        requestData.lastSyncDateTime = lastSyncDateTime;
        requestData.notes = notes;
        return this._http.post(fullUrl, requestData, this.getHttpOptions());
    };
    NotesWebServiceManagerService.prototype.getHttpOptions = function () {
        var headers = new http_1.HttpHeaders();
        var userToken = this._secureStorageService.getUserToken();
        if (userToken) {
            headers = headers.set('Content-Type', 'application/json').set('Authorization', userToken);
        }
        else {
            headers = headers.set('Content-Type', 'application/json');
        }
        var httpOptions = {
            headers: headers
        };
        return httpOptions;
    };
    NotesWebServiceManagerService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            secure_storage_service_1.SecureStorageService])
    ], NotesWebServiceManagerService);
    return NotesWebServiceManagerService;
}());
exports.NotesWebServiceManagerService = NotesWebServiceManagerService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90ZXMtd2ViLXNlcnZpY2UtbWFuYWdlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibm90ZXMtd2ViLXNlcnZpY2UtbWFuYWdlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQStEO0FBRy9ELDRFQUF5RTtBQUN6RSx3REFBc0Q7QUFFdEQsOEVBQTRFO0FBQzVFLG1FQUErRDtBQUMvRCxvRUFBK0Q7QUFDL0QsNENBQW1DO0FBR25DO0lBU0ksdUNBQW9CLEtBQWlCLEVBQ2pCLHFCQUEyQztRQUQzQyxVQUFLLEdBQUwsS0FBSyxDQUFZO1FBQ2pCLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBc0I7UUFSdkQsb0JBQWUsR0FBRywyREFBMkQsQ0FBQztRQUM5RSxvQkFBZSxHQUFHLE1BQU0sQ0FBQztRQUN6QixxQkFBZ0IsR0FBRyxPQUFPLENBQUM7UUFDM0IsK0JBQTBCLEdBQUcsaUJBQWlCLENBQUM7UUFDL0MsMkJBQXNCLEdBQUcsYUFBYSxDQUFDO0lBSy9DLENBQUM7SUFFWSw0Q0FBSSxHQUFqQjs7Ozs7Ozt3QkFFWSxjQUFjLEdBQUcsSUFBSSxPQUFPLENBQWMsVUFBQyxPQUFPLEVBQUUsTUFBTTs0QkFDMUQsSUFBSSxJQUFJLEdBQUcsVUFBVSxDQUFDO2dDQUNsQixZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQ25CLE9BQU8sQ0FBQywyQkFBVyxDQUFDLGtCQUFrQixDQUFDLENBQUM7NEJBQzVDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDYixDQUFDLENBQUMsQ0FBQzt3QkFDYyxxQkFBTSxPQUFPLENBQUMsSUFBSSxDQUFDO2dDQUNoQyxjQUFjO2dDQUNkLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxTQUFTLEVBQUU7NkJBQzVCLENBQUMsRUFBQTs7d0JBSEUsVUFBVSxHQUFHLFNBR2Y7d0JBQ0Ysc0JBQU8sSUFBSSxrQ0FBZSxDQUFDLFVBQVUsS0FBSywyQkFBVyxDQUFDLEVBQUUsQ0FBQyxFQUFDOzs7d0JBRTFELHNCQUFPLElBQUksa0NBQWUsQ0FBQyxLQUFLLENBQUMsRUFBQzs7Ozs7S0FFekM7SUFFTyw4Q0FBTSxHQUFkO1FBQ0ksSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQzVELE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBbUIsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxTQUFTLEVBQVgsQ0FBVyxDQUFDLENBQUMsQ0FBQztJQUNqRixDQUFDO0lBRU0sNkNBQUssR0FBWixVQUFhLFFBQWdCLEVBQUUsWUFBb0I7UUFDL0MsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDN0QsSUFBTSxXQUFXLEdBQUcsSUFBSSxnQkFBTSxDQUFDLFFBQVEsRUFBRSxZQUFZLENBQUMsQ0FBQztRQUV2RCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQXFCLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7SUFDNUYsQ0FBQztJQUVNLHVEQUFlLEdBQXRCLFVBQXVCLFFBQWdCLEVBQUUsWUFBb0I7UUFDekQsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUM7UUFDdkUsSUFBTSxXQUFXLEdBQUcsSUFBSSxnQkFBTSxDQUFDLFFBQVEsRUFBRSxZQUFZLENBQUMsQ0FBQztRQUV2RCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQW1CLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7SUFDMUYsQ0FBQztJQUVNLG1EQUFXLEdBQWxCLFVBQW1CLEtBQW9CLEVBQUUsZ0JBQXdCO1FBQzdELElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDO1FBQ25FLElBQU0sV0FBVyxHQUFJLElBQUksc0NBQWlCLEVBQUUsQ0FBQztRQUM3QyxXQUFXLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFDaEQsV0FBVyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFnQyxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZHLENBQUM7SUFFTyxzREFBYyxHQUF0QjtRQUNJLElBQUksT0FBTyxHQUFHLElBQUksa0JBQVcsRUFBRSxDQUFDO1FBQ2hDLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUM1RCxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ1osT0FBTyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUM5RixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixPQUFPLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUM5RCxDQUFDO1FBQ0QsSUFBTSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLE9BQU87U0FDbkIsQ0FBQztRQUNGLE1BQU0sQ0FBQyxXQUFXLENBQUM7SUFDdkIsQ0FBQztJQXRFUSw2QkFBNkI7UUFEekMsaUJBQVUsRUFBRTt5Q0FVa0IsaUJBQVU7WUFDTSw2Q0FBb0I7T0FWdEQsNkJBQTZCLENBd0V6QztJQUFELG9DQUFDO0NBQUEsQUF4RUQsSUF3RUM7QUF4RVksc0VBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFdzUmVzcG9uc2UgfSBmcm9tICd+L21vZGVscy93ZWItc2VydmljZS93cy1yZXNwb25zZSc7XHJcbmltcG9ydCB7IFNlY3VyZVN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnfi9zZXJ2aWNlcy9zZWN1cmUtc3RvcmFnZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgV3NVc2VyIH0gZnJvbSAnfi9tb2RlbHMvd2ViLXNlcnZpY2Uvd3MtdXNlcic7XHJcbmltcG9ydCB7IFdzTm90ZSB9IGZyb20gJ34vbW9kZWxzL3dlYi1zZXJ2aWNlL3dzLW5vdGUnO1xyXG5pbXBvcnQgeyBXc1N5bmNocm9uaXphdGlvbiB9IGZyb20gJ34vbW9kZWxzL3dlYi1zZXJ2aWNlL3dzLXN5bmNocm9uaXphdGlvbic7XHJcbmltcG9ydCB7T3BlcmF0aW9uUmVzdWx0fSBmcm9tIFwifi9tb2RlbHMvY29yZS9vcGVyYXRpb24tcmVzdWx0XCI7XHJcbmltcG9ydCB7V3NFcnJvclR5cGV9IGZyb20gXCJ+L21vZGVscy93ZWItc2VydmljZS93cy1lcnJvci10eXBlXCI7XHJcbmltcG9ydCB7bWFwfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE5vdGVzV2ViU2VydmljZU1hbmFnZXJTZXJ2aWNlIHtcclxuXHJcbiAgICBwcml2YXRlIF9iYXNlU2VydmljZVVybCA9IFwiaHR0cDovL25vdGVzYXBwMi0wMDEtc2l0ZTEuMXRlbXB1cmwuY29tL05vdGVzU2VydmljZS5zdmMvXCI7XHJcbiAgICBwcml2YXRlIF9waW5nTWV0aG9kTmFtZSA9IFwiUGluZ1wiO1xyXG4gICAgcHJpdmF0ZSBfbG9naW5NZXRob2ROYW1lID0gXCJMb2dpblwiO1xyXG4gICAgcHJpdmF0ZSBfcmVnaXN0ZXJOZXdVc2VyTWV0aG9kTmFtZSA9IFwiUmVnaXN0ZXJOZXdVc2VyXCI7XHJcbiAgICBwcml2YXRlIF9zeW5jaHJvbml6ZU1ldGhvZE5hbWUgPSBcIlN5bmNocm9uaXplXCI7XHJcblxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2h0dHA6IEh0dHBDbGllbnQsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9zZWN1cmVTdG9yYWdlU2VydmljZTogU2VjdXJlU3RvcmFnZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgcGluZyAoKTogUHJvbWlzZTxPcGVyYXRpb25SZXN1bHQ8bnVsbD4+IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBsZXQgdGltZW91dFByb21pc2UgPSBuZXcgUHJvbWlzZTxXc0Vycm9yVHlwZT4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IHdhaXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQod2FpdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShXc0Vycm9yVHlwZS5zZXJ2ZXJOb3RSZWFjaGFibGUpO1xyXG4gICAgICAgICAgICAgICAgfSwgMjAwMCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBsZXQgcGluZ1Jlc3VsdCA9IGF3YWl0IFByb21pc2UucmFjZShbXHJcbiAgICAgICAgICAgICAgICB0aW1lb3V0UHJvbWlzZSxcclxuICAgICAgICAgICAgICAgIHRoaXMuZG9QaW5nKCkudG9Qcm9taXNlKClcclxuICAgICAgICAgICAgXSk7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHBpbmdSZXN1bHQgPT09IFdzRXJyb3JUeXBlLm9rKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBkb1BpbmcoKTogT2JzZXJ2YWJsZTxXc0Vycm9yVHlwZT4ge1xyXG4gICAgICAgIGNvbnN0IGZ1bGxVcmwgPSB0aGlzLl9iYXNlU2VydmljZVVybCArIHRoaXMuX3BpbmdNZXRob2ROYW1lO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9odHRwLmdldDxXc1Jlc3BvbnNlPG51bGw+PihmdWxsVXJsKS5waXBlKG1hcChyID0+IHIuZXJyb3JUeXBlKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGxvZ2luKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkSGFzaDogc3RyaW5nKTogT2JzZXJ2YWJsZTxXc1Jlc3BvbnNlPHN0cmluZz4+IHtcclxuICAgICAgICBjb25zdCBmdWxsVXJsID0gdGhpcy5fYmFzZVNlcnZpY2VVcmwgKyB0aGlzLl9sb2dpbk1ldGhvZE5hbWU7XHJcbiAgICAgICAgY29uc3QgcmVxdWVzdERhdGEgPSBuZXcgV3NVc2VyKHVzZXJuYW1lLCBwYXNzd29yZEhhc2gpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5wb3N0PFdzUmVzcG9uc2U8c3RyaW5nPj4oZnVsbFVybCwgcmVxdWVzdERhdGEsIHRoaXMuZ2V0SHR0cE9wdGlvbnMoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlZ2lzdGVyTmV3VXNlcih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZEhhc2g6IHN0cmluZyk6IE9ic2VydmFibGU8V3NSZXNwb25zZTxudWxsPj4ge1xyXG4gICAgICAgIGNvbnN0IGZ1bGxVcmwgPSB0aGlzLl9iYXNlU2VydmljZVVybCArIHRoaXMuX3JlZ2lzdGVyTmV3VXNlck1ldGhvZE5hbWU7XHJcbiAgICAgICAgY29uc3QgcmVxdWVzdERhdGEgPSBuZXcgV3NVc2VyKHVzZXJuYW1lLCBwYXNzd29yZEhhc2gpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5wb3N0PFdzUmVzcG9uc2U8bnVsbD4+KGZ1bGxVcmwsIHJlcXVlc3REYXRhLCB0aGlzLmdldEh0dHBPcHRpb25zKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzeW5jaHJvbml6ZShub3RlczogQXJyYXk8V3NOb3RlPiwgbGFzdFN5bmNEYXRlVGltZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxXc1Jlc3BvbnNlPFdzU3luY2hyb25pemF0aW9uPj4ge1xyXG4gICAgICAgIGNvbnN0IGZ1bGxVcmwgPSB0aGlzLl9iYXNlU2VydmljZVVybCArIHRoaXMuX3N5bmNocm9uaXplTWV0aG9kTmFtZTtcclxuICAgICAgICBjb25zdCByZXF1ZXN0RGF0YSA9ICBuZXcgV3NTeW5jaHJvbml6YXRpb24oKTtcclxuICAgICAgICByZXF1ZXN0RGF0YS5sYXN0U3luY0RhdGVUaW1lID0gbGFzdFN5bmNEYXRlVGltZTtcclxuICAgICAgICByZXF1ZXN0RGF0YS5ub3RlcyA9IG5vdGVzO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9odHRwLnBvc3Q8V3NSZXNwb25zZTxXc1N5bmNocm9uaXphdGlvbj4+KGZ1bGxVcmwsIHJlcXVlc3REYXRhLCB0aGlzLmdldEh0dHBPcHRpb25zKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0SHR0cE9wdGlvbnMoKSB7XHJcbiAgICAgICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoKTtcclxuICAgICAgICBjb25zdCB1c2VyVG9rZW4gPSB0aGlzLl9zZWN1cmVTdG9yYWdlU2VydmljZS5nZXRVc2VyVG9rZW4oKTtcclxuICAgICAgICBpZiAodXNlclRva2VuKSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnMgPSBoZWFkZXJzLnNldCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKS5zZXQoJ0F1dGhvcml6YXRpb24nLCB1c2VyVG9rZW4pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnMgPSBoZWFkZXJzLnNldCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IGhlYWRlcnNcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBodHRwT3B0aW9ucztcclxuICAgIH1cclxuXHJcbn1cclxuIl19