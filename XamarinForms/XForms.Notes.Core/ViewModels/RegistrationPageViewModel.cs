﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using XForms.Notes.Core.Helpers;
using XForms.Notes.Core.Logic;
using XForms.Notes.Core.Models.IocInterfaces;
namespace XForms.Notes.Core.ViewModels
{
    public class RegistrationPageViewModel : MvxViewModel
    {
        private readonly IDialogService _dialogService;
        private readonly UserLogic _userLogic;

        private string _username;
        private string _password;
        private string _passwordConfirm;

        private MvxAsyncCommand _registrationCommand;
        private MvxAsyncCommand _navigateToLoginCommand;


        public RegistrationPageViewModel(IDialogService dialogService, UserLogic userLogic)
        {
            _userLogic = userLogic;
            _dialogService = dialogService;
        }

        public string Username
        {
            get => _username;
            set => SetProperty(ref _username, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public string PasswordConfirm
        {
            get => _passwordConfirm;
            set => SetProperty(ref _passwordConfirm, value);
        }

        public MvxAsyncCommand RegistrationCommand => _registrationCommand ?? (_registrationCommand = new MvxAsyncCommand(Registration));

        public MvxAsyncCommand NavigateToLoginCommand => _navigateToLoginCommand ?? (_navigateToLoginCommand = new MvxAsyncCommand(NavigateToLogin));

        private async Task NavigateToLogin()
        {
            await NavigationService.Navigate<LoginPageViewModel>();
        }

        private async Task Registration()
        {
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                _dialogService.ShowAlert("Registracija", "Vnesite uporabniško ime in geslo.");
                return;
            }
            if (Password != PasswordConfirm)
            {
                _dialogService.ShowAlert("Registracija", "Geslo se ne ujema s potrditvenim geslom.");
                return;
            }
            _dialogService.SetBusy(true);

            try
            {
                await _userLogic.RegisterNewUser(Username, Password);
                _dialogService.ShowAlert("Registracija", "Registracija je bila uspešna. Sedaj se lahko prijavite v aplikacijo.");
                await NavigationService.Navigate<LoginPageViewModel>();
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", "Registracija ni uspela. " + ExceptionHelper.GetErrorMessage(exception));
            }
            _dialogService.SetBusy(false);
        }
    }
}