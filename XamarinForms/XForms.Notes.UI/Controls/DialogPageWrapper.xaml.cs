﻿using MvvmCross;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XForms.Notes.Core.Models.IocInterfaces;

namespace XForms.Notes.UI.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DialogPageWrapper : ContentView
	{
		public DialogPageWrapper()
		{
			InitializeComponent ();

		    var dialogContext = Mvx.Resolve<IDialogService>();
		    AlertContent.BindingContext = dialogContext;
		    BusyContent.BindingContext = dialogContext;
		}

	    public View PageContent
	    {
	        get => PContent;
	        set => PContent.Content = value;
	    }
	}
}