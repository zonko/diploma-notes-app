﻿using System;
using System.Collections.Generic;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace XForms.Notes.Core.Database.Models
{
    internal class DbtNote
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
        public int? Cid { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime DateTimeChanged { get; set; }
        public bool Modified { get; set; }
        public bool Active { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]      // One to many relationship with DbtNoteAttachment
        public List<DbtNoteAttachment> Attachments { get; set; }
    }
}