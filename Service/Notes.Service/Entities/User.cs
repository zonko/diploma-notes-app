﻿using System.Runtime.Serialization;

namespace Notes.Service.Entities
{
    [DataContract(Name = "user")]
    public class User
    {
        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}