﻿using Newtonsoft.Json;

namespace XForms.Notes.Core.WebService.Models.Responses
{
    internal class WsResponseWithResult<T> : WsResponse
    {
        [JsonProperty("result")]
        public T Result { get; set; }
    }
}