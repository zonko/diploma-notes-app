"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
var element_registry_1 = require("nativescript-angular/element-registry");
var helpers_1 = require("~/helpers/helpers");
var notes_service_1 = require("~/services/notes.service");
var dialog_service_1 = require("~/services/dialog.service");
var error_type_1 = require("~/models/core/error-type");
element_registry_1.registerElement("Fab", function () { return require("nativescript-floatingactionbutton").Fab; });
var NotesComponent = /** @class */ (function () {
    function NotesComponent(_notesService, _dialogService) {
        this._notesService = _notesService;
        this._dialogService = _dialogService;
    }
    NotesComponent.prototype.ngOnInit = function () {
        this._drawer = this.drawerComponent.sideDrawer;
        this.getNotes();
    };
    NotesComponent.prototype.switchDrawerOpen = function () {
        if (this._drawer.getIsOpen()) {
            this._drawer.closeDrawer();
        }
        else {
            this._drawer.showDrawer();
        }
    };
    NotesComponent.prototype.deleteNote = function (item) {
        var _this = this;
        this._dialogService.setBusy(true);
        this._notesService.deactivateNote(item.id).then(function (res) {
            _this._dialogService.setBusy(false);
            if (res.success) {
                _this.getNotes();
            }
            else if (res.errorType < error_type_1.ErrorType.dbUserNotLogedIn) {
                _this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake med sinhronizacijo s centralo: "
                    + helpers_1.Helpers.getErrorMessage(res.errorType));
                _this.getNotes();
            }
            else {
                _this._dialogService.showMessageDialog("Napaka", "Zapisek ni bil shranjen: " + helpers_1.Helpers.getErrorMessage(res.errorType));
            }
        }).catch(function (err) {
            _this._dialogService.setBusy(false);
            _this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
        });
    };
    NotesComponent.prototype.getNotes = function () {
        var _this = this;
        this._dialogService.setBusy(true);
        this._notesService.listNotes().then(function (res) {
            _this._dialogService.setBusy(false);
            if (res.success) {
                _this.notes = res.result;
            }
            else {
                _this._dialogService.showMessageDialog("Napaka", helpers_1.Helpers.getErrorMessage(res.errorType));
            }
        }).catch(function (err) {
            _this._dialogService.setBusy(false);
            _this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
        });
    };
    __decorate([
        core_1.ViewChild(angular_1.RadSideDrawerComponent),
        __metadata("design:type", angular_1.RadSideDrawerComponent)
    ], NotesComponent.prototype, "drawerComponent", void 0);
    NotesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-notes',
            templateUrl: './notes.component.html',
            styleUrls: ['./notes.component.css']
        }),
        __metadata("design:paramtypes", [notes_service_1.NotesService,
            dialog_service_1.DialogService])
    ], NotesComponent);
    return NotesComponent;
}());
exports.NotesComponent = NotesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90ZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibm90ZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJEO0FBRTNELDhEQUEwRTtBQUUxRSwwRUFBd0U7QUFDeEUsNkNBQTBDO0FBQzFDLDBEQUFzRDtBQUN0RCw0REFBd0Q7QUFDeEQsdURBQW1EO0FBQ25ELGtDQUFlLENBQUMsS0FBSyxFQUFFLGNBQU0sT0FBQSxPQUFPLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxHQUFHLEVBQWhELENBQWdELENBQUMsQ0FBQztBQVEvRTtJQU1JLHdCQUFvQixhQUEyQixFQUMzQixjQUE2QjtRQUQ3QixrQkFBYSxHQUFiLGFBQWEsQ0FBYztRQUMzQixtQkFBYyxHQUFkLGNBQWMsQ0FBZTtJQUFJLENBQUM7SUFFdEQsaUNBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUM7UUFDL0MsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQy9CLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDOUIsQ0FBQztJQUNMLENBQUM7SUFFRCxtQ0FBVSxHQUFWLFVBQVcsSUFBaUI7UUFBNUIsaUJBa0JDO1FBakJHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO1lBQy9DLEtBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25DLEVBQUUsQ0FBQSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNiLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNwQixDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsc0JBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BELEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUMxQyxxREFBcUQ7c0JBQ25ELGlCQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBRSxDQUFDO2dCQUMvQyxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDcEIsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLDJCQUEyQixHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzFILENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO1lBQ1IsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsb0NBQW9DLENBQUMsQ0FBQztRQUMxRixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyxpQ0FBUSxHQUFoQjtRQUFBLGlCQWFDO1FBWkcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO1lBQ25DLEtBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25DLEVBQUUsQ0FBQSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNiLEtBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztZQUM1QixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsaUJBQU8sQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDNUYsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7WUFDUixLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQyxLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxvQ0FBb0MsQ0FBQyxDQUFDO1FBQzFGLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQXBEa0M7UUFBbEMsZ0JBQVMsQ0FBQyxnQ0FBc0IsQ0FBQztrQ0FBeUIsZ0NBQXNCOzJEQUFDO0lBSHpFLGNBQWM7UUFOMUIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsV0FBVztZQUNyQixXQUFXLEVBQUUsd0JBQXdCO1lBQ3JDLFNBQVMsRUFBRSxDQUFDLHVCQUF1QixDQUFDO1NBQ3ZDLENBQUM7eUNBT3FDLDRCQUFZO1lBQ1gsOEJBQWE7T0FQeEMsY0FBYyxDQXdEMUI7SUFBRCxxQkFBQztDQUFBLEFBeERELElBd0RDO0FBeERZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTm90ZURpc3BsYXkgfSBmcm9tICd+L21vZGVscy9jb3JlL25vdGUtZGlzcGxheSc7XHJcbmltcG9ydCB7UmFkU2lkZURyYXdlckNvbXBvbmVudH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyL2FuZ3VsYXJcIjtcclxuaW1wb3J0IHtSYWRTaWRlRHJhd2VyfSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXJcIjtcclxuaW1wb3J0IHsgcmVnaXN0ZXJFbGVtZW50IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnlcIjtcclxuaW1wb3J0IHtIZWxwZXJzfSBmcm9tIFwifi9oZWxwZXJzL2hlbHBlcnNcIjtcclxuaW1wb3J0IHtOb3Rlc1NlcnZpY2V9IGZyb20gXCJ+L3NlcnZpY2VzL25vdGVzLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtEaWFsb2dTZXJ2aWNlfSBmcm9tIFwifi9zZXJ2aWNlcy9kaWFsb2cuc2VydmljZVwiO1xyXG5pbXBvcnQge0Vycm9yVHlwZX0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvZXJyb3ItdHlwZVwiO1xyXG5yZWdpc3RlckVsZW1lbnQoXCJGYWJcIiwgKCkgPT4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1mbG9hdGluZ2FjdGlvbmJ1dHRvblwiKS5GYWIpO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdhcHAtbm90ZXMnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL25vdGVzLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL25vdGVzLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTm90ZXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgbm90ZXM6IEFycmF5PE5vdGVEaXNwbGF5PjtcclxuXHJcbiAgICBAVmlld0NoaWxkKFJhZFNpZGVEcmF3ZXJDb21wb25lbnQpIHB1YmxpYyBkcmF3ZXJDb21wb25lbnQ6IFJhZFNpZGVEcmF3ZXJDb21wb25lbnQ7XHJcbiAgICBwcml2YXRlIF9kcmF3ZXI6IFJhZFNpZGVEcmF3ZXI7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfbm90ZXNTZXJ2aWNlOiBOb3Rlc1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9kaWFsb2dTZXJ2aWNlOiBEaWFsb2dTZXJ2aWNlKSB7IH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLl9kcmF3ZXIgPSB0aGlzLmRyYXdlckNvbXBvbmVudC5zaWRlRHJhd2VyO1xyXG4gICAgICAgIHRoaXMuZ2V0Tm90ZXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBzd2l0Y2hEcmF3ZXJPcGVuKCkge1xyXG4gICAgICAgIGlmKHRoaXMuX2RyYXdlci5nZXRJc09wZW4oKSkge1xyXG4gICAgICAgICAgICB0aGlzLl9kcmF3ZXIuY2xvc2VEcmF3ZXIoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9kcmF3ZXIuc2hvd0RyYXdlcigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVOb3RlKGl0ZW06IE5vdGVEaXNwbGF5KSB7XHJcbiAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zZXRCdXN5KHRydWUpO1xyXG4gICAgICAgIHRoaXMuX25vdGVzU2VydmljZS5kZWFjdGl2YXRlTm90ZShpdGVtLmlkKS50aGVuKHJlcyA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2V0QnVzeShmYWxzZSk7XHJcbiAgICAgICAgICAgIGlmKHJlcy5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldE5vdGVzKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzLmVycm9yVHlwZSA8IEVycm9yVHlwZS5kYlVzZXJOb3RMb2dlZEluKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiTmFwYWthXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJQcmnFoWxvIGplIGRvIG5hcGFrZSBtZWQgc2luaHJvbml6YWNpam8gcyBjZW50cmFsbzogXCJcclxuICAgICAgICAgICAgICAgICAgICArIEhlbHBlcnMuZ2V0RXJyb3JNZXNzYWdlKHJlcy5lcnJvclR5cGUpICk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldE5vdGVzKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiTmFwYWthXCIsIFwiWmFwaXNlayBuaSBiaWwgc2hyYW5qZW46IFwiICsgSGVscGVycy5nZXRFcnJvck1lc3NhZ2UocmVzLmVycm9yVHlwZSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zZXRCdXN5KGZhbHNlKTtcclxuICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zaG93TWVzc2FnZURpYWxvZyhcIk5hcGFrYVwiLCBcIlByacWhbG8gamUgZG8gbmVwcmnEjWFrb3ZhbmUgbmFwYWtlLlwiKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldE5vdGVzKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2V0QnVzeSh0cnVlKTtcclxuICAgICAgICB0aGlzLl9ub3Rlc1NlcnZpY2UubGlzdE5vdGVzKCkudGhlbihyZXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNldEJ1c3koZmFsc2UpO1xyXG4gICAgICAgICAgICBpZihyZXMuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RlcyA9IHJlcy5yZXN1bHQ7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiTmFwYWthXCIsIEhlbHBlcnMuZ2V0RXJyb3JNZXNzYWdlKHJlcy5lcnJvclR5cGUpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2V0QnVzeShmYWxzZSk7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJOYXBha2FcIiwgXCJQcmnFoWxvIGplIGRvIG5lcHJpxI1ha292YW5lIG5hcGFrZS5cIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19