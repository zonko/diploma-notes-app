﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using Notes.Service.DataAccess;
using Notes.Service.DataAccess.Entities;
using Notes.Service.Entities;

namespace Notes.Service
{
    public class NotesService : INotesService
    {
        public ServiceResponse Ping()
        {
            return new ServiceResponse
            {
                ErrorType = ErrorType.Ok,
                ResponseMessage = ErrorType.Ok.ToString()
            };
        }

        public ServiceResponse RegisterNewUser(User user)
        {
            try
            {
                if (string.IsNullOrEmpty(user.Username) || string.IsNullOrEmpty(user.Password))
                {
                    return new ServiceResponse
                    {
                        ErrorType = ErrorType.BadRequest,
                        ErrorMessage = ErrorType.BadRequest.ToString(),
                        ResponseMessage = "Registration failed: Missing username or password"
                    };
                }

                using (var context = new NotesContext())
                {
                    var dbUser = context.Users.Where(u => u.Username == user.Username).AsEnumerable().FirstOrDefault(u => u.Username == user.Username);

                    if (dbUser != null)
                    {
                        return new ServiceResponse
                        {
                            ErrorType = ErrorType.UsernameIsTaken,
                            ResponseMessage = ErrorType.UsernameIsTaken.ToString(),
                            ErrorMessage = "Registration failed: Username is taken"
                        };
                    }

                    context.Users.Add(new DbUser
                    {
                        PasswordHash = SecurePasswordHasher.Hash(user.Password),
                        Username = user.Username
                    });
                    context.SaveChanges();

                    return new ServiceResponse
                    {
                        ErrorType = ErrorType.Ok,
                        ResponseMessage = ErrorType.Ok.ToString(),
                    };
                }
            }
            catch (Exception e)
            {
                return new ServiceResponse
                {
                    ErrorType = ErrorType.InternalServerError,
                    ResponseMessage = ErrorType.InternalServerError.ToString(),
                    ErrorMessage = e.Message
                };
            }
        }

        public ServiceResponseWithResult<Guid> Login(User user)
        {
            try
            {
                if (string.IsNullOrEmpty(user.Username) || string.IsNullOrEmpty(user.Password))
                {
                    return new ServiceResponseWithResult<Guid>
                    {
                        ErrorType = ErrorType.BadRequest,
                        ErrorMessage = ErrorType.BadRequest.ToString(),
                        ResponseMessage = "Login failed: Missing username or password"
                    };
                }

                using (var context = new NotesContext())
                {
                    var dbUser = context.Users.Where(u => u.Username == user.Username).AsEnumerable().FirstOrDefault(u => u.Username == user.Username);

                    if (dbUser == null || !SecurePasswordHasher.Verify(user.Password, dbUser.PasswordHash))
                    {
                        return new ServiceResponseWithResult<Guid>
                        {
                            ErrorType = ErrorType.WrongUsernameOrPassword,
                            ResponseMessage = ErrorType.WrongUsernameOrPassword.ToString(),
                            ErrorMessage = "Login failed: Wrong username or password"
                        };
                    }

                    return new ServiceResponseWithResult<Guid>
                    {
                        ErrorType = ErrorType.Ok,
                        ResponseMessage = ErrorType.Ok.ToString(),
                        Result = dbUser.Id
                    };
                }
            }
            catch (Exception e)
            {
                return new ServiceResponseWithResult<Guid>
                {
                    ErrorType = ErrorType.InternalServerError,
                    ResponseMessage = ErrorType.InternalServerError.ToString(),
                    ErrorMessage = e.Message
                };
            }
        }

        public ServiceResponseWithResult<Synchronization> Synchronize(Synchronization request)
        {
            try
            {
                var userId = CheckAuthorization();

                if (userId == null)
                {
                    return new ServiceResponseWithResult<Synchronization>
                    {
                        ErrorType = ErrorType.AuthorizationError,
                        ResponseMessage = ErrorType.AuthorizationError.ToString(),
                        ErrorMessage = "Authorization failed"
                    };
                }

                using (var context = new NotesContext())
                {
                    var user = context.Users.Find(userId);
                    if (user == null)
                    {
                        return new ServiceResponseWithResult<Synchronization>
                        {
                            ErrorType = ErrorType.AuthorizationError,
                            ResponseMessage = ErrorType.AuthorizationError.ToString(),
                            ErrorMessage = "Authorization failed"
                        };
                    }
                    DateTime syncDateTime = DateTime.Now.ToUniversalTime();
                    var notExistingNotes = new List<Note>();
                    var clientIds = new List<Tuple<long, long>>();
                    if (request.Notes?.Count > 0)
                    {
                        context.Database.BeginTransaction();
                        foreach (var requestNote in request.Notes)
                        {
                            var dateTimeChanged = DateTime.ParseExact(requestNote.DateTimeChanged, "yyyy_MM_dd_HH_mm_ss", null);
                            var dbNote = new DbNote();
                            if (requestNote.Id.HasValue)
                            {
                                dbNote = context.Notes.Include(nameof(DbNote.Attachments)).Include(nameof(DbNote.User)).FirstOrDefault(n => n.Id == requestNote.Id);
                                if (dbNote == null || dbNote.User.Id != user.Id)
                                {
                                    requestNote.Active = false;
                                    notExistingNotes.Add(requestNote);
                                    continue;
                                }
                                if (dbNote.DateTimeChanged > dateTimeChanged)
                                {
                                    clientIds.Add(new Tuple<long, long>(requestNote.ClientId.GetValueOrDefault(), dbNote.Id));
                                    continue;
                                }

                            }
                            else if (!requestNote.Active)
                            {
                                notExistingNotes.Add(requestNote);
                                continue;
                            }

                            dbNote.DateTimeChanged = dateTimeChanged;
                            dbNote.Active = requestNote.Active;
                            dbNote.Text = requestNote.Text;
                            dbNote.Title = requestNote.Title;
                            dbNote.User = user;

                            if (dbNote.Id == 0)
                            {
                                context.Notes.Add(dbNote);
                            }

                            context.SaveChanges();

                            if (dbNote.Attachments?.Count > 0) context.Attachments.RemoveRange(dbNote.Attachments);
                            dbNote.Attachments = new List<DbNoteAttachment>();

                            if (requestNote.Attachments?.Count > 0)
                            {
                                foreach (var requestNoteAttachment in requestNote.Attachments)
                                {
                                    dbNote.Attachments.Add(new DbNoteAttachment
                                    {
                                        Note = dbNote,
                                        Name = requestNoteAttachment.Name,
                                        Type = requestNoteAttachment.Type,
                                        Content = requestNoteAttachment.Content == null ? null : Convert.FromBase64String(requestNoteAttachment.Content)
                                    });
                                    context.SaveChanges();
                                }
                            }
                            

                            clientIds.Add(new Tuple<long, long>(requestNote.ClientId.GetValueOrDefault(), dbNote.Id));
                        }

                        context.Database.CurrentTransaction.Commit();
                    }

                    var dbNotes = context.Notes.Include(nameof(DbNote.Attachments)).Include(nameof(DbNote.User))
                        .Where(n => n.User.Id == user.Id && (request.LastSyncDateTime == null || n.DateTimeChanged > request.LastSyncDateTime.Value)).ToList();

                    var returnNotes = dbNotes
                        .Select(n => new Note
                        {
                            Active = n.Active,
                            Id = n.Id,
                            DateTimeChanged = n.DateTimeChanged.ToString("yyyy_MM_dd_HH_mm_ss"),
                            Text = n.Text,
                            Title = n.Title,
                            Attachments = n.Attachments?.Select(a => new Attachment
                            {
                                Name = a.Name,
                                Type = a.Type,
                                Content = a.Content == null ? null : Convert.ToBase64String(a.Content)
                            }).ToList(),
                            ClientId = clientIds.FirstOrDefault(i => i.Item2 == n.Id)?.Item1
                        }).ToList();

                    returnNotes.AddRange(notExistingNotes);

                    return new ServiceResponseWithResult<Synchronization>
                    {
                        ErrorType = ErrorType.Ok,
                        Result = new Synchronization
                        {
                            LastSyncDateTime = syncDateTime,
                            Notes = returnNotes
                        }
                    };

                }
            }
            catch (Exception e)
            {
                return new ServiceResponseWithResult<Synchronization>
                {
                    ErrorType = ErrorType.InternalServerError,
                    ResponseMessage = ErrorType.InternalServerError.ToString(),
                    ErrorMessage = e.Message
                };
            }
        }


        private Guid? CheckAuthorization()
        {
            var woc = WebOperationContext.Current?.IncomingRequest;
            string authorizationHeader = woc?.Headers["Authorization"];

            if (string.IsNullOrEmpty(authorizationHeader)) return null;

            if (!Guid.TryParse(authorizationHeader, out var userGuid)) return null;

            using (var context = new NotesContext())
            {
                return context.Users.Find(userGuid)?.Id;
            }
        }

    }
}
