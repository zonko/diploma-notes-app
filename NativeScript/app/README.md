### Folder Content

- App_Resources: Contains assets for iOS and Android applications
- components: Contains Angular components (screens/controls)
- helpers: Contains helper classes
- models: Contains models
- pipes: Contains pipes
- resources: Contains shared resources
- services: Contains Angular services (for dependency injection)
