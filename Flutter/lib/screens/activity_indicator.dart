import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ActivityIndicator extends StatelessWidget {

  final bool isLoading;

  const ActivityIndicator({Key key, this.isLoading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return isLoading
      ? Stack(children: <Widget>[
          Opacity(
            opacity: 0.3,
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.black,
            ),
          ),
          Center(child: CircularProgressIndicator())
        ])
      : Stack();
    }
}