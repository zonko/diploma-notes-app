﻿using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms.Xaml;
using XForms.Notes.Core.ViewModels;

namespace XForms.Notes.UI.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	[MvxContentPagePresentation(Animated = false)]
    public partial class RegistrationPage : MvxContentPage<RegistrationPageViewModel>
	{
		public RegistrationPage ()
		{
			InitializeComponent ();
		}
	}
}