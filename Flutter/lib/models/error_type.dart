class ErrorType {
  static const int wsOk = 1;
  static const int wsBadRequest = 2;
  static const int wsInternalServerError = 3;
  static const int wsUsernameIsTaken = 4;
  static const int wsWrongUsernameOrPassword = 5;
  static const int wsAuthorizationError = 6;
  static const int wsServerNotReachable = 15;

  static const int dbUserNotLogedIn = 100;
  static const int dbNotInitialized = 101;
  static const int dbCreateTablesError = 102;
  static const int dbOpenDbError = 103;
  static const int dbWriteError = 104;
  static const int dbReadError = 105;

  static const intnotificationSchedulingError = 200;
}
