"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ws_error_type_1 = require("~/models/web-service/ws-error-type");
var error_type_1 = require("~/models/core/error-type");
var note_1 = require("~/models/core/note");
var note_attachment_1 = require("~/models/core/note-attachment");
var ws_note_1 = require("~/models/web-service/ws-note");
var ws_note_attachment_1 = require("~/models/web-service/ws-note-attachment");
var Converters = /** @class */ (function () {
    function Converters() {
    }
    Converters.toCoreErrorType = function (wsErrorType) {
        switch (wsErrorType) {
            case ws_error_type_1.WsErrorType.ok: return error_type_1.ErrorType.ok;
            case ws_error_type_1.WsErrorType.authorizationError: return error_type_1.ErrorType.wsAuthorizationError;
            case ws_error_type_1.WsErrorType.badRequest: return error_type_1.ErrorType.wsBadRequest;
            case ws_error_type_1.WsErrorType.internalServerError: return error_type_1.ErrorType.wsInternalServerError;
            case ws_error_type_1.WsErrorType.usernameIsTaken: return error_type_1.ErrorType.wsUsernameIsTaken;
            case ws_error_type_1.WsErrorType.wrongUsernameOrPassword: return error_type_1.ErrorType.wsWrongUsernameOrPassword;
            case ws_error_type_1.WsErrorType.serverNotReachable: return error_type_1.ErrorType.wsServerNotReachable;
        }
    };
    Converters.toWsNote = function (note) {
        var wsNote = new ws_note_1.WsNote();
        wsNote.id = note.cid;
        wsNote.clientId = note.id;
        wsNote.active = note.active;
        wsNote.dateTimeChanged = note.dateTimeChanged;
        wsNote.title = note.title;
        wsNote.text = note.text;
        var attachments = new Array();
        if (note.attachments) {
            for (var _i = 0, _a = note.attachments; _i < _a.length; _i++) {
                var att = _a[_i];
                var wsAttachment = new ws_note_attachment_1.WsNoteAttachment();
                wsAttachment.name = att.name;
                wsAttachment.type = att.type;
                wsAttachment.content = att.content;
                attachments.push(wsAttachment);
            }
        }
        wsNote.attachments = attachments;
        return wsNote;
    };
    Converters.toCoreNote = function (wsNote) {
        var note = new note_1.Note();
        note.cid = wsNote.id;
        note.id = wsNote.clientId;
        note.active = wsNote.active;
        note.dateTimeChanged = wsNote.dateTimeChanged;
        note.title = wsNote.title;
        note.text = wsNote.text;
        note.modified = false;
        var attachments = new Array();
        if (wsNote.attachments) {
            for (var _i = 0, _a = wsNote.attachments; _i < _a.length; _i++) {
                var att = _a[_i];
                var attachment = new note_attachment_1.NoteAttachment();
                attachment.name = att.name;
                attachment.type = att.type;
                attachment.content = att.content;
                attachments.push(attachment);
            }
        }
        note.attachments = attachments;
        return note;
    };
    return Converters;
}());
exports.Converters = Converters;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udmVydGVycy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNvbnZlcnRlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxvRUFBaUU7QUFDakUsdURBQXFEO0FBQ3JELDJDQUEwQztBQUMxQyxpRUFBK0Q7QUFDL0Qsd0RBQXNEO0FBQ3RELDhFQUEyRTtBQUczRTtJQUFBO0lBa0VBLENBQUM7SUFqRWlCLDBCQUFlLEdBQTdCLFVBQThCLFdBQXdCO1FBQ2xELE1BQU0sQ0FBQSxDQUFDLFdBQVcsQ0FBQyxDQUFBLENBQUM7WUFDaEIsS0FBSywyQkFBVyxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsc0JBQVMsQ0FBQyxFQUFFLENBQUM7WUFDekMsS0FBSywyQkFBVyxDQUFDLGtCQUFrQixFQUFFLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLG9CQUFvQixDQUFDO1lBQzNFLEtBQUssMkJBQVcsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLHNCQUFTLENBQUMsWUFBWSxDQUFDO1lBQzNELEtBQUssMkJBQVcsQ0FBQyxtQkFBbUIsRUFBRSxNQUFNLENBQUMsc0JBQVMsQ0FBQyxxQkFBcUIsQ0FBQztZQUM3RSxLQUFLLDJCQUFXLENBQUMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLGlCQUFpQixDQUFDO1lBQ3JFLEtBQUssMkJBQVcsQ0FBQyx1QkFBdUIsRUFBRSxNQUFNLENBQUMsc0JBQVMsQ0FBQyx5QkFBeUIsQ0FBQztZQUNyRixLQUFLLDJCQUFXLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxDQUFDLHNCQUFTLENBQUMsb0JBQW9CLENBQUM7UUFDL0UsQ0FBQztJQUNMLENBQUM7SUFFYSxtQkFBUSxHQUF0QixVQUF1QixJQUFVO1FBQzdCLElBQU0sTUFBTSxHQUFHLElBQUksZ0JBQU0sRUFBRSxDQUFDO1FBQzVCLE1BQU0sQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUNyQixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7UUFDMUIsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzVCLE1BQU0sQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUM5QyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDMUIsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBRXhCLElBQU0sV0FBVyxHQUFHLElBQUksS0FBSyxFQUFvQixDQUFDO1FBRWxELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ25CLEdBQUcsQ0FBQyxDQUFZLFVBQWdCLEVBQWhCLEtBQUEsSUFBSSxDQUFDLFdBQVcsRUFBaEIsY0FBZ0IsRUFBaEIsSUFBZ0I7Z0JBQTNCLElBQUksR0FBRyxTQUFBO2dCQUNSLElBQU0sWUFBWSxHQUFHLElBQUkscUNBQWdCLEVBQUUsQ0FBQztnQkFDNUMsWUFBWSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO2dCQUM3QixZQUFZLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7Z0JBQzdCLFlBQVksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQztnQkFFbkMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNsQztRQUNMLENBQUM7UUFFRCxNQUFNLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUVqQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFYSxxQkFBVSxHQUF4QixVQUF5QixNQUFjO1FBQ25DLElBQU0sSUFBSSxHQUFHLElBQUksV0FBSSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDO1FBQzlDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFFdEIsSUFBTSxXQUFXLEdBQUcsSUFBSSxLQUFLLEVBQWtCLENBQUM7UUFFaEQsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDckIsR0FBRyxDQUFDLENBQVksVUFBa0IsRUFBbEIsS0FBQSxNQUFNLENBQUMsV0FBVyxFQUFsQixjQUFrQixFQUFsQixJQUFrQjtnQkFBN0IsSUFBSSxHQUFHLFNBQUE7Z0JBQ1IsSUFBTSxVQUFVLEdBQUcsSUFBSSxnQ0FBYyxFQUFFLENBQUM7Z0JBQ3hDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztnQkFDM0IsVUFBVSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO2dCQUMzQixVQUFVLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUM7Z0JBRWpDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEM7UUFDTCxDQUFDO1FBRUQsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDL0IsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBQ0wsaUJBQUM7QUFBRCxDQUFDLEFBbEVELElBa0VDO0FBbEVZLGdDQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgV3NFcnJvclR5cGUgfSBmcm9tIFwifi9tb2RlbHMvd2ViLXNlcnZpY2Uvd3MtZXJyb3ItdHlwZVwiO1xyXG5pbXBvcnQgeyBFcnJvclR5cGUgfSBmcm9tIFwifi9tb2RlbHMvY29yZS9lcnJvci10eXBlXCI7XHJcbmltcG9ydCB7IE5vdGUgfSBmcm9tIFwifi9tb2RlbHMvY29yZS9ub3RlXCI7XHJcbmltcG9ydCB7IE5vdGVBdHRhY2htZW50IH0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvbm90ZS1hdHRhY2htZW50XCI7XHJcbmltcG9ydCB7IFdzTm90ZSB9IGZyb20gXCJ+L21vZGVscy93ZWItc2VydmljZS93cy1ub3RlXCI7XHJcbmltcG9ydCB7IFdzTm90ZUF0dGFjaG1lbnQgfSBmcm9tIFwifi9tb2RlbHMvd2ViLXNlcnZpY2Uvd3Mtbm90ZS1hdHRhY2htZW50XCI7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIENvbnZlcnRlcnMge1xyXG4gICAgcHVibGljIHN0YXRpYyB0b0NvcmVFcnJvclR5cGUod3NFcnJvclR5cGU6IFdzRXJyb3JUeXBlKTogRXJyb3JUeXBlIHtcclxuICAgICAgICBzd2l0Y2god3NFcnJvclR5cGUpe1xyXG4gICAgICAgICAgICBjYXNlIFdzRXJyb3JUeXBlLm9rOiByZXR1cm4gRXJyb3JUeXBlLm9rO1xyXG4gICAgICAgICAgICBjYXNlIFdzRXJyb3JUeXBlLmF1dGhvcml6YXRpb25FcnJvcjogcmV0dXJuIEVycm9yVHlwZS53c0F1dGhvcml6YXRpb25FcnJvcjtcclxuICAgICAgICAgICAgY2FzZSBXc0Vycm9yVHlwZS5iYWRSZXF1ZXN0OiByZXR1cm4gRXJyb3JUeXBlLndzQmFkUmVxdWVzdDtcclxuICAgICAgICAgICAgY2FzZSBXc0Vycm9yVHlwZS5pbnRlcm5hbFNlcnZlckVycm9yOiByZXR1cm4gRXJyb3JUeXBlLndzSW50ZXJuYWxTZXJ2ZXJFcnJvcjtcclxuICAgICAgICAgICAgY2FzZSBXc0Vycm9yVHlwZS51c2VybmFtZUlzVGFrZW46IHJldHVybiBFcnJvclR5cGUud3NVc2VybmFtZUlzVGFrZW47XHJcbiAgICAgICAgICAgIGNhc2UgV3NFcnJvclR5cGUud3JvbmdVc2VybmFtZU9yUGFzc3dvcmQ6IHJldHVybiBFcnJvclR5cGUud3NXcm9uZ1VzZXJuYW1lT3JQYXNzd29yZDtcclxuICAgICAgICAgICAgY2FzZSBXc0Vycm9yVHlwZS5zZXJ2ZXJOb3RSZWFjaGFibGU6IHJldHVybiBFcnJvclR5cGUud3NTZXJ2ZXJOb3RSZWFjaGFibGU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgdG9Xc05vdGUobm90ZTogTm90ZSk6IFdzTm90ZSB7XHJcbiAgICAgICAgY29uc3Qgd3NOb3RlID0gbmV3IFdzTm90ZSgpO1xyXG4gICAgICAgIHdzTm90ZS5pZCA9IG5vdGUuY2lkO1xyXG4gICAgICAgIHdzTm90ZS5jbGllbnRJZCA9IG5vdGUuaWQ7XHJcbiAgICAgICAgd3NOb3RlLmFjdGl2ZSA9IG5vdGUuYWN0aXZlO1xyXG4gICAgICAgIHdzTm90ZS5kYXRlVGltZUNoYW5nZWQgPSBub3RlLmRhdGVUaW1lQ2hhbmdlZDtcclxuICAgICAgICB3c05vdGUudGl0bGUgPSBub3RlLnRpdGxlO1xyXG4gICAgICAgIHdzTm90ZS50ZXh0ID0gbm90ZS50ZXh0O1xyXG5cclxuICAgICAgICBjb25zdCBhdHRhY2htZW50cyA9IG5ldyBBcnJheTxXc05vdGVBdHRhY2htZW50PigpO1xyXG5cclxuICAgICAgICBpZiAobm90ZS5hdHRhY2htZW50cykge1xyXG4gICAgICAgICAgICBmb3IgKGxldCBhdHQgb2Ygbm90ZS5hdHRhY2htZW50cykge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgd3NBdHRhY2htZW50ID0gbmV3IFdzTm90ZUF0dGFjaG1lbnQoKTtcclxuICAgICAgICAgICAgICAgIHdzQXR0YWNobWVudC5uYW1lID0gYXR0Lm5hbWU7XHJcbiAgICAgICAgICAgICAgICB3c0F0dGFjaG1lbnQudHlwZSA9IGF0dC50eXBlO1xyXG4gICAgICAgICAgICAgICAgd3NBdHRhY2htZW50LmNvbnRlbnQgPSBhdHQuY29udGVudDtcclxuXHJcbiAgICAgICAgICAgICAgICBhdHRhY2htZW50cy5wdXNoKHdzQXR0YWNobWVudCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHdzTm90ZS5hdHRhY2htZW50cyA9IGF0dGFjaG1lbnRzO1xyXG5cclxuICAgICAgICByZXR1cm4gd3NOb3RlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgdG9Db3JlTm90ZSh3c05vdGU6IFdzTm90ZSk6IE5vdGUge1xyXG4gICAgICAgIGNvbnN0IG5vdGUgPSBuZXcgTm90ZSgpO1xyXG4gICAgICAgIG5vdGUuY2lkID0gd3NOb3RlLmlkO1xyXG4gICAgICAgIG5vdGUuaWQgPSB3c05vdGUuY2xpZW50SWQ7XHJcbiAgICAgICAgbm90ZS5hY3RpdmUgPSB3c05vdGUuYWN0aXZlO1xyXG4gICAgICAgIG5vdGUuZGF0ZVRpbWVDaGFuZ2VkID0gd3NOb3RlLmRhdGVUaW1lQ2hhbmdlZDtcclxuICAgICAgICBub3RlLnRpdGxlID0gd3NOb3RlLnRpdGxlO1xyXG4gICAgICAgIG5vdGUudGV4dCA9IHdzTm90ZS50ZXh0O1xyXG4gICAgICAgIG5vdGUubW9kaWZpZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgY29uc3QgYXR0YWNobWVudHMgPSBuZXcgQXJyYXk8Tm90ZUF0dGFjaG1lbnQ+KCk7XHJcblxyXG4gICAgICAgIGlmICh3c05vdGUuYXR0YWNobWVudHMpIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgYXR0IG9mIHdzTm90ZS5hdHRhY2htZW50cykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYXR0YWNobWVudCA9IG5ldyBOb3RlQXR0YWNobWVudCgpO1xyXG4gICAgICAgICAgICAgICAgYXR0YWNobWVudC5uYW1lID0gYXR0Lm5hbWU7XHJcbiAgICAgICAgICAgICAgICBhdHRhY2htZW50LnR5cGUgPSBhdHQudHlwZTtcclxuICAgICAgICAgICAgICAgIGF0dGFjaG1lbnQuY29udGVudCA9IGF0dC5jb250ZW50O1xyXG5cclxuICAgICAgICAgICAgICAgIGF0dGFjaG1lbnRzLnB1c2goYXR0YWNobWVudCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIG5vdGUuYXR0YWNobWVudHMgPSBhdHRhY2htZW50cztcclxuICAgICAgICByZXR1cm4gbm90ZTtcclxuICAgIH1cclxufSJdfQ==