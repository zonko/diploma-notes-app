﻿namespace XForms.Notes.Core.Models
{
    public class Notification
    {
        public int? Id { get; set; }
        public int NoteId { get; set; }
        public string User { get; set; }
    }
}