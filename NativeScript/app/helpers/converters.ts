import { WsErrorType } from "~/models/web-service/ws-error-type";
import { ErrorType } from "~/models/core/error-type";
import { Note } from "~/models/core/note";
import { NoteAttachment } from "~/models/core/note-attachment";
import { WsNote } from "~/models/web-service/ws-note";
import { WsNoteAttachment } from "~/models/web-service/ws-note-attachment";


export class Converters {
    public static toCoreErrorType(wsErrorType: WsErrorType): ErrorType {
        switch(wsErrorType){
            case WsErrorType.ok: return ErrorType.ok;
            case WsErrorType.authorizationError: return ErrorType.wsAuthorizationError;
            case WsErrorType.badRequest: return ErrorType.wsBadRequest;
            case WsErrorType.internalServerError: return ErrorType.wsInternalServerError;
            case WsErrorType.usernameIsTaken: return ErrorType.wsUsernameIsTaken;
            case WsErrorType.wrongUsernameOrPassword: return ErrorType.wsWrongUsernameOrPassword;
            case WsErrorType.serverNotReachable: return ErrorType.wsServerNotReachable;
        }
    }

    public static toWsNote(note: Note): WsNote {
        const wsNote = new WsNote();
        wsNote.id = note.cid;
        wsNote.clientId = note.id;
        wsNote.active = note.active;
        wsNote.dateTimeChanged = note.dateTimeChanged;
        wsNote.title = note.title;
        wsNote.text = note.text;

        const attachments = new Array<WsNoteAttachment>();

        if (note.attachments) {
            for (let att of note.attachments) {
                const wsAttachment = new WsNoteAttachment();
                wsAttachment.name = att.name;
                wsAttachment.type = att.type;
                wsAttachment.content = att.content;

                attachments.push(wsAttachment);
            }
        }

        wsNote.attachments = attachments;

        return wsNote;
    }

    public static toCoreNote(wsNote: WsNote): Note {
        const note = new Note();
        note.cid = wsNote.id;
        note.id = wsNote.clientId;
        note.active = wsNote.active;
        note.dateTimeChanged = wsNote.dateTimeChanged;
        note.title = wsNote.title;
        note.text = wsNote.text;
        note.modified = false;

        const attachments = new Array<NoteAttachment>();

        if (wsNote.attachments) {
            for (let att of wsNote.attachments) {
                const attachment = new NoteAttachment();
                attachment.name = att.name;
                attachment.type = att.type;
                attachment.content = att.content;

                attachments.push(attachment);
            }
        }

        note.attachments = attachments;
        return note;
    }
}