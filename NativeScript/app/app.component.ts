import {Component, OnInit} from "@angular/core";
import {DialogService} from "~/services/dialog.service";

@Component({
    selector: "app-app",
    templateUrl: "./app.component.html",
})

export class AppComponent implements OnInit{
    isAlertDisplayed = false;
    isBusy = false;
    alertTitle = "Title";
    alertText = "Text";
    alertButtonText = "OK";

    constructor (private _dialogService: DialogService) {
    }

    ngOnInit(): void {
        this._dialogService.showMessageDialog$.subscribe(options =>{
            this.alertTitle = options.title;
            this.alertText = options.text;
            this.alertButtonText = options.okButtonText;
            this.isAlertDisplayed = true;
        });
        this._dialogService.setBusyIndicator$.subscribe(busy => {
            this.isBusy = busy;
        })
    }

    onAlertConfirmed() {
        this.isAlertDisplayed = false;
    }


}
