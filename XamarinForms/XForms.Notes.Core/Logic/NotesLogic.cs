﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XForms.Notes.Core.Database;
using XForms.Notes.Core.Models.Enums;
using XForms.Notes.Core.Models.Notes;
using XForms.Notes.Core.WebService;

namespace XForms.Notes.Core.Logic
{
    public class NotesLogic
    {
        private readonly DataAccess _dataAccess;
        private readonly NotesServiceManager _notesServiceManager;

        public NotesLogic(DataAccess dataAccess, NotesServiceManager notesServiceManager)
        {
            _dataAccess = dataAccess;
            _notesServiceManager = notesServiceManager;
        }

        internal async Task Synchronize()
        {
            await _notesServiceManager.Ping();

            var modifiedNotes = await _dataAccess.ListModifiedNotes();
            var lastSyncDateTime = await _dataAccess.GetValue(KeyValueKey.LastSyncDateTime);

            var syncResult = await _notesServiceManager.Synchronize(modifiedNotes, lastSyncDateTime);
            if (syncResult.notes.Any())
            {
                await _dataAccess.SaveNotes(syncResult.notes);
            }
            await _dataAccess.SetValue(KeyValueKey.LastSyncDateTime, syncResult.syncDateTime);
        }

        internal async Task<(int? id, Exception syncException)> SaveNote(Note note)
        {
            note.Modified = true;
            note.Active = true;
            note.DateTimeChanged = DateTime.Now;

            var noteId = await _dataAccess.SaveNote(note);

            try
            {
                await Synchronize();
                return (noteId, null);
            }
            catch (Exception e)
            {
                return (noteId, e);
            }
        }

        internal async Task<Exception> DeactivateNote(int id)
        {
            await _dataAccess.DeactivateNote(id);

            try
            {
                await Synchronize();
                return null;
            }
            catch (Exception e)
            {
                return e;
            }
        }

        internal async Task<Note> GetNote(int id)
        {
            return await _dataAccess.GetNote(id);
        }

        internal async Task<List<NoteDisplay>> ListNotes()
        {
            return await _dataAccess.ListNoteDisplays();
        }
    }
}