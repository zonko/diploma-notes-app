﻿using System;
using SQLite;
using XForms.Notes.Core.WebService.Models;

namespace XForms.Notes.Core.Helpers
{
    public static class ExceptionHelper
    {
        public static string GetErrorMessage(Exception exception)
        {
            switch (exception)
            {
                case WsException wsException: return wsException.WsErrorType.ToErrorMessage();
                case SQLiteException sqLiteException: return "Prišlo je do napake ob uporabi lokalne podatkovne baze: " + sqLiteException.Message;
                default: return "Prišlo je do nepričakovane napake: " + exception.Message;
            }
        }
    }
}