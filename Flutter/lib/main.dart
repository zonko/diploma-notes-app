import 'package:flutter/material.dart';
import 'package:ft_notes_app/globals.dart';
import 'package:ft_notes_app/localization/locale/locales.dart';
import 'package:ft_notes_app/routes.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(new NotesApp());
}

class NotesApp extends StatefulWidget {
  @override
  NotesAppState createState() => NotesAppState();
}

class NotesAppState extends State<NotesApp> {
  AppLocalizationsDelegate _appLocalizationdelegate;

  @override
  void initState() {
    super.initState();
    _appLocalizationdelegate = new AppLocalizationsDelegate(Locale('sl', 'SI'));
  }

  onLocaleChange(Locale locale) {
    setState(() {
      _appLocalizationdelegate = new AppLocalizationsDelegate(locale);
    });
  }


  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      navigatorKey: Globals.navigatorKey,
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.grey,
        accentColor: Colors.orangeAccent,
        brightness: Brightness.dark
      ),
      routes: routes,
      localizationsDelegates: [
        _appLocalizationdelegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
    );
  }
}