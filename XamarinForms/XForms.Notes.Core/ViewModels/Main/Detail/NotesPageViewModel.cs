﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using XForms.Notes.Core.Helpers;
using XForms.Notes.Core.Logic;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.Models.Notes;

namespace XForms.Notes.Core.ViewModels.Main.Detail
{
    public class NotesPageViewModel : MvxViewModel
    {
        private MvxAsyncCommand _addNoteCommand;
        private MvxAsyncCommand _switchMasterPresentedCommand;
        private MvxAsyncCommand<NoteDisplay> _removeNoteCommand;

        private List<NoteDisplay> _notes;
        private NoteDisplay _selectedNoteDisplay;

        private readonly IDialogService _dialogService;
        private readonly NotesLogic _notesLogic;

        public NotesPageViewModel(IDialogService dialogService, NotesLogic notesLogic)
        {
            _notesLogic = notesLogic;
            _dialogService = dialogService;
        }

        public List<NoteDisplay> Notes
        {
            get => _notes;
            set => SetProperty(ref _notes, value);
        }

        public NoteDisplay SelectedNoteDisplay
        {
            get => _selectedNoteDisplay;
            set
            {
                SetProperty(ref _selectedNoteDisplay, value);
                NoteSelected();
            }
        }

        public MvxAsyncCommand AddNoteCommand => _addNoteCommand ?? (_addNoteCommand = new MvxAsyncCommand(AddNote));
        public MvxAsyncCommand SwitchMasterPresentedCommand => _switchMasterPresentedCommand ?? (_switchMasterPresentedCommand = new MvxAsyncCommand(SwitchMasterPresented));
        public MvxAsyncCommand<NoteDisplay> RemoveNoteCommand => _removeNoteCommand ?? (_removeNoteCommand = new MvxAsyncCommand<NoteDisplay>(RemoveNote));

        private async void NoteSelected()
        {
            await NavigationService.Navigate<NoteEditPageViewModel, int?>(SelectedNoteDisplay?.Id);
        }

        private async Task RemoveNote(NoteDisplay note)
        {
            _dialogService.SetBusy(true);
            try
            {
                var syncException = await _notesLogic.DeactivateNote(note.Id);

                if (syncException != null)
                {
                    _dialogService.ShowAlert("Napaka", "Zapisek je bil izbrisan v lokalni podatkovni bazi, sinhronizacija s strežnikom pa ni bila uspešna. " + ExceptionHelper.GetErrorMessage(syncException));
                }
                Notes = await _notesLogic.ListNotes();
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", "Brisanje zapiska ni uspelo. " + ExceptionHelper.GetErrorMessage(exception));
            }
            _dialogService.SetBusy(false);
        }

        private async Task AddNote()
        {
            await NavigationService.Navigate<NoteEditPageViewModel, int?>((int?)null);
        }

        private Task SwitchMasterPresented()
        {
            return Task.Run(() => _dialogService.InvokeSetMasterPagePresented(null));
        }

        public override async Task Initialize()
        {
            _dialogService.SetBusy(true);
            try
            {
                Notes = await _notesLogic.ListNotes();
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", ExceptionHelper.GetErrorMessage(exception));
            }
            _dialogService.SetBusy(false);
            await base.Initialize();
        }
    }
}