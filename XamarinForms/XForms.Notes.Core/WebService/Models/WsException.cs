﻿using System;
using XForms.Notes.Core.WebService.Models.Responses;

namespace XForms.Notes.Core.WebService.Models
{
    public class WsException : Exception
    {
        public WsErrorType WsErrorType { get; }

        public WsException(WsErrorType errorType, string message, Exception innerException) : base(message, innerException)
        {
            WsErrorType = errorType;
        }
    }
}