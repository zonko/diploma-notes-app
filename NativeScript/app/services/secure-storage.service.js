"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var nativescript_secure_storage_1 = require("nativescript-secure-storage");
var core_1 = require("@angular/core");
var SecureStorageService = /** @class */ (function () {
    function SecureStorageService() {
        this._userTokenKey = 'ntsUserToken';
        this._userKey = 'ntsUser';
        this._secureStorage = new nativescript_secure_storage_1.SecureStorage();
    }
    SecureStorageService.prototype.setUserToken = function (userToken) {
        this._secureStorage.setSync({ key: this._userTokenKey, value: userToken });
    };
    SecureStorageService.prototype.getUserToken = function () {
        return this._secureStorage.getSync({ key: this._userTokenKey });
    };
    SecureStorageService.prototype.setUser = function (user) {
        this._secureStorage.setSync({ key: this._userKey, value: user });
    };
    SecureStorageService.prototype.getUser = function () {
        return this._secureStorage.getSync({ key: this._userKey });
    };
    SecureStorageService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], SecureStorageService);
    return SecureStorageService;
}());
exports.SecureStorageService = SecureStorageService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VjdXJlLXN0b3JhZ2Uuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNlY3VyZS1zdG9yYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSwyRUFBNEQ7QUFDNUQsc0NBQTJDO0FBRzNDO0lBS0k7UUFIUSxrQkFBYSxHQUFHLGNBQWMsQ0FBQTtRQUM5QixhQUFRLEdBQUcsU0FBUyxDQUFBO1FBR3hCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSwyQ0FBYSxFQUFFLENBQUE7SUFDN0MsQ0FBQztJQUVNLDJDQUFZLEdBQW5CLFVBQW9CLFNBQWlCO1FBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDL0UsQ0FBQztJQUVNLDJDQUFZLEdBQW5CO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFTSxzQ0FBTyxHQUFkLFVBQWUsSUFBWTtRQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFFTSxzQ0FBTyxHQUFkO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUF2QlEsb0JBQW9CO1FBRGhDLGlCQUFVLEVBQUU7O09BQ0Esb0JBQW9CLENBd0JoQztJQUFELDJCQUFDO0NBQUEsQUF4QkQsSUF3QkM7QUF4Qlksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU2VjdXJlU3RvcmFnZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtc2VjdXJlLXN0b3JhZ2VcIjtcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBTZWN1cmVTdG9yYWdlU2VydmljZSB7XHJcbiAgICBwcml2YXRlIF9zZWN1cmVTdG9yYWdlOiBTZWN1cmVTdG9yYWdlO1xyXG4gICAgcHJpdmF0ZSBfdXNlclRva2VuS2V5ID0gJ250c1VzZXJUb2tlbidcclxuICAgIHByaXZhdGUgX3VzZXJLZXkgPSAnbnRzVXNlcidcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLl9zZWN1cmVTdG9yYWdlID0gbmV3IFNlY3VyZVN0b3JhZ2UoKVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRVc2VyVG9rZW4odXNlclRva2VuOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9zZWN1cmVTdG9yYWdlLnNldFN5bmMoeyBrZXk6IHRoaXMuX3VzZXJUb2tlbktleSwgdmFsdWU6IHVzZXJUb2tlbiB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0VXNlclRva2VuKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3NlY3VyZVN0b3JhZ2UuZ2V0U3luYyh7IGtleTogdGhpcy5fdXNlclRva2VuS2V5IH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRVc2VyKHVzZXI6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3NlY3VyZVN0b3JhZ2Uuc2V0U3luYyh7IGtleTogdGhpcy5fdXNlcktleSwgdmFsdWU6IHVzZXIgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldFVzZXIoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc2VjdXJlU3RvcmFnZS5nZXRTeW5jKHsga2V5OiB0aGlzLl91c2VyS2V5IH0pO1xyXG4gICAgfVxyXG59Il19