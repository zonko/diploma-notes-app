﻿using System.Runtime.Serialization;

namespace Notes.Service.Entities
{
    [DataContract(Name = "ServiceResponseWithResult")]
    public class ServiceResponseWithResult<T> : ServiceResponse
    {
        [DataMember(Name = "result")]
        public T Result { get; set; }
    }
}