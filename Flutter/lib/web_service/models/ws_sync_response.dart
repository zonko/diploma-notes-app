import 'package:ft_notes_app/web_service/models/ws_synchronization.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ws_sync_response.g.dart';

@JsonSerializable()
class WsSyncResponse {
  int errorType;  //WsErrorType
  String responseMessage;
  String errorMessage;
  WsSynchronization result;

  WsSyncResponse();

  factory WsSyncResponse.fromJson(Map<String, dynamic> json) => _$WsSyncResponseFromJson(json);

  toJson() => _$WsSyncResponseToJson(this);
}