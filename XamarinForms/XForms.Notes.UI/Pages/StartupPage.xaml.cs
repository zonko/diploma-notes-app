﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Forms.Presenters.Attributes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XForms.Notes.UI.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	[MvxContentPagePresentation(Animated = false)]
    public partial class StartupPage
	{
		public StartupPage ()
		{
			InitializeComponent ();
		}
	}
}