import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ft_notes_app/logic/date_time_converter.dart';

class SetNotificationScreen extends StatefulWidget {
  @override
  SetNotificationScreentState createState() => new SetNotificationScreentState();
}

class SetNotificationScreentState extends State<SetNotificationScreen> {
  DateTime _date = DateTime.now();
  TimeOfDay _time = TimeOfDay.now();

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: AppBar(
        title: Text('Nastavi obvestilo', style: TextStyle(color: Colors.orangeAccent)),
        iconTheme: IconThemeData(color: Colors.orangeAccent),
      ),
      body: Column(
          children: <Widget>[
            Expanded(
                child: ListView(
              children: <Widget>[
                SizedBox(height: 50.0),
                  Column( 
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.symmetric(horizontal: 20.0), child: Text('Izberi datum:', style: TextStyle(fontSize: 17.0))),
                      Container(
                          height: 40.0,
                          width: double.infinity,
                          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                          child: FlatButton (
                            color: Colors.orangeAccent,
                            child: Text(DateTimeConverter.toDateDisplayString(_date)),
                            onPressed: () { _selectDate(); },
                          )),
                        SizedBox(height: 40.0),
                        Padding(padding: EdgeInsets.symmetric(horizontal: 20.0), child: Text('Izberi uro:', style: TextStyle(fontSize: 17.0))),
                      Container(
                          height: 40.0,
                          width: double.infinity,
                          margin: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 15.0),
                          child: FlatButton (
                            color: Colors.orangeAccent,
                            child: Text(DateTimeConverter.toTimeDisplayString(_time)),
                            onPressed: () { _selectTime(); },
                          )),
                    ],
                  ),
                SizedBox(height: 20.0),
              ],
            )),
            Container(
              height: 50.0,
              width: double.infinity,
              child: FlatButton(
                child: Text('POTRDI'),
                onPressed: _submit,
                color: Colors.orange,
              ),
            )
          ],
      ),
    );
  }

  Future<Null> _selectDate() async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2018),
      lastDate: DateTime(2020)
    );
    if (picked == null) return;
    setState(() { _date = picked; });
  }

  Future<Null> _selectTime() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: _time,
    );
    if (picked == null) return;
    setState(() { _time = picked; });
  }

  void _submit(){

    if (_date == null || _time == null){
      Navigator.pop(context, null);
    } else {
      var selectedDateTime = DateTime(_date.year, _date.month, _date.day, _time.hour, _time.minute);
      Navigator.pop(context, selectedDateTime);
    }
  }
  
}