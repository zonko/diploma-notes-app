﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace XForms.Notes.Droid
{
    [Activity(Label = "XForms Notes App", Theme = "@style/LaunchScreenTheme", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {

        // Launches the startup task
        protected override void OnResume()
        {
            base.OnResume();
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
        }
    }
}