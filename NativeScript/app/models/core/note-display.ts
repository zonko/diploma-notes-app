export class NoteDisplay {
    id: number;
    title: string;
    dateTimeChanged: string;

    constructor (id: number, title: string, dateTimeChanged: string) {
        this.id = id;
        this.title = title;
        this.dateTimeChanged = dateTimeChanged;
    }
}