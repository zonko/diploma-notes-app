﻿using Newtonsoft.Json;

namespace XForms.Notes.Core.WebService.Models.Responses
{
    internal class WsResponse
    {
        [JsonProperty("errorType")]
        public WsErrorType ErrorType { get; set; }

        [JsonProperty("responseMessage")]
        public string ResponseMessage { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
    }
}