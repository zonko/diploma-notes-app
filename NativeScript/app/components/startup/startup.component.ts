import { Component, OnInit } from '@angular/core';
import { UserService } from '~/services/user.service';
import {Helpers} from "~/helpers/helpers";
import {Router} from "@angular/router";
import {DialogService} from "~/services/dialog.service";
import {Page} from "tns-core-modules/ui/page";
import {NotificationService} from "~/services/notification.service";
import * as LocalNotifications from "nativescript-local-notifications";

@Component({
    moduleId: module.id,
    selector: 'app-startup',
    templateUrl: './startup.component.html',
    styleUrls: ['./startup.component.css']
})
export class StartupComponent implements OnInit {

    constructor(private _userService: UserService,
                private _router: Router,
                private _dialogService: DialogService,
                private _page: Page) { }

    ngOnInit() {
        this._page.actionBarHidden = true;
        this._userService.checkLogin().then(res => {
            if (res.result) {
                if (!res.success) {
                    this._dialogService.showMessageDialog("Opozorilo", "Sinhronizacija ni uspela: " +Helpers.getErrorMessage(res.errorType));
                }
                this._router.navigateByUrl("notes");
                return;
            } else if (!res.success) {
                const errorMessage = "Preverjanje prijave ni uspelo: " + Helpers.getErrorMessage(res.errorType);
                this._dialogService.showMessageDialog("Napaka", errorMessage);
            }
            this._router.navigateByUrl("login");
        }).catch(err => {
            this._dialogService.showMessageDialog("Napaka", "Preverjanje prijave ni uspelo: Prišlo je do nepričakovane napake: " + err);
            this._router.navigateByUrl("login");
        });
    }

}
