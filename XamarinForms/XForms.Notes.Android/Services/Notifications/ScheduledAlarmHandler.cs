﻿using System.IO;
using System.Xml.Serialization;
using Android.Content;
using MvvmCross;
using XForms.Notes.Core.Models.IocInterfaces;

namespace XForms.Notes.Droid.Services.Notifications
{
    /// <summary>
    /// Broadcast receiver
    /// </summary>
    [BroadcastReceiver(Enabled = true, Label = "Local Notifications Plugin Broadcast Receiver")]
    public class ScheduledAlarmHandler : BroadcastReceiver
    {
        /// <summary>
        /// 
        /// </summary>
        public const string LocalNotificationKey = "LocalNotification";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="intent"></param>
        public override void OnReceive(Context context, Intent intent)
        {
            var extra = intent.GetStringExtra(LocalNotificationKey);
            var notification = DeserializeNotification(extra);

            Mvx.TryResolve<ILocalNotificationsService>(out var notificationsService);
            
            if(notificationsService == null) notificationsService =  new LocalNotificationsService();

            notificationsService.Show(notification.Title, notification.Body, notification.Id);
        }

        private LocalNotification DeserializeNotification(string notificationString)
        {
            var xmlSerializer = new XmlSerializer(typeof(LocalNotification));
            using (var stringReader = new StringReader(notificationString))
            {
                var notification = (LocalNotification)xmlSerializer.Deserialize(stringReader);
                return notification;
            }
        }
    }
}