﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using Plugin.Media;
using Plugin.Media.Abstractions;
using XForms.Notes.Core.Helpers;
using XForms.Notes.Core.Logic;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.Models.Notes;

namespace XForms.Notes.Core.ViewModels.Main.Detail
{
    public class NoteEditPageViewModel : MvxViewModel<int?>
    {
        private readonly IDialogService _dialogService;
        private readonly NotificationsLogic _notificationsLogic;
        private readonly NotesLogic _notesLogic;

        private MvxAsyncCommand _saveCommand;
        private MvxAsyncCommand _navigateToNotesCommand;
        private MvxAsyncCommand _addAttachmentCommand;
        private MvxAsyncCommand<NoteAttachment> _removeAttachmentCommand;
        private MvxAsyncCommand _setNotificationCommand;

        private Note _note;
        private DateTime? _notificationDateTime;

        public NoteEditPageViewModel(NotesLogic notesLogic, NotificationsLogic notificationsLogic, IDialogService dialogService)
        {
            _notesLogic = notesLogic;
            _dialogService = dialogService;
            _notificationsLogic = notificationsLogic;
        }

        public string Title
        {
            get => _note?.Title;
            set { _note.Title = value; RaisePropertyChanged(); } 
        }

        public string Text
        {
            get => _note?.Text;
            set { _note.Text = value; RaisePropertyChanged(); }
        }

        public List<NoteAttachment> Attachments
        {
            get => _note.Attachments;
            set { _note.Attachments = value; RaisePropertyChanged(); }
        }


        public MvxAsyncCommand SaveCommand => _saveCommand ?? (_saveCommand = new MvxAsyncCommand(Save));
        public MvxAsyncCommand NavigateToNotesCommand => _navigateToNotesCommand ?? (_navigateToNotesCommand = new MvxAsyncCommand(NavigateToNotes));
        public MvxAsyncCommand AddAttachmentCommand => _addAttachmentCommand ?? (_addAttachmentCommand = new MvxAsyncCommand(AddAttachment));
        public MvxAsyncCommand<NoteAttachment> RemoveAttachmentCommand => _removeAttachmentCommand ?? (_removeAttachmentCommand = new MvxAsyncCommand<NoteAttachment>(RemoveAttachment));
        public MvxAsyncCommand SetNotificationCommand => _setNotificationCommand ?? (_setNotificationCommand = new MvxAsyncCommand(SetNotification));


        private async Task Save()
        {
            if (string.IsNullOrEmpty(Title))
            {
                _dialogService.ShowAlert("Opozorilo", "Naslov je obvezen podatek.");
                return;
            }
            _dialogService.SetBusy(true);
            try
            {
                var result = await _notesLogic.SaveNote(_note);
                if (result.syncException != null)
                {
                    _dialogService.ShowAlert("Napaka",
                        "Zapisek je bil shranjen v lokalno podatkovno bazo. Sinhronizacija s strežnikom pa ni bila uspešna. " +
                        ExceptionHelper.GetErrorMessage(result.syncException));
                }
                if (_notificationDateTime.HasValue && result.id.HasValue && _notificationDateTime.Value > DateTime.Now)
                {
                    try
                    {
                        await _notificationsLogic.Schedule(result.id.Value, _notificationDateTime.Value);
                    }
                    catch (Exception exception)
                    {
                        _dialogService.ShowAlert("Napaka", "Nastavljanje obvestila ni bilo uspešno. " + ExceptionHelper.GetErrorMessage(exception));
                    }
                }

                await NavigateToNotes();
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", "Shranjevanje zapiska ni uspelo. " + ExceptionHelper.GetErrorMessage(exception));
            }
            _dialogService.SetBusy(false);
        }

        private async Task AddAttachment()
        {
            try
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    _dialogService.ShowAlert("Opozorilo", "Slike ni mogoče izbrati.");
                    return;
                }

                var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                {
                    MaxWidthHeight = 200,
                    CompressionQuality = 100
                });

                if (file == null) return;

                var stream = file.GetStream();
                byte[] imageBytes;

                using (var br = new BinaryReader(stream))
                {
                    imageBytes = br.ReadBytes((int)stream.Length);
                }

                var attachments = new List<NoteAttachment>();
                if (_note.Attachments?.Count > 0) attachments.AddRange(_note.Attachments);

                attachments.Add(new NoteAttachment
                {
                    Content = imageBytes,
                    Name = file.Path,
                    Type = file.Path.Split('.').ToList().LastOrDefault()
                });

                Attachments = attachments;
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", "Prišlo je do nepričakovane napake. " + exception.Message);
            }
            
        }

        private Task RemoveAttachment(NoteAttachment attachment)
        {
            return Task.Run(() =>
            {
                try
                {
                    Attachments = Attachments.Where(a => a != attachment).ToList();
                }
                catch (Exception exception)
                {
                    _dialogService.ShowAlert("Napaka", "Prišlo je do nepričakovane napake. " + exception.Message);
                }
            });
        }

        private async Task SetNotification()
        {
            _notificationDateTime = await NavigationService.Navigate<SetNotificationPageViewModel, DateTime?>();
        }

        private async Task NavigateToNotes()
        {
            await NavigationService.Navigate<NotesPageViewModel>();
        }

        public override async void Prepare(int? parameter)
        {
            _dialogService.SetBusy(true);
            try
            {
                _note = new Note();
                if (parameter.HasValue) _note = await _notesLogic.GetNote(parameter.Value);

                RaisePropertyChanged(nameof(Text));
                RaisePropertyChanged(nameof(Title));
                RaisePropertyChanged(nameof(Attachments));
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", ExceptionHelper.GetErrorMessage(exception));
            }
            _dialogService.SetBusy(false);
        }
    }
}