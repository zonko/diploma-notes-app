﻿using Newtonsoft.Json;

namespace XForms.Notes.Core.WebService.Models
{
    internal class WsUser
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}