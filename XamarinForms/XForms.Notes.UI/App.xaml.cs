﻿using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace XForms.Notes.UI
{
    public partial class FormsApp
    {
        public FormsApp()
        {
            InitializeComponent();
        }
    }
}
