export enum WsErrorType {
    ok = 1,
    badRequest = 2,
    internalServerError = 3,
    usernameIsTaken = 4,
    wrongUsernameOrPassword = 5,
    authorizationError = 6,
    serverNotReachable = 15
}