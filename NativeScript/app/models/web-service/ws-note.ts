import { WsNoteAttachment } from "~/models/web-service/ws-note-attachment";

export class WsNote {
    id: number;
    clientId: number;
    active: boolean;
    title: string;
    text: string;
    dateTimeChanged: string;
    attachments: Array<WsNoteAttachment>;
}