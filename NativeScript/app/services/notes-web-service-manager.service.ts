import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { WsResponse } from '~/models/web-service/ws-response';
import { SecureStorageService } from '~/services/secure-storage.service';
import { WsUser } from '~/models/web-service/ws-user';
import { WsNote } from '~/models/web-service/ws-note';
import { WsSynchronization } from '~/models/web-service/ws-synchronization';
import {OperationResult} from "~/models/core/operation-result";
import {WsErrorType} from "~/models/web-service/ws-error-type";
import {map} from "rxjs/operators";

@Injectable()
export class NotesWebServiceManagerService {

    private _baseServiceUrl = "http://notesapp2-001-site1.1tempurl.com/NotesService.svc/";
    private _pingMethodName = "Ping";
    private _loginMethodName = "Login";
    private _registerNewUserMethodName = "RegisterNewUser";
    private _synchronizeMethodName = "Synchronize";


    constructor(private _http: HttpClient,
                private _secureStorageService: SecureStorageService) {
    }

    public async ping (): Promise<OperationResult<null>> {
        try {
            let timeoutPromise = new Promise<WsErrorType>((resolve, reject) => {
                let wait = setTimeout(() => {
                    clearTimeout(wait);
                    resolve(WsErrorType.serverNotReachable);
                }, 2000);
            });
            let pingResult = await Promise.race([
                timeoutPromise,
                this.doPing().toPromise()
            ]);
            return new OperationResult(pingResult === WsErrorType.ok);
        } catch (e) {
            return new OperationResult(false);
        }
    }

    private doPing(): Observable<WsErrorType> {
        const fullUrl = this._baseServiceUrl + this._pingMethodName;
        return this._http.get<WsResponse<null>>(fullUrl).pipe(map(r => r.errorType));
    }

    public login(username: string, passwordHash: string): Observable<WsResponse<string>> {
        const fullUrl = this._baseServiceUrl + this._loginMethodName;
        const requestData = new WsUser(username, passwordHash);

        return this._http.post<WsResponse<string>>(fullUrl, requestData, this.getHttpOptions());
    }

    public registerNewUser(username: string, passwordHash: string): Observable<WsResponse<null>> {
        const fullUrl = this._baseServiceUrl + this._registerNewUserMethodName;
        const requestData = new WsUser(username, passwordHash);

        return this._http.post<WsResponse<null>>(fullUrl, requestData, this.getHttpOptions());
    }

    public synchronize(notes: Array<WsNote>, lastSyncDateTime: string): Observable<WsResponse<WsSynchronization>> {
        const fullUrl = this._baseServiceUrl + this._synchronizeMethodName;
        const requestData =  new WsSynchronization();
        requestData.lastSyncDateTime = lastSyncDateTime;
        requestData.notes = notes;
        return this._http.post<WsResponse<WsSynchronization>>(fullUrl, requestData, this.getHttpOptions());
    }

    private getHttpOptions() {
        let headers = new HttpHeaders();
        const userToken = this._secureStorageService.getUserToken();
        if (userToken) {
            headers = headers.set('Content-Type', 'application/json').set('Authorization', userToken);
        } else {
            headers = headers.set('Content-Type', 'application/json');
        }
        const httpOptions = {
            headers: headers
        };
        return httpOptions;
    }

}
