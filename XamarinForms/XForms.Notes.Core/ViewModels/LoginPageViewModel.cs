﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using XForms.Notes.Core.Helpers;
using XForms.Notes.Core.Logic;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.ViewModels.Main;
using XForms.Notes.Core.ViewModels.Main.Detail;

namespace XForms.Notes.Core.ViewModels
{
    public class LoginPageViewModel : MvxViewModel
    {
        private readonly IDialogService _dialogService;
        private readonly UserLogic _userLogic;
        private readonly NotesLogic _notesLogic;

        private string _username;
        private string _password;

        private MvxAsyncCommand _loginCommand;
        private MvxAsyncCommand _navigateToRegistrationCommand;
        

        public LoginPageViewModel(IDialogService dialogService, UserLogic userLogic, NotesLogic notesLogic)
        {
            _dialogService = dialogService;
            _userLogic = userLogic;
            _notesLogic = notesLogic;
        }

        public string Username
        {
            get => _username;
            set => SetProperty(ref _username, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public MvxAsyncCommand LoginCommand => _loginCommand ?? (_loginCommand = new MvxAsyncCommand(Login));
        public MvxAsyncCommand NavigateToRegistrationCommand => _navigateToRegistrationCommand ?? (_navigateToRegistrationCommand = new MvxAsyncCommand(NavigateToRegistration));

        private async Task Login()
        {
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                _dialogService.ShowAlert("Prijava", "Vnesite uporabniško ime in geslo.");
                return;
            }
            _dialogService.SetBusy(true);

            try
            {
                await _userLogic.Login(Username, Password);

                try
                {
                    await _notesLogic.Synchronize();
                }
                catch (Exception exception)
                {
                    _dialogService.ShowAlert("Napaka", "Sinhronizacija ni uspela. " + ExceptionHelper.GetErrorMessage(exception));
                }

                await NavigationService.Navigate<MasterPageViewModel>();
                await NavigationService.Navigate<NotesPageViewModel>();
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", "Prijava ni uspela. " + ExceptionHelper.GetErrorMessage(exception));
            }

            _dialogService.SetBusy(false);

        }

        private async Task NavigateToRegistration()
        {
            await NavigationService.Navigate<RegistrationPageViewModel>();
        }
    }
}