import 'package:ft_notes_app/helpers/error_message_helper.dart';
import 'package:ft_notes_app/logic/user_logic.dart';

abstract class RegistrationScreenContract {
  void onRegistrationSuccess();
  void onRegistratioError(String errorTxt);
}

class RegistrationScreenPresenter {
  RegistrationScreenContract _view;
  final UserLogic _userLogic = new UserLogic();
  RegistrationScreenPresenter(this._view);

  registerNewUser(String username, String password) async {
    try {
      await _userLogic.registerNewUser(username, password);
      _view.onRegistrationSuccess();
    } catch (error) {
      _view.onRegistratioError("Registracija ni uspela. "  + ErrorMessageHelper.getErrorMessage(error));
    }
  }
}