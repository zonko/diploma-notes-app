﻿using SQLite;

namespace XForms.Notes.Core.Database.Models
{
    internal class DbtNotification
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
        public string User { get; set; }
        public int NoteId { get; set; }
    }
}