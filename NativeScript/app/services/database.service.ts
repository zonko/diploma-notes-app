import {Injectable} from "@angular/core";
import {SecureStorageService} from "~/services/secure-storage.service";
import {ErrorType} from "~/models/core/error-type";
import {OperationResult} from "~/models/core/operation-result";
import {Note} from "~/models/core/note";
import {NoteDisplay} from "~/models/core/note-display";
import {NoteAttachment} from "~/models/core/note-attachment";
import {Notification} from "~/models/core/notification";

var Sqlite = require("nativescript-sqlite");

@Injectable()
export class DatabaseService {
    private _database
    private _notificationDatabase;

    constructor(private _secureStorageService: SecureStorageService) {
    }

    public async initializeNotificationsDatabase() {
        try {
            console.log("notification db init");
            const db = await new Sqlite('NotesNotifications.db').catch(err => {
                throw (ErrorType.dbOpenDbError);
            });
            await db.execSQL(`CREATE TABLE IF NOT EXISTS Notification (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                User TEXT NOT NULL,
                NoteId INTEGER NOT NULL
            );`).catch(err => {
                console.log("Obvestilo napaka:");
                console.log(err);
                throw (ErrorType.dbCreateTablesError);
            });

            this._notificationDatabase = db;
            this._notificationDatabase.resultType(Sqlite.RESULTSASOBJECT);
            return new OperationResult<null>(true);

        } catch (error) {
            return new OperationResult(false, null, error);
        }
    }

    public async initializeDatabase(): Promise<OperationResult<null>> {
        const user = this._secureStorageService.getUser();
        if (!user) {
            return new OperationResult(false, null, ErrorType.dbUserNotLogedIn)
        }

        try {
            // await Sqlite.deleteDatabase(user + '.db');
            const db = await new Sqlite(user + '.db').catch(err => {
                throw (ErrorType.dbOpenDbError);
            });

            await db.execSQL(`CREATE TABLE IF NOT EXISTS Note (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                Cid	INTEGER,
                Title TEXT,
                Text TEXT,
                DateTimeChanged TEXT NOT NULL,
                Modified INTEGER NOT NULL,
                Active INTEGER NOT NULL
            );`).catch(err => {
                throw (ErrorType.dbCreateTablesError);
            });

            await db.execSQL(`CREATE TABLE IF NOT EXISTS Attachment (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                NoteId INTEGER NOT NULL,
                Name TEXT,
                Type TEXT,
                Content	TEXT,
                FOREIGN KEY(NoteId) REFERENCES Note(Id)
            );`).catch(err => {
                throw (ErrorType.dbCreateTablesError);
            });

            await db.execSQL(`CREATE TABLE IF NOT EXISTS KeyValue (
                Key TEXT,
                Value TEXT,
                PRIMARY KEY(Key)
            );`).catch(err => {
                throw (ErrorType.dbCreateTablesError);
            });


            this._database = db;
            this._database.resultType(Sqlite.RESULTSASOBJECT);
            return new OperationResult<null>(true);

        } catch (error) {
            return new OperationResult(false, null, error);
        }
    }

    public async saveNotes(notes: Array<Note>): Promise<OperationResult<null>> {
        if (!this._database) {
            return new OperationResult(false, null, ErrorType.dbNotInitialized)
        }
        if (!notes) {
            return new OperationResult(true);
        }

        for (let note of notes) {
            var result = await this.saveNote(note, true);
            if (!result.success) {
                return new OperationResult(result.success, null, result.errorType, result.errorMessage);
            }
        }

        return new OperationResult(true);
    }

    public async saveNote(note: Note, deleteIfNotActive: boolean = false): Promise<OperationResult<number>> {
        try {
            if (!this._database) {
                return new OperationResult(false, null, ErrorType.dbNotInitialized)
            }
            if (!note) {
                return new OperationResult(true);
            }

            let dbNote = null;
            if (note.id) {  // try find by id
                dbNote = await this._database.get('SELECT * FROM Note WHERE Id = ?', [note.id]).catch(err => {
                    throw (ErrorType.dbReadError);
                });
            } else if (note.cid) { // try find by central id
                dbNote = await this._database.get('SELECT * FROM Note WHERE Cid = ?', [note.cid]).catch(err => {
                    throw (ErrorType.dbReadError);
                });
            }

            // update
            let noteId = null;
            if (dbNote) {
                noteId = dbNote.Id;
                if (deleteIfNotActive && !note.active) {
                    await this._database.execSQL("DELETE FROM Attachment WHERE NoteId = ?", [noteId]).catch(err => {
                        throw (ErrorType.dbWriteError);
                    });
                    await this._database.execSQL("DELETE FROM Note WHERE Id = ?", [noteId]).catch(err => {
                        throw (ErrorType.dbWriteError);
                    });
                    noteId = null;
                } else {
                    await this._database.execSQL(`UPDATE Note SET
                                            Cid	= ?,
                                            Title = ?,
                                            Text = ?,
                                            DateTimeChanged	= ?,
                                            Modified = ?,
                                            Active = ?
                                          WHERE Id = ?`, [note.cid, note.title, note.text, note.dateTimeChanged, note.modified, note.active, noteId]).catch(err => {
                        throw (ErrorType.dbWriteError);
                    });
                }

            } else {  // insert
                if (!note.active) return new OperationResult(true);  // do not insert if not active
                noteId = await this._database.execSQL(`INSERT INTO Note (Cid, Title, Text, DateTimeChanged, Modified, Active)
                                            VALUES (?, ?, ?, ?, ?, ?)`,
                    [note.cid, note.title, note.text, note.dateTimeChanged, note.modified, note.active]).catch(err => {
                    throw (ErrorType.dbWriteError);
                });
            }
            if (noteId) {  // update attachments
                await this._database.execSQL('DELETE FROM Attachment WHERE NoteId = ?', [noteId]).catch(err => {
                    throw (ErrorType.dbWriteError);
                });

                if (note.attachments) {
                    for (let attachment of note.attachments) {
                        await this._database.execSQL(`INSERT INTO Attachment (NoteId, Type, Content) VALUES (?, ?, ?)`, [noteId, attachment.type, attachment.content])
                            .catch(err => {
                                throw (ErrorType.dbWriteError);
                            });
                    }
                }
            }
            return new OperationResult(true, noteId);
        } catch (e) {
            return new OperationResult(false, null, e);
        }

    }

    public async deactivateNote(id: number): Promise<OperationResult<null>> {
        try {
            if (!this._database) {
                return new OperationResult(false, null, ErrorType.dbNotInitialized)
            }
            await this._database.execSQL("UPDATE Note SET Active = 'false', Modified = 'true' WHERE Id = ?", [id]).catch(err => {
                throw (ErrorType.dbWriteError);
            });
        } catch (e) {
            return new OperationResult(false, null, e);
        }
        return new OperationResult(true);
    }

    public async listNotes(searchText: string = ''): Promise<OperationResult<Array<NoteDisplay>>> {
        try {
            if (!this._database) {
                return new OperationResult(false, null, ErrorType.dbNotInitialized)
            }
            const rows = await this._database.all("SELECT Id, Title, DateTimeChanged FROM Note WHERE Active = 'true' AND Title LIKE '%" + searchText + "%' ORDER BY DateTimeChanged DESC").catch(err => {
                throw (ErrorType.dbReadError);
            });
            const results = new Array<NoteDisplay>();
            if (rows) {
                for (let row of rows) {
                    results.push(new NoteDisplay(row.Id, row.Title, row.DateTimeChanged));
                }
                return new OperationResult(true, results);
            }
        } catch (e) {
            return new OperationResult(false, null, e);
        }
    }

    public async listModifiedNotes(): Promise<OperationResult<Array<Note>>> {
        try {
            if (!this._database) {
                return new OperationResult(false, null, ErrorType.dbNotInitialized)
            }

            const rows = await this._database.all("SELECT * FROM Note WHERE Modified = 'true'").catch(err => {
                throw (ErrorType.dbReadError);
            });

            const results = new Array<Note>();
            if (rows) {
                for (let row of rows) {
                    const note = new Note();
                    note.active = row.Active;
                    note.cid = row.Cid;
                    note.dateTimeChanged = row.DateTimeChanged;
                    note.id = row.Id;
                    note.modified = row.Modified;
                    note.text = row.Text;
                    note.title = row.Title;

                    const attachmentRows = await this._database.all("SELECT * FROM Attachment WHERE NoteId = ?", [note.id]).catch(err => {
                        throw (ErrorType.dbReadError);
                    });
                    const attachments = new Array<NoteAttachment>();
                    if (attachmentRows) {
                        for (let att of attachmentRows) {
                            const attachment = new NoteAttachment();
                            attachment.content = att.Content;
                            attachment.type = att.Type;
                            attachment.name = att.Name;

                            attachments.push(attachment);
                        }
                    }

                    note.attachments = attachments;

                    results.push(note);
                }
            }

            return new OperationResult(true, results);
        } catch (e) {
            return new OperationResult(false, null, e);
        }
    }

    public async getNote(id: number): Promise<OperationResult<Note>> {
        try {
            if (!this._database) {
                return new OperationResult(false, null, ErrorType.dbNotInitialized)
            }
            if (!id) {
                return new OperationResult(false);
            }

            const dbNote = await this._database.get('SELECT * FROM Note WHERE Id = ?', [id]).catch(err => {
                throw (ErrorType.dbReadError);
            });
            const dbAttachments = await this._database.all('SELECT * FROM Attachment WHERE NoteId = ?', [id]).catch(err => {
                throw (ErrorType.dbReadError);
            });

            if (!dbNote) return new OperationResult(false);

            const note = new Note();
            note.id = dbNote.Id;
            note.cid = dbNote.Cid;
            note.active = dbNote.Active;
            note.dateTimeChanged = dbNote.DateTimeChanged;
            note.title = dbNote.Title;
            note.text = dbNote.Text;
            note.attachments = new Array<NoteAttachment>();

            if (dbAttachments) {
                for (let dbAttachment of dbAttachments) {
                    const attachment = new NoteAttachment();
                    attachment.content = dbAttachment.Content;
                    attachment.type = dbAttachment.Type;
                    note.attachments.push(attachment)
                }
            }

            return new OperationResult(true, note);
        } catch (e) {
            return new OperationResult(false, null, e);
        }
    }

    public async getValue(key: string): Promise<OperationResult<string>> {
        try {
            if (!this._database) {
                return new OperationResult(false, null, ErrorType.dbNotInitialized)
            }

            const dbValue = await this._database.get('SELECT * FROM KeyValue WHERE Key = ?', [key]).catch(err => {
                throw (ErrorType.dbReadError);
            });
            const value = dbValue ? dbValue.Value : null;

            return new OperationResult(true, value);
        } catch (e) {
            return new OperationResult(false, null, e);
        }
    }

    public async setValue(key: string, value: string): Promise<OperationResult<null>> {
        try {
            if (!this._database) {
                return new OperationResult(false, null, ErrorType.dbNotInitialized)
            }
            const dbValue = await this._database.get('SELECT * FROM KeyValue WHERE Key = ?', [key]).catch(err => {
                throw (ErrorType.dbReadError);
            });
            if (dbValue) {
                await this._database.execSQL("UPDATE KeyValue SET Value = ? WHERE Key = ?", [value, key]).catch(err => {
                    throw (ErrorType.dbWriteError);
                });
            } else {
                await this._database.execSQL("INSERT INTO KeyValue (Key, Value) VALUES (?, ?)", [key, value]).catch(err => {
                    throw (ErrorType.dbWriteError);
                });
            }
            return new OperationResult(true);
        } catch (e) {
            return new OperationResult(false, null, e);
        }
    }


    public async insertNewNotification (notification: Notification): Promise<OperationResult<number>> {
        try {
            if (!this._notificationDatabase) {
                return new OperationResult(false, null, ErrorType.dbNotInitialized)
            }
            const id = await this._notificationDatabase.execSQL('INSERT INTO Notification (User, NoteId) VALUES (?, ?)', [notification.user, notification.noteId]).catch(err => {
                console.log("Obvestilo napaka:");
                console.log(err);
                throw (ErrorType.dbWriteError);
            });
            return new OperationResult(true, id);

        } catch (e) {
            return new OperationResult(false, null, e);
        }
    }

    public async getNotification (id: number): Promise<OperationResult<Notification>> {
        try {
            if (!this._notificationDatabase) {
                return new OperationResult(false, null, ErrorType.dbNotInitialized)
            }
            const dbValue = await this._notificationDatabase.get('SELECT * FROM Notification WHERE Id = ?', [id]).catch(err => {
                throw (ErrorType.dbReadError);
            });
            if(!dbValue) {
                return new OperationResult(false, null, ErrorType.dbReadError);
            }
            const notification = new Notification(dbValue.User, dbValue.NoteId);
            return new OperationResult(true, notification);
        } catch (e) {
            return new OperationResult(false, null, e);
        }
    }
}