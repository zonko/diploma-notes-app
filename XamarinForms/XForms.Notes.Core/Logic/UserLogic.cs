﻿using System.Threading.Tasks;
using XForms.Notes.Core.Database;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.WebService;

namespace XForms.Notes.Core.Logic
{
    public class UserLogic
    {
        private readonly NotesServiceManager _notesServiceManager;
        private readonly DataAccess _dataAccess;
        private readonly ISecureStorageService _secureStorageService;

        public UserLogic(NotesServiceManager notesServiceManager, DataAccess dataAccess, ISecureStorageService secureStorageService)
        {
            _notesServiceManager = notesServiceManager;
            _dataAccess = dataAccess;
            _secureStorageService = secureStorageService;
        }

        internal async Task Login(string username, string password)
        {
            await _notesServiceManager.Ping();
            var userToken = await _notesServiceManager.Login(username, password);
            await _secureStorageService.SaveUserToken(userToken);
            await _secureStorageService.SaveUser(username);
            await _dataAccess.InitializeUserDatabase();
        }

        internal async Task RegisterNewUser(string username, string password)
        {
            await _notesServiceManager.Ping();
            await _notesServiceManager.RegisterNewUser(username, password);
        }

        internal async Task<bool> CheckLogin()
        {
            var userToken = await _secureStorageService.GetUserToken();
            var user = await _secureStorageService.GetUser();
            if (string.IsNullOrEmpty(userToken) || string.IsNullOrEmpty(user)) return false;

            await _dataAccess.InitializeUserDatabase();
            return true;
        }

        internal void Logout()
        {
            _secureStorageService.SaveUserToken(null);
            _secureStorageService.SaveUser(null);
        }
    }
}