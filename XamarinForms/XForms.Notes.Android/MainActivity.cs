﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using MvvmCross;
using MvvmCross.Forms.Platforms.Android.Views;
using Plugin.CurrentActivity;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.UI;

namespace XForms.Notes.Droid
{
    [Activity(
        Label = "XForms Notes App",
        Icon = "@drawable/icon",
        Theme = "@style/MainTheme",
        MainLauncher = false,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, LaunchMode = LaunchMode.SingleInstance)]
    public class MainActivity : MvxFormsAppCompatActivity<Setup, Core.App, FormsApp>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(savedInstanceState);

            CrossCurrentActivity.Current.Init(this, savedInstanceState);
        }

        public override void OnBackPressed()
        {
            
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            var notificationId = intent.GetIntExtra("NotificationId", -1);

            if (notificationId > -1)
            {
                Mvx.TryResolve<ILocalNotificationsService>(out var notificationsService);
                notificationsService?.InvokeNotificationOpened(notificationId);
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}