import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ft_notes_app/app_constants.dart';
import 'package:ft_notes_app/globals.dart';
import 'package:ft_notes_app/screens/activity_indicator.dart';
import 'package:ft_notes_app/screens/drawer/navigation_drawer_presenter.dart';
import 'package:ft_notes_app/screens/note_edit/note_edit_screen.dart';
import 'package:ft_notes_app/services/dialog_service.dart';

class NavigationDrawer extends StatefulWidget {
  @override
  NavigationDrawerState createState() => new NavigationDrawerState();
}

class NavigationDrawerState extends State<NavigationDrawer> implements NavigationDrawerContract {
  NavigationDrawerPresenter _presenter;
  final DialogService _dialogService = new DialogService();

  bool _isLoading = false;
  String _user;

  NavigationDrawerState() {
    _presenter = new NavigationDrawerPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Expanded(
                child: ListView(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(top: 40.0, bottom: 15.0),
                    child: Image.asset('assets/notes_logo.png', height: 80.0)),
                Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Row(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: Text('PRIJAVLJEN UPORABNIK:',
                                style: TextStyle(color: Colors.orangeAccent))),
                        Text( _user ?? '',
                            style: TextStyle(color: Colors.orangeAccent))
                      ],
                    )),
                Container(
                    height: 1.5,
                    color: Colors.orangeAccent,
                    margin: EdgeInsets.symmetric(vertical: 20.0)),
                Container(
                    height: 50.0,
                    width: double.infinity,
                    child: FlatButton(
                      color: Colors.transparent,
                      textColor: Colors.orangeAccent,
                      child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text('DODAJ ZAPISEK')),
                      onPressed: () { 
                        Navigator.pop(context); 
                        Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context) => new NoteEditScreen(null))); 
                      },
                    )),
                Container(
                    height: 50.0,
                    width: double.infinity,
                    child: FlatButton(
                      color: Colors.transparent,
                      textColor: Colors.orangeAccent,
                      child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text('SINHRONIZIRAJ')),
                      onPressed: _synchronize,
                    )),
                Container(
                    height: 1.5,
                    color: Colors.orangeAccent,
                    margin: EdgeInsets.symmetric(vertical: 20.0)),
                Container(
                    height: 50.0,
                    width: double.infinity,
                    child: FlatButton(
                      color: Colors.transparent,
                      textColor: Colors.orangeAccent,
                      child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text('ODJAVA')),
                      onPressed: () { _presenter.logout(); },
                    ))
                
              ],
            )),
            Align(
              alignment: FractionalOffset.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 15.0),
                child: FlatButton(
                  color: Colors.transparent,
                  textColor: Colors.orangeAccent,
                  child: Text('ZAPRI'),
                  onPressed: () => Navigator.pop(context),
              )),
              ) 
          ],
        ),
        Align(
            child: ActivityIndicator(isLoading: _isLoading),
            alignment: FractionalOffset.center)
      ],
    );
  }

  void _synchronize() {
    setState(() { _isLoading =  true; });
    _presenter.synchronize();
  }

  @override
  void onError(String errorMessage) async {
    setState(() { _isLoading = false; });
    await _dialogService.showAlert('Napaka', errorMessage, context);
  }

  @override
  void onSyncSuccess() {
    setState(() { _isLoading = false; });
    Globals.doRefreshController.add(true);
  }

  @override
  void onLoggedOut() {
    Navigator.pushReplacementNamed(context, ROUTE__LOGIN);
  }

  @override
  void setUser(String user) {
    setState(() { _user = user; });
  }
}
