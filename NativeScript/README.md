# NativeScript NotesApp

Simple application for saving notes, developed using NativeScript and Angular.

## Geting Started

Application was developed with NativeScript using Angular framework and 
TypeScript programming language. Project can be opened and run on both iOS and 
Android, with any code editor (e.g. VS Code), NativeScript and Angular 
frameworks must be installed beforehand.


### Folder Content

- app: Contains actual NativeScrpit app (TypeScript)
- hooks: Contains application lifecycle hooks (JavaScript)
- scr: Contains finished applications screenshots

