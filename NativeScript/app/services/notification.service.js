"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var database_service_1 = require("~/services/database.service");
var operation_result_1 = require("~/models/core/operation-result");
var notification_1 = require("~/models/core/notification");
var LocalNotifications = require("nativescript-local-notifications");
var secure_storage_service_1 = require("~/services/secure-storage.service");
var error_type_1 = require("~/models/core/error-type");
var dialog_service_1 = require("~/services/dialog.service");
var router_1 = require("@angular/router");
var NotificationService = /** @class */ (function () {
    function NotificationService(_databaseService, _secureStorageService, _dialogService, _router) {
        var _this = this;
        this._databaseService = _databaseService;
        this._secureStorageService = _secureStorageService;
        this._dialogService = _dialogService;
        this._router = _router;
        LocalNotifications.addOnMessageReceivedCallback(function (data) { return _this.handleNotificationReceived(data); });
        this._databaseService.initializeNotificationsDatabase();
    }
    NotificationService.prototype.schedule = function (noteId, title, text, date) {
        return __awaiter(this, void 0, void 0, function () {
            var user, getIdRes, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (date <= new Date())
                            return [2 /*return*/, new operation_result_1.OperationResult(true)];
                        user = this._secureStorageService.getUser();
                        if (!user)
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbUserNotLogedIn)];
                        return [4 /*yield*/, this._databaseService.insertNewNotification(new notification_1.Notification(user, noteId))];
                    case 1:
                        getIdRes = _a.sent();
                        if (!getIdRes.success)
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, getIdRes.errorType)];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, LocalNotifications.schedule([{
                                    id: getIdRes.result,
                                    title: title,
                                    body: text,
                                    badge: 1,
                                    ongoing: false,
                                    at: date
                                }]).catch(function (err) {
                                console.log("Obvestilo napaka:");
                                console.log(err);
                                throw error_type_1.ErrorType.notificationSchedulingError;
                            })];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(true)];
                    case 4:
                        e_1 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_1)];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    NotificationService.prototype.handleNotificationReceived = function (data) {
        var _this = this;
        var user = this._secureStorageService.getUser();
        if (!user)
            return;
        this._databaseService.getNotification(data.id).then(function (res) {
            if (!res.success) {
                _this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake med branjem obvestila.");
            }
            else if (!res.result) {
                return;
            }
            else if (res.result.user !== user) {
                _this._dialogService.showMessageDialog("Napaka", "Obvestilo je namenjeno drugemu uporabniku.");
            }
            else {
                _this._router.navigateByUrl("note/" + res.result.noteId);
            }
        }).catch(function (err) {
            _this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake med branjem obvestila.");
        });
    };
    NotificationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [database_service_1.DatabaseService,
            secure_storage_service_1.SecureStorageService,
            dialog_service_1.DialogService,
            router_1.Router])
    ], NotificationService);
    return NotificationService;
}());
exports.NotificationService = NotificationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJub3RpZmljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsc0NBQTJDO0FBQzNDLGdFQUE0RDtBQUM1RCxtRUFBK0Q7QUFDL0QsMkRBQXdEO0FBQ3hELHFFQUF1RTtBQUN2RSw0RUFBdUU7QUFDdkUsdURBQW1EO0FBRW5ELDREQUF3RDtBQUN4RCwwQ0FBdUM7QUFHdkM7SUFDSSw2QkFBb0IsZ0JBQWlDLEVBQ2pDLHFCQUEyQyxFQUMzQyxjQUE2QixFQUM3QixPQUFlO1FBSG5DLGlCQU1DO1FBTm1CLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7UUFDakMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUFzQjtRQUMzQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZTtRQUM3QixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQy9CLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsS0FBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxFQUFyQyxDQUFxQyxDQUFDLENBQUM7UUFDakcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLCtCQUErQixFQUFFLENBQUE7SUFDM0QsQ0FBQztJQUVZLHNDQUFRLEdBQXJCLFVBQXNCLE1BQWMsRUFBRSxLQUFhLEVBQUUsSUFBWSxFQUFFLElBQVU7Ozs7Ozt3QkFDekUsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFLENBQUM7NEJBQUMsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsSUFBSSxDQUFDLEVBQUM7d0JBQ25ELElBQUksR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ2xELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDOzRCQUFDLE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsc0JBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFDO3dCQUM5RCxxQkFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsSUFBSSwyQkFBWSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQyxFQUFBOzt3QkFBNUYsUUFBUSxHQUFHLFNBQWlGO3dCQUNsRyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7NEJBQUMsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUM7Ozs7d0JBRy9FLHFCQUFNLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO29DQUMvQixFQUFFLEVBQUUsUUFBUSxDQUFDLE1BQU07b0NBQ25CLEtBQUssRUFBRSxLQUFLO29DQUNaLElBQUksRUFBRSxJQUFJO29DQUNWLEtBQUssRUFBRSxDQUFDO29DQUNSLE9BQU8sRUFBRSxLQUFLO29DQUNkLEVBQUUsRUFBRSxJQUFJO2lDQUNYLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dDQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dDQUNqQixNQUFNLHNCQUFTLENBQUMsMkJBQTJCLENBQUE7NEJBQy9DLENBQUMsQ0FBQyxFQUFBOzt3QkFYRixTQVdFLENBQUM7d0JBQ0gsc0JBQU8sSUFBSSxrQ0FBZSxDQUFDLElBQUksQ0FBQyxFQUFDOzs7d0JBRWpDLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLEdBQUMsQ0FBQyxFQUFDOzs7OztLQUVsRDtJQUVNLHdEQUEwQixHQUFqQyxVQUFrQyxJQUEwQjtRQUE1RCxpQkFpQkM7UUFoQkcsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2xELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBRWxCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDbkQsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDZixLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSw0Q0FBNEMsQ0FBQyxDQUFDO1lBQ2xHLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDcEIsTUFBTSxDQUFDO1lBQ1gsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSw0Q0FBNEMsQ0FBQyxDQUFDO1lBQ2xHLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM1RCxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztZQUNSLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLDRDQUE0QyxDQUFDLENBQUM7UUFDbEcsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBcERRLG1CQUFtQjtRQUQvQixpQkFBVSxFQUFFO3lDQUU2QixrQ0FBZTtZQUNWLDZDQUFvQjtZQUMzQiw4QkFBYTtZQUNwQixlQUFNO09BSjFCLG1CQUFtQixDQXFEL0I7SUFBRCwwQkFBQztDQUFBLEFBckRELElBcURDO0FBckRZLGtEQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge0RhdGFiYXNlU2VydmljZX0gZnJvbSBcIn4vc2VydmljZXMvZGF0YWJhc2Uuc2VydmljZVwiO1xyXG5pbXBvcnQge09wZXJhdGlvblJlc3VsdH0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvb3BlcmF0aW9uLXJlc3VsdFwiO1xyXG5pbXBvcnQge05vdGlmaWNhdGlvbn0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvbm90aWZpY2F0aW9uXCI7XHJcbmltcG9ydCAqIGFzIExvY2FsTm90aWZpY2F0aW9ucyBmcm9tIFwibmF0aXZlc2NyaXB0LWxvY2FsLW5vdGlmaWNhdGlvbnNcIjtcclxuaW1wb3J0IHtTZWN1cmVTdG9yYWdlU2VydmljZX0gZnJvbSBcIn4vc2VydmljZXMvc2VjdXJlLXN0b3JhZ2Uuc2VydmljZVwiO1xyXG5pbXBvcnQge0Vycm9yVHlwZX0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvZXJyb3ItdHlwZVwiO1xyXG5pbXBvcnQge1JlY2VpdmVkTm90aWZpY2F0aW9ufSBmcm9tIFwibmF0aXZlc2NyaXB0LWxvY2FsLW5vdGlmaWNhdGlvbnNcIjtcclxuaW1wb3J0IHtEaWFsb2dTZXJ2aWNlfSBmcm9tIFwifi9zZXJ2aWNlcy9kaWFsb2cuc2VydmljZVwiO1xyXG5pbXBvcnQge1JvdXRlcn0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uU2VydmljZSB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9kYXRhYmFzZVNlcnZpY2U6IERhdGFiYXNlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX3NlY3VyZVN0b3JhZ2VTZXJ2aWNlOiBTZWN1cmVTdG9yYWdlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2RpYWxvZ1NlcnZpY2U6IERpYWxvZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcikge1xyXG4gICAgICAgIExvY2FsTm90aWZpY2F0aW9ucy5hZGRPbk1lc3NhZ2VSZWNlaXZlZENhbGxiYWNrKChkYXRhKSA9PiB0aGlzLmhhbmRsZU5vdGlmaWNhdGlvblJlY2VpdmVkKGRhdGEpKTtcclxuICAgICAgICB0aGlzLl9kYXRhYmFzZVNlcnZpY2UuaW5pdGlhbGl6ZU5vdGlmaWNhdGlvbnNEYXRhYmFzZSgpXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIHNjaGVkdWxlKG5vdGVJZDogbnVtYmVyLCB0aXRsZTogc3RyaW5nLCB0ZXh0OiBzdHJpbmcsIGRhdGU6IERhdGUpOiBQcm9taXNlPE9wZXJhdGlvblJlc3VsdDxudWxsPj4ge1xyXG4gICAgICAgIGlmIChkYXRlIDw9IG5ldyBEYXRlKCkpIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHRydWUpO1xyXG4gICAgICAgIGNvbnN0IHVzZXIgPSB0aGlzLl9zZWN1cmVTdG9yYWdlU2VydmljZS5nZXRVc2VyKCk7XHJcbiAgICAgICAgaWYgKCF1c2VyKSByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgRXJyb3JUeXBlLmRiVXNlck5vdExvZ2VkSW4pO1xyXG4gICAgICAgIGNvbnN0IGdldElkUmVzID0gYXdhaXQgdGhpcy5fZGF0YWJhc2VTZXJ2aWNlLmluc2VydE5ld05vdGlmaWNhdGlvbihuZXcgTm90aWZpY2F0aW9uKHVzZXIsIG5vdGVJZCkpO1xyXG4gICAgICAgIGlmICghZ2V0SWRSZXMuc3VjY2VzcykgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIGdldElkUmVzLmVycm9yVHlwZSk7XHJcblxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGF3YWl0IExvY2FsTm90aWZpY2F0aW9ucy5zY2hlZHVsZShbe1xyXG4gICAgICAgICAgICAgICAgaWQ6IGdldElkUmVzLnJlc3VsdCxcclxuICAgICAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcclxuICAgICAgICAgICAgICAgIGJvZHk6IHRleHQsXHJcbiAgICAgICAgICAgICAgICBiYWRnZTogMSxcclxuICAgICAgICAgICAgICAgIG9uZ29pbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgYXQ6IGRhdGVcclxuICAgICAgICAgICAgfV0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIk9idmVzdGlsbyBuYXBha2E6XCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyKTtcclxuICAgICAgICAgICAgICAgIHRocm93IEVycm9yVHlwZS5ub3RpZmljYXRpb25TY2hlZHVsaW5nRXJyb3JcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHRydWUpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIGUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaGFuZGxlTm90aWZpY2F0aW9uUmVjZWl2ZWQoZGF0YTogUmVjZWl2ZWROb3RpZmljYXRpb24pOiB2b2lkIHtcclxuICAgICAgICBjb25zdCB1c2VyID0gdGhpcy5fc2VjdXJlU3RvcmFnZVNlcnZpY2UuZ2V0VXNlcigpO1xyXG4gICAgICAgIGlmICghdXNlcikgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLl9kYXRhYmFzZVNlcnZpY2UuZ2V0Tm90aWZpY2F0aW9uKGRhdGEuaWQpLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgICAgaWYgKCFyZXMuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zaG93TWVzc2FnZURpYWxvZyhcIk5hcGFrYVwiLCBcIlByacWhbG8gamUgZG8gbmFwYWtlIG1lZCBicmFuamVtIG9idmVzdGlsYS5cIik7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZighcmVzLnJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJlcy5yZXN1bHQudXNlciAhPT0gdXNlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zaG93TWVzc2FnZURpYWxvZyhcIk5hcGFrYVwiLCBcIk9idmVzdGlsbyBqZSBuYW1lbmplbm8gZHJ1Z2VtdSB1cG9yYWJuaWt1LlwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZUJ5VXJsKFwibm90ZS9cIiArIHJlcy5yZXN1bHQubm90ZUlkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJOYXBha2FcIiwgXCJQcmnFoWxvIGplIGRvIG5hcGFrZSBtZWQgYnJhbmplbSBvYnZlc3RpbGEuXCIpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59Il19