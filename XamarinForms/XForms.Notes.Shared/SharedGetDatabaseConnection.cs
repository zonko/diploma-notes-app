﻿using System.IO;
using SQLite;

namespace XForms.Notes.Shared
{
    public static class SharedPlatformFunctions
    {
        public static SQLiteConnection GetDatabaseConnection(string filename)
        {
            string path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), filename);
            return new SQLiteConnection(path);
        }
    }
}
