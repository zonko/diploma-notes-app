﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XForms.Notes.UI.Extensions;

namespace XForms.Notes.UI.Controls
{
    public class ClickFrame : Frame
    {
        public ClickFrame() : base()
        {
            BackgroundColor = Color.Transparent;
        }

        public event Action Clicked;

        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ClickFrame), default(ICommand));

        public ICommand Command
        {
            get => GetValue(CommandProperty) as ICommand;
            set => SetValue(CommandProperty, value);
        }

        public bool AnimateContentOpacityOnClick { get; set; }

        public Color? ClickAnimationColor { get; set; }

        private Color _color;

        public Color Color
        {
            get => _color;
            set
            {
                _color = value;
                BackgroundColor = value;
            }
        }

        public void OnClick(object sender, EventArgs args)
        {
            RunClickAnimation();
            Clicked?.Invoke();
            Command?.Execute(EventArgs.Empty);
        }

        private void RunClickAnimation()
        {
            if (ClickAnimationColor != null)
            {
                Task.Run(async () =>
                {
                    await this.ColorTo(Color, ClickAnimationColor.Value, c => BackgroundColor = c, 120, Easing.CubicIn);
                    await this.ColorTo(ClickAnimationColor.Value, Color, c => BackgroundColor = c, 120, Easing.CubicInOut);
                });
            }
            if (AnimateContentOpacityOnClick && Content != null)
            {
                Task.Run(async () =>
                {
                    await Content.FadeTo(0.5, 120, Easing.CubicIn);
                    await Content.FadeTo(1, 120, Easing.CubicInOut);
                });
            }

        }


    }
}