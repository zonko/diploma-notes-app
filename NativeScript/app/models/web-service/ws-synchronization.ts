import { WsNote } from "~/models/web-service/ws-note";

export class WsSynchronization {
    lastSyncDateTime: string;
    notes: Array<WsNote>;
}