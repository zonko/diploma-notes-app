"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Notification = /** @class */ (function () {
    function Notification(user, noteId) {
        this.user = user;
        this.noteId = noteId;
    }
    return Notification;
}());
exports.Notification = Notification;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibm90aWZpY2F0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7SUFJSSxzQkFBYSxJQUFZLEVBQUUsTUFBYztRQUNyQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN6QixDQUFDO0lBQ0wsbUJBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQztBQVJZLG9DQUFZIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbiB7XHJcbiAgICB1c2VyOiBzdHJpbmc7XHJcbiAgICBub3RlSWQ6IG51bWJlcjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciAodXNlcjogc3RyaW5nLCBub3RlSWQ6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMudXNlciA9IHVzZXI7XHJcbiAgICAgICAgdGhpcy5ub3RlSWQgPSBub3RlSWQ7XHJcbiAgICB9XHJcbn0iXX0=