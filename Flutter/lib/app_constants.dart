// navigation routes
const String ROUTE__STARTUP = "/";
const String ROUTE__LOGIN = "/login";
const String ROUTE__REGISTRATION = "/registration";
const String ROUTE__NOTES = "/notes";
const String ROUTE__NOTE_EDIT = "/note";
const String ROUTE__SET_NOTIFICATION = "/notification";

// secure storage keys
const String SECURE_STORAGE_KEY__USER_TOKEN = "FNA_USER_TOKEN";
const String SECURE_STORAGE_KEY__USER = "FNA_USER";

// db key value Keys
const String DB_KEY__LAST_SYNC_DATE_TIME = "LAST_SYNC_DATE_TIME";

