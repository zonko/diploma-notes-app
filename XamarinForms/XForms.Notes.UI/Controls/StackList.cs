﻿using System.Collections;
using System.Collections.Generic;
using Xamarin.Forms;

namespace XForms.Notes.UI.Controls
{
    public class StackList : StackLayout
    {
        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create("ItemsSource", typeof(IEnumerable), typeof(StackList), default(IEnumerable), propertyChanged: OnItemsSourceChanged);

        public IEnumerable ItemsSource
        {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public static readonly BindableProperty ItemTemplateProperty =
            BindableProperty.Create("ItemTemplate", typeof(DataTemplate), typeof(StackList), default(DataTemplate));

        public DataTemplate ItemTemplate
        {
            get => (DataTemplate)GetValue(ItemTemplateProperty);
            set => SetValue(ItemTemplateProperty, value);
        }

        private static void OnItemsSourceChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            if(!(bindable is StackList stackList) || stackList.ItemTemplate == null) return;

            stackList.Children.Clear();

            foreach (var item in stackList.ItemsSource)
            {
                var viewCell = stackList.ItemTemplate.CreateContent() as ViewCell;
                if (viewCell == null) continue;
                viewCell.View.BindingContext = item;
                stackList.Children.Add(viewCell.View);
            }
        }
    }
}