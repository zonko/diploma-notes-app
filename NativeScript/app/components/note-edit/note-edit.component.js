"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var note_1 = require("~/models/core/note");
var router_1 = require("@angular/router");
var helpers_1 = require("~/helpers/helpers");
var note_attachment_1 = require("~/models/core/note-attachment");
var notes_service_1 = require("~/services/notes.service");
var dialog_service_1 = require("~/services/dialog.service");
var error_type_1 = require("~/models/core/error-type");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
var nativescript_angular_1 = require("nativescript-angular");
var set_notification_component_1 = require("~/components/set-notification/set-notification.component");
var notification_service_1 = require("~/services/notification.service");
//image picker
var imagepicker = require("nativescript-imagepicker");
var imageSource = require("image-source");
var NoteEditComponent = /** @class */ (function () {
    function NoteEditComponent(_notesService, _route, _router, _dialogService, _modalService, _vcRef, _notificationService) {
        this._notesService = _notesService;
        this._route = _route;
        this._router = _router;
        this._dialogService = _dialogService;
        this._modalService = _modalService;
        this._vcRef = _vcRef;
        this._notificationService = _notificationService;
        this.note = new note_1.Note();
    }
    NoteEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._drawer = this.drawerComponent.sideDrawer;
        var id = +this._route.snapshot.params["id"];
        if (id > 0) {
            this._dialogService.setBusy(true);
            this._notesService.getNote(id).then(function (res) {
                _this._dialogService.setBusy(false);
                if (res.success) {
                    _this.note = res.result;
                }
                else {
                    _this._dialogService.showMessageDialog("Napaka", helpers_1.Helpers.getErrorMessage(res.errorType));
                }
            }).catch(function (err) {
                _this._dialogService.setBusy(false);
                _this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
            });
        }
    };
    NoteEditComponent.prototype.onSetNotification = function () {
        var _this = this;
        var options = {
            context: {},
            fullscreen: true,
            viewContainerRef: this._vcRef
        };
        this._modalService.showModal(set_notification_component_1.SetNotificationComponent, options).then(function (res) {
            _this._notificationDateTime = res;
        });
    };
    NoteEditComponent.prototype.onSave = function () {
        var _this = this;
        if (!this.note.title || this.note.title === '') {
            this._dialogService.showMessageDialog("Opozorilo", "Naslov je obvezen podatek.");
            return;
        }
        this._dialogService.setBusy(true);
        this._notesService.saveNote(this.note).then(function (res) {
            _this._dialogService.setBusy(false);
            if (res.success) {
                _this.finishSaving(res.result);
            }
            else {
                if (res.errorType < error_type_1.ErrorType.dbUserNotLogedIn) {
                    _this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake med sinhronizacijo s centralo: "
                        + helpers_1.Helpers.getErrorMessage(res.errorType));
                    _this.finishSaving(res.result);
                }
                else {
                    _this._dialogService.showMessageDialog("Napaka", "Zapisek ni bil shranjen: " + helpers_1.Helpers.getErrorMessage(res.errorType));
                }
            }
        }).catch(function (err) {
            _this._dialogService.setBusy(false);
            _this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
        });
    };
    NoteEditComponent.prototype.finishSaving = function (noteId) {
        var _this = this;
        if (this._notificationDateTime) {
            this._notificationService.schedule(noteId, this.note.title, this.note.text, this._notificationDateTime).then(function (res) {
                if (!res.success) {
                    console.log(res);
                    _this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake, obvestilo ni bilo nastavljeno.");
                }
            }).catch(function (err) {
                _this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake, obvestilo ni bilo nastavljeno.");
            });
        }
        this._router.navigateByUrl("notes");
    };
    NoteEditComponent.prototype.onAddAttachment = function () {
        var _this = this;
        var context = imagepicker.create({
            mode: "single"
        });
        context.authorize()
            .then(function () {
            return context.present();
        }).then(function (selection) {
            selection.forEach(function (imageAsset) {
                imageSource.fromAsset(imageAsset).then(function (res) {
                    var base64 = res.toBase64String("jpeg", 100);
                    var attachment = new note_attachment_1.NoteAttachment();
                    attachment.content = base64;
                    attachment.type = "jpeg";
                    if (!_this.note.attachments)
                        _this.note.attachments = new Array();
                    _this.note.attachments.push(attachment);
                });
            });
        }).catch(function (err) {
            _this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
        });
    };
    NoteEditComponent.prototype.onRemoveAttachment = function (att) {
        var index = this.note.attachments.indexOf(att, 0);
        if (index > -1) {
            this.note.attachments.splice(index, 1);
        }
    };
    NoteEditComponent.prototype.onCloseDrawer = function () {
        this._drawer.closeDrawer();
    };
    __decorate([
        core_1.ViewChild(angular_1.RadSideDrawerComponent),
        __metadata("design:type", angular_1.RadSideDrawerComponent)
    ], NoteEditComponent.prototype, "drawerComponent", void 0);
    NoteEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-note-edit',
            templateUrl: './note-edit.component.html',
            styleUrls: ['./note-edit.component.css']
        }),
        __metadata("design:paramtypes", [notes_service_1.NotesService,
            router_1.ActivatedRoute,
            router_1.Router,
            dialog_service_1.DialogService,
            nativescript_angular_1.ModalDialogService,
            core_1.ViewContainerRef,
            notification_service_1.NotificationService])
    ], NoteEditComponent);
    return NoteEditComponent;
}());
exports.NoteEditComponent = NoteEditComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90ZS1lZGl0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5vdGUtZWRpdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBNkU7QUFDN0UsMkNBQTBDO0FBQzFDLDBDQUF5RDtBQUN6RCw2Q0FBNEM7QUFDNUMsaUVBQTZEO0FBRTdELDBEQUFzRDtBQUN0RCw0REFBd0Q7QUFDeEQsdURBQW1EO0FBRW5ELDhEQUEwRTtBQUMxRSw2REFBd0Q7QUFDeEQsdUdBQWtHO0FBQ2xHLHdFQUFvRTtBQUdwRSxjQUFjO0FBQ2QsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLDBCQUEwQixDQUFDLENBQUM7QUFDdEQsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBUTFDO0lBT0ksMkJBQW9CLGFBQTJCLEVBQzNCLE1BQXNCLEVBQ3RCLE9BQWUsRUFDZixjQUE2QixFQUM3QixhQUFpQyxFQUNqQyxNQUF3QixFQUN4QixvQkFBeUM7UUFOekMsa0JBQWEsR0FBYixhQUFhLENBQWM7UUFDM0IsV0FBTSxHQUFOLE1BQU0sQ0FBZ0I7UUFDdEIsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUNmLG1CQUFjLEdBQWQsY0FBYyxDQUFlO1FBQzdCLGtCQUFhLEdBQWIsYUFBYSxDQUFvQjtRQUNqQyxXQUFNLEdBQU4sTUFBTSxDQUFrQjtRQUN4Qix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXFCO1FBWjdELFNBQUksR0FBVSxJQUFJLFdBQUksRUFBRSxDQUFDO0lBWXdDLENBQUM7SUFFbEUsb0NBQVEsR0FBUjtRQUFBLGlCQWlCQztRQWhCRyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDO1FBQy9DLElBQU0sRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ1QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRztnQkFDbkMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ25DLEVBQUUsQ0FBQSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUNiLEtBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztnQkFDM0IsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDNUYsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0JBQ1IsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ25DLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLG9DQUFvQyxDQUFDLENBQUM7WUFDMUYsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO0lBQ0wsQ0FBQztJQUVELDZDQUFpQixHQUFqQjtRQUFBLGlCQVNDO1FBUkcsSUFBSSxPQUFPLEdBQUc7WUFDVixPQUFPLEVBQUUsRUFBRTtZQUNYLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ2hDLENBQUM7UUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxxREFBd0IsRUFBRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO1lBQ3BFLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLENBQUM7UUFDckMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsa0NBQU0sR0FBTjtRQUFBLGlCQXdCQztRQXZCRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztZQUNqRixNQUFNLENBQUM7UUFDWCxDQUFDO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDM0MsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbEMsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLEVBQUUsQ0FBQSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsc0JBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7b0JBQzVDLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUMxQyxxREFBcUQ7MEJBQ25ELGlCQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBRSxDQUFDO29CQUMvQyxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDbEMsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSwyQkFBMkIsR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDMUgsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO1lBQ1IsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsb0NBQW9DLENBQUMsQ0FBQztRQUMxRixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyx3Q0FBWSxHQUFwQixVQUFxQixNQUFjO1FBQW5DLGlCQWFDO1FBWkcsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO2dCQUM1RyxFQUFFLENBQUEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ2pCLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLHFEQUFxRCxDQUFDLENBQUM7Z0JBQzNHLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO2dCQUNSLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLHFEQUFxRCxDQUFDLENBQUM7WUFDM0csQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7SUFFeEMsQ0FBQztJQUVELDJDQUFlLEdBQWY7UUFBQSxpQkF3QkM7UUF2QkcsSUFBSSxPQUFPLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUM3QixJQUFJLEVBQUUsUUFBUTtTQUNqQixDQUFDLENBQUM7UUFFSCxPQUFPLENBQUMsU0FBUyxFQUFFO2FBQ2QsSUFBSSxDQUFDO1lBQ0YsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUM3QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxTQUFTO1lBQ2IsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFVBQXNCO2dCQUNyQyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7b0JBQ3RDLElBQU0sTUFBTSxHQUFHLEdBQUcsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUMvQyxJQUFNLFVBQVUsR0FBRyxJQUFJLGdDQUFjLEVBQUUsQ0FBQztvQkFDeEMsVUFBVSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7b0JBQzVCLFVBQVUsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO29CQUN6QixFQUFFLENBQUEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO3dCQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksS0FBSyxFQUFrQixDQUFDO29CQUUvRSxLQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzNDLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO1lBQ1osS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsb0NBQW9DLENBQUMsQ0FBQztRQUMxRixDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCw4Q0FBa0IsR0FBbEIsVUFBbUIsR0FBbUI7UUFDbEMsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwRCxFQUFFLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztRQUMzQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHlDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFySGtDO1FBQWxDLGdCQUFTLENBQUMsZ0NBQXNCLENBQUM7a0NBQXlCLGdDQUFzQjs4REFBQztJQUp6RSxpQkFBaUI7UUFON0IsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsZUFBZTtZQUN6QixXQUFXLEVBQUUsNEJBQTRCO1lBQ3pDLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO1NBQzNDLENBQUM7eUNBUXFDLDRCQUFZO1lBQ25CLHVCQUFjO1lBQ2IsZUFBTTtZQUNDLDhCQUFhO1lBQ2QseUNBQWtCO1lBQ3pCLHVCQUFnQjtZQUNGLDBDQUFtQjtPQWJwRCxpQkFBaUIsQ0EwSDdCO0lBQUQsd0JBQUM7Q0FBQSxBQTFIRCxJQTBIQztBQTFIWSw4Q0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWZ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOb3RlIH0gZnJvbSAnfi9tb2RlbHMvY29yZS9ub3RlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEhlbHBlcnMgfSBmcm9tICd+L2hlbHBlcnMvaGVscGVycyc7XHJcbmltcG9ydCB7Tm90ZUF0dGFjaG1lbnR9IGZyb20gXCJ+L21vZGVscy9jb3JlL25vdGUtYXR0YWNobWVudFwiO1xyXG5pbXBvcnQge0ltYWdlQXNzZXR9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2ltYWdlLWFzc2V0XCI7XHJcbmltcG9ydCB7Tm90ZXNTZXJ2aWNlfSBmcm9tIFwifi9zZXJ2aWNlcy9ub3Rlcy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7RGlhbG9nU2VydmljZX0gZnJvbSBcIn4vc2VydmljZXMvZGlhbG9nLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtFcnJvclR5cGV9IGZyb20gXCJ+L21vZGVscy9jb3JlL2Vycm9yLXR5cGVcIjtcclxuaW1wb3J0IHtSYWRTaWRlRHJhd2VyfSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXJcIjtcclxuaW1wb3J0IHtSYWRTaWRlRHJhd2VyQ29tcG9uZW50fSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXIvYW5ndWxhclwiO1xyXG5pbXBvcnQge01vZGFsRGlhbG9nU2VydmljZX0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyXCI7XHJcbmltcG9ydCB7U2V0Tm90aWZpY2F0aW9uQ29tcG9uZW50fSBmcm9tIFwifi9jb21wb25lbnRzL3NldC1ub3RpZmljYXRpb24vc2V0LW5vdGlmaWNhdGlvbi5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtOb3RpZmljYXRpb25TZXJ2aWNlfSBmcm9tIFwifi9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZVwiO1xyXG5cclxuXHJcbi8vaW1hZ2UgcGlja2VyXHJcbnZhciBpbWFnZXBpY2tlciA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtaW1hZ2VwaWNrZXJcIik7XHJcbnZhciBpbWFnZVNvdXJjZSA9IHJlcXVpcmUoXCJpbWFnZS1zb3VyY2VcIik7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ2FwcC1ub3RlLWVkaXQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL25vdGUtZWRpdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9ub3RlLWVkaXQuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb3RlRWRpdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBub3RlOiBOb3RlID0gIG5ldyBOb3RlKCk7XHJcbiAgICBwcml2YXRlIF9ub3RpZmljYXRpb25EYXRlVGltZTogRGF0ZTtcclxuXHJcbiAgICBAVmlld0NoaWxkKFJhZFNpZGVEcmF3ZXJDb21wb25lbnQpIHB1YmxpYyBkcmF3ZXJDb21wb25lbnQ6IFJhZFNpZGVEcmF3ZXJDb21wb25lbnQ7XHJcbiAgICBwcml2YXRlIF9kcmF3ZXI6IFJhZFNpZGVEcmF3ZXI7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfbm90ZXNTZXJ2aWNlOiBOb3Rlc1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9yb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2RpYWxvZ1NlcnZpY2U6IERpYWxvZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9tb2RhbFNlcnZpY2U6IE1vZGFsRGlhbG9nU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX3ZjUmVmOiBWaWV3Q29udGFpbmVyUmVmLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSkgeyB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5fZHJhd2VyID0gdGhpcy5kcmF3ZXJDb21wb25lbnQuc2lkZURyYXdlcjtcclxuICAgICAgICBjb25zdCBpZCA9ICt0aGlzLl9yb3V0ZS5zbmFwc2hvdC5wYXJhbXNbXCJpZFwiXTtcclxuICAgICAgICBpZiAoaWQgPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2V0QnVzeSh0cnVlKTtcclxuICAgICAgICAgICAgdGhpcy5fbm90ZXNTZXJ2aWNlLmdldE5vdGUoaWQpLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2V0QnVzeShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBpZihyZXMuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90ZSA9IHJlcy5yZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJOYXBha2FcIiwgSGVscGVycy5nZXRFcnJvck1lc3NhZ2UocmVzLmVycm9yVHlwZSkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zZXRCdXN5KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJOYXBha2FcIiwgXCJQcmnFoWxvIGplIGRvIG5lcHJpxI1ha292YW5lIG5hcGFrZS5cIik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvblNldE5vdGlmaWNhdGlvbigpIHtcclxuICAgICAgICBsZXQgb3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgY29udGV4dDoge30sXHJcbiAgICAgICAgICAgIGZ1bGxzY3JlZW46IHRydWUsXHJcbiAgICAgICAgICAgIHZpZXdDb250YWluZXJSZWY6IHRoaXMuX3ZjUmVmXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLl9tb2RhbFNlcnZpY2Uuc2hvd01vZGFsKFNldE5vdGlmaWNhdGlvbkNvbXBvbmVudCwgb3B0aW9ucykudGhlbihyZXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9ub3RpZmljYXRpb25EYXRlVGltZSA9IHJlcztcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvblNhdmUoKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLm5vdGUudGl0bGUgfHwgdGhpcy5ub3RlLnRpdGxlID09PSAnJykge1xyXG4gICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiT3Bvem9yaWxvXCIsIFwiTmFzbG92IGplIG9idmV6ZW4gcG9kYXRlay5cIik7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zZXRCdXN5KHRydWUpO1xyXG4gICAgICAgIHRoaXMuX25vdGVzU2VydmljZS5zYXZlTm90ZSh0aGlzLm5vdGUpLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zZXRCdXN5KGZhbHNlKTtcclxuICAgICAgICAgICAgaWYgKHJlcy5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpbmlzaFNhdmluZyhyZXMucmVzdWx0KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmKHJlcy5lcnJvclR5cGUgPCBFcnJvclR5cGUuZGJVc2VyTm90TG9nZWRJbikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJOYXBha2FcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJQcmnFoWxvIGplIGRvIG5hcGFrZSBtZWQgc2luaHJvbml6YWNpam8gcyBjZW50cmFsbzogXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgKyBIZWxwZXJzLmdldEVycm9yTWVzc2FnZShyZXMuZXJyb3JUeXBlKSApO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmluaXNoU2F2aW5nKHJlcy5yZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiTmFwYWthXCIsIFwiWmFwaXNlayBuaSBiaWwgc2hyYW5qZW46IFwiICsgSGVscGVycy5nZXRFcnJvck1lc3NhZ2UocmVzLmVycm9yVHlwZSkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zZXRCdXN5KGZhbHNlKTtcclxuICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zaG93TWVzc2FnZURpYWxvZyhcIk5hcGFrYVwiLCBcIlByacWhbG8gamUgZG8gbmVwcmnEjWFrb3ZhbmUgbmFwYWtlLlwiKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGZpbmlzaFNhdmluZyhub3RlSWQ6IG51bWJlcikge1xyXG4gICAgICAgIGlmKHRoaXMuX25vdGlmaWNhdGlvbkRhdGVUaW1lKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX25vdGlmaWNhdGlvblNlcnZpY2Uuc2NoZWR1bGUobm90ZUlkLCB0aGlzLm5vdGUudGl0bGUsIHRoaXMubm90ZS50ZXh0LCB0aGlzLl9ub3RpZmljYXRpb25EYXRlVGltZSkudGhlbihyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYoIXJlcy5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiTmFwYWthXCIsIFwiUHJpxaFsbyBqZSBkbyBuYXBha2UsIG9idmVzdGlsbyBuaSBiaWxvIG5hc3RhdmxqZW5vLlwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJOYXBha2FcIiwgXCJQcmnFoWxvIGplIGRvIG5hcGFrZSwgb2J2ZXN0aWxvIG5pIGJpbG8gbmFzdGF2bGplbm8uXCIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlQnlVcmwoXCJub3Rlc1wiKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgb25BZGRBdHRhY2htZW50KCkge1xyXG4gICAgICAgIHZhciBjb250ZXh0ID0gaW1hZ2VwaWNrZXIuY3JlYXRlKHtcclxuICAgICAgICAgICAgbW9kZTogXCJzaW5nbGVcIlxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb250ZXh0LmF1dGhvcml6ZSgpXHJcbiAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGNvbnRleHQucHJlc2VudCgpO1xyXG4gICAgICAgICAgICB9KS50aGVuKHNlbGVjdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb24uZm9yRWFjaCgoaW1hZ2VBc3NldDogSW1hZ2VBc3NldCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGltYWdlU291cmNlLmZyb21Bc3NldChpbWFnZUFzc2V0KS50aGVuKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGJhc2U2NCA9IHJlcy50b0Jhc2U2NFN0cmluZyhcImpwZWdcIiwgMTAwKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXR0YWNobWVudCA9IG5ldyBOb3RlQXR0YWNobWVudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdHRhY2htZW50LmNvbnRlbnQgPSBiYXNlNjQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF0dGFjaG1lbnQudHlwZSA9IFwianBlZ1wiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZighdGhpcy5ub3RlLmF0dGFjaG1lbnRzKSB0aGlzLm5vdGUuYXR0YWNobWVudHMgPSBuZXcgQXJyYXk8Tm90ZUF0dGFjaG1lbnQ+KCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGUuYXR0YWNobWVudHMucHVzaChhdHRhY2htZW50KTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KS5jYXRjaChlcnIgPT4gIHtcclxuICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zaG93TWVzc2FnZURpYWxvZyhcIk5hcGFrYVwiLCBcIlByacWhbG8gamUgZG8gbmVwcmnEjWFrb3ZhbmUgbmFwYWtlLlwiKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgb25SZW1vdmVBdHRhY2htZW50KGF0dDogTm90ZUF0dGFjaG1lbnQpIHtcclxuICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMubm90ZS5hdHRhY2htZW50cy5pbmRleE9mKGF0dCwgMCk7XHJcbiAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcclxuICAgICAgICAgICAgdGhpcy5ub3RlLmF0dGFjaG1lbnRzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2xvc2VEcmF3ZXIoKSB7XHJcbiAgICAgICAgdGhpcy5fZHJhd2VyLmNsb3NlRHJhd2VyKCk7XHJcbiAgICB9XHJcbn1cclxuIl19