import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ft_notes_app/app_constants.dart';
import 'package:ft_notes_app/screens/activity_indicator.dart';
import 'package:ft_notes_app/screens/login/login_screen_presenter.dart';
import 'package:ft_notes_app/services/dialog_service.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen>
    implements LoginScreenContract {
  LoginScreenPresenter _presenter;
  final DialogService _dialogService = new DialogService();
  final _formKey = new GlobalKey<FormState>();

  String _username;
  String _password;
  bool _isLoading = false;
  
  LoginScreenState() {
    _presenter =  new LoginScreenPresenter(this);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: null,
      body: Stack(children: <Widget>[
        Column(
          children: <Widget>[
            Expanded(
                child: ListView(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    FlatButton(
                      color: Colors.transparent,
                      textColor: Colors.orangeAccent,
                      child: Text(
                        'SLO',
                        style: TextStyle(fontSize: 16.0),
                      ),
                      onPressed: () => { }
                    ),
                    FlatButton(
                      color: Colors.transparent,
                      textColor: Colors.orangeAccent,
                      child: Text(
                        'REGISTRIRAJ SE',
                        style: TextStyle(fontSize: 16.0),
                      ),
                      onPressed: () => Navigator.pushNamed(context, ROUTE__REGISTRATION)
                    )
                  ]),
                SizedBox(height: 30.0),
                Image.asset('assets/notes_logo.png', height: 100.0),
                SizedBox(height: 20.0),
                Center(
                    child: Text('Prijava v aplikacijo',
                        style: TextStyle(
                            fontSize: 22.0, color: Colors.orangeAccent))),
                SizedBox(height: 10.0),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: TextFormField(
                            onSaved: (val) => _username = val,
                            decoration: InputDecoration(
                                hintText: 'Uporabniško ime',
                                labelText: 'Uporabniško ime:'),
                          )),
                      Container(
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: TextFormField(
                            onSaved: (val) => _password = val,
                            obscureText: true,
                            decoration: InputDecoration(
                                hintText: 'Geslo', labelText: 'Geslo:'),
                          )),
                    ],
                  ),
                ),
                SizedBox(height: 20.0),
              ],
            )),
            Container(
              height: 50.0,
              width: double.infinity,
              child: FlatButton(
                child: Text('PRIJAVA'),
                onPressed: _submit,
                color: Colors.orange,
              ),
            )
          ],
        ),
        Align(child: ActivityIndicator(isLoading: _isLoading), alignment: FractionalOffset.center)
      ]),
    );
  }

  void _submit() {
    final form = _formKey.currentState;
    form.save();
    if (_username.isEmpty || _password.isEmpty) {
      _dialogService.showAlert('Prijava', 'Vnesite uporabniško ime in geslo.', context);
      return;
    }
    setState(() => _isLoading = true);
    _presenter.doLogin(_username, _password);
  }

  @override
  Future<Null> onError(String errorTxt) async{
    setState(() => _isLoading = false);
    await _dialogService.showAlert('Napaka', errorTxt, context);
  }

  @override
  void onLoginSuccess() {
    setState(() => _isLoading = false);
    Navigator.pushReplacementNamed(context, ROUTE__NOTES);
  }

}
