import 'package:flutter/material.dart';
import 'package:ft_notes_app/app_constants.dart';
import 'package:ft_notes_app/screens/login/login_screen.dart';
import 'package:ft_notes_app/screens/notes/notes_screen.dart';
import 'package:ft_notes_app/screens/registration/registration_screen.dart';
import 'package:ft_notes_app/screens/set_notification_screen.dart';
import 'package:ft_notes_app/screens/startup_screen.dart';

final routes = {
  ROUTE__STARTUP:           (BuildContext context) => new StartupScreen(),
  ROUTE__LOGIN:             (BuildContext context) => new LoginScreen(),
  ROUTE__REGISTRATION:      (BuildContext context) => new RegistrationScreen(),
  ROUTE__NOTES:             (BuildContext context) => new NotesScreen(),
  ROUTE__SET_NOTIFICATION:  (BuildContext context) => new SetNotificationScreen(),
  //ROUTE__NOTE_EDIT:     (BuildContext context, int noteId) => new NoteEditScreen(noteId),
};