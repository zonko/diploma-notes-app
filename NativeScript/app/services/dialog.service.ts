import { Injectable } from "@angular/core";
import {Subject} from "rxjs/internal/Subject";
import {DialogOptions} from "~/models/core/dialog-options";

@Injectable()
export class DialogService {
    private _showMessageDialogSource = new Subject<DialogOptions>();
    private _setBusyIndicatorSource = new Subject<boolean>();

    public showMessageDialog$ = this._showMessageDialogSource.asObservable();
    public setBusyIndicator$ = this._setBusyIndicatorSource.asObservable();

    public setBusy(busy: boolean) {
        this._setBusyIndicatorSource.next(busy);
    }

    public showMessageDialog(title: string, message: string, buttonText: string = "OK"): void {
        var options = new DialogOptions(title, message, buttonText);
        this._showMessageDialogSource.next(options);
    }
}