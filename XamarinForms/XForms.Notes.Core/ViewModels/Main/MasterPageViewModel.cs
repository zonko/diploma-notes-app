﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using XForms.Notes.Core.Helpers;
using XForms.Notes.Core.Logic;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.ViewModels.Main.Detail;

namespace XForms.Notes.Core.ViewModels.Main
{
    public class MasterPageViewModel : MvxViewModel
    {
        private readonly ISecureStorageService _secureStorageService;
        private readonly IDialogService _dialogService;
        private readonly UserLogic _userLogic;
        private readonly NotesLogic _notesLogic;

        private MvxAsyncCommand _addNewNoteCommand;
        private MvxAsyncCommand _synchronizeCommand;
        private MvxAsyncCommand _logoutCommand;


        public MasterPageViewModel(ISecureStorageService secureStorageService,
            IDialogService dialogService, UserLogic userLogic, NotesLogic notesLogic)
        {
            _secureStorageService = secureStorageService;
            _dialogService = dialogService;
            _userLogic = userLogic;
            _notesLogic = notesLogic;
        }

        public string User { get; set; }

        public MvxAsyncCommand AddNewNoteCommand => _addNewNoteCommand ?? (_addNewNoteCommand = new MvxAsyncCommand(AddNewNote));

        public MvxAsyncCommand SynchronizeCommand => _synchronizeCommand ?? (_synchronizeCommand = new MvxAsyncCommand(Synchronize));

        public MvxAsyncCommand LogoutCommand => _logoutCommand ?? (_logoutCommand = new MvxAsyncCommand(Logout));


        public override async Task Initialize()
        {
            await base.Initialize();
            User = await _secureStorageService.GetUser();
            RaisePropertyChanged(nameof(User));
        }

        private async Task AddNewNote()
        {
            await NavigationService.Navigate<NoteEditPageViewModel, int?>((int?) null);
        }

        private async Task Synchronize()
        {
            _dialogService.SetBusy(true);
            try
            {
                await _notesLogic.Synchronize();
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", "Sinhronizacija ni uspela. " + ExceptionHelper.GetErrorMessage(exception));
            }
            _dialogService.SetBusy(false);
        }

        private async Task Logout()
        {
            _dialogService.SetBusy(true);
            try
            {
                _userLogic.Logout();
                await NavigationService.Navigate<LoginPageViewModel>();
            }
            catch (Exception exception)
            {
                _dialogService.ShowAlert("Napaka", ExceptionHelper.GetErrorMessage(exception));
            }
            _dialogService.SetBusy(false);
        }
    }
}