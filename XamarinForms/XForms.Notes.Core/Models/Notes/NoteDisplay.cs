﻿using System;

namespace XForms.Notes.Core.Models.Notes
{
    public class NoteDisplay
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateTimeChanged { get; set; }
    }
}