﻿using SQLite;

namespace XForms.Notes.Core.Database.Models
{
    internal class DbtKeyValue
    {
        [PrimaryKey]
        public int Key { get; set; }
        public string Value { get; set; }
    }
}