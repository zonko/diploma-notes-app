import 'package:ft_notes_app/models/error_type.dart';

class ErrorMessageHelper {
  static String getErrorMessage(dynamic exception) {

    int errorType = int.tryParse(exception.toString().replaceAll("Exception: ", ""));
    if(errorType == null) return 'Prišlo je do nepričakovane napake';
    
    switch(errorType){
      case ErrorType.wsOk: return '';
      case ErrorType.wsAuthorizationError: return 'Avtentikacija s spletnim srežnikom ni uspela.';
      case ErrorType.wsBadRequest: return 'Neznana zaheteva.';
      case ErrorType.wsInternalServerError: return 'Notranja napaka na strežniku.';
      case ErrorType.wsUsernameIsTaken: return 'Uporabniško ime je zasedeno.';
      case ErrorType.wsWrongUsernameOrPassword: return 'Napačno uporabniško ime ali geslo.';
      case ErrorType.wsServerNotReachable: return 'Strežnik ni dosegljiv.';
      case ErrorType.dbCreateTablesError: return 'Napaka ob ustvarjanju tabel podatkovne baze.';
      case ErrorType.dbNotInitialized: return 'Podatkovna baza ni inicializirana.';
      case ErrorType.dbOpenDbError: return 'Na podatkovno bazo se ni bilo mogoče povezati.';
      case ErrorType.dbUserNotLogedIn: return 'Uporabnik ni prijavljen. Podatkovne baze ni mogoče inicializirati.';
      case ErrorType.dbWriteError: return 'Napaka ob pisanju v podatkovno bazo.';
      case ErrorType.dbReadError: return 'Napaka ob branju iz podatkovne baze.';
      //case ErrorType.notificationSchedulingError: return 'Prišlo je do napake ob nastavljanju obvestila.';
      default: return "Prišlo je do nepričakovane napake";
    }
  }
}