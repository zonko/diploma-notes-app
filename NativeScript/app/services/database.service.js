"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var secure_storage_service_1 = require("~/services/secure-storage.service");
var error_type_1 = require("~/models/core/error-type");
var operation_result_1 = require("~/models/core/operation-result");
var note_1 = require("~/models/core/note");
var note_display_1 = require("~/models/core/note-display");
var note_attachment_1 = require("~/models/core/note-attachment");
var notification_1 = require("~/models/core/notification");
var Sqlite = require("nativescript-sqlite");
var DatabaseService = /** @class */ (function () {
    function DatabaseService(_secureStorageService) {
        this._secureStorageService = _secureStorageService;
    }
    DatabaseService.prototype.initializeNotificationsDatabase = function () {
        return __awaiter(this, void 0, void 0, function () {
            var db, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        console.log("notification db init");
                        return [4 /*yield*/, new Sqlite('NotesNotifications.db').catch(function (err) {
                                throw (error_type_1.ErrorType.dbOpenDbError);
                            })];
                    case 1:
                        db = _a.sent();
                        return [4 /*yield*/, db.execSQL("CREATE TABLE IF NOT EXISTS Notification (\n                Id INTEGER PRIMARY KEY AUTOINCREMENT,\n                User TEXT NOT NULL,\n                NoteId INTEGER NOT NULL\n            );").catch(function (err) {
                                console.log("Obvestilo napaka:");
                                console.log(err);
                                throw (error_type_1.ErrorType.dbCreateTablesError);
                            })];
                    case 2:
                        _a.sent();
                        this._notificationDatabase = db;
                        this._notificationDatabase.resultType(Sqlite.RESULTSASOBJECT);
                        return [2 /*return*/, new operation_result_1.OperationResult(true)];
                    case 3:
                        error_1 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_1)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService.prototype.initializeDatabase = function () {
        return __awaiter(this, void 0, void 0, function () {
            var user, db, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        user = this._secureStorageService.getUser();
                        if (!user) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbUserNotLogedIn)];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        return [4 /*yield*/, new Sqlite(user + '.db').catch(function (err) {
                                throw (error_type_1.ErrorType.dbOpenDbError);
                            })];
                    case 2:
                        db = _a.sent();
                        return [4 /*yield*/, db.execSQL("CREATE TABLE IF NOT EXISTS Note (\n                Id INTEGER PRIMARY KEY AUTOINCREMENT,\n                Cid\tINTEGER,\n                Title TEXT,\n                Text TEXT,\n                DateTimeChanged TEXT NOT NULL,\n                Modified INTEGER NOT NULL,\n                Active INTEGER NOT NULL\n            );").catch(function (err) {
                                throw (error_type_1.ErrorType.dbCreateTablesError);
                            })];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, db.execSQL("CREATE TABLE IF NOT EXISTS Attachment (\n                Id INTEGER PRIMARY KEY AUTOINCREMENT,\n                NoteId INTEGER NOT NULL,\n                Name TEXT,\n                Type TEXT,\n                Content\tTEXT,\n                FOREIGN KEY(NoteId) REFERENCES Note(Id)\n            );").catch(function (err) {
                                throw (error_type_1.ErrorType.dbCreateTablesError);
                            })];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, db.execSQL("CREATE TABLE IF NOT EXISTS KeyValue (\n                Key TEXT,\n                Value TEXT,\n                PRIMARY KEY(Key)\n            );").catch(function (err) {
                                throw (error_type_1.ErrorType.dbCreateTablesError);
                            })];
                    case 5:
                        _a.sent();
                        this._database = db;
                        this._database.resultType(Sqlite.RESULTSASOBJECT);
                        return [2 /*return*/, new operation_result_1.OperationResult(true)];
                    case 6:
                        error_2 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_2)];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService.prototype.saveNotes = function (notes) {
        return __awaiter(this, void 0, void 0, function () {
            var _i, notes_1, note, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._database) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        if (!notes) {
                            return [2 /*return*/, new operation_result_1.OperationResult(true)];
                        }
                        _i = 0, notes_1 = notes;
                        _a.label = 1;
                    case 1:
                        if (!(_i < notes_1.length)) return [3 /*break*/, 4];
                        note = notes_1[_i];
                        return [4 /*yield*/, this.saveNote(note, true)];
                    case 2:
                        result = _a.sent();
                        if (!result.success) {
                            return [2 /*return*/, new operation_result_1.OperationResult(result.success, null, result.errorType, result.errorMessage)];
                        }
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, new operation_result_1.OperationResult(true)];
                }
            });
        });
    };
    DatabaseService.prototype.saveNote = function (note, deleteIfNotActive) {
        if (deleteIfNotActive === void 0) { deleteIfNotActive = false; }
        return __awaiter(this, void 0, void 0, function () {
            var dbNote, noteId, _i, _a, attachment, e_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 18, , 19]);
                        if (!this._database) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        if (!note) {
                            return [2 /*return*/, new operation_result_1.OperationResult(true)];
                        }
                        dbNote = null;
                        if (!note.id) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._database.get('SELECT * FROM Note WHERE Id = ?', [note.id]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 1:
                        dbNote = _b.sent();
                        return [3 /*break*/, 4];
                    case 2:
                        if (!note.cid) return [3 /*break*/, 4];
                        return [4 /*yield*/, this._database.get('SELECT * FROM Note WHERE Cid = ?', [note.cid]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 3:
                        dbNote = _b.sent();
                        _b.label = 4;
                    case 4:
                        noteId = null;
                        if (!dbNote) return [3 /*break*/, 10];
                        noteId = dbNote.Id;
                        if (!(deleteIfNotActive && !note.active)) return [3 /*break*/, 7];
                        return [4 /*yield*/, this._database.execSQL("DELETE FROM Attachment WHERE NoteId = ?", [noteId]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbWriteError);
                            })];
                    case 5:
                        _b.sent();
                        return [4 /*yield*/, this._database.execSQL("DELETE FROM Note WHERE Id = ?", [noteId]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbWriteError);
                            })];
                    case 6:
                        _b.sent();
                        noteId = null;
                        return [3 /*break*/, 9];
                    case 7: return [4 /*yield*/, this._database.execSQL("UPDATE Note SET\n                                            Cid\t= ?,\n                                            Title = ?,\n                                            Text = ?,\n                                            DateTimeChanged\t= ?,\n                                            Modified = ?,\n                                            Active = ?\n                                          WHERE Id = ?", [note.cid, note.title, note.text, note.dateTimeChanged, note.modified, note.active, noteId]).catch(function (err) {
                            throw (error_type_1.ErrorType.dbWriteError);
                        })];
                    case 8:
                        _b.sent();
                        _b.label = 9;
                    case 9: return [3 /*break*/, 12];
                    case 10:
                        if (!note.active)
                            return [2 /*return*/, new operation_result_1.OperationResult(true)]; // do not insert if not active
                        return [4 /*yield*/, this._database.execSQL("INSERT INTO Note (Cid, Title, Text, DateTimeChanged, Modified, Active)\n                                            VALUES (?, ?, ?, ?, ?, ?)", [note.cid, note.title, note.text, note.dateTimeChanged, note.modified, note.active]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbWriteError);
                            })];
                    case 11:
                        noteId = _b.sent();
                        _b.label = 12;
                    case 12:
                        if (!noteId) return [3 /*break*/, 17];
                        return [4 /*yield*/, this._database.execSQL('DELETE FROM Attachment WHERE NoteId = ?', [noteId]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbWriteError);
                            })];
                    case 13:
                        _b.sent();
                        if (!note.attachments) return [3 /*break*/, 17];
                        _i = 0, _a = note.attachments;
                        _b.label = 14;
                    case 14:
                        if (!(_i < _a.length)) return [3 /*break*/, 17];
                        attachment = _a[_i];
                        return [4 /*yield*/, this._database.execSQL("INSERT INTO Attachment (NoteId, Type, Content) VALUES (?, ?, ?)", [noteId, attachment.type, attachment.content])
                                .catch(function (err) {
                                throw (error_type_1.ErrorType.dbWriteError);
                            })];
                    case 15:
                        _b.sent();
                        _b.label = 16;
                    case 16:
                        _i++;
                        return [3 /*break*/, 14];
                    case 17: return [2 /*return*/, new operation_result_1.OperationResult(true, noteId)];
                    case 18:
                        e_1 = _b.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_1)];
                    case 19: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService.prototype.deactivateNote = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (!this._database) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        return [4 /*yield*/, this._database.execSQL("UPDATE Note SET Active = 'false', Modified = 'true' WHERE Id = ?", [id]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbWriteError);
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_2)];
                    case 3: return [2 /*return*/, new operation_result_1.OperationResult(true)];
                }
            });
        });
    };
    DatabaseService.prototype.listNotes = function (searchText) {
        if (searchText === void 0) { searchText = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var rows, results, _i, rows_1, row, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (!this._database) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        return [4 /*yield*/, this._database.all("SELECT Id, Title, DateTimeChanged FROM Note WHERE Active = 'true' AND Title LIKE '%" + searchText + "%' ORDER BY DateTimeChanged DESC").catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 1:
                        rows = _a.sent();
                        results = new Array();
                        if (rows) {
                            for (_i = 0, rows_1 = rows; _i < rows_1.length; _i++) {
                                row = rows_1[_i];
                                results.push(new note_display_1.NoteDisplay(row.Id, row.Title, row.DateTimeChanged));
                            }
                            return [2 /*return*/, new operation_result_1.OperationResult(true, results)];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_3)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService.prototype.listModifiedNotes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var rows, results, _i, rows_2, row, note, attachmentRows, attachments, _a, attachmentRows_1, att, attachment, e_4;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 6, , 7]);
                        if (!this._database) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        return [4 /*yield*/, this._database.all("SELECT * FROM Note WHERE Modified = 'true'").catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 1:
                        rows = _b.sent();
                        results = new Array();
                        if (!rows) return [3 /*break*/, 5];
                        _i = 0, rows_2 = rows;
                        _b.label = 2;
                    case 2:
                        if (!(_i < rows_2.length)) return [3 /*break*/, 5];
                        row = rows_2[_i];
                        note = new note_1.Note();
                        note.active = row.Active;
                        note.cid = row.Cid;
                        note.dateTimeChanged = row.DateTimeChanged;
                        note.id = row.Id;
                        note.modified = row.Modified;
                        note.text = row.Text;
                        note.title = row.Title;
                        return [4 /*yield*/, this._database.all("SELECT * FROM Attachment WHERE NoteId = ?", [note.id]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 3:
                        attachmentRows = _b.sent();
                        attachments = new Array();
                        if (attachmentRows) {
                            for (_a = 0, attachmentRows_1 = attachmentRows; _a < attachmentRows_1.length; _a++) {
                                att = attachmentRows_1[_a];
                                attachment = new note_attachment_1.NoteAttachment();
                                attachment.content = att.Content;
                                attachment.type = att.Type;
                                attachment.name = att.Name;
                                attachments.push(attachment);
                            }
                        }
                        note.attachments = attachments;
                        results.push(note);
                        _b.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5: return [2 /*return*/, new operation_result_1.OperationResult(true, results)];
                    case 6:
                        e_4 = _b.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_4)];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService.prototype.getNote = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var dbNote, dbAttachments, note, _i, dbAttachments_1, dbAttachment, attachment, e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        if (!this._database) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        if (!id) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false)];
                        }
                        return [4 /*yield*/, this._database.get('SELECT * FROM Note WHERE Id = ?', [id]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 1:
                        dbNote = _a.sent();
                        return [4 /*yield*/, this._database.all('SELECT * FROM Attachment WHERE NoteId = ?', [id]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 2:
                        dbAttachments = _a.sent();
                        if (!dbNote)
                            return [2 /*return*/, new operation_result_1.OperationResult(false)];
                        note = new note_1.Note();
                        note.id = dbNote.Id;
                        note.cid = dbNote.Cid;
                        note.active = dbNote.Active;
                        note.dateTimeChanged = dbNote.DateTimeChanged;
                        note.title = dbNote.Title;
                        note.text = dbNote.Text;
                        note.attachments = new Array();
                        if (dbAttachments) {
                            for (_i = 0, dbAttachments_1 = dbAttachments; _i < dbAttachments_1.length; _i++) {
                                dbAttachment = dbAttachments_1[_i];
                                attachment = new note_attachment_1.NoteAttachment();
                                attachment.content = dbAttachment.Content;
                                attachment.type = dbAttachment.Type;
                                note.attachments.push(attachment);
                            }
                        }
                        return [2 /*return*/, new operation_result_1.OperationResult(true, note)];
                    case 3:
                        e_5 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_5)];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService.prototype.getValue = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var dbValue, value, e_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (!this._database) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        return [4 /*yield*/, this._database.get('SELECT * FROM KeyValue WHERE Key = ?', [key]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 1:
                        dbValue = _a.sent();
                        value = dbValue ? dbValue.Value : null;
                        return [2 /*return*/, new operation_result_1.OperationResult(true, value)];
                    case 2:
                        e_6 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_6)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService.prototype.setValue = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var dbValue, e_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 6, , 7]);
                        if (!this._database) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        return [4 /*yield*/, this._database.get('SELECT * FROM KeyValue WHERE Key = ?', [key]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 1:
                        dbValue = _a.sent();
                        if (!dbValue) return [3 /*break*/, 3];
                        return [4 /*yield*/, this._database.execSQL("UPDATE KeyValue SET Value = ? WHERE Key = ?", [value, key]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbWriteError);
                            })];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this._database.execSQL("INSERT INTO KeyValue (Key, Value) VALUES (?, ?)", [key, value]).catch(function (err) {
                            throw (error_type_1.ErrorType.dbWriteError);
                        })];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5: return [2 /*return*/, new operation_result_1.OperationResult(true)];
                    case 6:
                        e_7 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_7)];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService.prototype.insertNewNotification = function (notification) {
        return __awaiter(this, void 0, void 0, function () {
            var id, e_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (!this._notificationDatabase) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        return [4 /*yield*/, this._notificationDatabase.execSQL('INSERT INTO Notification (User, NoteId) VALUES (?, ?)', [notification.user, notification.noteId]).catch(function (err) {
                                console.log("Obvestilo napaka:");
                                console.log(err);
                                throw (error_type_1.ErrorType.dbWriteError);
                            })];
                    case 1:
                        id = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(true, id)];
                    case 2:
                        e_8 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_8)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService.prototype.getNotification = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var dbValue, notification, e_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (!this._notificationDatabase) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbNotInitialized)];
                        }
                        return [4 /*yield*/, this._notificationDatabase.get('SELECT * FROM Notification WHERE Id = ?', [id]).catch(function (err) {
                                throw (error_type_1.ErrorType.dbReadError);
                            })];
                    case 1:
                        dbValue = _a.sent();
                        if (!dbValue) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.dbReadError)];
                        }
                        notification = new notification_1.Notification(dbValue.User, dbValue.NoteId);
                        return [2 /*return*/, new operation_result_1.OperationResult(true, notification)];
                    case 2:
                        e_9 = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(false, null, e_9)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DatabaseService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [secure_storage_service_1.SecureStorageService])
    ], DatabaseService);
    return DatabaseService;
}());
exports.DatabaseService = DatabaseService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YWJhc2Uuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRhdGFiYXNlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHNDQUF5QztBQUN6Qyw0RUFBdUU7QUFDdkUsdURBQW1EO0FBQ25ELG1FQUErRDtBQUMvRCwyQ0FBd0M7QUFDeEMsMkRBQXVEO0FBQ3ZELGlFQUE2RDtBQUM3RCwyREFBd0Q7QUFFeEQsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLHFCQUFxQixDQUFDLENBQUM7QUFHNUM7SUFJSSx5QkFBb0IscUJBQTJDO1FBQTNDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBc0I7SUFDL0QsQ0FBQztJQUVZLHlEQUErQixHQUE1Qzs7Ozs7Ozt3QkFFUSxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUM7d0JBQ3pCLHFCQUFNLElBQUksTUFBTSxDQUFDLHVCQUF1QixDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQ0FDMUQsTUFBTSxDQUFDLHNCQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7NEJBQ3BDLENBQUMsQ0FBQyxFQUFBOzt3QkFGSSxFQUFFLEdBQUcsU0FFVDt3QkFDRixxQkFBTSxFQUFFLENBQUMsT0FBTyxDQUFDLGdNQUlkLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO2dDQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQ0FDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQ0FDakIsTUFBTSxDQUFDLHNCQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQzs0QkFDMUMsQ0FBQyxDQUFDLEVBQUE7O3dCQVJGLFNBUUUsQ0FBQzt3QkFFSCxJQUFJLENBQUMscUJBQXFCLEdBQUcsRUFBRSxDQUFDO3dCQUNoQyxJQUFJLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQzt3QkFDOUQsc0JBQU8sSUFBSSxrQ0FBZSxDQUFPLElBQUksQ0FBQyxFQUFDOzs7d0JBR3ZDLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQUssQ0FBQyxFQUFDOzs7OztLQUV0RDtJQUVZLDRDQUFrQixHQUEvQjs7Ozs7O3dCQUNVLElBQUksR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ2xELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs0QkFDUixNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLHNCQUFTLENBQUMsZ0JBQWdCLENBQUMsRUFBQTt3QkFDdkUsQ0FBQzs7Ozt3QkFJYyxxQkFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQ0FDL0MsTUFBTSxDQUFDLHNCQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7NEJBQ3BDLENBQUMsQ0FBQyxFQUFBOzt3QkFGSSxFQUFFLEdBQUcsU0FFVDt3QkFFRixxQkFBTSxFQUFFLENBQUMsT0FBTyxDQUFDLHVVQVFkLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO2dDQUNWLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLG1CQUFtQixDQUFDLENBQUM7NEJBQzFDLENBQUMsQ0FBQyxFQUFBOzt3QkFWRixTQVVFLENBQUM7d0JBRUgscUJBQU0sRUFBRSxDQUFDLE9BQU8sQ0FBQywyU0FPZCxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQ0FDVixNQUFNLENBQUMsc0JBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDOzRCQUMxQyxDQUFDLENBQUMsRUFBQTs7d0JBVEYsU0FTRSxDQUFDO3dCQUVILHFCQUFNLEVBQUUsQ0FBQyxPQUFPLENBQUMsaUpBSWQsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQ1YsTUFBTSxDQUFDLHNCQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQzs0QkFDMUMsQ0FBQyxDQUFDLEVBQUE7O3dCQU5GLFNBTUUsQ0FBQzt3QkFHSCxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQzt3QkFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3dCQUNsRCxzQkFBTyxJQUFJLGtDQUFlLENBQU8sSUFBSSxDQUFDLEVBQUM7Ozt3QkFHdkMsc0JBQU8sSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBSyxDQUFDLEVBQUM7Ozs7O0tBRXREO0lBRVksbUNBQVMsR0FBdEIsVUFBdUIsS0FBa0I7Ozs7Ozt3QkFDckMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzs0QkFDbEIsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxzQkFBUyxDQUFDLGdCQUFnQixDQUFDLEVBQUE7d0JBQ3ZFLENBQUM7d0JBQ0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzRCQUNULE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFDLElBQUksQ0FBQyxFQUFDO3dCQUNyQyxDQUFDOzhCQUVxQixFQUFMLGVBQUs7Ozs2QkFBTCxDQUFBLG1CQUFLLENBQUE7d0JBQWIsSUFBSTt3QkFDSSxxQkFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBQTs7d0JBQXhDLE1BQU0sR0FBRyxTQUErQjt3QkFDNUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDbEIsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQUM7d0JBQzVGLENBQUM7Ozt3QkFKWSxJQUFLLENBQUE7OzRCQU90QixzQkFBTyxJQUFJLGtDQUFlLENBQUMsSUFBSSxDQUFDLEVBQUM7Ozs7S0FDcEM7SUFFWSxrQ0FBUSxHQUFyQixVQUFzQixJQUFVLEVBQUUsaUJBQWtDO1FBQWxDLGtDQUFBLEVBQUEseUJBQWtDOzs7Ozs7O3dCQUU1RCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDOzRCQUNsQixNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLHNCQUFTLENBQUMsZ0JBQWdCLENBQUMsRUFBQTt3QkFDdkUsQ0FBQzt3QkFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7NEJBQ1IsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsSUFBSSxDQUFDLEVBQUM7d0JBQ3JDLENBQUM7d0JBRUcsTUFBTSxHQUFHLElBQUksQ0FBQzs2QkFDZCxJQUFJLENBQUMsRUFBRSxFQUFQLHdCQUFPO3dCQUNFLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQ0FDckYsTUFBTSxDQUFDLHNCQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQ2xDLENBQUMsQ0FBQyxFQUFBOzt3QkFGRixNQUFNLEdBQUcsU0FFUCxDQUFDOzs7NkJBQ0ksSUFBSSxDQUFDLEdBQUcsRUFBUix3QkFBUTt3QkFDTixxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQ3ZGLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzRCQUNsQyxDQUFDLENBQUMsRUFBQTs7d0JBRkYsTUFBTSxHQUFHLFNBRVAsQ0FBQzs7O3dCQUlILE1BQU0sR0FBRyxJQUFJLENBQUM7NkJBQ2QsTUFBTSxFQUFOLHlCQUFNO3dCQUNOLE1BQU0sR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDOzZCQUNmLENBQUEsaUJBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFBLEVBQWpDLHdCQUFpQzt3QkFDakMscUJBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMseUNBQXlDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQ3ZGLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDOzRCQUNuQyxDQUFDLENBQUMsRUFBQTs7d0JBRkYsU0FFRSxDQUFDO3dCQUNILHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLCtCQUErQixFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO2dDQUM3RSxNQUFNLENBQUMsc0JBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQzs0QkFDbkMsQ0FBQyxDQUFDLEVBQUE7O3dCQUZGLFNBRUUsQ0FBQzt3QkFDSCxNQUFNLEdBQUcsSUFBSSxDQUFDOzs0QkFFZCxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxxYUFPTSxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHOzRCQUN2SSxNQUFNLENBQUMsc0JBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDbkMsQ0FBQyxDQUFDLEVBQUE7O3dCQVRGLFNBU0UsQ0FBQzs7Ozt3QkFJUCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7NEJBQUMsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsSUFBSSxDQUFDLEVBQUMsQ0FBRSw4QkFBOEI7d0JBQzFFLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLCtJQUNnQixFQUNsRCxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO2dDQUM5RixNQUFNLENBQUMsc0JBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQzs0QkFDbkMsQ0FBQyxDQUFDLEVBQUE7O3dCQUpGLE1BQU0sR0FBRyxTQUlQLENBQUM7Ozs2QkFFSCxNQUFNLEVBQU4seUJBQU07d0JBQ04scUJBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMseUNBQXlDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQ3ZGLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDOzRCQUNuQyxDQUFDLENBQUMsRUFBQTs7d0JBRkYsU0FFRSxDQUFDOzZCQUVDLElBQUksQ0FBQyxXQUFXLEVBQWhCLHlCQUFnQjs4QkFDdUIsRUFBaEIsS0FBQSxJQUFJLENBQUMsV0FBVzs7OzZCQUFoQixDQUFBLGNBQWdCLENBQUE7d0JBQTlCLFVBQVU7d0JBQ2YscUJBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsaUVBQWlFLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7aUNBQ3pJLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQ04sTUFBTSxDQUFDLHNCQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7NEJBQ25DLENBQUMsQ0FBQyxFQUFBOzt3QkFITixTQUdNLENBQUM7Ozt3QkFKWSxJQUFnQixDQUFBOzs2QkFRL0Msc0JBQU8sSUFBSSxrQ0FBZSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsRUFBQzs7O3dCQUV6QyxzQkFBTyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFDLENBQUMsRUFBQzs7Ozs7S0FHbEQ7SUFFWSx3Q0FBYyxHQUEzQixVQUE0QixFQUFVOzs7Ozs7O3dCQUU5QixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDOzRCQUNsQixNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLHNCQUFTLENBQUMsZ0JBQWdCLENBQUMsRUFBQTt3QkFDdkUsQ0FBQzt3QkFDRCxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxrRUFBa0UsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQ0FDNUcsTUFBTSxDQUFDLHNCQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7NEJBQ25DLENBQUMsQ0FBQyxFQUFBOzt3QkFGRixTQUVFLENBQUM7Ozs7d0JBRUgsc0JBQU8sSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBQyxDQUFDLEVBQUM7NEJBRS9DLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxJQUFJLENBQUMsRUFBQzs7OztLQUNwQztJQUVZLG1DQUFTLEdBQXRCLFVBQXVCLFVBQXVCO1FBQXZCLDJCQUFBLEVBQUEsZUFBdUI7Ozs7Ozs7d0JBRXRDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7NEJBQ2xCLE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsc0JBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBO3dCQUN2RSxDQUFDO3dCQUNZLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHFGQUFxRixHQUFHLFVBQVUsR0FBRyxrQ0FBa0MsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQ3BMLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzRCQUNsQyxDQUFDLENBQUMsRUFBQTs7d0JBRkksSUFBSSxHQUFHLFNBRVg7d0JBQ0ksT0FBTyxHQUFHLElBQUksS0FBSyxFQUFlLENBQUM7d0JBQ3pDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7NEJBQ1AsR0FBRyxDQUFDLE9BQWdCLEVBQUosYUFBSSxFQUFKLGtCQUFJLEVBQUosSUFBSTtnQ0FBWCxHQUFHO2dDQUNSLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSwwQkFBVyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQzs2QkFDekU7NEJBQ0QsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxFQUFDO3dCQUM5QyxDQUFDOzs7O3dCQUVELHNCQUFPLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLEdBQUMsQ0FBQyxFQUFDOzs7OztLQUVsRDtJQUVZLDJDQUFpQixHQUE5Qjs7Ozs7Ozt3QkFFUSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDOzRCQUNsQixNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLHNCQUFTLENBQUMsZ0JBQWdCLENBQUMsRUFBQTt3QkFDdkUsQ0FBQzt3QkFFWSxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyw0Q0FBNEMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQ3pGLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzRCQUNsQyxDQUFDLENBQUMsRUFBQTs7d0JBRkksSUFBSSxHQUFHLFNBRVg7d0JBRUksT0FBTyxHQUFHLElBQUksS0FBSyxFQUFRLENBQUM7NkJBQzlCLElBQUksRUFBSix3QkFBSTs4QkFDZ0IsRUFBSixhQUFJOzs7NkJBQUosQ0FBQSxrQkFBSSxDQUFBO3dCQUFYLEdBQUc7d0JBQ0YsSUFBSSxHQUFHLElBQUksV0FBSSxFQUFFLENBQUM7d0JBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQzt3QkFDekIsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDO3dCQUNuQixJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQyxlQUFlLENBQUM7d0JBQzNDLElBQUksQ0FBQyxFQUFFLEdBQUcsR0FBRyxDQUFDLEVBQUUsQ0FBQzt3QkFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO3dCQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7d0JBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQzt3QkFFQSxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQywyQ0FBMkMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQzdHLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzRCQUNsQyxDQUFDLENBQUMsRUFBQTs7d0JBRkksY0FBYyxHQUFHLFNBRXJCO3dCQUNJLFdBQVcsR0FBRyxJQUFJLEtBQUssRUFBa0IsQ0FBQzt3QkFDaEQsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQzs0QkFDakIsR0FBRyxDQUFDLE9BQTBCLEVBQWQsaUNBQWMsRUFBZCw0QkFBYyxFQUFkLElBQWM7Z0NBQXJCLEdBQUc7Z0NBQ0YsVUFBVSxHQUFHLElBQUksZ0NBQWMsRUFBRSxDQUFDO2dDQUN4QyxVQUFVLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUM7Z0NBQ2pDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztnQ0FDM0IsVUFBVSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO2dDQUUzQixXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzZCQUNoQzt3QkFDTCxDQUFDO3dCQUVELElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO3dCQUUvQixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzs7d0JBM0JQLElBQUksQ0FBQTs7NEJBK0J4QixzQkFBTyxJQUFJLGtDQUFlLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxFQUFDOzs7d0JBRTFDLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLEdBQUMsQ0FBQyxFQUFDOzs7OztLQUVsRDtJQUVZLGlDQUFPLEdBQXBCLFVBQXFCLEVBQVU7Ozs7Ozs7d0JBRXZCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7NEJBQ2xCLE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsc0JBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBO3dCQUN2RSxDQUFDO3dCQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzs0QkFDTixNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLENBQUMsRUFBQzt3QkFDdEMsQ0FBQzt3QkFFYyxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQ0FDdEYsTUFBTSxDQUFDLHNCQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQ2xDLENBQUMsQ0FBQyxFQUFBOzt3QkFGSSxNQUFNLEdBQUcsU0FFYjt3QkFDb0IscUJBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsMkNBQTJDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQ3ZHLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzRCQUNsQyxDQUFDLENBQUMsRUFBQTs7d0JBRkksYUFBYSxHQUFHLFNBRXBCO3dCQUVGLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDOzRCQUFDLE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFDLEtBQUssQ0FBQyxFQUFDO3dCQUV6QyxJQUFJLEdBQUcsSUFBSSxXQUFJLEVBQUUsQ0FBQzt3QkFDeEIsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO3dCQUNwQixJQUFJLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7d0JBQ3RCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQzt3QkFDNUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDO3dCQUM5QyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7d0JBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQzt3QkFDeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLEtBQUssRUFBa0IsQ0FBQzt3QkFFL0MsRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQzs0QkFDaEIsR0FBRyxDQUFDLE9BQWtDLEVBQWIsK0JBQWEsRUFBYiwyQkFBYSxFQUFiLElBQWE7Z0NBQTdCLFlBQVk7Z0NBQ1gsVUFBVSxHQUFHLElBQUksZ0NBQWMsRUFBRSxDQUFDO2dDQUN4QyxVQUFVLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUM7Z0NBQzFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQztnQ0FDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7NkJBQ3BDO3dCQUNMLENBQUM7d0JBRUQsc0JBQU8sSUFBSSxrQ0FBZSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBQzs7O3dCQUV2QyxzQkFBTyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFDLENBQUMsRUFBQzs7Ozs7S0FFbEQ7SUFFWSxrQ0FBUSxHQUFyQixVQUFzQixHQUFXOzs7Ozs7O3dCQUV6QixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDOzRCQUNsQixNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLHNCQUFTLENBQUMsZ0JBQWdCLENBQUMsRUFBQTt3QkFDdkUsQ0FBQzt3QkFFZSxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQ0FDN0YsTUFBTSxDQUFDLHNCQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQ2xDLENBQUMsQ0FBQyxFQUFBOzt3QkFGSSxPQUFPLEdBQUcsU0FFZDt3QkFDSSxLQUFLLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBRTdDLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUM7Ozt3QkFFeEMsc0JBQU8sSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBQyxDQUFDLEVBQUM7Ozs7O0tBRWxEO0lBRVksa0NBQVEsR0FBckIsVUFBc0IsR0FBVyxFQUFFLEtBQWE7Ozs7Ozs7d0JBRXhDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7NEJBQ2xCLE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsc0JBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBO3dCQUN2RSxDQUFDO3dCQUNlLHFCQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO2dDQUM3RixNQUFNLENBQUMsc0JBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQzs0QkFDbEMsQ0FBQyxDQUFDLEVBQUE7O3dCQUZJLE9BQU8sR0FBRyxTQUVkOzZCQUNFLE9BQU8sRUFBUCx3QkFBTzt3QkFDUCxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyw2Q0FBNkMsRUFBRSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQy9GLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDOzRCQUNuQyxDQUFDLENBQUMsRUFBQTs7d0JBRkYsU0FFRSxDQUFDOzs0QkFFSCxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxpREFBaUQsRUFBRSxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7NEJBQ25HLE1BQU0sQ0FBQyxzQkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUNuQyxDQUFDLENBQUMsRUFBQTs7d0JBRkYsU0FFRSxDQUFDOzs0QkFFUCxzQkFBTyxJQUFJLGtDQUFlLENBQUMsSUFBSSxDQUFDLEVBQUM7Ozt3QkFFakMsc0JBQU8sSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBQyxDQUFDLEVBQUM7Ozs7O0tBRWxEO0lBR1ksK0NBQXFCLEdBQWxDLFVBQW9DLFlBQTBCOzs7Ozs7O3dCQUV0RCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7NEJBQzlCLE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsc0JBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBO3dCQUN2RSxDQUFDO3dCQUNVLHFCQUFNLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsdURBQXVELEVBQUUsQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7Z0NBQzVKLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQ0FDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQ0FDakIsTUFBTSxDQUFDLHNCQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7NEJBQ25DLENBQUMsQ0FBQyxFQUFBOzt3QkFKSSxFQUFFLEdBQUcsU0FJVDt3QkFDRixzQkFBTyxJQUFJLGtDQUFlLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxFQUFDOzs7d0JBR3JDLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLEdBQUMsQ0FBQyxFQUFDOzs7OztLQUVsRDtJQUVZLHlDQUFlLEdBQTVCLFVBQThCLEVBQVU7Ozs7Ozs7d0JBRWhDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQzs0QkFDOUIsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxzQkFBUyxDQUFDLGdCQUFnQixDQUFDLEVBQUE7d0JBQ3ZFLENBQUM7d0JBQ2UscUJBQU0sSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyx5Q0FBeUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUEsR0FBRztnQ0FDM0csTUFBTSxDQUFDLHNCQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQ2xDLENBQUMsQ0FBQyxFQUFBOzt3QkFGSSxPQUFPLEdBQUcsU0FFZDt3QkFDRixFQUFFLENBQUEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7NEJBQ1YsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxzQkFBUyxDQUFDLFdBQVcsQ0FBQyxFQUFDO3dCQUNuRSxDQUFDO3dCQUNLLFlBQVksR0FBRyxJQUFJLDJCQUFZLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ3BFLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxJQUFJLEVBQUUsWUFBWSxDQUFDLEVBQUM7Ozt3QkFFL0Msc0JBQU8sSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBQyxDQUFDLEVBQUM7Ozs7O0tBRWxEO0lBeFhRLGVBQWU7UUFEM0IsaUJBQVUsRUFBRTt5Q0FLa0MsNkNBQW9CO09BSnRELGVBQWUsQ0F5WDNCO0lBQUQsc0JBQUM7Q0FBQSxBQXpYRCxJQXlYQztBQXpYWSwwQ0FBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtTZWN1cmVTdG9yYWdlU2VydmljZX0gZnJvbSBcIn4vc2VydmljZXMvc2VjdXJlLXN0b3JhZ2Uuc2VydmljZVwiO1xyXG5pbXBvcnQge0Vycm9yVHlwZX0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvZXJyb3ItdHlwZVwiO1xyXG5pbXBvcnQge09wZXJhdGlvblJlc3VsdH0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvb3BlcmF0aW9uLXJlc3VsdFwiO1xyXG5pbXBvcnQge05vdGV9IGZyb20gXCJ+L21vZGVscy9jb3JlL25vdGVcIjtcclxuaW1wb3J0IHtOb3RlRGlzcGxheX0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvbm90ZS1kaXNwbGF5XCI7XHJcbmltcG9ydCB7Tm90ZUF0dGFjaG1lbnR9IGZyb20gXCJ+L21vZGVscy9jb3JlL25vdGUtYXR0YWNobWVudFwiO1xyXG5pbXBvcnQge05vdGlmaWNhdGlvbn0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvbm90aWZpY2F0aW9uXCI7XHJcblxyXG52YXIgU3FsaXRlID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1zcWxpdGVcIik7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBEYXRhYmFzZVNlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSBfZGF0YWJhc2VcclxuICAgIHByaXZhdGUgX25vdGlmaWNhdGlvbkRhdGFiYXNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3NlY3VyZVN0b3JhZ2VTZXJ2aWNlOiBTZWN1cmVTdG9yYWdlU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBpbml0aWFsaXplTm90aWZpY2F0aW9uc0RhdGFiYXNlKCkge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwibm90aWZpY2F0aW9uIGRiIGluaXRcIik7XHJcbiAgICAgICAgICAgIGNvbnN0IGRiID0gYXdhaXQgbmV3IFNxbGl0ZSgnTm90ZXNOb3RpZmljYXRpb25zLmRiJykuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJPcGVuRGJFcnJvcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBhd2FpdCBkYi5leGVjU1FMKGBDUkVBVEUgVEFCTEUgSUYgTk9UIEVYSVNUUyBOb3RpZmljYXRpb24gKFxyXG4gICAgICAgICAgICAgICAgSWQgSU5URUdFUiBQUklNQVJZIEtFWSBBVVRPSU5DUkVNRU5ULFxyXG4gICAgICAgICAgICAgICAgVXNlciBURVhUIE5PVCBOVUxMLFxyXG4gICAgICAgICAgICAgICAgTm90ZUlkIElOVEVHRVIgTk9UIE5VTExcclxuICAgICAgICAgICAgKTtgKS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJPYnZlc3RpbG8gbmFwYWthOlwiKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyAoRXJyb3JUeXBlLmRiQ3JlYXRlVGFibGVzRXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX25vdGlmaWNhdGlvbkRhdGFiYXNlID0gZGI7XHJcbiAgICAgICAgICAgIHRoaXMuX25vdGlmaWNhdGlvbkRhdGFiYXNlLnJlc3VsdFR5cGUoU3FsaXRlLlJFU1VMVFNBU09CSkVDVCk7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0PG51bGw+KHRydWUpO1xyXG5cclxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgZXJyb3IpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgaW5pdGlhbGl6ZURhdGFiYXNlKCk6IFByb21pc2U8T3BlcmF0aW9uUmVzdWx0PG51bGw+PiB7XHJcbiAgICAgICAgY29uc3QgdXNlciA9IHRoaXMuX3NlY3VyZVN0b3JhZ2VTZXJ2aWNlLmdldFVzZXIoKTtcclxuICAgICAgICBpZiAoIXVzZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIEVycm9yVHlwZS5kYlVzZXJOb3RMb2dlZEluKVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgLy8gYXdhaXQgU3FsaXRlLmRlbGV0ZURhdGFiYXNlKHVzZXIgKyAnLmRiJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IGRiID0gYXdhaXQgbmV3IFNxbGl0ZSh1c2VyICsgJy5kYicpLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyAoRXJyb3JUeXBlLmRiT3BlbkRiRXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGF3YWl0IGRiLmV4ZWNTUUwoYENSRUFURSBUQUJMRSBJRiBOT1QgRVhJU1RTIE5vdGUgKFxyXG4gICAgICAgICAgICAgICAgSWQgSU5URUdFUiBQUklNQVJZIEtFWSBBVVRPSU5DUkVNRU5ULFxyXG4gICAgICAgICAgICAgICAgQ2lkXHRJTlRFR0VSLFxyXG4gICAgICAgICAgICAgICAgVGl0bGUgVEVYVCxcclxuICAgICAgICAgICAgICAgIFRleHQgVEVYVCxcclxuICAgICAgICAgICAgICAgIERhdGVUaW1lQ2hhbmdlZCBURVhUIE5PVCBOVUxMLFxyXG4gICAgICAgICAgICAgICAgTW9kaWZpZWQgSU5URUdFUiBOT1QgTlVMTCxcclxuICAgICAgICAgICAgICAgIEFjdGl2ZSBJTlRFR0VSIE5PVCBOVUxMXHJcbiAgICAgICAgICAgICk7YCkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJDcmVhdGVUYWJsZXNFcnJvcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgYXdhaXQgZGIuZXhlY1NRTChgQ1JFQVRFIFRBQkxFIElGIE5PVCBFWElTVFMgQXR0YWNobWVudCAoXHJcbiAgICAgICAgICAgICAgICBJZCBJTlRFR0VSIFBSSU1BUlkgS0VZIEFVVE9JTkNSRU1FTlQsXHJcbiAgICAgICAgICAgICAgICBOb3RlSWQgSU5URUdFUiBOT1QgTlVMTCxcclxuICAgICAgICAgICAgICAgIE5hbWUgVEVYVCxcclxuICAgICAgICAgICAgICAgIFR5cGUgVEVYVCxcclxuICAgICAgICAgICAgICAgIENvbnRlbnRcdFRFWFQsXHJcbiAgICAgICAgICAgICAgICBGT1JFSUdOIEtFWShOb3RlSWQpIFJFRkVSRU5DRVMgTm90ZShJZClcclxuICAgICAgICAgICAgKTtgKS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgKEVycm9yVHlwZS5kYkNyZWF0ZVRhYmxlc0Vycm9yKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBhd2FpdCBkYi5leGVjU1FMKGBDUkVBVEUgVEFCTEUgSUYgTk9UIEVYSVNUUyBLZXlWYWx1ZSAoXHJcbiAgICAgICAgICAgICAgICBLZXkgVEVYVCxcclxuICAgICAgICAgICAgICAgIFZhbHVlIFRFWFQsXHJcbiAgICAgICAgICAgICAgICBQUklNQVJZIEtFWShLZXkpXHJcbiAgICAgICAgICAgICk7YCkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJDcmVhdGVUYWJsZXNFcnJvcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2RhdGFiYXNlID0gZGI7XHJcbiAgICAgICAgICAgIHRoaXMuX2RhdGFiYXNlLnJlc3VsdFR5cGUoU3FsaXRlLlJFU1VMVFNBU09CSkVDVCk7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0PG51bGw+KHRydWUpO1xyXG5cclxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgZXJyb3IpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgc2F2ZU5vdGVzKG5vdGVzOiBBcnJheTxOb3RlPik6IFByb21pc2U8T3BlcmF0aW9uUmVzdWx0PG51bGw+PiB7XHJcbiAgICAgICAgaWYgKCF0aGlzLl9kYXRhYmFzZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgRXJyb3JUeXBlLmRiTm90SW5pdGlhbGl6ZWQpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghbm90ZXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQodHJ1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmb3IgKGxldCBub3RlIG9mIG5vdGVzKSB7XHJcbiAgICAgICAgICAgIHZhciByZXN1bHQgPSBhd2FpdCB0aGlzLnNhdmVOb3RlKG5vdGUsIHRydWUpO1xyXG4gICAgICAgICAgICBpZiAoIXJlc3VsdC5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChyZXN1bHQuc3VjY2VzcywgbnVsbCwgcmVzdWx0LmVycm9yVHlwZSwgcmVzdWx0LmVycm9yTWVzc2FnZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBzYXZlTm90ZShub3RlOiBOb3RlLCBkZWxldGVJZk5vdEFjdGl2ZTogYm9vbGVhbiA9IGZhbHNlKTogUHJvbWlzZTxPcGVyYXRpb25SZXN1bHQ8bnVtYmVyPj4ge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5fZGF0YWJhc2UpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBFcnJvclR5cGUuZGJOb3RJbml0aWFsaXplZClcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIW5vdGUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHRydWUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgZGJOb3RlID0gbnVsbDtcclxuICAgICAgICAgICAgaWYgKG5vdGUuaWQpIHsgIC8vIHRyeSBmaW5kIGJ5IGlkXHJcbiAgICAgICAgICAgICAgICBkYk5vdGUgPSBhd2FpdCB0aGlzLl9kYXRhYmFzZS5nZXQoJ1NFTEVDVCAqIEZST00gTm90ZSBXSEVSRSBJZCA9ID8nLCBbbm90ZS5pZF0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgKEVycm9yVHlwZS5kYlJlYWRFcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChub3RlLmNpZCkgeyAvLyB0cnkgZmluZCBieSBjZW50cmFsIGlkXHJcbiAgICAgICAgICAgICAgICBkYk5vdGUgPSBhd2FpdCB0aGlzLl9kYXRhYmFzZS5nZXQoJ1NFTEVDVCAqIEZST00gTm90ZSBXSEVSRSBDaWQgPSA/JywgW25vdGUuY2lkXSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aHJvdyAoRXJyb3JUeXBlLmRiUmVhZEVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyB1cGRhdGVcclxuICAgICAgICAgICAgbGV0IG5vdGVJZCA9IG51bGw7XHJcbiAgICAgICAgICAgIGlmIChkYk5vdGUpIHtcclxuICAgICAgICAgICAgICAgIG5vdGVJZCA9IGRiTm90ZS5JZDtcclxuICAgICAgICAgICAgICAgIGlmIChkZWxldGVJZk5vdEFjdGl2ZSAmJiAhbm90ZS5hY3RpdmUpIHtcclxuICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLl9kYXRhYmFzZS5leGVjU1FMKFwiREVMRVRFIEZST00gQXR0YWNobWVudCBXSEVSRSBOb3RlSWQgPSA/XCIsIFtub3RlSWRdKS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyAoRXJyb3JUeXBlLmRiV3JpdGVFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5fZGF0YWJhc2UuZXhlY1NRTChcIkRFTEVURSBGUk9NIE5vdGUgV0hFUkUgSWQgPSA/XCIsIFtub3RlSWRdKS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyAoRXJyb3JUeXBlLmRiV3JpdGVFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbm90ZUlkID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5fZGF0YWJhc2UuZXhlY1NRTChgVVBEQVRFIE5vdGUgU0VUXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQ2lkXHQ9ID8sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVGl0bGUgPSA/LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFRleHQgPSA/LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIERhdGVUaW1lQ2hhbmdlZFx0PSA/LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE1vZGlmaWVkID0gPyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBBY3RpdmUgPSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFdIRVJFIElkID0gP2AsIFtub3RlLmNpZCwgbm90ZS50aXRsZSwgbm90ZS50ZXh0LCBub3RlLmRhdGVUaW1lQ2hhbmdlZCwgbm90ZS5tb2RpZmllZCwgbm90ZS5hY3RpdmUsIG5vdGVJZF0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJXcml0ZUVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSB7ICAvLyBpbnNlcnRcclxuICAgICAgICAgICAgICAgIGlmICghbm90ZS5hY3RpdmUpIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHRydWUpOyAgLy8gZG8gbm90IGluc2VydCBpZiBub3QgYWN0aXZlXHJcbiAgICAgICAgICAgICAgICBub3RlSWQgPSBhd2FpdCB0aGlzLl9kYXRhYmFzZS5leGVjU1FMKGBJTlNFUlQgSU5UTyBOb3RlIChDaWQsIFRpdGxlLCBUZXh0LCBEYXRlVGltZUNoYW5nZWQsIE1vZGlmaWVkLCBBY3RpdmUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVkFMVUVTICg/LCA/LCA/LCA/LCA/LCA/KWAsXHJcbiAgICAgICAgICAgICAgICAgICAgW25vdGUuY2lkLCBub3RlLnRpdGxlLCBub3RlLnRleHQsIG5vdGUuZGF0ZVRpbWVDaGFuZ2VkLCBub3RlLm1vZGlmaWVkLCBub3RlLmFjdGl2ZV0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgKEVycm9yVHlwZS5kYldyaXRlRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKG5vdGVJZCkgeyAgLy8gdXBkYXRlIGF0dGFjaG1lbnRzXHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLl9kYXRhYmFzZS5leGVjU1FMKCdERUxFVEUgRlJPTSBBdHRhY2htZW50IFdIRVJFIE5vdGVJZCA9ID8nLCBbbm90ZUlkXSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aHJvdyAoRXJyb3JUeXBlLmRiV3JpdGVFcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAobm90ZS5hdHRhY2htZW50cykge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGF0dGFjaG1lbnQgb2Ygbm90ZS5hdHRhY2htZW50cykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLl9kYXRhYmFzZS5leGVjU1FMKGBJTlNFUlQgSU5UTyBBdHRhY2htZW50IChOb3RlSWQsIFR5cGUsIENvbnRlbnQpIFZBTFVFUyAoPywgPywgPylgLCBbbm90ZUlkLCBhdHRhY2htZW50LnR5cGUsIGF0dGFjaG1lbnQuY29udGVudF0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyAoRXJyb3JUeXBlLmRiV3JpdGVFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQodHJ1ZSwgbm90ZUlkKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBkZWFjdGl2YXRlTm90ZShpZDogbnVtYmVyKTogUHJvbWlzZTxPcGVyYXRpb25SZXN1bHQ8bnVsbD4+IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuX2RhdGFiYXNlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgRXJyb3JUeXBlLmRiTm90SW5pdGlhbGl6ZWQpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5fZGF0YWJhc2UuZXhlY1NRTChcIlVQREFURSBOb3RlIFNFVCBBY3RpdmUgPSAnZmFsc2UnLCBNb2RpZmllZCA9ICd0cnVlJyBXSEVSRSBJZCA9ID9cIiwgW2lkXSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJXcml0ZUVycm9yKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBsaXN0Tm90ZXMoc2VhcmNoVGV4dDogc3RyaW5nID0gJycpOiBQcm9taXNlPE9wZXJhdGlvblJlc3VsdDxBcnJheTxOb3RlRGlzcGxheT4+PiB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLl9kYXRhYmFzZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIEVycm9yVHlwZS5kYk5vdEluaXRpYWxpemVkKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHJvd3MgPSBhd2FpdCB0aGlzLl9kYXRhYmFzZS5hbGwoXCJTRUxFQ1QgSWQsIFRpdGxlLCBEYXRlVGltZUNoYW5nZWQgRlJPTSBOb3RlIFdIRVJFIEFjdGl2ZSA9ICd0cnVlJyBBTkQgVGl0bGUgTElLRSAnJVwiICsgc2VhcmNoVGV4dCArIFwiJScgT1JERVIgQlkgRGF0ZVRpbWVDaGFuZ2VkIERFU0NcIikuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJSZWFkRXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgY29uc3QgcmVzdWx0cyA9IG5ldyBBcnJheTxOb3RlRGlzcGxheT4oKTtcclxuICAgICAgICAgICAgaWYgKHJvd3MpIHtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IHJvdyBvZiByb3dzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0cy5wdXNoKG5ldyBOb3RlRGlzcGxheShyb3cuSWQsIHJvdy5UaXRsZSwgcm93LkRhdGVUaW1lQ2hhbmdlZCkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQodHJ1ZSwgcmVzdWx0cyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIGxpc3RNb2RpZmllZE5vdGVzKCk6IFByb21pc2U8T3BlcmF0aW9uUmVzdWx0PEFycmF5PE5vdGU+Pj4ge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5fZGF0YWJhc2UpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBFcnJvclR5cGUuZGJOb3RJbml0aWFsaXplZClcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3Qgcm93cyA9IGF3YWl0IHRoaXMuX2RhdGFiYXNlLmFsbChcIlNFTEVDVCAqIEZST00gTm90ZSBXSEVSRSBNb2RpZmllZCA9ICd0cnVlJ1wiKS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgKEVycm9yVHlwZS5kYlJlYWRFcnJvcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcmVzdWx0cyA9IG5ldyBBcnJheTxOb3RlPigpO1xyXG4gICAgICAgICAgICBpZiAocm93cykge1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgcm93IG9mIHJvd3MpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBub3RlID0gbmV3IE5vdGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBub3RlLmFjdGl2ZSA9IHJvdy5BY3RpdmU7XHJcbiAgICAgICAgICAgICAgICAgICAgbm90ZS5jaWQgPSByb3cuQ2lkO1xyXG4gICAgICAgICAgICAgICAgICAgIG5vdGUuZGF0ZVRpbWVDaGFuZ2VkID0gcm93LkRhdGVUaW1lQ2hhbmdlZDtcclxuICAgICAgICAgICAgICAgICAgICBub3RlLmlkID0gcm93LklkO1xyXG4gICAgICAgICAgICAgICAgICAgIG5vdGUubW9kaWZpZWQgPSByb3cuTW9kaWZpZWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgbm90ZS50ZXh0ID0gcm93LlRleHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgbm90ZS50aXRsZSA9IHJvdy5UaXRsZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYXR0YWNobWVudFJvd3MgPSBhd2FpdCB0aGlzLl9kYXRhYmFzZS5hbGwoXCJTRUxFQ1QgKiBGUk9NIEF0dGFjaG1lbnQgV0hFUkUgTm90ZUlkID0gP1wiLCBbbm90ZS5pZF0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJSZWFkRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGF0dGFjaG1lbnRzID0gbmV3IEFycmF5PE5vdGVBdHRhY2htZW50PigpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChhdHRhY2htZW50Um93cykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBhdHQgb2YgYXR0YWNobWVudFJvd3MpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGF0dGFjaG1lbnQgPSBuZXcgTm90ZUF0dGFjaG1lbnQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dGFjaG1lbnQuY29udGVudCA9IGF0dC5Db250ZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0YWNobWVudC50eXBlID0gYXR0LlR5cGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRhY2htZW50Lm5hbWUgPSBhdHQuTmFtZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRhY2htZW50cy5wdXNoKGF0dGFjaG1lbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBub3RlLmF0dGFjaG1lbnRzID0gYXR0YWNobWVudHM7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdHMucHVzaChub3RlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQodHJ1ZSwgcmVzdWx0cyk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBnZXROb3RlKGlkOiBudW1iZXIpOiBQcm9taXNlPE9wZXJhdGlvblJlc3VsdDxOb3RlPj4ge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5fZGF0YWJhc2UpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBFcnJvclR5cGUuZGJOb3RJbml0aWFsaXplZClcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoIWlkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGRiTm90ZSA9IGF3YWl0IHRoaXMuX2RhdGFiYXNlLmdldCgnU0VMRUNUICogRlJPTSBOb3RlIFdIRVJFIElkID0gPycsIFtpZF0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyAoRXJyb3JUeXBlLmRiUmVhZEVycm9yKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGRiQXR0YWNobWVudHMgPSBhd2FpdCB0aGlzLl9kYXRhYmFzZS5hbGwoJ1NFTEVDVCAqIEZST00gQXR0YWNobWVudCBXSEVSRSBOb3RlSWQgPSA/JywgW2lkXSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJSZWFkRXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGlmICghZGJOb3RlKSByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBub3RlID0gbmV3IE5vdGUoKTtcclxuICAgICAgICAgICAgbm90ZS5pZCA9IGRiTm90ZS5JZDtcclxuICAgICAgICAgICAgbm90ZS5jaWQgPSBkYk5vdGUuQ2lkO1xyXG4gICAgICAgICAgICBub3RlLmFjdGl2ZSA9IGRiTm90ZS5BY3RpdmU7XHJcbiAgICAgICAgICAgIG5vdGUuZGF0ZVRpbWVDaGFuZ2VkID0gZGJOb3RlLkRhdGVUaW1lQ2hhbmdlZDtcclxuICAgICAgICAgICAgbm90ZS50aXRsZSA9IGRiTm90ZS5UaXRsZTtcclxuICAgICAgICAgICAgbm90ZS50ZXh0ID0gZGJOb3RlLlRleHQ7XHJcbiAgICAgICAgICAgIG5vdGUuYXR0YWNobWVudHMgPSBuZXcgQXJyYXk8Tm90ZUF0dGFjaG1lbnQ+KCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoZGJBdHRhY2htZW50cykge1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgZGJBdHRhY2htZW50IG9mIGRiQXR0YWNobWVudHMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhdHRhY2htZW50ID0gbmV3IE5vdGVBdHRhY2htZW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYXR0YWNobWVudC5jb250ZW50ID0gZGJBdHRhY2htZW50LkNvbnRlbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgYXR0YWNobWVudC50eXBlID0gZGJBdHRhY2htZW50LlR5cGU7XHJcbiAgICAgICAgICAgICAgICAgICAgbm90ZS5hdHRhY2htZW50cy5wdXNoKGF0dGFjaG1lbnQpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHRydWUsIG5vdGUpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIGUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgZ2V0VmFsdWUoa2V5OiBzdHJpbmcpOiBQcm9taXNlPE9wZXJhdGlvblJlc3VsdDxzdHJpbmc+PiB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLl9kYXRhYmFzZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIEVycm9yVHlwZS5kYk5vdEluaXRpYWxpemVkKVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBkYlZhbHVlID0gYXdhaXQgdGhpcy5fZGF0YWJhc2UuZ2V0KCdTRUxFQ1QgKiBGUk9NIEtleVZhbHVlIFdIRVJFIEtleSA9ID8nLCBba2V5XSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJSZWFkRXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBkYlZhbHVlID8gZGJWYWx1ZS5WYWx1ZSA6IG51bGw7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdCh0cnVlLCB2YWx1ZSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBzZXRWYWx1ZShrZXk6IHN0cmluZywgdmFsdWU6IHN0cmluZyk6IFByb21pc2U8T3BlcmF0aW9uUmVzdWx0PG51bGw+PiB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLl9kYXRhYmFzZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIEVycm9yVHlwZS5kYk5vdEluaXRpYWxpemVkKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGRiVmFsdWUgPSBhd2FpdCB0aGlzLl9kYXRhYmFzZS5nZXQoJ1NFTEVDVCAqIEZST00gS2V5VmFsdWUgV0hFUkUgS2V5ID0gPycsIFtrZXldKS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgKEVycm9yVHlwZS5kYlJlYWRFcnJvcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBpZiAoZGJWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5fZGF0YWJhc2UuZXhlY1NRTChcIlVQREFURSBLZXlWYWx1ZSBTRVQgVmFsdWUgPSA/IFdIRVJFIEtleSA9ID9cIiwgW3ZhbHVlLCBrZXldKS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJXcml0ZUVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5fZGF0YWJhc2UuZXhlY1NRTChcIklOU0VSVCBJTlRPIEtleVZhbHVlIChLZXksIFZhbHVlKSBWQUxVRVMgKD8sID8pXCIsIFtrZXksIHZhbHVlXSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aHJvdyAoRXJyb3JUeXBlLmRiV3JpdGVFcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdCh0cnVlKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBpbnNlcnROZXdOb3RpZmljYXRpb24gKG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uKTogUHJvbWlzZTxPcGVyYXRpb25SZXN1bHQ8bnVtYmVyPj4ge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5fbm90aWZpY2F0aW9uRGF0YWJhc2UpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBFcnJvclR5cGUuZGJOb3RJbml0aWFsaXplZClcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBpZCA9IGF3YWl0IHRoaXMuX25vdGlmaWNhdGlvbkRhdGFiYXNlLmV4ZWNTUUwoJ0lOU0VSVCBJTlRPIE5vdGlmaWNhdGlvbiAoVXNlciwgTm90ZUlkKSBWQUxVRVMgKD8sID8pJywgW25vdGlmaWNhdGlvbi51c2VyLCBub3RpZmljYXRpb24ubm90ZUlkXSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiT2J2ZXN0aWxvIG5hcGFrYTpcIik7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgKEVycm9yVHlwZS5kYldyaXRlRXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQodHJ1ZSwgaWQpO1xyXG5cclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIGdldE5vdGlmaWNhdGlvbiAoaWQ6IG51bWJlcik6IFByb21pc2U8T3BlcmF0aW9uUmVzdWx0PE5vdGlmaWNhdGlvbj4+IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuX25vdGlmaWNhdGlvbkRhdGFiYXNlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgRXJyb3JUeXBlLmRiTm90SW5pdGlhbGl6ZWQpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgZGJWYWx1ZSA9IGF3YWl0IHRoaXMuX25vdGlmaWNhdGlvbkRhdGFiYXNlLmdldCgnU0VMRUNUICogRlJPTSBOb3RpZmljYXRpb24gV0hFUkUgSWQgPSA/JywgW2lkXSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRocm93IChFcnJvclR5cGUuZGJSZWFkRXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYoIWRiVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBFcnJvclR5cGUuZGJSZWFkRXJyb3IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IG5vdGlmaWNhdGlvbiA9IG5ldyBOb3RpZmljYXRpb24oZGJWYWx1ZS5Vc2VyLCBkYlZhbHVlLk5vdGVJZCk7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHRydWUsIG5vdGlmaWNhdGlvbik7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59Il19