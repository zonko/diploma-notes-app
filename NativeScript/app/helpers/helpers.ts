import { ErrorType } from "~/models/core/error-type";

export class Helpers {
    public static getDateTimeString(date: Date = new Date()): string {
        return date.getUTCFullYear().toString() + '_' +
            this.leftPad(date.getUTCMonth() + 1, 2) + '_' +
            this.leftPad(date.getUTCDate(), 2) + '_' +
            this.leftPad(date.getUTCHours(), 2) + '_' +
            this.leftPad(date.getUTCMinutes(), 2) + '_' +
            this.leftPad(date.getUTCSeconds(), 2)
    }

    public static getErrorMessage(errorType: ErrorType): string {
        switch(errorType) {
            case ErrorType.ok: return '';
            case ErrorType.wsAuthorizationError: return 'Avtentikacija s spletnim srežnikom ni uspela.';
            case ErrorType.wsBadRequest: return 'Neznana zaheteva.';
            case ErrorType.wsInternalServerError: return 'Notranja napaka na strežniku.';
            case ErrorType.wsUsernameIsTaken: return 'Uporabniško ime je zasedeno.';
            case ErrorType.wsWrongUsernameOrPassword: return 'Napačno uporabniško ime ali geslo.';
            case ErrorType.wsServerNotReachable: return 'Strežnik ni dosegljiv.';
            case ErrorType.dbCreateTablesError: return 'Napaka ob ustvarjanju tabel podatkovne baze.';
            case ErrorType.dbNotInitialized: return 'Podatkovna baza ni inicializirana.';
            case ErrorType.dbOpenDbError: return 'Na podatkovno bazo se ni bilo mogoče povezati.';
            case ErrorType.dbUserNotLogedIn: return 'Uporabnik ni prijavljen. Podatkovne baze ni mogoče inicializirati.';
            case ErrorType.dbWriteError: return 'Napaka ob pisanju v podatkovno bazo.';
            case ErrorType.dbReadError: return 'Napaka ob branju iz podatkovne baze.';
            case ErrorType.notificationSchedulingError: return 'Prišlo je do napake ob nastavljanju obvestila.';
        }
    }

    private static leftPad(input: number, targetLength: number): string {
        let output = input + '';
        while (output.length < targetLength) {
            output = '0' + output;
        }
        return output;
    }

    public static isNullOrEmpty(value: string): boolean {
        return !value || value.replace(/\s/g,"") === ""
    }
    
}