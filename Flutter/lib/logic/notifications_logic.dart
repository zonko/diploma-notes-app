import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:ft_notes_app/database/data_access.dart';
import 'package:ft_notes_app/globals.dart';
import 'package:ft_notes_app/logic/user_logic.dart';
import 'package:ft_notes_app/models/notification.dart';
import 'package:ft_notes_app/screens/note_edit/note_edit_screen.dart';
import 'package:ft_notes_app/services/dialog_service.dart';

class NotificationsLogic {
   // next three lines makes this class a Singleton
  static NotificationsLogic _instance = new NotificationsLogic.internal();
  NotificationsLogic.internal();
  factory NotificationsLogic() => _instance;

  final _dataAccess =  new DataAccess();
  final _userLogic = new UserLogic();
  final _dialogService = new DialogService();

  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;
  NotificationDetails _platformChannelSpecifics;

  Future<Null> initialize() async {

    await _dataAccess.initializeNotificationsDatabase();
    var initializationSettingsAndroid = new AndroidInitializationSettings('icon');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
    _flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    _flutterLocalNotificationsPlugin.initialize(initializationSettings, selectNotification: _onSelectNotification);

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'ft.notes.app',
        'FT Notes App',
        'Notes notification channel');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    _platformChannelSpecifics = new NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  }

  Future<void> scheduleNotification(int noteId, String title, String text, DateTime dateTime) async {
    var notification = new NotesNotification();
    notification.noteId = noteId;
    notification.user = await _userLogic.getLoggedInUser();
    var notificationId = await _dataAccess.insertNewNotification(notification);
    
    await _flutterLocalNotificationsPlugin.schedule (
      notificationId,
      title,
      text,
      dateTime, 
      _platformChannelSpecifics,
      payload: notificationId.toString()
    );
  }

  Future _onSelectNotification(String payload) async {
    try {
      int notificationId = int.parse(payload);
      var notification = await _dataAccess.getNotification(notificationId);

      if (notification == null) return;
      if (!(await _userLogic.checkLogin())) return;
 
      if (notification.user == await _userLogic.getLoggedInUser()) {
        Globals.navigatorKey.currentState.push(MaterialPageRoute(builder: (BuildContext context) => new NoteEditScreen(notification.noteId)));
      } else {
        await _dialogService.showAlert('Opozorilo', 'Obvestilo je bilo namenjeno drugemu uporabniku', Globals.navigatorKey.currentContext);
      }
      
    } catch (err) {
      await _dialogService.showAlert('Napaka', 'Prišlo je do napake med odpiranjem obvestila', Globals.navigatorKey.currentContext);
    }
  }
}