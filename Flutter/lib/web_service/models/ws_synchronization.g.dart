// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ws_synchronization.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WsSynchronization _$WsSynchronizationFromJson(Map<String, dynamic> json) {
  return WsSynchronization()
    ..lastSyncDateTime = json['lastSyncDateTime'] as String
    ..notes = (json['notes'] as List)
        ?.map((e) =>
            e == null ? null : WsNote.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$WsSynchronizationToJson(WsSynchronization instance) =>
    <String, dynamic>{
      'lastSyncDateTime': instance.lastSyncDateTime,
      'notes': instance.notes
    };
