﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Notes.Service.Entities
{
    [DataContract(Name = "note")]
    public class Note
    {
        [DataMember(Name = "id")]
        public long? Id { get; set; }

        [DataMember(Name = "clientId")]
        public long? ClientId { get; set; }

        [DataMember(Name = "active")]
        public bool Active { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "dateTimeChanged")]
        public string DateTimeChanged { get; set; }

        [DataMember(Name = "attachments")]
        public List<Attachment> Attachments { get; set; }
    }
}