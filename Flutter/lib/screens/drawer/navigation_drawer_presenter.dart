import 'package:ft_notes_app/helpers/error_message_helper.dart';
import 'package:ft_notes_app/logic/notes_logic.dart';
import 'package:ft_notes_app/logic/user_logic.dart';

abstract class NavigationDrawerContract {
  void onError(String errorMessage);
  void onSyncSuccess();
  void onLoggedOut();
  void setUser(String user);
}

class NavigationDrawerPresenter {
  NavigationDrawerContract _view;
  NavigationDrawerPresenter(this._view) {
    _loadUser();
  }

  final _userLogic = new UserLogic();
  final _notesLogic = new NotesLogic();

  _loadUser() async {
    var user = await _userLogic.getLoggedInUser();
    _view.setUser(user);
  }

  synchronize() async {
    try {
      await _notesLogic.synchronize();
      _view.onSyncSuccess();
    } catch (err) {
      _view.onError('Sinhronizacija ni bila uspešna. ' + ErrorMessageHelper.getErrorMessage(err));
    }
  }

  logout() async {
    try {
      await _userLogic.logout();
      _view.onLoggedOut();
    } catch (err) {
      _view.onError('Med odjavljanjem je prišlo do napake. ' + ErrorMessageHelper.getErrorMessage(err));
      _view.onLoggedOut();
    }
  }
}