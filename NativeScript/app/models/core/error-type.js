"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorType;
(function (ErrorType) {
    ErrorType[ErrorType["ok"] = 1] = "ok";
    // web service errors
    ErrorType[ErrorType["wsBadRequest"] = 2] = "wsBadRequest";
    ErrorType[ErrorType["wsInternalServerError"] = 3] = "wsInternalServerError";
    ErrorType[ErrorType["wsUsernameIsTaken"] = 4] = "wsUsernameIsTaken";
    ErrorType[ErrorType["wsWrongUsernameOrPassword"] = 5] = "wsWrongUsernameOrPassword";
    ErrorType[ErrorType["wsAuthorizationError"] = 6] = "wsAuthorizationError";
    ErrorType[ErrorType["wsServerNotReachable"] = 15] = "wsServerNotReachable";
    // db errors
    ErrorType[ErrorType["dbUserNotLogedIn"] = 100] = "dbUserNotLogedIn";
    ErrorType[ErrorType["dbNotInitialized"] = 101] = "dbNotInitialized";
    ErrorType[ErrorType["dbCreateTablesError"] = 102] = "dbCreateTablesError";
    ErrorType[ErrorType["dbOpenDbError"] = 103] = "dbOpenDbError";
    ErrorType[ErrorType["dbWriteError"] = 104] = "dbWriteError";
    ErrorType[ErrorType["dbReadError"] = 105] = "dbReadError";
    //
    ErrorType[ErrorType["notificationSchedulingError"] = 200] = "notificationSchedulingError";
})(ErrorType = exports.ErrorType || (exports.ErrorType = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3ItdHlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVycm9yLXR5cGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxJQUFZLFNBb0JYO0FBcEJELFdBQVksU0FBUztJQUNqQixxQ0FBTSxDQUFBO0lBQ04scUJBQXFCO0lBQ3JCLHlEQUFnQixDQUFBO0lBQ2hCLDJFQUF5QixDQUFBO0lBQ3pCLG1FQUFxQixDQUFBO0lBQ3JCLG1GQUE2QixDQUFBO0lBQzdCLHlFQUF3QixDQUFBO0lBQ3hCLDBFQUF5QixDQUFBO0lBRXpCLFlBQVk7SUFDWixtRUFBc0IsQ0FBQTtJQUN0QixtRUFBc0IsQ0FBQTtJQUN0Qix5RUFBeUIsQ0FBQTtJQUN6Qiw2REFBbUIsQ0FBQTtJQUNuQiwyREFBa0IsQ0FBQTtJQUNsQix5REFBaUIsQ0FBQTtJQUVqQixFQUFFO0lBQ0YseUZBQWlDLENBQUE7QUFDckMsQ0FBQyxFQXBCVyxTQUFTLEdBQVQsaUJBQVMsS0FBVCxpQkFBUyxRQW9CcEIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBFcnJvclR5cGUge1xyXG4gICAgb2sgPSAxLFxyXG4gICAgLy8gd2ViIHNlcnZpY2UgZXJyb3JzXHJcbiAgICB3c0JhZFJlcXVlc3QgPSAyLFxyXG4gICAgd3NJbnRlcm5hbFNlcnZlckVycm9yID0gMyxcclxuICAgIHdzVXNlcm5hbWVJc1Rha2VuID0gNCxcclxuICAgIHdzV3JvbmdVc2VybmFtZU9yUGFzc3dvcmQgPSA1LFxyXG4gICAgd3NBdXRob3JpemF0aW9uRXJyb3IgPSA2LFxyXG4gICAgd3NTZXJ2ZXJOb3RSZWFjaGFibGUgPSAxNSxcclxuXHJcbiAgICAvLyBkYiBlcnJvcnNcclxuICAgIGRiVXNlck5vdExvZ2VkSW4gPSAxMDAsXHJcbiAgICBkYk5vdEluaXRpYWxpemVkID0gMTAxLFxyXG4gICAgZGJDcmVhdGVUYWJsZXNFcnJvciA9IDEwMixcclxuICAgIGRiT3BlbkRiRXJyb3IgPSAxMDMsXHJcbiAgICBkYldyaXRlRXJyb3IgPSAxMDQsXHJcbiAgICBkYlJlYWRFcnJvciA9IDEwNSxcclxuXHJcbiAgICAvL1xyXG4gICAgbm90aWZpY2F0aW9uU2NoZWR1bGluZ0Vycm9yID0gMjAwXHJcbn0iXX0=