"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("~/services/user.service");
var router_1 = require("@angular/router");
var helpers_1 = require("~/helpers/helpers");
var dialog_service_1 = require("~/services/dialog.service");
var page_1 = require("ui/page");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(_userService, _router, _dialogService, _page) {
        this._userService = _userService;
        this._router = _router;
        this._dialogService = _dialogService;
        this._page = _page;
        this.isBusy = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this._page.actionBarHidden = true;
    };
    LoginComponent.prototype.onLogin = function () {
        var _this = this;
        if (helpers_1.Helpers.isNullOrEmpty(this.username) || helpers_1.Helpers.isNullOrEmpty(this.password)) {
            this._dialogService.showMessageDialog("Prijava", "Vnesite uporabniško ime in geslo");
            return;
        }
        this._dialogService.setBusy(true);
        // this.isBusy = true;
        this._userService.login(this.username, this.password).then(function (res) {
            _this._dialogService.setBusy(false);
            _this.username = '';
            _this.password = '';
            if (res.success) {
                _this._router.navigateByUrl("notes");
            }
            else {
                var errorMessage = "Prijava ni uspela: " + helpers_1.Helpers.getErrorMessage(res.errorType);
                _this._dialogService.showMessageDialog("Napaka", errorMessage);
            }
        }).catch(function (err) {
            _this._dialogService.setBusy(false);
            _this.username = '';
            _this.password = '';
            _this._dialogService.showMessageDialog("Napaka", "Prijava ni uspela: Prišlo je do nepričakovane napake: " + err);
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        }),
        __metadata("design:paramtypes", [user_service_1.UserService,
            router_1.Router,
            dialog_service_1.DialogService,
            page_1.Page])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQWtEO0FBQ2xELHdEQUFzRDtBQUN0RCwwQ0FBeUM7QUFDekMsNkNBQTRDO0FBQzVDLDREQUF3RDtBQUN4RCxnQ0FBK0I7QUFRL0I7SUFPSSx3QkFBb0IsWUFBeUIsRUFDekIsT0FBZSxFQUNmLGNBQTZCLEVBQzdCLEtBQVc7UUFIWCxpQkFBWSxHQUFaLFlBQVksQ0FBYTtRQUN6QixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ2YsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0IsVUFBSyxHQUFMLEtBQUssQ0FBTTtRQUwvQixXQUFNLEdBQUcsS0FBSyxDQUFDO0lBTWYsQ0FBQztJQUVELGlDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7SUFDdEMsQ0FBQztJQUdELGdDQUFPLEdBQVA7UUFBQSxpQkF1QkM7UUF0QkcsRUFBRSxDQUFBLENBQUMsaUJBQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLGlCQUFPLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsa0NBQWtDLENBQUMsQ0FBQztZQUNyRixNQUFNLENBQUM7UUFDWCxDQUFDO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEMsc0JBQXNCO1FBQ3RCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDMUQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkMsS0FBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbkIsS0FBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbkIsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEMsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQU0sWUFBWSxHQUFHLHFCQUFxQixHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDcEYsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUM7WUFDbEUsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7WUFDUixLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQyxLQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNuQixLQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNuQixLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSx3REFBd0QsR0FBRyxHQUFHLENBQUMsQ0FBQztRQUNwSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUF6Q1EsY0FBYztRQU4xQixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFdBQVcsRUFBRSx3QkFBd0I7WUFDckMsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7U0FDdkMsQ0FBQzt5Q0FRb0MsMEJBQVc7WUFDaEIsZUFBTTtZQUNDLDhCQUFhO1lBQ3RCLFdBQUk7T0FWdEIsY0FBYyxDQTBDMUI7SUFBRCxxQkFBQztDQUFBLEFBMUNELElBMENDO0FBMUNZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICd+L3NlcnZpY2VzL3VzZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEhlbHBlcnMgfSBmcm9tICd+L2hlbHBlcnMvaGVscGVycyc7XHJcbmltcG9ydCB7RGlhbG9nU2VydmljZX0gZnJvbSBcIn4vc2VydmljZXMvZGlhbG9nLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ1aS9wYWdlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICBzZWxlY3RvcjogJ2FwcC1sb2dpbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vbG9naW4uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vbG9naW4uY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgdXNlcm5hbWU6IHN0cmluZztcclxuICAgIHBhc3N3b3JkOiBzdHJpbmc7XHJcblxyXG4gICAgaXNCdXN5ID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9kaWFsb2dTZXJ2aWNlOiBEaWFsb2dTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfcGFnZTogUGFnZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuX3BhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgb25Mb2dpbigpIHtcclxuICAgICAgICBpZihIZWxwZXJzLmlzTnVsbE9yRW1wdHkodGhpcy51c2VybmFtZSkgfHwgSGVscGVycy5pc051bGxPckVtcHR5KHRoaXMucGFzc3dvcmQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJQcmlqYXZhXCIsIFwiVm5lc2l0ZSB1cG9yYWJuacWha28gaW1lIGluIGdlc2xvXCIpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2V0QnVzeSh0cnVlKTtcclxuICAgICAgICAvLyB0aGlzLmlzQnVzeSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5fdXNlclNlcnZpY2UubG9naW4odGhpcy51c2VybmFtZSwgdGhpcy5wYXNzd29yZCkudGhlbihyZXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNldEJ1c3koZmFsc2UpO1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJuYW1lID0gJyc7XHJcbiAgICAgICAgICAgIHRoaXMucGFzc3dvcmQgPSAnJztcclxuICAgICAgICAgICAgaWYgKHJlcy5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGVCeVVybChcIm5vdGVzXCIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXJyb3JNZXNzYWdlID0gXCJQcmlqYXZhIG5pIHVzcGVsYTogXCIgKyBIZWxwZXJzLmdldEVycm9yTWVzc2FnZShyZXMuZXJyb3JUeXBlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJOYXBha2FcIiwgZXJyb3JNZXNzYWdlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2V0QnVzeShmYWxzZSk7XHJcbiAgICAgICAgICAgIHRoaXMudXNlcm5hbWUgPSAnJztcclxuICAgICAgICAgICAgdGhpcy5wYXNzd29yZCA9ICcnO1xyXG4gICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiTmFwYWthXCIsIFwiUHJpamF2YSBuaSB1c3BlbGE6IFByacWhbG8gamUgZG8gbmVwcmnEjWFrb3ZhbmUgbmFwYWtlOiBcIiArIGVycik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19