import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ft_notes_app/models/synchronization.dart';
import 'package:ft_notes_app/web_service/models/ws_error_type.dart';
import 'package:ft_notes_app/web_service/models/ws_note.dart';
import 'package:ft_notes_app/web_service/models/ws_sync_response.dart';
import 'package:ft_notes_app/web_service/models/ws_synchronization.dart';
import 'package:queries/collections.dart';
import 'package:http/http.dart' as http;

class NotesWebServiceManager {
  // next three lines makes this class a Singleton
  static NotesWebServiceManager _instance = new NotesWebServiceManager.internal();
  NotesWebServiceManager.internal();
  factory NotesWebServiceManager() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  static final String _baseUrl = "http://notesapp2-001-site1.1tempurl.com/NotesService.svc/";
  //static final String baseUrl = "http://84.255.235.247:8282/Notes/NotesService/NotesService.svc/";
  static final String _loginMethodUrl = _baseUrl + "Login";
  static final String _registerNewUserMethodUrl = _baseUrl + "RegisterNewUser";
  static final String _synchronizeMethodUrl = _baseUrl + "Synchronize";
  static final String _pingMethodUrl = _baseUrl + "Ping";

  Future<String> login(String username, String password) async {
    await _ping();
    var body = jsonEncode({"username": username, "password": password});
    var headers = {"Content-Type": "application/json"};
    var jsonResult = await _post(_loginMethodUrl, body: body, headers: headers);
    var result = _decoder.convert(jsonResult);
    
    if (result["errorType"] != 1) throw new Exception(result["errorType"]);
    return result["result"];
  }

  Future<void> registerNewUser(String username, String password) async {
    await _ping();
    var body = jsonEncode({"username": username, "password": password});
    var headers = {"Content-Type": "application/json"};
    var jsonResult = await _post(_registerNewUserMethodUrl, body: body, headers: headers);
    var result = _decoder.convert(jsonResult);

    if (result["errorType"] != 1) throw new Exception(result["errorType"]);
  }

  Future<Synchronization> synchronize(Synchronization synchronization, String userToken) async {
    await _ping();
    var syncRequest = new WsSynchronization();
    syncRequest.notes = Collection(synchronization.notes).select((note) => WsNote.fromNote(note)).toList();
    syncRequest.lastSyncDateTime = synchronization.lastSyncDateTime;
    var body = json.encode(syncRequest);

    var headers =  {"Content-Type": "application/json", "Authorization": userToken };

    var jsonResult = await _post(_synchronizeMethodUrl, body: body, headers: headers);
    var resultMap = json.decode(jsonResult);
    var response = WsSyncResponse.fromJson(resultMap);

    if (response.errorType != WsErrorType.ok) throw Exception('ws napaka');

    var syncResult = new Synchronization();
    syncResult.lastSyncDateTime = response.result?.lastSyncDateTime;

    if (response.result.notes != null) {
       syncResult.notes = Collection(response.result.notes).select((note) => note.getNote()).toList();
    }
   
    return syncResult;
  }

  Future<String> _post(String url, {Map headers, body, encoding}) {
    return http
        .post(url, body: body, headers: headers, encoding: encoding)
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      return res;
    });
  }


  Future<Null> _ping() async {
    try {
       final client = new HttpClient();
       client.connectionTimeout = const Duration(seconds: 3);
       await client.getUrl(Uri.parse(_pingMethodUrl))
        .then((HttpClientRequest request) {
          return request.close();
        });
    } catch (err) {
      throw new Exception(WsErrorType.serverNotReachable);
    }
  }
}
