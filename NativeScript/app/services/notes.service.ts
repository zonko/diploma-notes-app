import { Injectable } from "@angular/core";
import { DatabaseService } from "~/services/database.service";
import { NotesWebServiceManagerService } from "~/services/notes-web-service-manager.service";
import { OperationResult } from "~/models/core/operation-result";
import { WsNote } from "~/models/web-service/ws-note";
import { Note } from "~/models/core/note";
import { WsErrorType } from "~/models/web-service/ws-error-type";
import { Converters } from "~/helpers/converters";
import {ErrorType} from "~/models/core/error-type";
import {NoteDisplay} from "~/models/core/note-display";
import {Helpers} from "~/helpers/helpers";

@Injectable()
export class NotesService {
    constructor(private _databaseService: DatabaseService,
                private _wsManagerService: NotesWebServiceManagerService) { 
    }

    public async synchronize(): Promise<OperationResult<null>> {
        const pingRes = await this._wsManagerService.ping();
        if (!pingRes.success) {
            return new OperationResult(false, null, ErrorType.wsServerNotReachable);
        }
        const dbNotesResult = await this._databaseService.listModifiedNotes();
        if(!dbNotesResult.success) return new OperationResult(false, null, dbNotesResult.errorType);

        const getLastSyncDateTimeResult = await this._databaseService.getValue("LastSyncDateTime");
        if(!getLastSyncDateTimeResult.success) return new OperationResult(false, null, getLastSyncDateTimeResult.errorType);

        const wsNotes = new Array<WsNote>();
        if (dbNotesResult.result) {
            for(let note of dbNotesResult.result) {
                wsNotes.push(Converters.toWsNote(note));
            }
        }

        const syncResult = await this._wsManagerService.synchronize(wsNotes, getLastSyncDateTimeResult.result).toPromise();

        if(syncResult.errorType != WsErrorType.ok) return new OperationResult(false, null, Converters.toCoreErrorType(syncResult.errorType));

        if (syncResult.result && syncResult.result.notes) {
            const notes = new Array<Note>();
            for (let wsNote of syncResult.result.notes) {
                notes.push(Converters.toCoreNote(wsNote));
            }
            const saveNotesResult = await this._databaseService.saveNotes(notes);
            if(!saveNotesResult.success) return new OperationResult(false, null, saveNotesResult.errorType);
           
        }
        if (syncResult.result && syncResult.result.lastSyncDateTime) {
            await this._databaseService.setValue("LastSyncDateTime", syncResult.result.lastSyncDateTime);
        }

        return new OperationResult(true);
    }

    public async saveNote(note: Note): Promise<OperationResult<number>> {
        note.dateTimeChanged = Helpers.getDateTimeString();
        note.modified = true;
        note.active = true;
        const saveResult = await this._databaseService.saveNote(note);
        if(!saveResult.success) return saveResult;

        const pingRes = await this._wsManagerService.ping();
        if (!pingRes.success) {
            return new OperationResult(false, saveResult.result, ErrorType.wsServerNotReachable);
        }

        const syncResult = await this.synchronize();
        return new OperationResult(syncResult.success, saveResult.result, syncResult.errorType, syncResult.errorMessage)
    }

    public async deactivateNote(noteId: number): Promise<OperationResult<null>> {
        const deactivationResult = await this._databaseService.deactivateNote(noteId);
        if(!deactivationResult.success) return deactivationResult;

        const pingRes = await this._wsManagerService.ping();
        if (!pingRes.success) {
            return new OperationResult(false, null, ErrorType.wsServerNotReachable);
        }

        return await this.synchronize();
    }

    public async getNote(id: number): Promise<OperationResult<Note>> {
        return await this._databaseService.getNote(id);
    }

    public async listNotes(): Promise<OperationResult<Array<NoteDisplay>>> {
        return await this._databaseService.listNotes();
    }
}