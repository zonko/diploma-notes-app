﻿namespace XForms.Notes.Core.WebService.Models.Responses
{
    public enum WsErrorType
    {
        Ok = 1,
        BadRequest = 2,
        InternalServerError = 3,
        UsernameIsTaken = 4,
        WrongUsernameOrPassword = 5,
        AuthorizationError = 6,
        ServerNotReachable = 15
    }
}