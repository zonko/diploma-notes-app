"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var imageSource = require("image-source");
var NoteAttachment = /** @class */ (function () {
    function NoteAttachment() {
    }
    // imageSource: ImageSource;
    NoteAttachment.prototype.getImageSource = function () {
        if (!this.content)
            return null;
        return imageSource.fromBase64(this.content);
    };
    return NoteAttachment;
}());
exports.NoteAttachment = NoteAttachment;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90ZS1hdHRhY2htZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibm90ZS1hdHRhY2htZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBRTFDO0lBQUE7SUFXQSxDQUFDO0lBTkcsNEJBQTRCO0lBRTVCLHVDQUFjLEdBQWQ7UUFDSSxFQUFFLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQzlCLE1BQU0sQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBQ0wscUJBQUM7QUFBRCxDQUFDLEFBWEQsSUFXQztBQVhZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbWFnZVNvdXJjZX0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvaW1hZ2Utc291cmNlXCI7XHJcbnZhciBpbWFnZVNvdXJjZSA9IHJlcXVpcmUoXCJpbWFnZS1zb3VyY2VcIik7XHJcblxyXG5leHBvcnQgY2xhc3MgTm90ZUF0dGFjaG1lbnQge1xyXG4gICAgbmFtZTogc3RyaW5nO1xyXG4gICAgdHlwZTogc3RyaW5nO1xyXG4gICAgY29udGVudDogc3RyaW5nO1xyXG5cclxuICAgIC8vIGltYWdlU291cmNlOiBJbWFnZVNvdXJjZTtcclxuXHJcbiAgICBnZXRJbWFnZVNvdXJjZSgpOiBJbWFnZVNvdXJjZSB7XHJcbiAgICAgICAgaWYoIXRoaXMuY29udGVudCkgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgcmV0dXJuIGltYWdlU291cmNlLmZyb21CYXNlNjQodGhpcy5jb250ZW50KTtcclxuICAgIH1cclxufSJdfQ==