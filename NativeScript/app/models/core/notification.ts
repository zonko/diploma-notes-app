export class Notification {
    user: string;
    noteId: number;

    constructor (user: string, noteId: number) {
        this.user = user;
        this.noteId = noteId;
    }
}