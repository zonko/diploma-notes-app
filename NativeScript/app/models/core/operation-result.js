"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var error_type_1 = require("~/models/core/error-type");
var OperationResult = /** @class */ (function () {
    function OperationResult(success, result, errorType, errorMessage) {
        if (result === void 0) { result = null; }
        if (errorType === void 0) { errorType = error_type_1.ErrorType.ok; }
        if (errorMessage === void 0) { errorMessage = ''; }
        this.success = success;
        this.errorType = errorType;
        this.errorMessage = errorMessage;
        this.result = result;
    }
    return OperationResult;
}());
exports.OperationResult = OperationResult;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9uLXJlc3VsdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm9wZXJhdGlvbi1yZXN1bHQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx1REFBcUQ7QUFFckQ7SUFNSSx5QkFBWSxPQUFnQixFQUFFLE1BQWdCLEVBQUUsU0FBbUMsRUFBRSxZQUF5QjtRQUFoRix1QkFBQSxFQUFBLGFBQWdCO1FBQUUsMEJBQUEsRUFBQSxZQUF1QixzQkFBUyxDQUFDLEVBQUU7UUFBRSw2QkFBQSxFQUFBLGlCQUF5QjtRQUMxRyxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUMzQixJQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztRQUNqQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN6QixDQUFDO0lBQ0wsc0JBQUM7QUFBRCxDQUFDLEFBWkQsSUFZQztBQVpZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRXJyb3JUeXBlIH0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvZXJyb3ItdHlwZVwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIE9wZXJhdGlvblJlc3VsdDxUPiB7XHJcbiAgICBzdWNjZXNzOiBib29sZWFuO1xyXG4gICAgZXJyb3JUeXBlOiBFcnJvclR5cGU7XHJcbiAgICBlcnJvck1lc3NhZ2U6IHN0cmluZztcclxuICAgIHJlc3VsdDogVDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihzdWNjZXNzOiBib29sZWFuLCByZXN1bHQ6IFQgPSBudWxsLCBlcnJvclR5cGU6IEVycm9yVHlwZSA9IEVycm9yVHlwZS5vaywgZXJyb3JNZXNzYWdlOiBzdHJpbmcgPSAnJykge1xyXG4gICAgICAgIHRoaXMuc3VjY2VzcyA9IHN1Y2Nlc3M7XHJcbiAgICAgICAgdGhpcy5lcnJvclR5cGUgPSBlcnJvclR5cGU7XHJcbiAgICAgICAgdGhpcy5lcnJvck1lc3NhZ2UgPSBlcnJvck1lc3NhZ2U7XHJcbiAgICAgICAgdGhpcy5yZXN1bHQgPSByZXN1bHQ7XHJcbiAgICB9XHJcbn0iXX0=