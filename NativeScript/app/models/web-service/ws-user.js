"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WsUser = /** @class */ (function () {
    function WsUser(username, password) {
        this.username = username;
        this.password = password;
    }
    return WsUser;
}());
exports.WsUser = WsUser;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid3MtdXNlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIndzLXVzZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtJQUlJLGdCQUFZLFFBQWdCLEVBQUUsUUFBZ0I7UUFDMUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7SUFDN0IsQ0FBQztJQUNMLGFBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQztBQVJZLHdCQUFNIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFdzVXNlciB7XHJcbiAgICB1c2VybmFtZTogc3RyaW5nO1xyXG4gICAgcGFzc3dvcmQ6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy51c2VybmFtZSA9IHVzZXJuYW1lO1xyXG4gICAgICAgIHRoaXMucGFzc3dvcmQgPSBwYXNzd29yZDtcclxuICAgIH1cclxufSJdfQ==