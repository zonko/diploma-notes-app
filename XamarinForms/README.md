# Xamarin Forms NotesApp

Simple application for saving notes, developed using Xamarin Forms.

## Getting Started

Application was developed with Xamarin Forms and C# programming language.
Project/Solution can be opened and run on both iOS and Android, with Visual 
studio, for iOS a connection to a Mac computer or Visual Studio for Mac is 
required.

### Folder Content

- XForms.Notes.Android: Contains Android host application (Xamarin Android without Forms, C#)
- XForms.Notes.Core: Contains application logic (C#)
- XForms.Notes.Shared: Contains code shared between all supported platforms, accessed through host applications (C#)
- XForms.Notes.UI: Contains Xamarin Forms user interface implementation (C#)
- XForms.Notes.Wpf: Contains WPF host application (WPF, C#)
- XForms.Notes.iOS: Contains iOS host application (Xamarin iOS without Forms, C#)
- scr: Contains finished applications screenshots