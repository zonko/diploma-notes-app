﻿using System.Collections.Generic;
using Newtonsoft.Json;
using XForms.Notes.Core.WebService.Models.Notes;

namespace XForms.Notes.Core.WebService.Models
{
    internal class Synchronization
    {
        [JsonProperty("lastSyncDateTime")]
        public string LastSyncDateTime { get; set; }

        [JsonProperty("notes")]
        public List<WsNote> Notes { get; set; }
    }
}