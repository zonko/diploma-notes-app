class NotesNotification {
  String user;
  int noteId;

  NotesNotification();

  Map<String, dynamic> toDbMap() {
    var map = <String, dynamic> {
      'User': user,
      'NoteId': noteId
    };
    return map;
  }

  NotesNotification.fromDbMap(Map<String, dynamic> map) {
    user = map['User'];
    noteId = map['NoteId'];
  }
}