"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DialogOptions = /** @class */ (function () {
    function DialogOptions(title, text, okButtonText) {
        this.title = title;
        this.text = text;
        this.okButtonText = okButtonText;
    }
    return DialogOptions;
}());
exports.DialogOptions = DialogOptions;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLW9wdGlvbnMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkaWFsb2ctb3B0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBS0ksdUJBQWEsS0FBYSxFQUFFLElBQVksRUFBRSxZQUFvQjtRQUMxRCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztJQUNyQyxDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQUFDLEFBVkQsSUFVQztBQVZZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIERpYWxvZ09wdGlvbnMge1xyXG4gICAgdGl0bGU6IHN0cmluZztcclxuICAgIHRleHQ6IHN0cmluZztcclxuICAgIG9rQnV0dG9uVGV4dDogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yICh0aXRsZTogc3RyaW5nLCB0ZXh0OiBzdHJpbmcsIG9rQnV0dG9uVGV4dDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy50aXRsZSA9IHRpdGxlO1xyXG4gICAgICAgIHRoaXMudGV4dCA9IHRleHQ7XHJcbiAgICAgICAgdGhpcy5va0J1dHRvblRleHQgPSBva0J1dHRvblRleHQ7XHJcbiAgICB9XHJcbn0iXX0=