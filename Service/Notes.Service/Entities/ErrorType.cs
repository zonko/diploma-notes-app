﻿namespace Notes.Service.Entities
{
    public enum ErrorType
    {
        Ok = 1,
        BadRequest = 2,
        InternalServerError = 3,
        UsernameIsTaken = 4,
        WrongUsernameOrPassword = 5,
        AuthorizationError = 6
    }
}