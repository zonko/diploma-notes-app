"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_angular_1 = require("nativescript-angular");
var SetNotificationComponent = /** @class */ (function () {
    function SetNotificationComponent(params) {
        this.params = params;
        this._selectedDateTime = new Date(Date.now());
    }
    SetNotificationComponent.prototype.ngOnInit = function () {
    };
    SetNotificationComponent.prototype.onClose = function () {
        this.params.closeCallback(null);
    };
    SetNotificationComponent.prototype.onConfirm = function () {
        this.params.closeCallback(this._selectedDateTime);
    };
    SetNotificationComponent.prototype.onDatePickerLoaded = function (args) {
        var datePicker = args.object;
        datePicker.date = this._selectedDateTime;
    };
    SetNotificationComponent.prototype.onDateChanged = function (args) {
        this._selectedDateTime.setFullYear(args.value.getFullYear());
        this._selectedDateTime.setMonth(args.value.getMonth());
        this._selectedDateTime.setDate(args.value.getDate());
    };
    SetNotificationComponent.prototype.onTimePickerLoaded = function (args) {
        var timePicker = args.object;
        timePicker.time = this._selectedDateTime;
    };
    SetNotificationComponent.prototype.onTimePickerTimeChanged = function (args) {
        this._selectedDateTime.setHours(args.value.getHours());
        this._selectedDateTime.setMinutes(args.value.getMinutes());
    };
    SetNotificationComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-set-notification',
            templateUrl: './set-notification.component.html',
            styleUrls: ['./set-notification.component.css']
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.ModalDialogParams])
    ], SetNotificationComponent);
    return SetNotificationComponent;
}());
exports.SetNotificationComponent = SetNotificationComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0LW5vdGlmaWNhdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzZXQtbm90aWZpY2F0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHNDQUFnRDtBQUNoRCw2REFBdUQ7QUFVdkQ7SUFHSSxrQ0FBb0IsTUFBeUI7UUFBekIsV0FBTSxHQUFOLE1BQU0sQ0FBbUI7UUFGckMsc0JBQWlCLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFHakQsQ0FBQztJQUVELDJDQUFRLEdBQVI7SUFFQSxDQUFDO0lBRU0sMENBQU8sR0FBZDtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFTSw0Q0FBUyxHQUFoQjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFHRCxxREFBa0IsR0FBbEIsVUFBbUIsSUFBSTtRQUNuQixJQUFJLFVBQVUsR0FBZSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3pDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO0lBQzdDLENBQUM7SUFFRCxnREFBYSxHQUFiLFVBQWMsSUFBSTtRQUNkLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRCxxREFBa0IsR0FBbEIsVUFBbUIsSUFBSTtRQUNuQixJQUFJLFVBQVUsR0FBZSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3pDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO0lBQzdDLENBQUM7SUFFRCwwREFBdUIsR0FBdkIsVUFBd0IsSUFBSTtRQUN4QixJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBdENRLHdCQUF3QjtRQU5wQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsV0FBVyxFQUFFLG1DQUFtQztZQUNoRCxTQUFTLEVBQUUsQ0FBQyxrQ0FBa0MsQ0FBQztTQUNsRCxDQUFDO3lDQUk4Qix3Q0FBaUI7T0FIcEMsd0JBQXdCLENBdUNwQztJQUFELCtCQUFDO0NBQUEsQUF2Q0QsSUF1Q0M7QUF2Q1ksNERBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7TW9kYWxEaWFsb2dQYXJhbXN9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhclwiO1xyXG5pbXBvcnQge0RhdGVQaWNrZXJ9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RhdGUtcGlja2VyXCI7XHJcbmltcG9ydCB7VGltZVBpY2tlcn0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvdGltZS1waWNrZXJcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnYXBwLXNldC1ub3RpZmljYXRpb24nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3NldC1ub3RpZmljYXRpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vc2V0LW5vdGlmaWNhdGlvbi5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFNldE5vdGlmaWNhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBwcml2YXRlIF9zZWxlY3RlZERhdGVUaW1lID0gbmV3IERhdGUoRGF0ZS5ub3coKSk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBwYXJhbXM6IE1vZGFsRGlhbG9nUGFyYW1zKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBvbkNsb3NlKCkge1xyXG4gICAgICAgIHRoaXMucGFyYW1zLmNsb3NlQ2FsbGJhY2sobnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uQ29uZmlybSgpIHtcclxuICAgICAgICB0aGlzLnBhcmFtcy5jbG9zZUNhbGxiYWNrKHRoaXMuX3NlbGVjdGVkRGF0ZVRpbWUpO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBvbkRhdGVQaWNrZXJMb2FkZWQoYXJncykge1xyXG4gICAgICAgIGxldCBkYXRlUGlja2VyID0gPERhdGVQaWNrZXI+YXJncy5vYmplY3Q7XHJcbiAgICAgICAgZGF0ZVBpY2tlci5kYXRlID0gdGhpcy5fc2VsZWN0ZWREYXRlVGltZTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRhdGVDaGFuZ2VkKGFyZ3MpIHtcclxuICAgICAgICB0aGlzLl9zZWxlY3RlZERhdGVUaW1lLnNldEZ1bGxZZWFyKGFyZ3MudmFsdWUuZ2V0RnVsbFllYXIoKSk7XHJcbiAgICAgICAgdGhpcy5fc2VsZWN0ZWREYXRlVGltZS5zZXRNb250aChhcmdzLnZhbHVlLmdldE1vbnRoKCkpO1xyXG4gICAgICAgIHRoaXMuX3NlbGVjdGVkRGF0ZVRpbWUuc2V0RGF0ZShhcmdzLnZhbHVlLmdldERhdGUoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25UaW1lUGlja2VyTG9hZGVkKGFyZ3MpIHtcclxuICAgICAgICBsZXQgdGltZVBpY2tlciA9IDxUaW1lUGlja2VyPmFyZ3Mub2JqZWN0O1xyXG4gICAgICAgIHRpbWVQaWNrZXIudGltZSA9IHRoaXMuX3NlbGVjdGVkRGF0ZVRpbWU7XHJcbiAgICB9XHJcblxyXG4gICAgb25UaW1lUGlja2VyVGltZUNoYW5nZWQoYXJncykge1xyXG4gICAgICAgIHRoaXMuX3NlbGVjdGVkRGF0ZVRpbWUuc2V0SG91cnMoYXJncy52YWx1ZS5nZXRIb3VycygpKTtcclxuICAgICAgICB0aGlzLl9zZWxlY3RlZERhdGVUaW1lLnNldE1pbnV0ZXMoYXJncy52YWx1ZS5nZXRNaW51dGVzKCkpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==