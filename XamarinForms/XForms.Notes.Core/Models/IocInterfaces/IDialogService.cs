﻿using System;

namespace XForms.Notes.Core.Models.IocInterfaces
{
    public interface IDialogService
    {
        void SetBusy(bool busy);
        void ShowAlert(string title, string text);

        event Action<bool?> SetMasterPagePresented;
        void InvokeSetMasterPagePresented(bool? presented);
    }
}