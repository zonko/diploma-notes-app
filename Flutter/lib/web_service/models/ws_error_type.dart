class WsErrorType {
  static const int ok = 1;
  static const int badRequest = 2;
  static const int internalServerError = 3;
  static const int usernameIsTaken = 4;
  static const int wrongUsernameOrPassword = 5;
  static const int authorizationError = 6;
  static const int serverNotReachable = 15;
}