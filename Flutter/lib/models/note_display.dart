import 'package:intl/intl.dart';

class NoteDisplay {
  int id;
  String title;
  DateTime dateTimeChanged;

  final DateFormat _dateFormat = new DateFormat('yyyy_MM_dd_HH_mm_ss');

  NoteDisplay.fromDbNoteMap(Map<String, dynamic> map) {
    id = map['Id'];
    title = map['Title'];
    dateTimeChanged = _dateFormat.parse(map['DateTimeChanged']);
  }
}