import 'dart:async';

import 'package:ft_notes_app/helpers/error_message_helper.dart';
import 'package:ft_notes_app/logic/notes_logic.dart';
import 'package:ft_notes_app/logic/user_logic.dart';

abstract class LoginScreenContract {
  void onLoginSuccess();
  Future<Null> onError(String errorTxt);
}

class LoginScreenPresenter {
  LoginScreenContract _view;
  final UserLogic _userLogic = new UserLogic();
  final NotesLogic _notesLogic = new NotesLogic();
  LoginScreenPresenter(this._view);

  doLogin(String username, String password) async {
    try {
      await _userLogic.login(username, password);
      try {
        await _notesLogic.synchronize();
      } catch (syncError) {
        await _view.onError('Sinhronizacija ni uspela. ' + ErrorMessageHelper.getErrorMessage(syncError));
      }
      _view.onLoginSuccess();
    } catch (error) {
      await _view.onError('Prijava ni uspela. ' + ErrorMessageHelper.getErrorMessage(error));
    }
  }
}