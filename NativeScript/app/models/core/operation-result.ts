import { ErrorType } from "~/models/core/error-type";

export class OperationResult<T> {
    success: boolean;
    errorType: ErrorType;
    errorMessage: string;
    result: T;

    constructor(success: boolean, result: T = null, errorType: ErrorType = ErrorType.ok, errorMessage: string = '') {
        this.success = success;
        this.errorType = errorType;
        this.errorMessage = errorMessage;
        this.result = result;
    }
}