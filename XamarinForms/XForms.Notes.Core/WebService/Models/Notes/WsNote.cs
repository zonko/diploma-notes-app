﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace XForms.Notes.Core.WebService.Models.Notes
{
    internal class WsNote
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("clientId")]
        public int? ClientId { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("dateTimeChanged")]
        public string DateTimeChanged { get; set; }

        [JsonProperty("attachments")]
        public List<WsNoteAttachment> Attachments { get; set; }
    }
}