export class WsNoteAttachment {
    name: string;
    type: string;
    content: string;
}