﻿using MvvmCross;
using MvvmCross.Forms.Presenters.Attributes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XForms.Notes.Core.Models.IocInterfaces;

namespace XForms.Notes.UI.Pages.Main
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	[MvxMasterDetailPagePresentation(Position = MasterDetailPosition.Master, WrapInNavigationPage = false, NoHistory = false, Animated = false)]
    public partial class MasterPage
	{
		public MasterPage ()
		{
			InitializeComponent();

		    Mvx.Resolve<IDialogService>().SetMasterPagePresented += OnSetMasterPagePresented;
#if __IOS__
            if(Parent is MasterDetailPage master)
                master.IsGestureEnabled = false;
#endif
		}

	    private void OnSetMasterPagePresented(bool? presented)
	    {
	        if (Parent is MasterDetailPage master)
	        {
	            master.IsPresented = presented ?? !master.IsPresented;
	        }
	    }

	    private void OnHideMasterClicked()
	    {
	        OnSetMasterPagePresented(false);
	    }
	}
}