﻿using Newtonsoft.Json;

namespace XForms.Notes.Core.WebService.Models.Notes
{
    internal class WsNoteAttachment
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }
    }
}