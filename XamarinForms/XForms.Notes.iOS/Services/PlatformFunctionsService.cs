﻿using SQLite;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Shared;

namespace XForms.Notes.iOS.Services
{
    public class PlatformFunctionsService : IPlatformFunctionsService
    {
        public SQLiteConnection GetDatabaseConnection(string filename)
        {
            return SharedPlatformFunctions.GetDatabaseConnection(filename);
        }
    }
}