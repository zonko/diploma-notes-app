"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("~/services/user.service");
var helpers_1 = require("~/helpers/helpers");
var dialog_service_1 = require("~/services/dialog.service");
var router_1 = require("@angular/router");
var RegistrationComponent = /** @class */ (function () {
    function RegistrationComponent(_userService, _dialogService, _router) {
        this._userService = _userService;
        this._dialogService = _dialogService;
        this._router = _router;
        this.isBusy = false;
    }
    RegistrationComponent.prototype.ngOnInit = function () { };
    RegistrationComponent.prototype.onRegisterNewUser = function () {
        var _this = this;
        if (helpers_1.Helpers.isNullOrEmpty(this.username) || helpers_1.Helpers.isNullOrEmpty(this.password)) {
            this._dialogService.showMessageDialog("Registracija", "Vnesite uporabniško ime in geslo.");
            return;
        }
        if (this.password !== this.confirmPassword) {
            this._dialogService.showMessageDialog("Registracija", "Geslo se ne ujema s potrditvenim geslom.");
            return;
        }
        this.isBusy = true;
        this._userService.registerNewUser(this.username, this.password).then(function (res) {
            _this.isBusy = false;
            _this.username = '';
            _this.password = '';
            _this.confirmPassword = '';
            if (!res.success) {
                _this._dialogService.showMessageDialog("Napaka", "Registracija ni uspela: " + helpers_1.Helpers.getErrorMessage(res.errorType));
            }
            else {
                _this._dialogService.showMessageDialog("Registracija", "Registracija je bila uspešna. Sedaj se lahko prijavite v aplikacijo.");
                _this._router.navigateByUrl("login");
            }
        }).catch(function (err) {
            _this.isBusy = false;
            _this.username = '';
            _this.password = '';
            _this.confirmPassword = '';
            _this._dialogService.showMessageDialog("Napaka", "Registracija ni uspela. Prišlo je do nepričakovane napake: " + err);
        });
    };
    RegistrationComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-registration',
            templateUrl: './registration.component.html',
            styleUrls: ['./registration.component.css']
        }),
        __metadata("design:paramtypes", [user_service_1.UserService,
            dialog_service_1.DialogService,
            router_1.Router])
    ], RegistrationComponent);
    return RegistrationComponent;
}());
exports.RegistrationComponent = RegistrationComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaXN0cmF0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInJlZ2lzdHJhdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBa0Q7QUFDbEQsd0RBQXNEO0FBQ3RELDZDQUEwQztBQUMxQyw0REFBd0Q7QUFDeEQsMENBQXVDO0FBUXZDO0lBTUksK0JBQW9CLFlBQXlCLEVBQ3pCLGNBQTZCLEVBQzdCLE9BQWU7UUFGZixpQkFBWSxHQUFaLFlBQVksQ0FBYTtRQUN6QixtQkFBYyxHQUFkLGNBQWMsQ0FBZTtRQUM3QixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBUG5DLFdBQU0sR0FBRyxLQUFLLENBQUM7SUFPd0IsQ0FBQztJQUV4Qyx3Q0FBUSxHQUFSLGNBQWEsQ0FBQztJQUVQLGlEQUFpQixHQUF4QjtRQUFBLGlCQTZCQztRQTVCRyxFQUFFLENBQUMsQ0FBQyxpQkFBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksaUJBQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvRSxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLGNBQWMsRUFBRSxtQ0FBbUMsQ0FBQyxDQUFDO1lBQzNGLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFLDBDQUEwQyxDQUFDLENBQUM7WUFDbEcsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDcEUsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEIsS0FBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbkIsS0FBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbkIsS0FBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFDMUIsRUFBRSxDQUFBLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDZCxLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSwwQkFBMEIsR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUN6SCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsc0VBQXNFLENBQUMsQ0FBQztnQkFDOUgsS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEdBQUc7WUFDUixLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwQixLQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNuQixLQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNuQixLQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztZQUMxQixLQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSw2REFBNkQsR0FBRyxHQUFHLENBQUMsQ0FBQztRQUN6SCxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUF6Q1EscUJBQXFCO1FBTmpDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixXQUFXLEVBQUUsK0JBQStCO1lBQzVDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO1NBQzlDLENBQUM7eUNBT29DLDBCQUFXO1lBQ1QsOEJBQWE7WUFDcEIsZUFBTTtPQVIxQixxQkFBcUIsQ0EyQ2pDO0lBQUQsNEJBQUM7Q0FBQSxBQTNDRCxJQTJDQztBQTNDWSxzREFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJ34vc2VydmljZXMvdXNlci5zZXJ2aWNlJztcclxuaW1wb3J0IHtIZWxwZXJzfSBmcm9tIFwifi9oZWxwZXJzL2hlbHBlcnNcIjtcclxuaW1wb3J0IHtEaWFsb2dTZXJ2aWNlfSBmcm9tIFwifi9zZXJ2aWNlcy9kaWFsb2cuc2VydmljZVwiO1xyXG5pbXBvcnQge1JvdXRlcn0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdhcHAtcmVnaXN0cmF0aW9uJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9yZWdpc3RyYXRpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUmVnaXN0cmF0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGlzQnVzeSA9IGZhbHNlO1xyXG4gICAgdXNlcm5hbWU6IHN0cmluZztcclxuICAgIHBhc3N3b3JkOiBzdHJpbmc7XHJcbiAgICBjb25maXJtUGFzc3dvcmQ6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF91c2VyU2VydmljZTogVXNlclNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9kaWFsb2dTZXJ2aWNlOiBEaWFsb2dTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIpIHsgfVxyXG5cclxuICAgIG5nT25Jbml0KCkgeyB9XHJcblxyXG4gICAgcHVibGljIG9uUmVnaXN0ZXJOZXdVc2VyKCkge1xyXG4gICAgICAgIGlmIChIZWxwZXJzLmlzTnVsbE9yRW1wdHkodGhpcy51c2VybmFtZSkgfHwgSGVscGVycy5pc051bGxPckVtcHR5KHRoaXMucGFzc3dvcmQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJSZWdpc3RyYWNpamFcIiwgXCJWbmVzaXRlIHVwb3JhYm5pxaFrbyBpbWUgaW4gZ2VzbG8uXCIpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnBhc3N3b3JkICE9PSB0aGlzLmNvbmZpcm1QYXNzd29yZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiUmVnaXN0cmFjaWphXCIsIFwiR2VzbG8gc2UgbmUgdWplbWEgcyBwb3RyZGl0dmVuaW0gZ2VzbG9tLlwiKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmlzQnVzeSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5fdXNlclNlcnZpY2UucmVnaXN0ZXJOZXdVc2VyKHRoaXMudXNlcm5hbWUsIHRoaXMucGFzc3dvcmQpLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgICAgdGhpcy5pc0J1c3kgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy51c2VybmFtZSA9ICcnO1xyXG4gICAgICAgICAgICB0aGlzLnBhc3N3b3JkID0gJyc7XHJcbiAgICAgICAgICAgIHRoaXMuY29uZmlybVBhc3N3b3JkID0gJyc7XHJcbiAgICAgICAgICAgIGlmKCFyZXMuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zaG93TWVzc2FnZURpYWxvZyhcIk5hcGFrYVwiLCBcIlJlZ2lzdHJhY2lqYSBuaSB1c3BlbGE6IFwiICsgSGVscGVycy5nZXRFcnJvck1lc3NhZ2UocmVzLmVycm9yVHlwZSkpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zaG93TWVzc2FnZURpYWxvZyhcIlJlZ2lzdHJhY2lqYVwiLCBcIlJlZ2lzdHJhY2lqYSBqZSBiaWxhIHVzcGXFoW5hLiBTZWRhaiBzZSBsYWhrbyBwcmlqYXZpdGUgdiBhcGxpa2FjaWpvLlwiKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZUJ5VXJsKFwibG9naW5cIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmlzQnVzeSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJuYW1lID0gJyc7XHJcbiAgICAgICAgICAgIHRoaXMucGFzc3dvcmQgPSAnJztcclxuICAgICAgICAgICAgdGhpcy5jb25maXJtUGFzc3dvcmQgPSAnJztcclxuICAgICAgICAgICAgdGhpcy5fZGlhbG9nU2VydmljZS5zaG93TWVzc2FnZURpYWxvZyhcIk5hcGFrYVwiLCBcIlJlZ2lzdHJhY2lqYSBuaSB1c3BlbGEuIFByacWhbG8gamUgZG8gbmVwcmnEjWFrb3ZhbmUgbmFwYWtlOiBcIiArIGVycik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=