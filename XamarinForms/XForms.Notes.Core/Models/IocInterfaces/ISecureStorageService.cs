﻿using System.Threading.Tasks;

namespace XForms.Notes.Core.Models.IocInterfaces
{
    public interface ISecureStorageService
    {
        Task SaveUserToken(string userToken);
        Task SaveUser(string user);
        Task<string> GetUserToken();
        Task<string> GetUser();
    }
}