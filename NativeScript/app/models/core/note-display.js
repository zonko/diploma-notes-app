"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NoteDisplay = /** @class */ (function () {
    function NoteDisplay(id, title, dateTimeChanged) {
        this.id = id;
        this.title = title;
        this.dateTimeChanged = dateTimeChanged;
    }
    return NoteDisplay;
}());
exports.NoteDisplay = NoteDisplay;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90ZS1kaXNwbGF5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibm90ZS1kaXNwbGF5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7SUFLSSxxQkFBYSxFQUFVLEVBQUUsS0FBYSxFQUFFLGVBQXVCO1FBQzNELElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDO1FBQ2IsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7SUFDM0MsQ0FBQztJQUNMLGtCQUFDO0FBQUQsQ0FBQyxBQVZELElBVUM7QUFWWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBOb3RlRGlzcGxheSB7XHJcbiAgICBpZDogbnVtYmVyO1xyXG4gICAgdGl0bGU6IHN0cmluZztcclxuICAgIGRhdGVUaW1lQ2hhbmdlZDogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yIChpZDogbnVtYmVyLCB0aXRsZTogc3RyaW5nLCBkYXRlVGltZUNoYW5nZWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuaWQgPSBpZDtcclxuICAgICAgICB0aGlzLnRpdGxlID0gdGl0bGU7XHJcbiAgICAgICAgdGhpcy5kYXRlVGltZUNoYW5nZWQgPSBkYXRlVGltZUNoYW5nZWQ7XHJcbiAgICB9XHJcbn0iXX0=