﻿using System;
using System.IO;
using System.Threading.Tasks;
using XForms.Notes.Core.Models.IocInterfaces;

namespace XForms.Notes.Wpf.Services
{
    public class SecureStorageService : ISecureStorageService
    {
        private string _userToken;
        private string _user;

        public Task SaveUserToken(string userToken)
        {
            File.WriteAllText("UserToken.txt", userToken);
            return Task.Run(() => _userToken = userToken);
        }

        public Task SaveUser(string user)
        {
            File.WriteAllText("User.txt", user);
            return Task.Run(() => _user = user);
        }

        public Task<string> GetUserToken()
        {
            try
            {
                var token = _userToken ?? File.ReadAllText("UserToken.txt");
                return Task.Run(() => token);
            }
            catch (Exception e)
            {
                return Task.Run(() => _userToken);
            }
        }

        public Task<string> GetUser()
        {
            try
            {
                var user = _user ?? File.ReadAllText("User.txt");
                return Task.Run(() => user);
            }
            catch (Exception e)
            {
                return Task.Run(() => _user);
            }
        }
    }
}