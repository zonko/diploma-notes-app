import {ImageSource} from "tns-core-modules/image-source";
var imageSource = require("image-source");

export class NoteAttachment {
    name: string;
    type: string;
    content: string;

    // imageSource: ImageSource;

    getImageSource(): ImageSource {
        if(!this.content) return null;
        return imageSource.fromBase64(this.content);
    }
}