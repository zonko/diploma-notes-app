import 'dart:convert';
import 'package:ft_notes_app/models/note_attachment.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ws_note_attachment.g.dart';

@JsonSerializable()
class WsNoteAttachment {
  String name;
  String type;
  String content;

  WsNoteAttachment();

  WsNoteAttachment.fromNoteAttachment(NoteAttachment attachment){
    name = attachment.name;
    type = attachment.type;
    content = base64Encode(attachment.content);
  }

  NoteAttachment getNoteAttachment() {
    var attachment = new NoteAttachment();
    attachment.type = type;
    attachment.name = name;
    attachment.content = base64Decode(content);

    return attachment;
  }

  factory WsNoteAttachment.fromJson(Map<String, dynamic> json) => _$WsNoteAttachmentFromJson(json);

  toJson() => _$WsNoteAttachmentToJson(this);
}