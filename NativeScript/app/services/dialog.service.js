"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/internal/Subject");
var dialog_options_1 = require("~/models/core/dialog-options");
var DialogService = /** @class */ (function () {
    function DialogService() {
        this._showMessageDialogSource = new Subject_1.Subject();
        this._setBusyIndicatorSource = new Subject_1.Subject();
        this.showMessageDialog$ = this._showMessageDialogSource.asObservable();
        this.setBusyIndicator$ = this._setBusyIndicatorSource.asObservable();
    }
    DialogService.prototype.setBusy = function (busy) {
        this._setBusyIndicatorSource.next(busy);
    };
    DialogService.prototype.showMessageDialog = function (title, message, buttonText) {
        if (buttonText === void 0) { buttonText = "OK"; }
        var options = new dialog_options_1.DialogOptions(title, message, buttonText);
        this._showMessageDialogSource.next(options);
    };
    DialogService = __decorate([
        core_1.Injectable()
    ], DialogService);
    return DialogService;
}());
exports.DialogService = DialogService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkaWFsb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLHNDQUEyQztBQUMzQyxpREFBOEM7QUFDOUMsK0RBQTJEO0FBRzNEO0lBREE7UUFFWSw2QkFBd0IsR0FBRyxJQUFJLGlCQUFPLEVBQWlCLENBQUM7UUFDeEQsNEJBQXVCLEdBQUcsSUFBSSxpQkFBTyxFQUFXLENBQUM7UUFFbEQsdUJBQWtCLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ2xFLHNCQUFpQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQVUzRSxDQUFDO0lBUlUsK0JBQU8sR0FBZCxVQUFlLElBQWE7UUFDeEIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRU0seUNBQWlCLEdBQXhCLFVBQXlCLEtBQWEsRUFBRSxPQUFlLEVBQUUsVUFBeUI7UUFBekIsMkJBQUEsRUFBQSxpQkFBeUI7UUFDOUUsSUFBSSxPQUFPLEdBQUcsSUFBSSw4QkFBYSxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBZFEsYUFBYTtRQUR6QixpQkFBVSxFQUFFO09BQ0EsYUFBYSxDQWV6QjtJQUFELG9CQUFDO0NBQUEsQUFmRCxJQWVDO0FBZlksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtTdWJqZWN0fSBmcm9tIFwicnhqcy9pbnRlcm5hbC9TdWJqZWN0XCI7XHJcbmltcG9ydCB7RGlhbG9nT3B0aW9uc30gZnJvbSBcIn4vbW9kZWxzL2NvcmUvZGlhbG9nLW9wdGlvbnNcIjtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIERpYWxvZ1NlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSBfc2hvd01lc3NhZ2VEaWFsb2dTb3VyY2UgPSBuZXcgU3ViamVjdDxEaWFsb2dPcHRpb25zPigpO1xyXG4gICAgcHJpdmF0ZSBfc2V0QnVzeUluZGljYXRvclNvdXJjZSA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gICAgcHVibGljIHNob3dNZXNzYWdlRGlhbG9nJCA9IHRoaXMuX3Nob3dNZXNzYWdlRGlhbG9nU291cmNlLmFzT2JzZXJ2YWJsZSgpO1xyXG4gICAgcHVibGljIHNldEJ1c3lJbmRpY2F0b3IkID0gdGhpcy5fc2V0QnVzeUluZGljYXRvclNvdXJjZS5hc09ic2VydmFibGUoKTtcclxuXHJcbiAgICBwdWJsaWMgc2V0QnVzeShidXN5OiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5fc2V0QnVzeUluZGljYXRvclNvdXJjZS5uZXh0KGJ1c3kpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaG93TWVzc2FnZURpYWxvZyh0aXRsZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcsIGJ1dHRvblRleHQ6IHN0cmluZyA9IFwiT0tcIik6IHZvaWQge1xyXG4gICAgICAgIHZhciBvcHRpb25zID0gbmV3IERpYWxvZ09wdGlvbnModGl0bGUsIG1lc3NhZ2UsIGJ1dHRvblRleHQpO1xyXG4gICAgICAgIHRoaXMuX3Nob3dNZXNzYWdlRGlhbG9nU291cmNlLm5leHQob3B0aW9ucyk7XHJcbiAgICB9XHJcbn0iXX0=