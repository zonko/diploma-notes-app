"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var notes_web_service_manager_service_1 = require("~/services/notes-web-service-manager.service");
var secure_storage_service_1 = require("~/services/secure-storage.service");
var converters_1 = require("~/helpers/converters");
var ws_error_type_1 = require("~/models/web-service/ws-error-type");
var operation_result_1 = require("~/models/core/operation-result");
var database_service_1 = require("~/services/database.service");
var notes_service_1 = require("~/services/notes.service");
var error_type_1 = require("~/models/core/error-type");
var UserService = /** @class */ (function () {
    function UserService(_notesWebServiceManager, _secureStorageService, _databaseService, _notesService) {
        this._notesWebServiceManager = _notesWebServiceManager;
        this._secureStorageService = _secureStorageService;
        this._databaseService = _databaseService;
        this._notesService = _notesService;
    }
    UserService.prototype.login = function (username, password) {
        return __awaiter(this, void 0, void 0, function () {
            var pingRes, loginResult;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._notesWebServiceManager.ping()];
                    case 1:
                        pingRes = _a.sent();
                        if (!pingRes.success) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.wsServerNotReachable)];
                        }
                        return [4 /*yield*/, this._notesWebServiceManager.login(username, password).toPromise()];
                    case 2:
                        loginResult = _a.sent();
                        if (loginResult.errorType != ws_error_type_1.WsErrorType.ok)
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, converters_1.Converters.toCoreErrorType(loginResult.errorType), loginResult.errorMessage)];
                        this._secureStorageService.setUserToken(loginResult.result);
                        this._secureStorageService.setUser(username);
                        return [2 /*return*/, this.prepareDatabaseAndSynchronize()];
                }
            });
        });
    };
    UserService.prototype.registerNewUser = function (username, password) {
        return __awaiter(this, void 0, void 0, function () {
            var pingRes, res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._notesWebServiceManager.ping()];
                    case 1:
                        pingRes = _a.sent();
                        if (!pingRes.success) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.wsServerNotReachable)];
                        }
                        return [4 /*yield*/, this._notesWebServiceManager.registerNewUser(username, password).toPromise()];
                    case 2:
                        res = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(res.errorType === ws_error_type_1.WsErrorType.ok, null, converters_1.Converters.toCoreErrorType(res.errorType), res.errorMessage)];
                }
            });
        });
    };
    UserService.prototype.checkLogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var userToken, user, syncPrepareResult;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        userToken = this._secureStorageService.getUserToken();
                        user = this._secureStorageService.getUser();
                        if (!userToken || !user)
                            return [2 /*return*/, new operation_result_1.OperationResult(true, false)];
                        return [4 /*yield*/, this.prepareDatabaseAndSynchronize()];
                    case 1:
                        syncPrepareResult = _a.sent();
                        if (!syncPrepareResult.success) {
                            if (syncPrepareResult.errorType >= error_type_1.ErrorType.dbUserNotLogedIn) {
                                this._secureStorageService.setUser('');
                                this._secureStorageService.setUserToken('');
                            }
                            return [2 /*return*/, new operation_result_1.OperationResult(false, syncPrepareResult.errorType < error_type_1.ErrorType.dbUserNotLogedIn, syncPrepareResult.errorType, syncPrepareResult.errorMessage)];
                        }
                        else {
                            return [2 /*return*/, new operation_result_1.OperationResult(true, true)];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    UserService.prototype.prepareDatabaseAndSynchronize = function () {
        return __awaiter(this, void 0, void 0, function () {
            var dbInitResult;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._databaseService.initializeDatabase()];
                    case 1:
                        dbInitResult = _a.sent();
                        if (!dbInitResult.success)
                            return [2 /*return*/, dbInitResult];
                        return [4 /*yield*/, this._notesService.synchronize()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UserService.prototype.logout = function () {
        this._secureStorageService.setUser('');
        this._secureStorageService.setUserToken('');
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [notes_web_service_manager_service_1.NotesWebServiceManagerService,
            secure_storage_service_1.SecureStorageService,
            database_service_1.DatabaseService,
            notes_service_1.NotesService])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxzQ0FBMkM7QUFDM0Msa0dBQTZGO0FBQzdGLDRFQUF5RTtBQUN6RSxtREFBa0Q7QUFDbEQsb0VBQWlFO0FBQ2pFLG1FQUFpRTtBQUNqRSxnRUFBOEQ7QUFDOUQsMERBQXdEO0FBQ3hELHVEQUFtRDtBQUduRDtJQUVJLHFCQUFvQix1QkFBc0QsRUFDdEQscUJBQTJDLEVBQzNDLGdCQUFpQyxFQUNqQyxhQUEyQjtRQUgzQiw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQStCO1FBQ3RELDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBc0I7UUFDM0MscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFpQjtRQUNqQyxrQkFBYSxHQUFiLGFBQWEsQ0FBYztJQUFJLENBQUM7SUFFdkMsMkJBQUssR0FBbEIsVUFBbUIsUUFBZ0IsRUFBRSxRQUFnQjs7Ozs7NEJBQ2pDLHFCQUFNLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsRUFBQTs7d0JBQW5ELE9BQU8sR0FBRyxTQUF5Qzt3QkFDekQsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDbkIsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxzQkFBUyxDQUFDLG9CQUFvQixDQUFDLEVBQUM7d0JBQzVFLENBQUM7d0JBQ29CLHFCQUFNLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFBdkYsV0FBVyxHQUFJLFNBQXdFO3dCQUM3RixFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsU0FBUyxJQUFJLDJCQUFXLENBQUMsRUFBRSxDQUFDOzRCQUFDLE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFPLEtBQUssRUFBRSxJQUFJLEVBQUUsdUJBQVUsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxFQUFFLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBQzt3QkFFeEssSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQzVELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBRTdDLHNCQUFPLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxFQUFDOzs7O0tBQy9DO0lBRVkscUNBQWUsR0FBNUIsVUFBNkIsUUFBZ0IsRUFBRSxRQUFnQjs7Ozs7NEJBQzNDLHFCQUFNLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsRUFBQTs7d0JBQW5ELE9BQU8sR0FBRyxTQUF5Qzt3QkFDekQsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDbkIsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxzQkFBUyxDQUFDLG9CQUFvQixDQUFDLEVBQUM7d0JBQzVFLENBQUM7d0JBQ1kscUJBQU0sSUFBSSxDQUFDLHVCQUF1QixDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUE7O3dCQUF6RixHQUFHLEdBQUksU0FBa0Y7d0JBQy9GLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxHQUFHLENBQUMsU0FBUyxLQUFLLDJCQUFXLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBVSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEVBQUUsR0FBRyxDQUFDLFlBQVksQ0FBQyxFQUFDOzs7O0tBQ25JO0lBRVksZ0NBQVUsR0FBdkI7Ozs7Ozt3QkFDVSxTQUFTLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRSxDQUFDO3dCQUN0RCxJQUFJLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUVsRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQzs0QkFBQyxNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUM7d0JBRXZDLHFCQUFNLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxFQUFBOzt3QkFBOUQsaUJBQWlCLEdBQUcsU0FBMEM7d0JBQ3BFLEVBQUUsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFFN0IsRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsU0FBUyxJQUFJLHNCQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dDQUM1RCxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dDQUN2QyxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDOzRCQUNoRCxDQUFDOzRCQUVELE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsc0JBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsaUJBQWlCLENBQUMsWUFBWSxDQUFDLEVBQUM7d0JBQzdKLENBQUM7d0JBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ0osTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFDO3dCQUMzQyxDQUFDOzs7OztLQUNKO0lBRWEsbURBQTZCLEdBQTNDOzs7Ozs0QkFDeUIscUJBQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFLEVBQUE7O3dCQUEvRCxZQUFZLEdBQUcsU0FBZ0Q7d0JBQ3JFLEVBQUUsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQzs0QkFBQyxNQUFNLGdCQUFDLFlBQVksRUFBQzt3QkFFeEMscUJBQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsRUFBQTs0QkFBN0Msc0JBQU8sU0FBc0MsRUFBQzs7OztLQUNqRDtJQUVELDRCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQTVEUSxXQUFXO1FBRHZCLGlCQUFVLEVBQUU7eUNBR29DLGlFQUE2QjtZQUMvQiw2Q0FBb0I7WUFDekIsa0NBQWU7WUFDbEIsNEJBQVk7T0FMdEMsV0FBVyxDQTZEdkI7SUFBRCxrQkFBQztDQUFBLEFBN0RELElBNkRDO0FBN0RZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOb3Rlc1dlYlNlcnZpY2VNYW5hZ2VyU2VydmljZSB9IGZyb20gJ34vc2VydmljZXMvbm90ZXMtd2ViLXNlcnZpY2UtbWFuYWdlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VjdXJlU3RvcmFnZVNlcnZpY2UgfSBmcm9tICd+L3NlcnZpY2VzL3NlY3VyZS1zdG9yYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb252ZXJ0ZXJzIH0gZnJvbSAnfi9oZWxwZXJzL2NvbnZlcnRlcnMnO1xyXG5pbXBvcnQgeyBXc0Vycm9yVHlwZSB9IGZyb20gJ34vbW9kZWxzL3dlYi1zZXJ2aWNlL3dzLWVycm9yLXR5cGUnO1xyXG5pbXBvcnQgeyBPcGVyYXRpb25SZXN1bHQgfSBmcm9tICd+L21vZGVscy9jb3JlL29wZXJhdGlvbi1yZXN1bHQnO1xyXG5pbXBvcnQgeyBEYXRhYmFzZVNlcnZpY2UgfSBmcm9tICd+L3NlcnZpY2VzL2RhdGFiYXNlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3Rlc1NlcnZpY2UgfSBmcm9tICd+L3NlcnZpY2VzL25vdGVzLnNlcnZpY2UnO1xyXG5pbXBvcnQge0Vycm9yVHlwZX0gZnJvbSBcIn4vbW9kZWxzL2NvcmUvZXJyb3ItdHlwZVwiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX25vdGVzV2ViU2VydmljZU1hbmFnZXI6IE5vdGVzV2ViU2VydmljZU1hbmFnZXJTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfc2VjdXJlU3RvcmFnZVNlcnZpY2U6IFNlY3VyZVN0b3JhZ2VTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfZGF0YWJhc2VTZXJ2aWNlOiBEYXRhYmFzZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9ub3Rlc1NlcnZpY2U6IE5vdGVzU2VydmljZSkgeyB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIGxvZ2luKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcpOiBQcm9taXNlPE9wZXJhdGlvblJlc3VsdDxudWxsPj4ge1xyXG4gICAgICAgIGNvbnN0IHBpbmdSZXMgPSBhd2FpdCB0aGlzLl9ub3Rlc1dlYlNlcnZpY2VNYW5hZ2VyLnBpbmcoKTtcclxuICAgICAgICBpZiAoIXBpbmdSZXMuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgRXJyb3JUeXBlLndzU2VydmVyTm90UmVhY2hhYmxlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgbG9naW5SZXN1bHQgPSAgYXdhaXQgdGhpcy5fbm90ZXNXZWJTZXJ2aWNlTWFuYWdlci5sb2dpbih1c2VybmFtZSwgcGFzc3dvcmQpLnRvUHJvbWlzZSgpO1xyXG4gICAgICAgIGlmIChsb2dpblJlc3VsdC5lcnJvclR5cGUgIT0gV3NFcnJvclR5cGUub2spIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0PG51bGw+KGZhbHNlLCBudWxsLCBDb252ZXJ0ZXJzLnRvQ29yZUVycm9yVHlwZShsb2dpblJlc3VsdC5lcnJvclR5cGUpLCBsb2dpblJlc3VsdC5lcnJvck1lc3NhZ2UpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuX3NlY3VyZVN0b3JhZ2VTZXJ2aWNlLnNldFVzZXJUb2tlbihsb2dpblJlc3VsdC5yZXN1bHQpO1xyXG4gICAgICAgIHRoaXMuX3NlY3VyZVN0b3JhZ2VTZXJ2aWNlLnNldFVzZXIodXNlcm5hbWUpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiB0aGlzLnByZXBhcmVEYXRhYmFzZUFuZFN5bmNocm9uaXplKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIHJlZ2lzdGVyTmV3VXNlcih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogUHJvbWlzZTxPcGVyYXRpb25SZXN1bHQ8bnVsbD4+IHtcclxuICAgICAgICBjb25zdCBwaW5nUmVzID0gYXdhaXQgdGhpcy5fbm90ZXNXZWJTZXJ2aWNlTWFuYWdlci5waW5nKCk7XHJcbiAgICAgICAgaWYgKCFwaW5nUmVzLnN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIEVycm9yVHlwZS53c1NlcnZlck5vdFJlYWNoYWJsZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHJlcyA9ICBhd2FpdCB0aGlzLl9ub3Rlc1dlYlNlcnZpY2VNYW5hZ2VyLnJlZ2lzdGVyTmV3VXNlcih1c2VybmFtZSwgcGFzc3dvcmQpLnRvUHJvbWlzZSgpO1xyXG4gICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHJlcy5lcnJvclR5cGUgPT09IFdzRXJyb3JUeXBlLm9rLCBudWxsLCBDb252ZXJ0ZXJzLnRvQ29yZUVycm9yVHlwZShyZXMuZXJyb3JUeXBlKSwgcmVzLmVycm9yTWVzc2FnZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIGNoZWNrTG9naW4oKTogUHJvbWlzZTxPcGVyYXRpb25SZXN1bHQ8Ym9vbGVhbj4+IHtcclxuICAgICAgICBjb25zdCB1c2VyVG9rZW4gPSB0aGlzLl9zZWN1cmVTdG9yYWdlU2VydmljZS5nZXRVc2VyVG9rZW4oKTtcclxuICAgICAgICBjb25zdCB1c2VyID0gdGhpcy5fc2VjdXJlU3RvcmFnZVNlcnZpY2UuZ2V0VXNlcigpO1xyXG5cclxuICAgICAgICBpZiAoIXVzZXJUb2tlbiB8fCAhdXNlcikgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQodHJ1ZSwgZmFsc2UpO1xyXG5cclxuICAgICAgICBjb25zdCBzeW5jUHJlcGFyZVJlc3VsdCA9IGF3YWl0IHRoaXMucHJlcGFyZURhdGFiYXNlQW5kU3luY2hyb25pemUoKTtcclxuICAgICAgICBpZiAoIXN5bmNQcmVwYXJlUmVzdWx0LnN1Y2Nlc3MpIHtcclxuXHJcbiAgICAgICAgICAgIGlmIChzeW5jUHJlcGFyZVJlc3VsdC5lcnJvclR5cGUgPj0gRXJyb3JUeXBlLmRiVXNlck5vdExvZ2VkSW4pIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3NlY3VyZVN0b3JhZ2VTZXJ2aWNlLnNldFVzZXIoJycpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2VjdXJlU3RvcmFnZVNlcnZpY2Uuc2V0VXNlclRva2VuKCcnKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIHN5bmNQcmVwYXJlUmVzdWx0LmVycm9yVHlwZSA8IEVycm9yVHlwZS5kYlVzZXJOb3RMb2dlZEluLCBzeW5jUHJlcGFyZVJlc3VsdC5lcnJvclR5cGUsIHN5bmNQcmVwYXJlUmVzdWx0LmVycm9yTWVzc2FnZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQodHJ1ZSwgdHJ1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYXN5bmMgcHJlcGFyZURhdGFiYXNlQW5kU3luY2hyb25pemUoKTogUHJvbWlzZTxPcGVyYXRpb25SZXN1bHQ8bnVsbD4+IHtcclxuICAgICAgICBjb25zdCBkYkluaXRSZXN1bHQgPSBhd2FpdCB0aGlzLl9kYXRhYmFzZVNlcnZpY2UuaW5pdGlhbGl6ZURhdGFiYXNlKCk7XHJcbiAgICAgICAgaWYgKCFkYkluaXRSZXN1bHQuc3VjY2VzcykgcmV0dXJuIGRiSW5pdFJlc3VsdDtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIHJldHVybiBhd2FpdCB0aGlzLl9ub3Rlc1NlcnZpY2Uuc3luY2hyb25pemUoKTtcclxuICAgIH1cclxuXHJcbiAgICBsb2dvdXQoKSB7XHJcbiAgICAgICAgdGhpcy5fc2VjdXJlU3RvcmFnZVNlcnZpY2Uuc2V0VXNlcignJyk7XHJcbiAgICAgICAgdGhpcy5fc2VjdXJlU3RvcmFnZVNlcnZpY2Uuc2V0VXNlclRva2VuKCcnKTtcclxuICAgIH1cclxufVxyXG4iXX0=