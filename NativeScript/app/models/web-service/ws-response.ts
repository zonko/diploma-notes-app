import { WsErrorType } from "~/models/web-service/ws-error-type";

export class WsResponse<T> {
    errorType: WsErrorType;
    responseMessage: string;
    errorMessage: string;
    result: T;
}