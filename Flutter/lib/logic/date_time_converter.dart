import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateTimeConverter {
  static final DateFormat _dateFormat = new DateFormat('yyyy_MM_dd_HH_mm_ss');

  static DateTime parseFromUtc(String dateTime) {
    var dt = _dateFormat.parse(dateTime, true);
    return dt.toLocal();
  }

  static String toUtcString(DateTime dateTime) {
    dateTime = dateTime.toUtc();
    return _dateFormat.format(dateTime);
  }

  static String toDateTimeDisplayString(DateTime dateTime) {
    return dateTime.day.toString() + '.' + dateTime.month.toString() + '.' + dateTime.year.toString() + ' ' + dateTime.hour.toString() + ':' + dateTime.minute.toString();
  }

  static String toDateDisplayString(DateTime date) {
    return date.day.toString() + '.' + date.month.toString() + '.' + date.year.toString();
  }

  static String toTimeDisplayString(TimeOfDay time) {
    return time.hour.toString() + ':' + time.minute.toString();
  }
}