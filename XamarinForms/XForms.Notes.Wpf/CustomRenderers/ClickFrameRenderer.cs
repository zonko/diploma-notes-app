﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.WPF;
using XForms.Notes.UI.Controls;
using XForms.Notes.Wpf.CustomRenderers;

[assembly: ExportRenderer(typeof(ClickFrame), typeof(ClickFrameRenderer))]
namespace XForms.Notes.Wpf.CustomRenderers
{
    public class ClickFrameRenderer : FrameRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            var newElement = e.NewElement as ClickFrame;
            var oldElement = e.OldElement as ClickFrame;

            if (newElement != null && oldElement == null)
            {
                Control.MouseUp += newElement.OnClick;
            }

            if (newElement == null && oldElement != null)
            {
                Control.MouseUp -= oldElement.OnClick;
            }
        }
    }
}