import 'dart:async';
import 'package:ft_notes_app/models/error_type.dart';
import 'package:ft_notes_app/models/note.dart';
import 'package:ft_notes_app/models/note_attachment.dart';
import 'package:ft_notes_app/models/note_display.dart';
import 'package:ft_notes_app/models/notification.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DataAccess {
  // next three lines makes this class a Singleton
  static DataAccess _instance = new DataAccess.internal();
  DataAccess.internal();
  factory DataAccess() => _instance;

  final String _notificationsDbName = 'notesNotifications.db';
  Database _userDatabase;
  Database _notificationsDatabase;

  Future<Null> initializeNotificationsDatabase() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, _notificationsDbName);

    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.transaction((transaction) async {
        await transaction.execute("""CREATE TABLE IF NOT EXISTS Notification (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                User TEXT NOT NULL,
                NoteId INTEGER NOT NULL
            );""");
      });
    });
    _notificationsDatabase = database;
  }

  Future<void> initializeUserDatabase(String user) async {
    try {
      var databasesPath = await getDatabasesPath();
      String path = join(databasesPath, user + '.db');

      Database database = await openDatabase(path, version: 1,
          onCreate: (Database db, int version) async {
        await db.transaction((transaction) async {
          await transaction.execute("""CREATE TABLE IF NOT EXISTS Note (
              Id INTEGER PRIMARY KEY AUTOINCREMENT,
              Cid	INTEGER,
              Title TEXT,
              Text TEXT,
              DateTimeChanged TEXT NOT NULL,
              Modified INTEGER NOT NULL,
              Active INTEGER NOT NULL
          );""");
          await transaction.execute("""CREATE TABLE IF NOT EXISTS Attachment (
              Id INTEGER PRIMARY KEY AUTOINCREMENT,
              NoteId INTEGER NOT NULL,
              Name TEXT,
              Type TEXT,
              Content	TEXT,
              FOREIGN KEY(NoteId) REFERENCES Note(Id)
          );""");
          await transaction.execute("""CREATE TABLE IF NOT EXISTS KeyValue (
              Key TEXT,
              Value TEXT,
              PRIMARY KEY(Key)
          );""");
        });
      });
      _userDatabase = database;
    } catch (err) {
      throw new Exception(ErrorType.dbOpenDbError);
    }
  }

  Future<Null> saveNotes(List<Note> notes) async {
    if (_userDatabase == null) throw new Exception(ErrorType.dbNotInitialized);
    if (notes == null) return;

    for (var note in notes) {
      await saveNote(note);
    }
  }

  Future<int> saveNote(Note note) async {
    if (_userDatabase == null) throw new Exception(ErrorType.dbNotInitialized);
    if (note == null) return null;

    if (note.id != null) {
      var res = await _userDatabase.query('Note',
          columns: ['Id'], where: 'Id = ?', whereArgs: [note.id]);
      if (res != null && res.length == 1) note.id = res[0]['Id'];
    } else if (note.cid != null) {
      var res = await _userDatabase.query('Note',
          columns: ['Id'], where: 'Cid = ?', whereArgs: [note.cid]);
      if (res != null && res.length == 1) note.id = res[0]['Id'];
    }

    int noteId;
    await _userDatabase.transaction((transaction) async {
      if (note.id != null) {
        await transaction
            .delete('Attachment', where: 'NoteId = ?', whereArgs: [note.id]);
        if (!note.active) {
          await transaction
              .delete('Note', where: 'Id = ?', whereArgs: [note.id]);
          return;
        } else {
          await transaction.update('Note', note.toDbMap(),
              where: 'Id = ?', whereArgs: [note.id]);
          noteId = note.id;
        }
      } else {
        noteId = await transaction.insert('Note', note.toDbMap());
      }
      for (var att in note.attachments) {
        await transaction.insert('Attachment', att.toDbMap(noteId));
      }
    });

    return noteId;
  }

  Future<void> deactivateNote(int id) async {
    if (_userDatabase == null) throw new Exception(ErrorType.dbNotInitialized);
    await _userDatabase.rawUpdate(
        'UPDATE Note SET Active = 0, Modified = 1 WHERE Id = ?', [id]);
  }

  Future<Note> getNote(int id) async {
    if (_userDatabase == null) throw new Exception(ErrorType.dbNotInitialized);
    if (id == null) return null;

    var res = await _userDatabase.query('Note',
        columns: [
          'Id',
          'Cid',
          'Title',
          'Text',
          'DateTimeChanged',
          'Modified',
          'Active'
        ],
        where: 'Id = ?',
        whereArgs: [id]);
    if (res.length != 1) return null;

    var note = Note.fromDbMap(res[0]);
    var dbAttachments = await _userDatabase.query('Attachment',
        columns: ['Name', 'Type', 'Content'],
        where: 'NoteId = ?',
        whereArgs: [id]);
    if (dbAttachments.length > 0) {
      note.attachments = new List<NoteAttachment>();
      dbAttachments.forEach(
          (att) => note.attachments.add(NoteAttachment.fromDbMap(att)));
    }

    return note;
  }

  Future<List<NoteDisplay>> listNotes() async {
    if (_userDatabase == null) throw new Exception(ErrorType.dbNotInitialized);
    var dbResult = await _userDatabase.query('Note',
        columns: ['Id', 'Title', 'DateTimeChanged'], where: 'Active = 1');

    var result = new List<NoteDisplay>();
    if (dbResult != null && dbResult.length > 0) {
      dbResult.forEach((n) => result.add(new NoteDisplay.fromDbNoteMap(n)));
    }

    return result;
  }

  Future<List<Note>> listModified() async {
    if (_userDatabase == null) throw new Exception(ErrorType.dbNotInitialized);
    var dbResult = await _userDatabase.query('Note',
        columns: [
          'Id',
          'Cid',
          'Title',
          'Text',
          'DateTimeChanged',
          'Modified',
          'Active'
        ],
        where: 'Modified = 1');
    var result = new List<Note>();

    for (var noteMap in dbResult) {
      var note = Note.fromDbMap(noteMap);
      var dbAttachments = await _userDatabase.query('Attachment',
          columns: ['Name', 'Type', 'Content'],
          where: 'NoteId = ?',
          whereArgs: [note.id]);
      if (dbAttachments.length > 0) {
        note.attachments = new List<NoteAttachment>();
        dbAttachments.forEach(
            (att) => note.attachments.add(NoteAttachment.fromDbMap(att)));
      }
      result.add(note);
    }

    return result;
  }

  Future<String> getValue(String key) async {
    if (_userDatabase == null) throw new Exception(ErrorType.dbNotInitialized);
    var dbResult = await _userDatabase.query('KeyValue',
        columns: ['Value'], where: 'Key = ?', whereArgs: [key]);
    if (dbResult.length > 0) return dbResult[0]['Value'];

    return null;
  }

  Future<Null> setValue(String key, String value) async {
    if (_userDatabase == null) throw new Exception(ErrorType.dbNotInitialized);
    var dbResult = await _userDatabase.query('KeyValue',
        columns: ['Value'], where: 'Key = ?', whereArgs: [key]);
    var map = <String, dynamic>{
      'Key': key,
      'Value': value,
    };
    if (dbResult.length > 0) {
      await _userDatabase
          .update('KeyValue', dbResult[0], where: 'Key = ?', whereArgs: [key]);
      return;
    }

    await _userDatabase.insert('KeyValue', map);
  }

  Future<int> insertNewNotification(NotesNotification notification) async {
    return await _notificationsDatabase.insert(
        'Notification', notification.toDbMap());
  }

  Future<NotesNotification> getNotification(int id) async {
    var res = await _notificationsDatabase.query('Notification',
        columns: ['User', 'NoteId'], where: 'Id = ?', whereArgs: [id]);
    if (res.length != 1) return null;

    await _notificationsDatabase
        .delete('Notification', where: 'Id = ?', whereArgs: [id]);
    return NotesNotification.fromDbMap(res[0]);
  }
}
