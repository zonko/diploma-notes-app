import { Component, OnInit } from '@angular/core';
import { UserService } from '~/services/user.service';
import { Router } from '@angular/router';
import { Helpers } from '~/helpers/helpers';
import {DialogService} from "~/services/dialog.service";
import { Page } from "ui/page";

@Component({
    moduleId: module.id,
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    username: string;
    password: string;

    isBusy = false;

    constructor(private _userService: UserService,
                private _router: Router,
                private _dialogService: DialogService,
                private _page: Page) {
    }

    ngOnInit() {
        this._page.actionBarHidden = true;
    }


    onLogin() {
        if(Helpers.isNullOrEmpty(this.username) || Helpers.isNullOrEmpty(this.password)) {
            this._dialogService.showMessageDialog("Prijava", "Vnesite uporabniško ime in geslo");
            return;
        }
        this._dialogService.setBusy(true);
        // this.isBusy = true;
        this._userService.login(this.username, this.password).then(res => {
            this._dialogService.setBusy(false);
            this.username = '';
            this.password = '';
            if (res.success) {
                this._router.navigateByUrl("notes");
            } else {
                const errorMessage = "Prijava ni uspela: " + Helpers.getErrorMessage(res.errorType);
                this._dialogService.showMessageDialog("Napaka", errorMessage);
            }
        }).catch(err => {
            this._dialogService.setBusy(false);
            this.username = '';
            this.password = '';
            this._dialogService.showMessageDialog("Napaka", "Prijava ni uspela: Prišlo je do nepričakovane napake: " + err);
        });
    }
}
