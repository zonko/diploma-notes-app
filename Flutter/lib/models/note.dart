import 'package:ft_notes_app/logic/date_time_converter.dart';
import 'package:ft_notes_app/models/note_attachment.dart';

class Note {
  int id;
  int cid;
  String title;
  String text;
  DateTime dateTimeChanged;
  bool modified;
  bool active;
  List<NoteAttachment> attachments;

  Note();

  Note.fromDbMap(Map<String, dynamic> map) {
    id = map['Id'];
    cid = map['Cid'];
    title = map['Title'];
    text = map['Text'];
    dateTimeChanged = DateTimeConverter.parseFromUtc(map['DateTimeChanged']);
    modified = map['Modified'] == 1;
    active = map['Active'] == 1;
  }

  Map<String, dynamic> toDbMap() {
    var map = <String, dynamic> {
      'Cid': cid,
      'Title': title,
      'Text': text,
      'DateTimeChanged': DateTimeConverter.toUtcString(dateTimeChanged),
      'Modified': modified,
      'Active': active,
    };
    if(id != null) {
      map['Id'] = id;
    }
    return map;
  }

}