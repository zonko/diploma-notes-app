import {Component, OnInit, ViewChild} from '@angular/core';
import { NoteDisplay } from '~/models/core/note-display';
import {RadSideDrawerComponent} from "nativescript-ui-sidedrawer/angular";
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import { registerElement } from "nativescript-angular/element-registry";
import {Helpers} from "~/helpers/helpers";
import {NotesService} from "~/services/notes.service";
import {DialogService} from "~/services/dialog.service";
import {ErrorType} from "~/models/core/error-type";
registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);

@Component({
    moduleId: module.id,
    selector: 'app-notes',
    templateUrl: './notes.component.html',
    styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
    notes: Array<NoteDisplay>;

    @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
    private _drawer: RadSideDrawer;

    constructor(private _notesService: NotesService,
                private _dialogService: DialogService) { }

    ngOnInit() {
        this._drawer = this.drawerComponent.sideDrawer;
        this.getNotes();
    }

    switchDrawerOpen() {
        if(this._drawer.getIsOpen()) {
            this._drawer.closeDrawer();
        } else {
            this._drawer.showDrawer();
        }
    }

    deleteNote(item: NoteDisplay) {
        this._dialogService.setBusy(true);
        this._notesService.deactivateNote(item.id).then(res => {
            this._dialogService.setBusy(false);
            if(res.success) {
                this.getNotes();
            } else if (res.errorType < ErrorType.dbUserNotLogedIn) {
                this._dialogService.showMessageDialog("Napaka",
                    "Prišlo je do napake med sinhronizacijo s centralo: "
                    + Helpers.getErrorMessage(res.errorType) );
                this.getNotes();
            } else {
                this._dialogService.showMessageDialog("Napaka", "Zapisek ni bil shranjen: " + Helpers.getErrorMessage(res.errorType));
            }
        }).catch(err => {
            this._dialogService.setBusy(false);
            this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
        });
    }

    private getNotes(): void {
        this._dialogService.setBusy(true);
        this._notesService.listNotes().then(res => {
            this._dialogService.setBusy(false);
            if(res.success) {
                this.notes = res.result;
            } else {
                this._dialogService.showMessageDialog("Napaka", Helpers.getErrorMessage(res.errorType));
            }
        }).catch(err => {
            this._dialogService.setBusy(false);
            this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
        });
    }
}
