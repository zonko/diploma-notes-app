import { Injectable } from '@angular/core';
import { NotesWebServiceManagerService } from '~/services/notes-web-service-manager.service';
import { SecureStorageService } from '~/services/secure-storage.service';
import { Converters } from '~/helpers/converters';
import { WsErrorType } from '~/models/web-service/ws-error-type';
import { OperationResult } from '~/models/core/operation-result';
import { DatabaseService } from '~/services/database.service';
import { NotesService } from '~/services/notes.service';
import {ErrorType} from "~/models/core/error-type";

@Injectable()
export class UserService {

    constructor(private _notesWebServiceManager: NotesWebServiceManagerService,
                private _secureStorageService: SecureStorageService,
                private _databaseService: DatabaseService,
                private _notesService: NotesService) { }

    public async login(username: string, password: string): Promise<OperationResult<null>> {
        const pingRes = await this._notesWebServiceManager.ping();
        if (!pingRes.success) {
            return new OperationResult(false, null, ErrorType.wsServerNotReachable);
        }
        const loginResult =  await this._notesWebServiceManager.login(username, password).toPromise();
        if (loginResult.errorType != WsErrorType.ok) return new OperationResult<null>(false, null, Converters.toCoreErrorType(loginResult.errorType), loginResult.errorMessage);
        
        this._secureStorageService.setUserToken(loginResult.result);
        this._secureStorageService.setUser(username);
        
        return this.prepareDatabaseAndSynchronize();
    }

    public async registerNewUser(username: string, password: string): Promise<OperationResult<null>> {
        const pingRes = await this._notesWebServiceManager.ping();
        if (!pingRes.success) {
            return new OperationResult(false, null, ErrorType.wsServerNotReachable);
        }
        const res =  await this._notesWebServiceManager.registerNewUser(username, password).toPromise();
        return new OperationResult(res.errorType === WsErrorType.ok, null, Converters.toCoreErrorType(res.errorType), res.errorMessage);
    }

    public async checkLogin(): Promise<OperationResult<boolean>> {
        const userToken = this._secureStorageService.getUserToken();
        const user = this._secureStorageService.getUser();

        if (!userToken || !user) return new OperationResult(true, false);

        const syncPrepareResult = await this.prepareDatabaseAndSynchronize();
        if (!syncPrepareResult.success) {

            if (syncPrepareResult.errorType >= ErrorType.dbUserNotLogedIn) {
                this._secureStorageService.setUser('');
                this._secureStorageService.setUserToken('');
            }

            return new OperationResult(false, syncPrepareResult.errorType < ErrorType.dbUserNotLogedIn, syncPrepareResult.errorType, syncPrepareResult.errorMessage);
        } else {
            return new OperationResult(true, true);
        }
    }

    private async prepareDatabaseAndSynchronize(): Promise<OperationResult<null>> {
        const dbInitResult = await this._databaseService.initializeDatabase();
        if (!dbInitResult.success) return dbInitResult;
                
        return await this._notesService.synchronize();
    }

    logout() {
        this._secureStorageService.setUser('');
        this._secureStorageService.setUserToken('');
    }
}
