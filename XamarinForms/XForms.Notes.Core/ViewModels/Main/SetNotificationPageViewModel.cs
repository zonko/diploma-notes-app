﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace XForms.Notes.Core.ViewModels.Main
{
    public class SetNotificationPageViewModel : MvxViewModelResult<DateTime?>
    {
        private DateTime _selectedDate;
        private TimeSpan _selectedTime;
        private MvxAsyncCommand _confirmCommand;
        private MvxAsyncCommand _cancelCommand;

        public SetNotificationPageViewModel()
        {
            _selectedDate = DateTime.Now.Date;
            _selectedTime = DateTime.Now - DateTime.Now.Date;
        }

        public DateTime SelectedDate
        {
            get => _selectedDate;
            set => SetProperty(ref _selectedDate, value);
        }

        public TimeSpan SelectedTime
        {
            get => _selectedTime;
            set => SetProperty(ref _selectedTime, value);
        }

        public MvxAsyncCommand ConfirmCommand => _confirmCommand ?? (_confirmCommand = new MvxAsyncCommand(Confirm));
        
        public MvxAsyncCommand CancelCommand => _cancelCommand ?? (_cancelCommand = new MvxAsyncCommand(Cancel));

        private async Task Confirm()
        {
            await NavigationService.Close(this, SelectedDate + SelectedTime);
        }

        private async Task Cancel()
        {
            await NavigationService.Close(this, null);
        }
    }
}