import 'dart:async';

import 'package:ft_notes_app/app_constants.dart';
import 'package:ft_notes_app/database/data_access.dart';
import 'package:ft_notes_app/web_service/notes_web_service_manager.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class UserLogic {
  // next three lines makes this class a Singleton
  static UserLogic _instance = new UserLogic.internal();
  UserLogic.internal();
  factory UserLogic() => _instance;

  final _serviceManager = new NotesWebServiceManager();
  final _secureStorage = new FlutterSecureStorage();
  final _dataAccess = new DataAccess();

  Future<void> login(String username, String password) async {
    var loginResult = await _serviceManager.login(username, password);
    await _secureStorage.write(key: SECURE_STORAGE_KEY__USER_TOKEN, value: loginResult);
    await _secureStorage.write(key: SECURE_STORAGE_KEY__USER, value: username);

    await _dataAccess.initializeUserDatabase(username);
  }

  Future<void> registerNewUser(String username, String password) async {
    await _serviceManager.registerNewUser(username, password);
  }

  Future<bool> checkLogin() async {
    var userToken = await _secureStorage.read(key: SECURE_STORAGE_KEY__USER_TOKEN);
    var user = await _secureStorage.read(key: SECURE_STORAGE_KEY__USER);
    if (userToken == null || userToken.isEmpty || user == null || user.isEmpty) return false;

    await _dataAccess.initializeUserDatabase(user);
    return true;
  }

  Future<String> getLoggedInUser() async {
    return await _secureStorage.read(key: SECURE_STORAGE_KEY__USER);
  }

  Future<void> logout() async {
    await _secureStorage.delete(key: SECURE_STORAGE_KEY__USER_TOKEN);
    await _secureStorage.delete(key: SECURE_STORAGE_KEY__USER);
  }

}