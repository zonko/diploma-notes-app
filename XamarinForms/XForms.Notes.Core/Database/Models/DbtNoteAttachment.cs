﻿using SQLite;
using SQLiteNetExtensions.Attributes;

namespace XForms.Notes.Core.Database.Models
{
    internal class DbtNoteAttachment
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }

        [ForeignKey(typeof(DbtNote))]
        public int NoteId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public byte[] Content { get; set; }
    }
}