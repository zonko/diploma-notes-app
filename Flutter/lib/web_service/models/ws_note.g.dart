// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ws_note.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WsNote _$WsNoteFromJson(Map<String, dynamic> json) {
  return WsNote()
    ..id = json['id'] as int
    ..clientId = json['clientId'] as int
    ..active = json['active'] as bool
    ..title = json['title'] as String
    ..text = json['text'] as String
    ..dateTimeChanged = json['dateTimeChanged'] as String
    ..attachments = (json['attachments'] as List)
        ?.map((e) => e == null
            ? null
            : WsNoteAttachment.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$WsNoteToJson(WsNote instance) => <String, dynamic>{
      'id': instance.id,
      'clientId': instance.clientId,
      'active': instance.active,
      'title': instance.title,
      'text': instance.text,
      'dateTimeChanged': instance.dateTimeChanged,
      'attachments': instance.attachments
    };
