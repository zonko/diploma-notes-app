import 'dart:async';

import 'package:ft_notes_app/helpers/error_message_helper.dart';
import 'package:ft_notes_app/logic/notes_logic.dart';
import 'package:ft_notes_app/logic/notifications_logic.dart';
import 'package:ft_notes_app/models/note.dart';

abstract class NoteEditScreenContract {
  onNoteLoaded(Note note);
  onNoteSaved(String message);
  Future<Null> onError(String errorMessage);
}

class NoteEditScreenPresenter {
  NoteEditScreenContract _view;
  final _notesLogic = new NotesLogic();
  final _notificationsLogic = new NotificationsLogic();

  NoteEditScreenPresenter(this._view);

  loadNote(int id) async {
    try {
      var note = await _notesLogic.getNote(id);
      _view.onNoteLoaded(note);
    } catch (error) {
      await _view.onError("Prišlo je do napake. "  + ErrorMessageHelper.getErrorMessage(error));
    }
  }

  saveNote(Note note, DateTime notificationDateTime) async {
    try {
      var saveResult = await _notesLogic.saveNote(note);
      var now = DateTime.now();
      if (saveResult != null && notificationDateTime != null && notificationDateTime.compareTo(now) > 0) {
        _notificationsLogic.scheduleNotification(saveResult, note.title, note.text, notificationDateTime);
      }
      try {
        await _notesLogic.synchronize();
      } catch (err) {
         await _view.onNoteSaved('Zapisek je bil shranjen lokalno, sinhronizacija s strežnikom pa ni uspela.'  + ErrorMessageHelper.getErrorMessage(err));
      }
      _view.onNoteSaved(null);
    } catch (error) {
      await _view.onError('Shranjevanje zapiska ni uspelo. '  + ErrorMessageHelper.getErrorMessage(error));
    }
  }
 
}