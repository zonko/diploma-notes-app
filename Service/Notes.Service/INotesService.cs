﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Notes.Service.Entities;

namespace Notes.Service
{
    [ServiceContract]
    public interface INotesService
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = nameof(Ping),
            BodyStyle = WebMessageBodyStyle.Bare)]
        ServiceResponse Ping();

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = nameof(RegisterNewUser),
            BodyStyle = WebMessageBodyStyle.Bare)]
        ServiceResponse RegisterNewUser(User user);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = nameof(Login),
            BodyStyle = WebMessageBodyStyle.Bare)]
        ServiceResponseWithResult<Guid> Login(User user);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = nameof(Synchronize),
            BodyStyle = WebMessageBodyStyle.Bare)]
        ServiceResponseWithResult<Synchronization> Synchronize(Synchronization request);

    }

}
