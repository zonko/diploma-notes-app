import { Component, OnInit } from '@angular/core';
import { UserService } from '~/services/user.service';
import {Helpers} from "~/helpers/helpers";
import {DialogService} from "~/services/dialog.service";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
    isBusy = false;
    username: string;
    password: string;
    confirmPassword: string;

    constructor(private _userService: UserService,
                private _dialogService: DialogService,
                private _router: Router) { }

    ngOnInit() { }

    public onRegisterNewUser() {
        if (Helpers.isNullOrEmpty(this.username) || Helpers.isNullOrEmpty(this.password)) {
            this._dialogService.showMessageDialog("Registracija", "Vnesite uporabniško ime in geslo.");
            return;
        }
        if (this.password !== this.confirmPassword) {
            this._dialogService.showMessageDialog("Registracija", "Geslo se ne ujema s potrditvenim geslom.");
            return;
        }
        this.isBusy = true;
        this._userService.registerNewUser(this.username, this.password).then(res => {
            this.isBusy = false;
            this.username = '';
            this.password = '';
            this.confirmPassword = '';
            if(!res.success) {
                this._dialogService.showMessageDialog("Napaka", "Registracija ni uspela: " + Helpers.getErrorMessage(res.errorType));
            } else {
                this._dialogService.showMessageDialog("Registracija", "Registracija je bila uspešna. Sedaj se lahko prijavite v aplikacijo.");
                this._router.navigateByUrl("login");
            }
        }).catch(err => {
            this.isBusy = false;
            this.username = '';
            this.password = '';
            this.confirmPassword = '';
            this._dialogService.showMessageDialog("Napaka", "Registracija ni uspela. Prišlo je do nepričakovane napake: " + err);
        });

    }

}
