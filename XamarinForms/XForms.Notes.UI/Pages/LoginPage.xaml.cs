﻿using System.Reflection;
using MvvmCross.Forms.Presenters.Attributes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XForms.Notes.UI.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	[MvxContentPagePresentation(Animated = false)]
    public partial class LoginPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
		}
	}
}