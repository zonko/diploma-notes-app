export enum ErrorType {
    ok = 1,
    // web service errors
    wsBadRequest = 2,
    wsInternalServerError = 3,
    wsUsernameIsTaken = 4,
    wsWrongUsernameOrPassword = 5,
    wsAuthorizationError = 6,
    wsServerNotReachable = 15,

    // db errors
    dbUserNotLogedIn = 100,
    dbNotInitialized = 101,
    dbCreateTablesError = 102,
    dbOpenDbError = 103,
    dbWriteError = 104,
    dbReadError = 105,

    //
    notificationSchedulingError = 200
}