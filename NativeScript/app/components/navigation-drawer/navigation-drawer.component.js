"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var secure_storage_service_1 = require("~/services/secure-storage.service");
var router_1 = require("@angular/router");
var user_service_1 = require("~/services/user.service");
var notes_service_1 = require("~/services/notes.service");
var dialog_service_1 = require("~/services/dialog.service");
var helpers_1 = require("~/helpers/helpers");
var NavigationDrawerComponent = /** @class */ (function () {
    function NavigationDrawerComponent(_secureStorageService, _router, _userService, _notesService, _dialogService) {
        this._secureStorageService = _secureStorageService;
        this._router = _router;
        this._userService = _userService;
        this._notesService = _notesService;
        this._dialogService = _dialogService;
        this.close = new core_1.EventEmitter();
    }
    NavigationDrawerComponent.prototype.ngOnInit = function () {
        this.loggedInUser = this._secureStorageService.getUser();
    };
    NavigationDrawerComponent.prototype.onAddNote = function () {
        this._router.navigateByUrl("note/-1");
    };
    NavigationDrawerComponent.prototype.onLogout = function () {
        this._userService.logout();
        this._router.navigateByUrl("login");
    };
    NavigationDrawerComponent.prototype.onSynchronize = function () {
        var _this = this;
        this._dialogService.setBusy(true);
        this._notesService.synchronize().then(function (res) {
            _this._dialogService.setBusy(false);
            if (!res.success) {
                _this._dialogService.showMessageDialog("Napaka", "Sinhronizacija ni uspela: " + helpers_1.Helpers.getErrorMessage(res.errorType));
            }
        }).catch(function (err) {
            _this._dialogService.setBusy(false);
            _this._dialogService.showMessageDialog("Napaka", "Sinhronizacija ni uspela: " + err.toString());
        });
    };
    NavigationDrawerComponent.prototype.onClose = function () {
        this.close.emit();
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], NavigationDrawerComponent.prototype, "close", void 0);
    NavigationDrawerComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-navigation-drawer',
            templateUrl: './navigation-drawer.component.html',
            styleUrls: ['./navigation-drawer.component.css']
        }),
        __metadata("design:paramtypes", [secure_storage_service_1.SecureStorageService,
            router_1.Router,
            user_service_1.UserService,
            notes_service_1.NotesService,
            dialog_service_1.DialogService])
    ], NavigationDrawerComponent);
    return NavigationDrawerComponent;
}());
exports.NavigationDrawerComponent = NavigationDrawerComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1kcmF3ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibmF2aWdhdGlvbi1kcmF3ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsc0NBQXNFO0FBQ3RFLDRFQUF1RTtBQUN2RSwwQ0FBdUM7QUFDdkMsd0RBQW9EO0FBQ3BELDBEQUFzRDtBQUV0RCw0REFBd0Q7QUFDeEQsNkNBQTBDO0FBUzFDO0lBTUksbUNBQW9CLHFCQUEyQyxFQUMzQyxPQUFlLEVBQ2YsWUFBeUIsRUFDekIsYUFBMkIsRUFDM0IsY0FBNkI7UUFKN0IsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUFzQjtRQUMzQyxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ2YsaUJBQVksR0FBWixZQUFZLENBQWE7UUFDekIsa0JBQWEsR0FBYixhQUFhLENBQWM7UUFDM0IsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFSakQsVUFBSyxHQUFHLElBQUksbUJBQVksRUFBRSxDQUFDO0lBUzNCLENBQUM7SUFFRCw0Q0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDN0QsQ0FBQztJQUVELDZDQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQsNENBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVELGlEQUFhLEdBQWI7UUFBQSxpQkFXQztRQVZHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRztZQUNyQyxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNmLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLDRCQUE0QixHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzNILENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQSxHQUFHO1lBQ1IsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbkMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsNEJBQTRCLEdBQUcsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDbkcsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsMkNBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQXZDRDtRQURDLGFBQU0sRUFBRTs7NERBQ2tCO0lBRmxCLHlCQUF5QjtRQU5yQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSx1QkFBdUI7WUFDakMsV0FBVyxFQUFFLG9DQUFvQztZQUNqRCxTQUFTLEVBQUUsQ0FBQyxtQ0FBbUMsQ0FBQztTQUNuRCxDQUFDO3lDQU82Qyw2Q0FBb0I7WUFDbEMsZUFBTTtZQUNELDBCQUFXO1lBQ1YsNEJBQVk7WUFDWCw4QkFBYTtPQVZ4Qyx5QkFBeUIsQ0EwQ3JDO0lBQUQsZ0NBQUM7Q0FBQSxBQTFDRCxJQTBDQztBQTFDWSw4REFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7U2VjdXJlU3RvcmFnZVNlcnZpY2V9IGZyb20gXCJ+L3NlcnZpY2VzL3NlY3VyZS1zdG9yYWdlLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtVc2VyU2VydmljZX0gZnJvbSBcIn4vc2VydmljZXMvdXNlci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7Tm90ZXNTZXJ2aWNlfSBmcm9tIFwifi9zZXJ2aWNlcy9ub3Rlcy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7Yn0gZnJvbSBcIkBhbmd1bGFyL2NvcmUvc3JjL3JlbmRlcjNcIjtcclxuaW1wb3J0IHtEaWFsb2dTZXJ2aWNlfSBmcm9tIFwifi9zZXJ2aWNlcy9kaWFsb2cuc2VydmljZVwiO1xyXG5pbXBvcnQge0hlbHBlcnN9IGZyb20gXCJ+L2hlbHBlcnMvaGVscGVyc1wiO1xyXG5pbXBvcnQge1N1YmplY3R9IGZyb20gXCJyeGpzL2ludGVybmFsL1N1YmplY3RcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnYXBwLW5hdmlnYXRpb24tZHJhd2VyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9uYXZpZ2F0aW9uLWRyYXdlci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9uYXZpZ2F0aW9uLWRyYXdlci5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIE5hdmlnYXRpb25EcmF3ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBjbG9zZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgICBwdWJsaWMgbG9nZ2VkSW5Vc2VyOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfc2VjdXJlU3RvcmFnZVNlcnZpY2U6IFNlY3VyZVN0b3JhZ2VTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF91c2VyU2VydmljZTogVXNlclNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9ub3Rlc1NlcnZpY2U6IE5vdGVzU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2RpYWxvZ1NlcnZpY2U6IERpYWxvZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLmxvZ2dlZEluVXNlciA9IHRoaXMuX3NlY3VyZVN0b3JhZ2VTZXJ2aWNlLmdldFVzZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkFkZE5vdGUoKSB7XHJcbiAgICAgICAgdGhpcy5fcm91dGVyLm5hdmlnYXRlQnlVcmwoXCJub3RlLy0xXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uTG9nb3V0KCkge1xyXG4gICAgICAgIHRoaXMuX3VzZXJTZXJ2aWNlLmxvZ291dCgpO1xyXG4gICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZUJ5VXJsKFwibG9naW5cIik7XHJcbiAgICB9XHJcblxyXG4gICAgb25TeW5jaHJvbml6ZSgpIHtcclxuICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNldEJ1c3kodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5fbm90ZXNTZXJ2aWNlLnN5bmNocm9uaXplKCkudGhlbihyZXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNldEJ1c3koZmFsc2UpO1xyXG4gICAgICAgICAgICBpZiAoIXJlcy5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9kaWFsb2dTZXJ2aWNlLnNob3dNZXNzYWdlRGlhbG9nKFwiTmFwYWthXCIsIFwiU2luaHJvbml6YWNpamEgbmkgdXNwZWxhOiBcIiArIEhlbHBlcnMuZ2V0RXJyb3JNZXNzYWdlKHJlcy5lcnJvclR5cGUpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2V0QnVzeShmYWxzZSk7XHJcbiAgICAgICAgICAgIHRoaXMuX2RpYWxvZ1NlcnZpY2Uuc2hvd01lc3NhZ2VEaWFsb2coXCJOYXBha2FcIiwgXCJTaW5ocm9uaXphY2lqYSBuaSB1c3BlbGE6IFwiICsgZXJyLnRvU3RyaW5nKCkpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2xvc2UoKSB7XHJcbiAgICAgICAgdGhpcy5jbG9zZS5lbWl0KCk7XHJcbiAgICB9XHJcbn0iXX0=