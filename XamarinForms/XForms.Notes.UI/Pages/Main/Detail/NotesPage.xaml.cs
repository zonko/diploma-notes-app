﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XForms.Notes.Core.ViewModels.Main.Detail;

namespace XForms.Notes.UI.Pages.Main.Detail
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	[MvxMasterDetailPagePresentation(Position = MasterDetailPosition.Detail, WrapInNavigationPage = true, NoHistory = false, Animated = false)]
    public partial class NotesPage : MvxContentPage<NotesPageViewModel>
    {
		public NotesPage ()
		{
			InitializeComponent ();
		}
	}
}