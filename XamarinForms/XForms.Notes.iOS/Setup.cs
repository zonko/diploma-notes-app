﻿using MvvmCross;
using MvvmCross.Forms.Platforms.Ios.Core;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.iOS.Services;
using XForms.Notes.UI;

namespace XForms.Notes.iOS
{
    public class Setup : MvxFormsIosSetup<Core.App, FormsApp>
    {
        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();
            Mvx.RegisterSingleton<IPlatformFunctionsService>(() => new PlatformFunctionsService());
        }
    }
}