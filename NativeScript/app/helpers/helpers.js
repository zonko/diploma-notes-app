"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var error_type_1 = require("~/models/core/error-type");
var Helpers = /** @class */ (function () {
    function Helpers() {
    }
    Helpers.getDateTimeString = function (date) {
        if (date === void 0) { date = new Date(); }
        return date.getUTCFullYear().toString() + '_' +
            this.leftPad(date.getUTCMonth() + 1, 2) + '_' +
            this.leftPad(date.getUTCDate(), 2) + '_' +
            this.leftPad(date.getUTCHours(), 2) + '_' +
            this.leftPad(date.getUTCMinutes(), 2) + '_' +
            this.leftPad(date.getUTCSeconds(), 2);
    };
    Helpers.getErrorMessage = function (errorType) {
        switch (errorType) {
            case error_type_1.ErrorType.ok: return '';
            case error_type_1.ErrorType.wsAuthorizationError: return 'Avtentikacija s spletnim srežnikom ni uspela.';
            case error_type_1.ErrorType.wsBadRequest: return 'Neznana zaheteva.';
            case error_type_1.ErrorType.wsInternalServerError: return 'Notranja napaka na strežniku.';
            case error_type_1.ErrorType.wsUsernameIsTaken: return 'Uporabniško ime je zasedeno.';
            case error_type_1.ErrorType.wsWrongUsernameOrPassword: return 'Napačno uporabniško ime ali geslo.';
            case error_type_1.ErrorType.wsServerNotReachable: return 'Strežnik ni dosegljiv.';
            case error_type_1.ErrorType.dbCreateTablesError: return 'Napaka ob ustvarjanju tabel podatkovne baze.';
            case error_type_1.ErrorType.dbNotInitialized: return 'Podatkovna baza ni inicializirana.';
            case error_type_1.ErrorType.dbOpenDbError: return 'Na podatkovno bazo se ni bilo mogoče povezati.';
            case error_type_1.ErrorType.dbUserNotLogedIn: return 'Uporabnik ni prijavljen. Podatkovne baze ni mogoče inicializirati.';
            case error_type_1.ErrorType.dbWriteError: return 'Napaka ob pisanju v podatkovno bazo.';
            case error_type_1.ErrorType.dbReadError: return 'Napaka ob branju iz podatkovne baze.';
            case error_type_1.ErrorType.notificationSchedulingError: return 'Prišlo je do napake ob nastavljanju obvestila.';
        }
    };
    Helpers.leftPad = function (input, targetLength) {
        var output = input + '';
        while (output.length < targetLength) {
            output = '0' + output;
        }
        return output;
    };
    Helpers.isNullOrEmpty = function (value) {
        return !value || value.replace(/\s/g, "") === "";
    };
    return Helpers;
}());
exports.Helpers = Helpers;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVscGVycy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImhlbHBlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx1REFBcUQ7QUFFckQ7SUFBQTtJQXlDQSxDQUFDO0lBeENpQix5QkFBaUIsR0FBL0IsVUFBZ0MsSUFBdUI7UUFBdkIscUJBQUEsRUFBQSxXQUFpQixJQUFJLEVBQUU7UUFDbkQsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxHQUFHO1lBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHO1lBQzdDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLEdBQUc7WUFDeEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsR0FBRztZQUN6QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHO1lBQzNDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO0lBQzdDLENBQUM7SUFFYSx1QkFBZSxHQUE3QixVQUE4QixTQUFvQjtRQUM5QyxNQUFNLENBQUEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2YsS0FBSyxzQkFBUyxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsRUFBRSxDQUFDO1lBQzdCLEtBQUssc0JBQVMsQ0FBQyxvQkFBb0IsRUFBRSxNQUFNLENBQUMsK0NBQStDLENBQUM7WUFDNUYsS0FBSyxzQkFBUyxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsbUJBQW1CLENBQUM7WUFDeEQsS0FBSyxzQkFBUyxDQUFDLHFCQUFxQixFQUFFLE1BQU0sQ0FBQywrQkFBK0IsQ0FBQztZQUM3RSxLQUFLLHNCQUFTLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLDhCQUE4QixDQUFDO1lBQ3hFLEtBQUssc0JBQVMsQ0FBQyx5QkFBeUIsRUFBRSxNQUFNLENBQUMsb0NBQW9DLENBQUM7WUFDdEYsS0FBSyxzQkFBUyxDQUFDLG9CQUFvQixFQUFFLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQztZQUNyRSxLQUFLLHNCQUFTLENBQUMsbUJBQW1CLEVBQUUsTUFBTSxDQUFDLDhDQUE4QyxDQUFDO1lBQzFGLEtBQUssc0JBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxNQUFNLENBQUMsb0NBQW9DLENBQUM7WUFDN0UsS0FBSyxzQkFBUyxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsZ0RBQWdELENBQUM7WUFDdEYsS0FBSyxzQkFBUyxDQUFDLGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxvRUFBb0UsQ0FBQztZQUM3RyxLQUFLLHNCQUFTLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUMzRSxLQUFLLHNCQUFTLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxzQ0FBc0MsQ0FBQztZQUMxRSxLQUFLLHNCQUFTLENBQUMsMkJBQTJCLEVBQUUsTUFBTSxDQUFDLGdEQUFnRCxDQUFDO1FBQ3hHLENBQUM7SUFDTCxDQUFDO0lBRWMsZUFBTyxHQUF0QixVQUF1QixLQUFhLEVBQUUsWUFBb0I7UUFDdEQsSUFBSSxNQUFNLEdBQUcsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUN4QixPQUFPLE1BQU0sQ0FBQyxNQUFNLEdBQUcsWUFBWSxFQUFFLENBQUM7WUFDbEMsTUFBTSxHQUFHLEdBQUcsR0FBRyxNQUFNLENBQUM7UUFDMUIsQ0FBQztRQUNELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVhLHFCQUFhLEdBQTNCLFVBQTRCLEtBQWE7UUFDckMsTUFBTSxDQUFDLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUNuRCxDQUFDO0lBRUwsY0FBQztBQUFELENBQUMsQUF6Q0QsSUF5Q0M7QUF6Q1ksMEJBQU8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFcnJvclR5cGUgfSBmcm9tIFwifi9tb2RlbHMvY29yZS9lcnJvci10eXBlXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgSGVscGVycyB7XHJcbiAgICBwdWJsaWMgc3RhdGljIGdldERhdGVUaW1lU3RyaW5nKGRhdGU6IERhdGUgPSBuZXcgRGF0ZSgpKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gZGF0ZS5nZXRVVENGdWxsWWVhcigpLnRvU3RyaW5nKCkgKyAnXycgK1xyXG4gICAgICAgICAgICB0aGlzLmxlZnRQYWQoZGF0ZS5nZXRVVENNb250aCgpICsgMSwgMikgKyAnXycgK1xyXG4gICAgICAgICAgICB0aGlzLmxlZnRQYWQoZGF0ZS5nZXRVVENEYXRlKCksIDIpICsgJ18nICtcclxuICAgICAgICAgICAgdGhpcy5sZWZ0UGFkKGRhdGUuZ2V0VVRDSG91cnMoKSwgMikgKyAnXycgK1xyXG4gICAgICAgICAgICB0aGlzLmxlZnRQYWQoZGF0ZS5nZXRVVENNaW51dGVzKCksIDIpICsgJ18nICtcclxuICAgICAgICAgICAgdGhpcy5sZWZ0UGFkKGRhdGUuZ2V0VVRDU2Vjb25kcygpLCAyKVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0RXJyb3JNZXNzYWdlKGVycm9yVHlwZTogRXJyb3JUeXBlKTogc3RyaW5nIHtcclxuICAgICAgICBzd2l0Y2goZXJyb3JUeXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgRXJyb3JUeXBlLm9rOiByZXR1cm4gJyc7XHJcbiAgICAgICAgICAgIGNhc2UgRXJyb3JUeXBlLndzQXV0aG9yaXphdGlvbkVycm9yOiByZXR1cm4gJ0F2dGVudGlrYWNpamEgcyBzcGxldG5pbSBzcmXFvm5pa29tIG5pIHVzcGVsYS4nO1xyXG4gICAgICAgICAgICBjYXNlIEVycm9yVHlwZS53c0JhZFJlcXVlc3Q6IHJldHVybiAnTmV6bmFuYSB6YWhldGV2YS4nO1xyXG4gICAgICAgICAgICBjYXNlIEVycm9yVHlwZS53c0ludGVybmFsU2VydmVyRXJyb3I6IHJldHVybiAnTm90cmFuamEgbmFwYWthIG5hIHN0cmXFvm5pa3UuJztcclxuICAgICAgICAgICAgY2FzZSBFcnJvclR5cGUud3NVc2VybmFtZUlzVGFrZW46IHJldHVybiAnVXBvcmFibmnFoWtvIGltZSBqZSB6YXNlZGVuby4nO1xyXG4gICAgICAgICAgICBjYXNlIEVycm9yVHlwZS53c1dyb25nVXNlcm5hbWVPclBhc3N3b3JkOiByZXR1cm4gJ05hcGHEjW5vIHVwb3JhYm5pxaFrbyBpbWUgYWxpIGdlc2xvLic7XHJcbiAgICAgICAgICAgIGNhc2UgRXJyb3JUeXBlLndzU2VydmVyTm90UmVhY2hhYmxlOiByZXR1cm4gJ1N0cmXFvm5payBuaSBkb3NlZ2xqaXYuJztcclxuICAgICAgICAgICAgY2FzZSBFcnJvclR5cGUuZGJDcmVhdGVUYWJsZXNFcnJvcjogcmV0dXJuICdOYXBha2Egb2IgdXN0dmFyamFuanUgdGFiZWwgcG9kYXRrb3ZuZSBiYXplLic7XHJcbiAgICAgICAgICAgIGNhc2UgRXJyb3JUeXBlLmRiTm90SW5pdGlhbGl6ZWQ6IHJldHVybiAnUG9kYXRrb3ZuYSBiYXphIG5pIGluaWNpYWxpemlyYW5hLic7XHJcbiAgICAgICAgICAgIGNhc2UgRXJyb3JUeXBlLmRiT3BlbkRiRXJyb3I6IHJldHVybiAnTmEgcG9kYXRrb3ZubyBiYXpvIHNlIG5pIGJpbG8gbW9nb8SNZSBwb3ZlemF0aS4nO1xyXG4gICAgICAgICAgICBjYXNlIEVycm9yVHlwZS5kYlVzZXJOb3RMb2dlZEluOiByZXR1cm4gJ1Vwb3JhYm5payBuaSBwcmlqYXZsamVuLiBQb2RhdGtvdm5lIGJhemUgbmkgbW9nb8SNZSBpbmljaWFsaXppcmF0aS4nO1xyXG4gICAgICAgICAgICBjYXNlIEVycm9yVHlwZS5kYldyaXRlRXJyb3I6IHJldHVybiAnTmFwYWthIG9iIHBpc2FuanUgdiBwb2RhdGtvdm5vIGJhem8uJztcclxuICAgICAgICAgICAgY2FzZSBFcnJvclR5cGUuZGJSZWFkRXJyb3I6IHJldHVybiAnTmFwYWthIG9iIGJyYW5qdSBpeiBwb2RhdGtvdm5lIGJhemUuJztcclxuICAgICAgICAgICAgY2FzZSBFcnJvclR5cGUubm90aWZpY2F0aW9uU2NoZWR1bGluZ0Vycm9yOiByZXR1cm4gJ1ByacWhbG8gamUgZG8gbmFwYWtlIG9iIG5hc3RhdmxqYW5qdSBvYnZlc3RpbGEuJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzdGF0aWMgbGVmdFBhZChpbnB1dDogbnVtYmVyLCB0YXJnZXRMZW5ndGg6IG51bWJlcik6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IG91dHB1dCA9IGlucHV0ICsgJyc7XHJcbiAgICAgICAgd2hpbGUgKG91dHB1dC5sZW5ndGggPCB0YXJnZXRMZW5ndGgpIHtcclxuICAgICAgICAgICAgb3V0cHV0ID0gJzAnICsgb3V0cHV0O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gb3V0cHV0O1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgaXNOdWxsT3JFbXB0eSh2YWx1ZTogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICF2YWx1ZSB8fCB2YWx1ZS5yZXBsYWNlKC9cXHMvZyxcIlwiKSA9PT0gXCJcIlxyXG4gICAgfVxyXG4gICAgXHJcbn0iXX0=