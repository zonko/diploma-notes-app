import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SecureStorageService} from "~/services/secure-storage.service";
import {Router} from "@angular/router";
import {UserService} from "~/services/user.service";
import {NotesService} from "~/services/notes.service";
import {b} from "@angular/core/src/render3";
import {DialogService} from "~/services/dialog.service";
import {Helpers} from "~/helpers/helpers";
import {Subject} from "rxjs/internal/Subject";

@Component({
    moduleId: module.id,
    selector: 'app-navigation-drawer',
    templateUrl: './navigation-drawer.component.html',
    styleUrls: ['./navigation-drawer.component.css']
})
export class NavigationDrawerComponent implements OnInit {
    @Output()
    close = new EventEmitter();

    public loggedInUser: string;

    constructor(private _secureStorageService: SecureStorageService,
                private _router: Router,
                private _userService: UserService,
                private _notesService: NotesService,
                private _dialogService: DialogService) {
    }

    ngOnInit() {
        this.loggedInUser = this._secureStorageService.getUser();
    }

    onAddNote() {
        this._router.navigateByUrl("note/-1");
    }

    onLogout() {
        this._userService.logout();
        this._router.navigateByUrl("login");
    }

    onSynchronize() {
        this._dialogService.setBusy(true);
        this._notesService.synchronize().then(res => {
            this._dialogService.setBusy(false);
            if (!res.success) {
                this._dialogService.showMessageDialog("Napaka", "Sinhronizacija ni uspela: " + Helpers.getErrorMessage(res.errorType));
            }
        }).catch(err => {
            this._dialogService.setBusy(false);
            this._dialogService.showMessageDialog("Napaka", "Sinhronizacija ni uspela: " + err.toString());
        });
    }

    onClose() {
        this.close.emit();
    }
}