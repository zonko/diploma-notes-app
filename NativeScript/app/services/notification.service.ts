import { Injectable } from "@angular/core";
import {DatabaseService} from "~/services/database.service";
import {OperationResult} from "~/models/core/operation-result";
import {Notification} from "~/models/core/notification";
import * as LocalNotifications from "nativescript-local-notifications";
import {SecureStorageService} from "~/services/secure-storage.service";
import {ErrorType} from "~/models/core/error-type";
import {ReceivedNotification} from "nativescript-local-notifications";
import {DialogService} from "~/services/dialog.service";
import {Router} from "@angular/router";

@Injectable()
export class NotificationService {
    constructor(private _databaseService: DatabaseService,
                private _secureStorageService: SecureStorageService,
                private _dialogService: DialogService,
                private _router: Router) {
        LocalNotifications.addOnMessageReceivedCallback((data) => this.handleNotificationReceived(data));
        this._databaseService.initializeNotificationsDatabase()
    }

    public async schedule(noteId: number, title: string, text: string, date: Date): Promise<OperationResult<null>> {
        if (date <= new Date()) return new OperationResult(true);
        const user = this._secureStorageService.getUser();
        if (!user) return new OperationResult(false, null, ErrorType.dbUserNotLogedIn);
        const getIdRes = await this._databaseService.insertNewNotification(new Notification(user, noteId));
        if (!getIdRes.success) return new OperationResult(false, null, getIdRes.errorType);

        try {
            await LocalNotifications.schedule([{
                id: getIdRes.result,
                title: title,
                body: text,
                badge: 1,
                ongoing: false,
                at: date
            }]).catch(err => {
                console.log("Obvestilo napaka:");
                console.log(err);
                throw ErrorType.notificationSchedulingError
            });
            return new OperationResult(true);
        } catch (e) {
            return new OperationResult(false, null, e);
        }
    }

    public handleNotificationReceived(data: ReceivedNotification): void {
        const user = this._secureStorageService.getUser();
        if (!user) return;

        this._databaseService.getNotification(data.id).then(res => {
            if (!res.success) {
                this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake med branjem obvestila.");
            } else if(!res.result) {
                return;
            } else if (res.result.user !== user) {
                this._dialogService.showMessageDialog("Napaka", "Obvestilo je namenjeno drugemu uporabniku.");
            } else {
                this._router.navigateByUrl("note/" + res.result.noteId);
            }
        }).catch(err => {
            this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake med branjem obvestila.");
        });
    }
}