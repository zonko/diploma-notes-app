import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ft_notes_app/globals.dart';
import 'package:ft_notes_app/logic/date_time_converter.dart';
import 'package:ft_notes_app/models/note_display.dart';
import 'package:ft_notes_app/screens/activity_indicator.dart';
import 'package:ft_notes_app/screens/drawer/navigation_drawer.dart';
import 'package:ft_notes_app/screens/note_edit/note_edit_screen.dart';
import 'package:ft_notes_app/screens/notes/notes_screen_presenter.dart';
import 'package:ft_notes_app/services/dialog_service.dart';

class NotesScreen extends StatefulWidget {
  @override
  NotesScreenState createState() => new NotesScreenState();
}

class NotesScreenState extends State<NotesScreen>
    with RouteAware
    implements NotesScreenContract {
  NotesScreenPresenter _presenter;
  List<NoteDisplay> _notes = new List<NoteDisplay>();
  final DialogService _dialogService = new DialogService();

  bool _isLoading = true;

  NotesScreenState() {
    _presenter = new NotesScreenPresenter(this);
    _presenter.loadNotes();
    
    Globals.doRefresh.listen((_) { 
      setState(() { _isLoading = true; });
      _presenter.loadNotes();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title:
                Text('Zapiski', style: TextStyle(color: Colors.orangeAccent)),
            iconTheme: IconThemeData(color: Colors.orangeAccent)),
        body: Stack(
          children: <Widget>[
            Column(children: <Widget>[
              Expanded(
                  child: RefreshIndicator(
                      onRefresh: _onRefresh,
                      child: ListView.builder(
                        itemCount: _notes.length,
                        itemBuilder: (context, index) {
                        return  Container(
                                  child:  Column(children: <Widget>[
                                    FlatButton(
                                      padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
                                      onPressed: () { _navigateToEdit(_notes[index].id); },
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(_notes[index].title),
                                              SizedBox(height: 10.0),
                                              Text(DateTimeConverter.toDateTimeDisplayString(_notes[index].dateTimeChanged))
                                            ],
                                          ),
                                          FlatButton(
                                              color: Colors.red[900],
                                              child: Text('ODSTRANI'),
                                              onPressed: () { _onRemoveItem(_notes[index]); })
                                        ],
                                    )),
                                    Container(
                                        height: 1.5,
                                        color: Colors.grey)
                                  ]));
                        },
                      )))
            ]),
            Align(
                child: ActivityIndicator(isLoading: _isLoading),
                alignment: FractionalOffset.center)
          ],
        ),
        drawer: Drawer(
          child: NavigationDrawer(),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {_navigateToEdit(null); },
          child: Icon(Icons.add, color: Colors.white, size: 30.0,),
          ),
        );
  }

  void _navigateToEdit(int id) {
    Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context) => new NoteEditScreen(id)));
  }

  void _onRemoveItem(NoteDisplay note) {
    setState(() { _isLoading = true; });
    _presenter.removeNote(note);
  }

  Future<Null> _onRefresh() async {
    await _presenter.loadNotes();
  }

  @override
  onNotesLoaded(List<NoteDisplay> notes) {
    setState(() {
      _notes = notes;
      _isLoading = false;
    });
  }

  @override
  Future<Null> onError(String errorMessage) async {
    setState(() {
      _isLoading = false;
    });
    await _dialogService.showAlert("Napaka", errorMessage, context);
  }
}
