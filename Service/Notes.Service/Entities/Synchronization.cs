﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Notes.Service.Entities
{
    [DataContract(Name = "synchronization")]
    public class Synchronization
    {
        [DataMember(Name = "lastSyncDateTime")]
        public DateTime? LastSyncDateTime { get; set; }

        [DataMember(Name = "notes")]
        public List<Note> Notes { get; set; }
    }
}