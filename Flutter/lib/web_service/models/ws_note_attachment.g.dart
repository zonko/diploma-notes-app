// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ws_note_attachment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WsNoteAttachment _$WsNoteAttachmentFromJson(Map<String, dynamic> json) {
  return WsNoteAttachment()
    ..name = json['name'] as String
    ..type = json['type'] as String
    ..content = json['content'] as String;
}

Map<String, dynamic> _$WsNoteAttachmentToJson(WsNoteAttachment instance) =>
    <String, dynamic>{
      'name': instance.name,
      'type': instance.type,
      'content': instance.content
    };
