import { SecureStorage } from "nativescript-secure-storage";
import { Injectable } from "@angular/core";

@Injectable()
export class SecureStorageService {
    private _secureStorage: SecureStorage;
    private _userTokenKey = 'ntsUserToken'
    private _userKey = 'ntsUser'

    constructor() {
        this._secureStorage = new SecureStorage()
    }

    public setUserToken(userToken: string): void {
        this._secureStorage.setSync({ key: this._userTokenKey, value: userToken });
    }

    public getUserToken(): string {
        return this._secureStorage.getSync({ key: this._userTokenKey });
    }

    public setUser(user: string): void {
        this._secureStorage.setSync({ key: this._userKey, value: user });
    }

    public getUser(): string {
        return this._secureStorage.getSync({ key: this._userKey });
    }
}