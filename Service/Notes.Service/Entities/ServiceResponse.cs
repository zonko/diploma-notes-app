﻿using System.Runtime.Serialization;

namespace Notes.Service.Entities
{
    [DataContract(Name = "serviceResponse")]
    public class ServiceResponse
    {
        [DataMember(Name = "errorType")]
        public ErrorType ErrorType { get; set; }

        [DataMember(Name = "responseMessage")]
        public string ResponseMessage { get; set; }

        [DataMember(Name = "errorMessage")]
        public string ErrorMessage { get; set; }
    }
}