import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { LoginComponent } from "~/components/login/login.component";
import { RegistrationComponent } from "~/components/registration/registration.component";
import { NotesComponent } from "~/components/notes/notes.component";
import { NoteEditComponent } from "~/components/note-edit/note-edit.component";
import {StartupComponent} from "~/components/startup/startup.component";

const routes: Routes = [
    { path: "", redirectTo: "/startup", pathMatch: "full" },
    { path: "startup", component: StartupComponent },
    { path: "login", component: LoginComponent },
    { path: "registration", component: RegistrationComponent },
    { path: "notes", component: NotesComponent },
    { path: "note/:id", component: NoteEditComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }