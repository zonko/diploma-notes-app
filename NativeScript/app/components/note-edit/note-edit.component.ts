import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Note } from '~/models/core/note';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '~/helpers/helpers';
import {NoteAttachment} from "~/models/core/note-attachment";
import {ImageAsset} from "tns-core-modules/image-asset";
import {NotesService} from "~/services/notes.service";
import {DialogService} from "~/services/dialog.service";
import {ErrorType} from "~/models/core/error-type";
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import {RadSideDrawerComponent} from "nativescript-ui-sidedrawer/angular";
import {ModalDialogService} from "nativescript-angular";
import {SetNotificationComponent} from "~/components/set-notification/set-notification.component";
import {NotificationService} from "~/services/notification.service";


//image picker
var imagepicker = require("nativescript-imagepicker");
var imageSource = require("image-source");

@Component({
    moduleId: module.id,
    selector: 'app-note-edit',
    templateUrl: './note-edit.component.html',
    styleUrls: ['./note-edit.component.css']
})
export class NoteEditComponent implements OnInit {
    note: Note =  new Note();
    private _notificationDateTime: Date;

    @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
    private _drawer: RadSideDrawer;

    constructor(private _notesService: NotesService,
                private _route: ActivatedRoute,
                private _router: Router,
                private _dialogService: DialogService,
                private _modalService: ModalDialogService,
                private _vcRef: ViewContainerRef,
                private _notificationService: NotificationService) { }

    ngOnInit() {
        this._drawer = this.drawerComponent.sideDrawer;
        const id = +this._route.snapshot.params["id"];
        if (id > 0) {
            this._dialogService.setBusy(true);
            this._notesService.getNote(id).then(res => {
                this._dialogService.setBusy(false);
                if(res.success) {
                    this.note = res.result;
                } else {
                    this._dialogService.showMessageDialog("Napaka", Helpers.getErrorMessage(res.errorType));
                }
            }).catch(err => {
                this._dialogService.setBusy(false);
                this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
            });
        }
    }

    onSetNotification() {
        let options = {
            context: {},
            fullscreen: true,
            viewContainerRef: this._vcRef
        };
        this._modalService.showModal(SetNotificationComponent, options).then(res => {
            this._notificationDateTime = res;
        });
    }

    onSave() {
        if (!this.note.title || this.note.title === '') {
            this._dialogService.showMessageDialog("Opozorilo", "Naslov je obvezen podatek.");
            return;
        }
        this._dialogService.setBusy(true);
        this._notesService.saveNote(this.note).then(res => {
            this._dialogService.setBusy(false);
            if (res.success) {
                this.finishSaving(res.result);
            } else {
                if(res.errorType < ErrorType.dbUserNotLogedIn) {
                    this._dialogService.showMessageDialog("Napaka",
                        "Prišlo je do napake med sinhronizacijo s centralo: "
                        + Helpers.getErrorMessage(res.errorType) );
                    this.finishSaving(res.result);
                } else {
                    this._dialogService.showMessageDialog("Napaka", "Zapisek ni bil shranjen: " + Helpers.getErrorMessage(res.errorType));
                }
            }
        }).catch(err => {
            this._dialogService.setBusy(false);
            this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
        });
    }

    private finishSaving(noteId: number) {
        if(this._notificationDateTime) {
            this._notificationService.schedule(noteId, this.note.title, this.note.text, this._notificationDateTime).then(res => {
                if(!res.success) {
                    console.log(res);
                    this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake, obvestilo ni bilo nastavljeno.");
                }
            }).catch(err => {
                this._dialogService.showMessageDialog("Napaka", "Prišlo je do napake, obvestilo ni bilo nastavljeno.");
            });
        }
        this._router.navigateByUrl("notes");

    }

    onAddAttachment() {
        var context = imagepicker.create({
            mode: "single"
        });

        context.authorize()
            .then(function() {
                return context.present();
            }).then(selection => {
                selection.forEach((imageAsset: ImageAsset) => {
                    imageSource.fromAsset(imageAsset).then(res => {
                        const base64 = res.toBase64String("jpeg", 100);
                        const attachment = new NoteAttachment();
                        attachment.content = base64;
                        attachment.type = "jpeg";
                        if(!this.note.attachments) this.note.attachments = new Array<NoteAttachment>();

                        this.note.attachments.push(attachment);
                    });
                });
            }).catch(err =>  {
            this._dialogService.showMessageDialog("Napaka", "Prišlo je do nepričakovane napake.");
        });

    }

    onRemoveAttachment(att: NoteAttachment) {
        const index = this.note.attachments.indexOf(att, 0);
        if (index > -1) {
            this.note.attachments.splice(index, 1);
        }
    }

    onCloseDrawer() {
        this._drawer.closeDrawer();
    }
}
