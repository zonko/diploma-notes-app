﻿using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XForms.Notes.Droid.CustomRenderers;
using XForms.Notes.UI.Controls;

[assembly: ExportRenderer(typeof(ClickFrame), typeof(ClickFrameRenderer))]
namespace XForms.Notes.Droid.CustomRenderers
{
    public class ClickFrameRenderer : FrameRenderer
    {
        public ClickFrameRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            var newElement = e.NewElement as ClickFrame;
            var oldElement = e.OldElement as ClickFrame;

            if (newElement != null && oldElement == null)
            {
                Click += newElement.OnClick;
            }

            if (newElement == null && oldElement != null)
            {
                Click -= oldElement.OnClick;
            }
        }
    }
}