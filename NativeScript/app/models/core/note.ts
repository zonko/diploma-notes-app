import { NoteAttachment } from "~/models/core/note-attachment";

export class Note {
    id: number;
    cid: number;
    title: string;
    text: string;
    dateTimeChanged: string;
    modified: boolean;
    active: boolean;
    attachments: Array<NoteAttachment>;
}