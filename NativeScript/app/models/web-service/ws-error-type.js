"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WsErrorType;
(function (WsErrorType) {
    WsErrorType[WsErrorType["ok"] = 1] = "ok";
    WsErrorType[WsErrorType["badRequest"] = 2] = "badRequest";
    WsErrorType[WsErrorType["internalServerError"] = 3] = "internalServerError";
    WsErrorType[WsErrorType["usernameIsTaken"] = 4] = "usernameIsTaken";
    WsErrorType[WsErrorType["wrongUsernameOrPassword"] = 5] = "wrongUsernameOrPassword";
    WsErrorType[WsErrorType["authorizationError"] = 6] = "authorizationError";
    WsErrorType[WsErrorType["serverNotReachable"] = 15] = "serverNotReachable";
})(WsErrorType = exports.WsErrorType || (exports.WsErrorType = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid3MtZXJyb3ItdHlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIndzLWVycm9yLXR5cGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxJQUFZLFdBUVg7QUFSRCxXQUFZLFdBQVc7SUFDbkIseUNBQU0sQ0FBQTtJQUNOLHlEQUFjLENBQUE7SUFDZCwyRUFBdUIsQ0FBQTtJQUN2QixtRUFBbUIsQ0FBQTtJQUNuQixtRkFBMkIsQ0FBQTtJQUMzQix5RUFBc0IsQ0FBQTtJQUN0QiwwRUFBdUIsQ0FBQTtBQUMzQixDQUFDLEVBUlcsV0FBVyxHQUFYLG1CQUFXLEtBQVgsbUJBQVcsUUFRdEIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBXc0Vycm9yVHlwZSB7XHJcbiAgICBvayA9IDEsXHJcbiAgICBiYWRSZXF1ZXN0ID0gMixcclxuICAgIGludGVybmFsU2VydmVyRXJyb3IgPSAzLFxyXG4gICAgdXNlcm5hbWVJc1Rha2VuID0gNCxcclxuICAgIHdyb25nVXNlcm5hbWVPclBhc3N3b3JkID0gNSxcclxuICAgIGF1dGhvcml6YXRpb25FcnJvciA9IDYsXHJcbiAgICBzZXJ2ZXJOb3RSZWFjaGFibGUgPSAxNVxyXG59Il19