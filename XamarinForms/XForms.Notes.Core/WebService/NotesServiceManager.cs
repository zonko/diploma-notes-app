﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.Models.Notes;
using XForms.Notes.Core.WebService.Models;
using XForms.Notes.Core.WebService.Models.Notes;
using XForms.Notes.Core.WebService.Models.Responses;

namespace XForms.Notes.Core.WebService
{
    public class NotesServiceManager
    {
        private readonly ISecureStorageService _secureStorageService;

        private const string BaseServiceUrl = "http://notesapp2-001-site1.1tempurl.com/NotesService.svc/";
        //private const string BaseServiceUrl = "http://192.168.0.120:62905/NotesService.svc/";
        private const string PingMethodName = "Ping";
        private const string LoginMethodName = "Login";
        private const string RegisterNewUserMethodName = "RegisterNewUser";
        private const string SynchronizeMethodName = "Synchronize";

        private const string Post = "POST";
        private const string Get = "GET";

        private static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Include,
            Formatting = Formatting.None,
            Culture = CultureInfo.InvariantCulture
        };

        public NotesServiceManager(ISecureStorageService secureStorageService)
        {
            _secureStorageService = secureStorageService;
        }

        internal async Task<string> Login(string username, string password)
        {
            var loginRequest = new WsUser
            {
                Username = username,
                Password = password
            };

            var result = await PostGet<WsResponseWithResult<string>>(string.Concat(BaseServiceUrl, LoginMethodName), Post, loginRequest);

            if (result.ErrorType != WsErrorType.Ok) throw new WsException(result.ErrorType, result.ErrorMessage, null);

            return result.Result;
        }

        internal async Task RegisterNewUser(string username, string password)
        {
            var registerNewUserRequest = new WsUser
            {
                Username = username,
                Password = password
            };

            var result = await PostGet<WsResponse>(string.Concat(BaseServiceUrl, RegisterNewUserMethodName), Post, registerNewUserRequest);

            if (result.ErrorType != WsErrorType.Ok) throw new WsException(result.ErrorType, result.ErrorMessage, null);
        }

        internal async Task<(List<Note> notes, string syncDateTime)> Synchronize(List<Note> notes, string lastSyncDateTime)
        {
            var syncRequest = new Synchronization
            {
                LastSyncDateTime = lastSyncDateTime,
                Notes = notes?.Select(n => new WsNote
                {
                    Id = n.Cid,
                    Active = n.Active,
                    ClientId = n.Id,
                    DateTimeChanged = n.DateTimeChanged.ToUniversalTime().ToString("yyyy_MM_dd_HH_mm_ss"),
                    Text = n.Text,
                    Title = n.Title,
                    Attachments = n.Attachments?.Select(a => new WsNoteAttachment
                    {
                        Name = a.Name,
                        Type = a.Type,
                        Content = a.Content == null ? string.Empty : Convert.ToBase64String(a.Content)
                    }).ToList()
                }).ToList()
            };

            var result = await PostGet<WsResponseWithResult<Synchronization>>(string.Concat(BaseServiceUrl, SynchronizeMethodName), Post, syncRequest);

            if (result.ErrorType != WsErrorType.Ok) throw new WsException(result.ErrorType, result.ErrorMessage, null);

            var notesRes = result.Result.Notes?.Select(n => new Note
            {
                Cid = n.Id,
                Active = n.Active,
                Modified = false,
                DateTimeChanged = DateTime.ParseExact(n.DateTimeChanged, "yyyy_MM_dd_HH_mm_ss", null).ToLocalTime(),
                Id = n.ClientId,
                Text = n.Text,
                Title = n.Title,
                Attachments = n.Attachments?.Select(a => new NoteAttachment
                {
                    Name = a.Name,
                    Type = a.Type,
                    Content = a.Content == null ? null : Convert.FromBase64String(a.Content)
                }).ToList()
            }).ToList();

            return (notesRes, result.Result.LastSyncDateTime);
        }

        internal async Task Ping()
        {
            try
            {
                var result = await PostGet<WsResponse>(string.Concat(BaseServiceUrl, PingMethodName), Get, null, 3000);
                if (result.ErrorType != WsErrorType.Ok) throw new WsException(result.ErrorType, result.ErrorMessage, null);
            }
            catch (Exception e)
            {
                throw new WsException(WsErrorType.ServerNotReachable, e.Message, e);
            }
        }

        internal async Task<T> PostGet<T>(string url, string method = Post, object requestObject = null, int timeout = 60000)
        {
            var request = WebRequest.Create(url);
            request.ContentType = "application/json;";
            request.Method = method;
            request.Timeout = timeout;
            var authenticationToken = await _secureStorageService.GetUserToken();
            if (!string.IsNullOrEmpty(authenticationToken))
            {
                request.Headers.Add("Authorization", authenticationToken);
            }

            if (method == Post)
            {
                var data = requestObject == null ? new byte[0] : System.Text.Encoding.UTF8.GetBytes(
                    JsonConvert.SerializeObject(requestObject, JsonSerializerSettings));
                request.ContentLength = data.Length;
                var requestStream = await request.GetRequestStreamAsync();
                requestStream.Write(data, 0, data.Length);
                requestStream.Close();
            }

            string result;
            using (var response = (HttpWebResponse)await request.GetResponseAsync())
            {
                using (var reader = new StreamReader(response.GetResponseStream() ?? throw new WebException("No response from server")))
                {
                    result = reader.ReadToEnd();
                }
                response.Close();
            }

            return JsonConvert.DeserializeObject<T>(result, JsonSerializerSettings);
        }
    }
}