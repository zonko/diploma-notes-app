﻿using System;
using System.Threading.Tasks;
using MvvmCross.Navigation;
using XForms.Notes.Core.Database;
using XForms.Notes.Core.Helpers;
using XForms.Notes.Core.Models;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.ViewModels.Main.Detail;

namespace XForms.Notes.Core.Logic
{
    public class NotificationsLogic
    {
        private readonly ILocalNotificationsService _notificationsService;
        private readonly ISecureStorageService _secureStorageService;
        private readonly IMvxNavigationService _navigationService;
        private readonly IDialogService _dialogService;
        private readonly DataAccess _dataAccess;

        public NotificationsLogic(DataAccess dataAccess, ILocalNotificationsService notificationsService,
            ISecureStorageService secureStorageService, IMvxNavigationService navigationService, IDialogService dialogService)
        {
            _dataAccess = dataAccess;
            _notificationsService = notificationsService;
            _secureStorageService = secureStorageService;
            _navigationService = navigationService;
            _dialogService = dialogService;

            _dataAccess.InitializeNotificationsDatabase();
            _notificationsService.NotificationOpened += OnNotificationOpened;
        }

        public async Task Schedule(int noteId, DateTime dateTime)
        {
            var note = await _dataAccess.GetNote(noteId);
            if(note == null) return;

            var notification = new Notification
            {
                NoteId = noteId,
                User = await _secureStorageService.GetUser()
            };

            var notificationId = _dataAccess.InsertNotification(notification);
            _notificationsService.Show(note.Title, note.Text, notificationId, dateTime);
        }

        private async void OnNotificationOpened(int notificationId)
        {
            try
            {
                var user = await _secureStorageService.GetUser();
                if (string.IsNullOrEmpty(user)) return;

                var notification = _dataAccess.GetNotification(notificationId);
                if (notification == null) return;

                if (notification.User != user)
                {
                    _dialogService.ShowAlert("Opozorilo", "Obvestilo je bilo namenjeno drugemu uporabniku.");
                    return;
                }

                await _navigationService.Navigate<NoteEditPageViewModel, int?>(notification.NoteId);
            }
            catch (Exception e)
            {
                _dialogService.ShowAlert("Napaka", "Obravnavanje obvestila ni uspelo. " + ExceptionHelper.GetErrorMessage(e));
            }
        }

    }
}