# Flutter NotesApp

Simple application for saving notes, developed using Flutter.

## Getting Started

Application was developed with Flutter and Dart programming language.
Project can be opened and run on both iOS and Android, with Android studio, 
Flutter framework must be installed beforehand. 

### Folder Content

- android: Contains Android host application (Java)
- assets: Contains common assetes
- ios: Contains iOS host application (Objective-C)
- lib: Contains actual Flutter application source (Dart)
- scr: Contains finished applications screenshots