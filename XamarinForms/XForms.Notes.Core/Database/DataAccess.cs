﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SQLite;
using SQLiteNetExtensions.Extensions;
using XForms.Notes.Core.Database.Models;
using XForms.Notes.Core.Models;
using XForms.Notes.Core.Models.Enums;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Core.Models.Notes;

// ReSharper disable ReplaceWithSingleCallToFirstOrDefault

namespace XForms.Notes.Core.Database
{
    public class DataAccess
    {
        private const string NotificationsDatabaseFilename = "NotesNotifications.db";
        private readonly ISecureStorageService _secureStorageService;
        private readonly IPlatformFunctionsService _platformFunctionsService;

        public DataAccess(ISecureStorageService secureStorageService, IPlatformFunctionsService platformFunctionsService)
        {
            _secureStorageService = secureStorageService;
            _platformFunctionsService = platformFunctionsService;
        }

        internal async Task InitializeUserDatabase()
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(await GetUserDatabaseFilename()))
            {
                connection.CreateTable<DbtKeyValue>();
                connection.CreateTable<DbtNote>();
                connection.CreateTable<DbtNoteAttachment>();
            }
        }

        internal void InitializeNotificationsDatabase()
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(NotificationsDatabaseFilename))
            {
                connection.CreateTable<DbtNotification>();
            }
        }

        #region Notes

        internal async Task SaveNotes(List<Note> notes)
        {
            if (notes == null) return;

            foreach (var note in notes)
            {
                await SaveNote(note);
            }
        }

        internal async Task<int?> SaveNote(Note note)
        {
            if (note == null) return null;

            using (var connection = _platformFunctionsService.GetDatabaseConnection(await GetUserDatabaseFilename()))
            {
                connection.BeginTransaction();
                var dbNote = new DbtNote();
                if (note.Id.HasValue)
                {
                    dbNote = connection.Find<DbtNote>(note.Id.Value) ?? dbNote;
                }
                else if (note.Cid.HasValue)
                {
                    dbNote = connection.Table<DbtNote>().Where(n => n.Cid == note.Cid).FirstOrDefault() ?? dbNote;
                }

                if (!note.Active)
                {
                    if (!dbNote.Id.HasValue) return null;

                    connection.Delete(dbNote, true);
                    connection.Commit();
                    return null;
                }

                dbNote.Cid = note.Cid;
                dbNote.Text = note.Text;
                dbNote.Title = note.Title;
                dbNote.Active = note.Active;
                dbNote.DateTimeChanged = note.DateTimeChanged;
                dbNote.Modified = note.Modified;
                dbNote.Attachments = note.Attachments?.Select(a => new DbtNoteAttachment
                {
                    Content = a.Content,
                    Name = a.Name,
                    Type = a.Type,
                    NoteId = dbNote.Id ?? default(int)
                }).ToList();

                if (dbNote.Id.HasValue)
                {
                    connection.UpdateWithChildren(dbNote);
                    connection.InsertAll(dbNote.Attachments);
                }
                else
                {
                    connection.InsertWithChildren(dbNote);
                }

                connection.Commit();
                return dbNote.Id;
            }
        }

        internal async Task DeactivateNote(int noteId)
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(await GetUserDatabaseFilename()))
            {
                var dbNote = connection.Find<DbtNote>(noteId);
                dbNote.Active = false;
                dbNote.DateTimeChanged = DateTime.Now;
                dbNote.Modified = true;
                connection.Update(dbNote);
            }
        }

        internal async Task<List<NoteDisplay>> ListNoteDisplays(string searchText = "")
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(await GetUserDatabaseFilename()))
            {
                var noSerchText = string.IsNullOrEmpty(searchText);
                return connection.Table<DbtNote>().Where(n =>
                        n.Active && (noSerchText || n.Title.Contains(searchText)))
                    .Select(n => new NoteDisplay
                    {
                        Id = n.Id.GetValueOrDefault(),
                        DateTimeChanged = n.DateTimeChanged,
                        Title = n.Title
                    }).ToList();
            }
        }

        internal async Task<List<Note>> ListModifiedNotes()
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(await GetUserDatabaseFilename()))
            {
                return connection.GetAllWithChildren<DbtNote>(n => n.Modified, true)
                    .Select(n => new Note
                    {
                        Id = n.Id,
                        DateTimeChanged = n.DateTimeChanged,
                        Title = n.Title,
                        Active = n.Active,
                        Cid = n.Cid,
                        Modified = n.Modified,
                        Text = n.Text,
                        Attachments = n.Attachments?.Select(att => new NoteAttachment
                        {
                            Content = att.Content,
                            Name = att.Name,
                            Type = att.Type
                        }).ToList()
                    }).ToList();
            }
        }

        internal async Task<Note> GetNote(int id)
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(await GetUserDatabaseFilename()))
            {
                var dbNote = connection.GetWithChildren<DbtNote>(id, true);

                return dbNote == null
                    ? null
                    : new Note
                    {
                        Id = dbNote.Id,
                        DateTimeChanged = dbNote.DateTimeChanged,
                        Title = dbNote.Title,
                        Active = dbNote.Active,
                        Cid = dbNote.Cid,
                        Modified = dbNote.Modified,
                        Text = dbNote.Text,
                        Attachments = dbNote.Attachments?.Select(att => new NoteAttachment
                        {
                            Content = att.Content,
                            Name = att.Name,
                            Type = att.Type
                        }).ToList()
                    };
            }
        }

        #endregion

        #region KeyValue

        internal async Task SetValue(KeyValueKey key, string value)
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(await GetUserDatabaseFilename()))
            {
                var dbValue = connection.Find<DbtKeyValue>((int)key) ?? new DbtKeyValue();

                dbValue.Key = (int) key;
                dbValue.Value = value;

                connection.InsertOrReplace(dbValue);
            }
        }

        internal async Task<string> GetValue(KeyValueKey key)
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(await GetUserDatabaseFilename()))
            {
                var dbValue = connection.Find<DbtKeyValue>((int)key);
                return dbValue?.Value;
            }
        }

        #endregion

        #region Notifications

        internal int InsertNotification(Notification notification)
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(NotificationsDatabaseFilename))
            {
                var dbtNotification = new DbtNotification
                {
                    User = notification.User,
                    NoteId = notification.NoteId
                };

                connection.Insert(dbtNotification);

                return dbtNotification.Id.GetValueOrDefault();
            }
        }

        internal Notification GetNotification(int id)
        {
            using (var connection = _platformFunctionsService.GetDatabaseConnection(NotificationsDatabaseFilename))
            {
                var dbtNotification = connection.Find<DbtNotification>(id);
                if (dbtNotification == null) return null;

                connection.Delete<DbtNotification>(dbtNotification.Id);

                return new Notification
                {
                    User = dbtNotification.User,
                    NoteId = dbtNotification.NoteId,
                    Id = dbtNotification.Id
                };
            }
        }

        #endregion

        internal async Task<string> GetUserDatabaseFilename()
        {
            var user = await _secureStorageService.GetUser();
            if (string.IsNullOrEmpty(user)) throw SQLiteException.New(SQLite3.Result.AccessDenied, "User is not logged in");

            return $"{user}.db";
        }
    }
}