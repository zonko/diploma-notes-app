import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ft_notes_app/app_constants.dart';
import 'package:ft_notes_app/globals.dart';
import 'package:ft_notes_app/logic/notes_logic.dart';
import 'package:ft_notes_app/logic/notifications_logic.dart';
import 'package:ft_notes_app/logic/user_logic.dart';
import 'package:ft_notes_app/screens/activity_indicator.dart';

class StartupScreen extends StatelessWidget {
  final _userLogic = new UserLogic();
  final _notesLogic = new NotesLogic();
  final _notificationsLogic = new NotificationsLogic();

  StartupScreen() {
    _init();
  }

  Future<void> _init() async {
    await _notificationsLogic.initialize();
    if (await _userLogic.checkLogin()) {
      _notesLogic.synchronize();
      Globals.navigatorKey.currentState.pushReplacementNamed(ROUTE__NOTES);
    } else {
      Globals.navigatorKey.currentState.pushReplacementNamed(ROUTE__LOGIN);
    }
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: null,
        body: Container(
          child: Align(child: ActivityIndicator(isLoading: true), alignment: FractionalOffset.center)
        ),
      );
    }
}