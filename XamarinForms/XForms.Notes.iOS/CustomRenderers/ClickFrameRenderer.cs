﻿using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using XForms.Notes.iOS.CustomRenderers;
using XForms.Notes.UI.Controls;

[assembly: ExportRenderer(typeof(ClickFrame), typeof(ClickFrameRenderer))]
namespace XForms.Notes.iOS.CustomRenderers
{
    public class ClickFrameRenderer : FrameRenderer
    {
        private readonly UITapGestureRecognizer _tapGestureRecognizer;

        public ClickFrameRenderer()
        {
            _tapGestureRecognizer = new UITapGestureRecognizer(OnTapped);
        }

        private void OnTapped()
        {
            var clickFrame = Element as ClickFrame;

            clickFrame?.OnClick(this, EventArgs.Empty);
        }


        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            var newElement = e.NewElement as ClickFrame;
            var oldElement = e.OldElement as ClickFrame;

            if (newElement != null && oldElement == null)
            {
                AddGestureRecognizer(_tapGestureRecognizer);
            }

            if (newElement == null && oldElement != null)
            {
                RemoveGestureRecognizer(_tapGestureRecognizer);
            }
        }
    }
}