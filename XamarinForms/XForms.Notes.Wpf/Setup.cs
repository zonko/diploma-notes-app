﻿using MvvmCross;
using MvvmCross.Forms.Platforms.Wpf.Core;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.UI;
using XForms.Notes.Wpf.Services;
using SecureStorageService = XForms.Notes.Wpf.Services.SecureStorageService;

namespace XForms.Notes.Wpf
{
    public class Setup : MvxFormsWpfSetup<Core.App, FormsApp>
    {
        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();
            Mvx.RegisterSingleton<IPlatformFunctionsService>(() => new PlatformFunctionsService());
            Mvx.RegisterSingleton<ISecureStorageService>(() => new SecureStorageService());
        }
    }
}