import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ft_notes_app/screens/activity_indicator.dart';
import 'package:ft_notes_app/screens/registration/registration_screen_presenter.dart';
import 'package:ft_notes_app/services/dialog_service.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  RegistrationScreenState createState() => new RegistrationScreenState();
}

class RegistrationScreenState extends State<RegistrationScreen>
    implements RegistrationScreenContract {
  RegistrationScreenPresenter _presenter;
  final DialogService _dialogService = new DialogService();
  final _formKey = new GlobalKey<FormState>();
  
  bool _isLoading = false;
  String _username;
  String _password;
  String _confirmPassword;

  RegistrationScreenState() {
    _presenter =  new RegistrationScreenPresenter(this);
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: AppBar(
        title: Text('Zapiski', style: TextStyle(color: Colors.orangeAccent)),
        iconTheme: IconThemeData(color: Colors.orangeAccent),
      ),
      body: Stack(children: <Widget>[
        Column(
          children: <Widget>[
            Expanded(
                child: ListView(
              children: <Widget>[
                SizedBox(height: 30.0),
                Image.asset('assets/notes_logo.png', height: 100.0),
                SizedBox(height: 20.0),
                Center(
                    child: Text('Registracija uporabnika',
                        style: TextStyle(
                            fontSize: 22.0, color: Colors.orangeAccent))),
                SizedBox(height: 10.0),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 15.0),
                          child: TextFormField(
                            onSaved: (val) => _username = val,
                            decoration: InputDecoration(
                              hintText: 'Uporabniško ime',
                              labelText: 'Uporabniško ime:'))),
                      Container(
                          margin: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 15.0),
                          child: TextFormField(
                            obscureText: true,
                            onSaved: (val) => _password = val,
                            decoration: InputDecoration(
                              hintText: 'Geslo', labelText: 'Geslo:'),
                          )),
                      Container(
                          margin: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 15.0),
                          child: TextFormField(
                            obscureText: true,
                            onSaved: (val) => _confirmPassword = val,
                            decoration: InputDecoration(
                                hintText: 'Geslo', labelText: 'Ponovi geslo:'),
                          ))
                    ],
                  ),
                ),
                SizedBox(height: 20.0),
              ],
            )),
            Container(
              height: 50.0,
              width: double.infinity,
              child: FlatButton(
                child: Text('REGISTRACIJA'),
                onPressed: _submit,
                color: Colors.orange,
              ),
            )
          ],
      ),
        Align(child: ActivityIndicator(isLoading: _isLoading), alignment: FractionalOffset.center)
      ]),
    );
  }

  void _submit() async{
    final form = _formKey.currentState;
    form.save();
    if (_username.isEmpty || _password.isEmpty) {
      _dialogService.showAlert('Registracija', 'Vnesite uporabniško ime in geslo.', context);
      return;
    } else if (_password != _confirmPassword) {
       await _dialogService.showAlert('Registracija', 'Geslo se ne ujema s potrditvenim geslom.', context);
       return;
    }
    setState(() => _isLoading = true);
    _presenter.registerNewUser(_username, _password);
  }

  @override
  void onRegistratioError(String errorTxt) {
    setState(() => _isLoading = false);
     _dialogService.showAlert('Napaka', errorTxt, context);
  }

  @override
  void onRegistrationSuccess() async {
    setState(() => _isLoading = false);
    await _dialogService.showAlert('Registracija', 'Registracija je bila uspešna. Sedaj se lahko prijavite v aplikacijo.', context);
    Navigator.pop(context);
  }
}
