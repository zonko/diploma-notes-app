﻿namespace XForms.Notes.Core.Models.Notes
{
    public class NoteAttachment
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public byte[] Content { get; set; }
    }
}