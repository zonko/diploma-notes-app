import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ft_notes_app/globals.dart';

class DialogService {
  // next three lines makes this class a Singleton
  static DialogService _instance = new DialogService.internal();
  DialogService.internal();
  factory DialogService() => _instance;

  Future<void> showAlert(String title, String text, BuildContext context) async {
    await showDialog(
      context: context,
      builder: (BuildContext _context) {
        return AlertDialog(
          title: Text(title, style: TextStyle(color: Colors.orange)),
          content: Text(text),
          actions: <Widget>[
            Container (
              color: Colors.orange,
              height: 40.0,
              width: 60.0,
              child: FlatButton(
                child: Text('OK', style: TextStyle(color: Colors.white)),
                onPressed: () { Navigator.of(context).pop(); },
              ),
            )
          ]
        );
      }
    );
  }
}