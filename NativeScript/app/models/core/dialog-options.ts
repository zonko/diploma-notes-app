export class DialogOptions {
    title: string;
    text: string;
    okButtonText: string;

    constructor (title: string, text: string, okButtonText: string) {
        this.title = title;
        this.text = text;
        this.okButtonText = okButtonText;
    }
}