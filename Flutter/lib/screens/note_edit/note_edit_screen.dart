import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ft_notes_app/app_constants.dart';
import 'package:ft_notes_app/globals.dart';
import 'package:ft_notes_app/models/note.dart';
import 'package:ft_notes_app/models/note_attachment.dart';
import 'package:ft_notes_app/screens/activity_indicator.dart';
import 'package:ft_notes_app/screens/note_edit/note_edit_screen_presenter.dart';
import 'package:ft_notes_app/services/dialog_service.dart';
import 'package:image_picker/image_picker.dart';

class NoteEditScreen extends StatefulWidget {
  final int _noteId;
  NoteEditScreen(this._noteId);

  @override
  NoteEditScreenState createState() => NoteEditScreenState(_noteId);
}

class NoteEditScreenState extends State<NoteEditScreen>
    implements NoteEditScreenContract {
  NoteEditScreenPresenter _presenter;

  final DialogService _dialogService = DialogService();
  final _titleEditingController = TextEditingController();
  final _textEditingController = TextEditingController();

  bool _isLoading = true;
  List<NoteAttachment> _attachments = List<NoteAttachment>();
  Note _note;
  DateTime _notificationDateTime;

  NoteEditScreenState(int noteId) {
    _presenter = NoteEditScreenPresenter(this);
    if (noteId == null) {
      _note = new Note();
      _isLoading = false;
    } else {
      _presenter.loadNote(noteId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Zapiski', style: TextStyle(color: Colors.orangeAccent)),
        iconTheme: IconThemeData(color: Colors.orangeAccent),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.notifications), onPressed: _setNotification)
        ],
      ),
      body: Stack(
        children: <Widget>[
          Column(children: <Widget>[
            Expanded(
                child: CustomScrollView(
                  scrollDirection: Axis.vertical,
              slivers: <Widget>[
                SliverPadding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                    sliver: SliverToBoxAdapter(
                      child: TextField(
                      controller: _titleEditingController,
                      decoration: InputDecoration(
                          hintText: 'Naslov', labelText: 'Naslov:'),
                    ))),
                SliverPadding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                    sliver: SliverToBoxAdapter(child: TextField(
                      keyboardType: TextInputType.multiline,
                      maxLines: 7,
                      controller: _textEditingController,
                      decoration: InputDecoration(
                          hintText: 'Besedilo', labelText: 'Besedilo:'),
                    ))),
                SliverPadding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                    sliver: SliverToBoxAdapter(child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Priponke:'),
                        Align(
                          alignment: Alignment.centerRight,
                          child: FlatButton(
                              onPressed: _addImage,
                              child: Text('DODAJ'),
                              color: Colors.orange))
                    ]))),
                    SliverFixedExtentList(
                        itemExtent: 80.0,
                        
                        delegate: SliverChildBuilderDelegate ((context, index) {
                          
                          return Container(
                            margin: EdgeInsets.all(10.0),
                            child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                              Image.memory(_attachments[index].content, height: 60.0, fit: BoxFit.scaleDown),
                              FlatButton(
                                color: Colors.red[900],
                                child: Text('ODSTRANI'),
                                onPressed: () { _onRemoveAttachment(index); })
                            ],)
                          );
                      },
                      childCount: _attachments.length))
              ],
            )),
            Container(
              height: 50.0,
              width: double.infinity,
              child: FlatButton(
                child: Text('SHRANI'),
                onPressed: _save,
                color: Colors.orange,
              ),
            )
          ]),
          Align(
              child: ActivityIndicator(isLoading: _isLoading),
              alignment: FractionalOffset.center)
        ],
      ),
    );
  }

  void _setNotification() async {
    var result = await Navigator.pushNamed(context, ROUTE__SET_NOTIFICATION);
    if (result != null) _notificationDateTime = result;
  }

  void _addImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery, maxHeight: 1000.0, maxWidth: 1000.0);
    if (image == null) return;
    
    var attachment = new NoteAttachment();
    attachment.content = image.readAsBytesSync();
    attachment.name = image.path;
    attachment.type = image.path.split('.').last;
    
    setState(() {
       _attachments.add(attachment); 
    });
  }

  void _onRemoveAttachment(int index) {
    setState(() {
      _attachments.removeAt(index);
    });
  }

  void _save() async {
    _note.title = _titleEditingController.text;
    _note.text = _textEditingController.text;
    _note.attachments = _attachments;
    if (_note.title.isEmpty) {
      await _dialogService.showAlert(
          'Opozorilo', 'Naslov je obvezen podatek.', context);
      return;
    }
    setState(() => _isLoading = true);
    await _presenter.saveNote(_note, _notificationDateTime);
  }

  @override
  onError(String errorMessage) async {
    setState(() {
      _isLoading = false;
    });
    await _dialogService.showAlert("Napaka", errorMessage, context);
  }

  @override
  onNoteLoaded(Note note) {
    setState(() {
      _note = note;
      _attachments = note.attachments == null ? new List<NoteAttachment>() : note.attachments;
      _titleEditingController.text = note.title;
      _textEditingController.text = note.text;
      _isLoading = false;
    });
  }

  @override
  onNoteSaved(String message) async {
    setState(() { _isLoading = false; });
    if (message != null) {
      await _dialogService.showAlert("Opozorilo", message, context);
    }
    Navigator.pop(context);
    Globals.doRefreshController.add(true);
  }
}
