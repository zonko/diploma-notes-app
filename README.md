# Diploma Notes App

## Repository description

This repository contains the source code used for my bachelor's thesis: 
Comparison of frameworks for concurrent cross-development of mobile applications 
for multiple platforms. 
The thesis is located here: http://eprints.fri.uni-lj.si/4318/.


## Project description

The task of my thesis was to develop the same simple mobile application using 
three different frameworks for concurrent cross-development of mobile 
applications and then to compare the development process, differences in 
implementation of key application features and the end applications where 
also a few test users tested and compared the developed applications.


Developed application is a simple app for saving notes, that allows users to:
- Register new account
- Log in 
- Add, edit and delete notes
- Add images to notes
- Set notifications for notes

And uses basic functionalities of every mobile application:
- Saving data into a local database
- Consuming a REST web service for authentication and data synchronization
- Secure data storage for sensitive data
- File/Image selection from device storage
- Display of local notifications
- Localization


### Folder content

- Flutter: Contains NotesApp source developed with Flutter
- NativeScript: Contains NotesApp source developed with NativeScript
- XamarinForms: Contains NotesApp source developed with Xamarin Forms
- Service: Contains source of the NotesApp Backend used by all developed client applications

