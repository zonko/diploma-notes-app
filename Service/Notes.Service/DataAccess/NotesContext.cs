﻿using System.Data.Entity;
using Notes.Service.DataAccess.Entities;

namespace Notes.Service.DataAccess
{
    internal class NotesContext : DbContext 
    {
        public DbSet<DbUser> Users { get; set; }
        public DbSet<DbNote> Notes { get; set; }
        public DbSet<DbNoteAttachment> Attachments { get; set; }
    }
}