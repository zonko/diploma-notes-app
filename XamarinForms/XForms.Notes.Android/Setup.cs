﻿using MvvmCross;
using MvvmCross.Forms.Platforms.Android.Core;
using XForms.Notes.Core.Models.IocInterfaces;
using XForms.Notes.Droid.Services;
using XForms.Notes.Droid.Services.Notifications;
using XForms.Notes.UI;

namespace XForms.Notes.Droid
{
    public class Setup : MvxFormsAndroidSetup<Core.App, FormsApp>
    {
        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();
            Mvx.RegisterSingleton<IPlatformFunctionsService>(() => new PlatformFunctionsService());
            Mvx.RegisterSingleton<ILocalNotificationsService>(() => new LocalNotificationsService());
        }
    }
}