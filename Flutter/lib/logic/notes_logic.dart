import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ft_notes_app/app_constants.dart';
import 'package:ft_notes_app/database/data_access.dart';
import 'package:ft_notes_app/models/error_type.dart';
import 'package:ft_notes_app/models/note.dart';
import 'package:ft_notes_app/models/note_display.dart';
import 'package:ft_notes_app/models/synchronization.dart';
import 'package:ft_notes_app/web_service/notes_web_service_manager.dart';

class NotesLogic {
   // next three lines makes this class a Singleton
  static NotesLogic _instance = new NotesLogic.internal();
  NotesLogic.internal();
  factory NotesLogic() => _instance;

  final _dataAccess = new DataAccess();
  final _serviceManager = new NotesWebServiceManager();
  final _secureStorage = new FlutterSecureStorage();

  Future<Null> synchronize() async{
    var userToken = await _secureStorage.read(key: SECURE_STORAGE_KEY__USER_TOKEN);
    if (userToken == null) throw Exception(ErrorType.dbUserNotLogedIn);
    var modifiedNotes = await _dataAccess.listModified();
    var lastSyncDateTime = await _dataAccess.getValue(DB_KEY__LAST_SYNC_DATE_TIME);
    
    var syncRequest = new Synchronization();
    syncRequest.notes = modifiedNotes;
    syncRequest.lastSyncDateTime = lastSyncDateTime;

    var result = await _serviceManager.synchronize(syncRequest, userToken);

    await _dataAccess.saveNotes(result.notes);
    await _dataAccess.setValue(DB_KEY__LAST_SYNC_DATE_TIME, result.lastSyncDateTime);
  }

  Future<Note> getNote(int id) async {
    return await _dataAccess.getNote(id);
  }

  Future<List<NoteDisplay>> listNotes() async {
    return await _dataAccess.listNotes();
  }

  Future<int> saveNote(Note note) async {
    note.dateTimeChanged = DateTime.now();
    note.modified = true;
    note.active = true;
    return await _dataAccess.saveNote(note);
  }

  Future<void> deactivateNote(int id) async {
    return await _dataAccess.deactivateNote(id);
  }

}
