import 'package:ft_notes_app/models/note.dart';

class Synchronization {
  String lastSyncDateTime;
  List<Note> notes;
}