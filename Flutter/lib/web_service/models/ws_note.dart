import 'package:ft_notes_app/logic/date_time_converter.dart';
import 'package:ft_notes_app/models/note.dart';
import 'package:ft_notes_app/web_service/models/ws_note_attachment.dart';
import 'package:queries/collections.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ws_note.g.dart';

@JsonSerializable()
class WsNote {
  int id;
  int clientId;
  bool active;
  String title;
  String text;
  String dateTimeChanged;
  List<WsNoteAttachment> attachments;

  WsNote();

  WsNote.fromNote(Note note){
    id = note.cid;
    clientId = note.id;
    active = note.active;
    title = note.title;
    text = note.text;
    dateTimeChanged = DateTimeConverter.toUtcString(note.dateTimeChanged);
    attachments = Collection(note.attachments).select<WsNoteAttachment>((a) => WsNoteAttachment.fromNoteAttachment(a)).toList();
  }

  Note getNote() {
    var note = new Note();
    note.active = active;
    note.cid = id;
    note.id = clientId;
    note.modified = false;
    note.text = text;
    note.title = title;
    note.dateTimeChanged = DateTimeConverter.parseFromUtc(dateTimeChanged);
    note.attachments = Collection(attachments).select((att) => att.getNoteAttachment()).toList();

    return note;
  }

  factory WsNote.fromJson(Map<String, dynamic> json) => _$WsNoteFromJson(json);

  toJson() => _$WsNoteToJson(this);
}