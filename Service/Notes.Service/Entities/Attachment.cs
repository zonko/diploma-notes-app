﻿using System.Runtime.Serialization;

namespace Notes.Service.Entities
{
    [DataContract(Name = "attachment")]
    public class Attachment
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "content")]
        public string Content { get; set; }
    }
}