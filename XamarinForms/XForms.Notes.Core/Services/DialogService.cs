﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MvvmCross.Commands;
using XForms.Notes.Core.Annotations;
using XForms.Notes.Core.Models.IocInterfaces;

namespace XForms.Notes.Core.Services
{
    public class DialogService : IDialogService, INotifyPropertyChanged
    {
        private bool _isBusy;
        private bool _isAlertDisplayed;
        private string _alertTitle;
        private string _alertText;
        private MvxCommand _alertConfirmedCommand;

        public event Action<bool?> SetMasterPagePresented;

        public bool IsBusy
        {
            get => _isBusy;
            set { _isBusy = value; OnPropertyChanged(); }
        }

        public bool IsAlertDisplayed
        {
            get => _isAlertDisplayed;
            set { _isAlertDisplayed = value; OnPropertyChanged(); }
        }

        public string AlertTitle
        {
            get => _alertTitle;
            set { _alertTitle = value; OnPropertyChanged(); }
        }

        public string AlertText
        {
            get => _alertText;
            set { _alertText = value; OnPropertyChanged(); }
        }

        public MvxCommand AlertConfirmedCommand => _alertConfirmedCommand ?? (_alertConfirmedCommand = new MvxCommand(AlertConfirmed));

        private void AlertConfirmed()
        {
            IsAlertDisplayed = false;
        }

        public void SetBusy(bool busy)
        {
            IsBusy = busy;
            if (IsBusy) SetMasterPagePresented?.Invoke(false);
        }

        public void ShowAlert(string title, string text)
        {
            AlertText = text;
            AlertTitle = title;
            IsAlertDisplayed = true;
            SetMasterPagePresented?.Invoke(false);
        }

        public void InvokeSetMasterPagePresented(bool? presented)
        {
            SetMasterPagePresented?.Invoke(presented);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}