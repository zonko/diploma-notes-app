"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var database_service_1 = require("~/services/database.service");
var notes_web_service_manager_service_1 = require("~/services/notes-web-service-manager.service");
var operation_result_1 = require("~/models/core/operation-result");
var ws_error_type_1 = require("~/models/web-service/ws-error-type");
var converters_1 = require("~/helpers/converters");
var error_type_1 = require("~/models/core/error-type");
var helpers_1 = require("~/helpers/helpers");
var NotesService = /** @class */ (function () {
    function NotesService(_databaseService, _wsManagerService) {
        this._databaseService = _databaseService;
        this._wsManagerService = _wsManagerService;
    }
    NotesService.prototype.synchronize = function () {
        return __awaiter(this, void 0, void 0, function () {
            var pingRes, dbNotesResult, getLastSyncDateTimeResult, wsNotes, _i, _a, note, syncResult, notes, _b, _c, wsNote, saveNotesResult;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, this._wsManagerService.ping()];
                    case 1:
                        pingRes = _d.sent();
                        if (!pingRes.success) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.wsServerNotReachable)];
                        }
                        return [4 /*yield*/, this._databaseService.listModifiedNotes()];
                    case 2:
                        dbNotesResult = _d.sent();
                        if (!dbNotesResult.success)
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, dbNotesResult.errorType)];
                        return [4 /*yield*/, this._databaseService.getValue("LastSyncDateTime")];
                    case 3:
                        getLastSyncDateTimeResult = _d.sent();
                        if (!getLastSyncDateTimeResult.success)
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, getLastSyncDateTimeResult.errorType)];
                        wsNotes = new Array();
                        if (dbNotesResult.result) {
                            for (_i = 0, _a = dbNotesResult.result; _i < _a.length; _i++) {
                                note = _a[_i];
                                wsNotes.push(converters_1.Converters.toWsNote(note));
                            }
                        }
                        return [4 /*yield*/, this._wsManagerService.synchronize(wsNotes, getLastSyncDateTimeResult.result).toPromise()];
                    case 4:
                        syncResult = _d.sent();
                        if (syncResult.errorType != ws_error_type_1.WsErrorType.ok)
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, converters_1.Converters.toCoreErrorType(syncResult.errorType))];
                        if (!(syncResult.result && syncResult.result.notes)) return [3 /*break*/, 6];
                        notes = new Array();
                        for (_b = 0, _c = syncResult.result.notes; _b < _c.length; _b++) {
                            wsNote = _c[_b];
                            notes.push(converters_1.Converters.toCoreNote(wsNote));
                        }
                        return [4 /*yield*/, this._databaseService.saveNotes(notes)];
                    case 5:
                        saveNotesResult = _d.sent();
                        if (!saveNotesResult.success)
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, saveNotesResult.errorType)];
                        _d.label = 6;
                    case 6:
                        if (!(syncResult.result && syncResult.result.lastSyncDateTime)) return [3 /*break*/, 8];
                        return [4 /*yield*/, this._databaseService.setValue("LastSyncDateTime", syncResult.result.lastSyncDateTime)];
                    case 7:
                        _d.sent();
                        _d.label = 8;
                    case 8: return [2 /*return*/, new operation_result_1.OperationResult(true)];
                }
            });
        });
    };
    NotesService.prototype.saveNote = function (note) {
        return __awaiter(this, void 0, void 0, function () {
            var saveResult, pingRes, syncResult;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        note.dateTimeChanged = helpers_1.Helpers.getDateTimeString();
                        note.modified = true;
                        note.active = true;
                        return [4 /*yield*/, this._databaseService.saveNote(note)];
                    case 1:
                        saveResult = _a.sent();
                        if (!saveResult.success)
                            return [2 /*return*/, saveResult];
                        return [4 /*yield*/, this._wsManagerService.ping()];
                    case 2:
                        pingRes = _a.sent();
                        if (!pingRes.success) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, saveResult.result, error_type_1.ErrorType.wsServerNotReachable)];
                        }
                        return [4 /*yield*/, this.synchronize()];
                    case 3:
                        syncResult = _a.sent();
                        return [2 /*return*/, new operation_result_1.OperationResult(syncResult.success, saveResult.result, syncResult.errorType, syncResult.errorMessage)];
                }
            });
        });
    };
    NotesService.prototype.deactivateNote = function (noteId) {
        return __awaiter(this, void 0, void 0, function () {
            var deactivationResult, pingRes;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._databaseService.deactivateNote(noteId)];
                    case 1:
                        deactivationResult = _a.sent();
                        if (!deactivationResult.success)
                            return [2 /*return*/, deactivationResult];
                        return [4 /*yield*/, this._wsManagerService.ping()];
                    case 2:
                        pingRes = _a.sent();
                        if (!pingRes.success) {
                            return [2 /*return*/, new operation_result_1.OperationResult(false, null, error_type_1.ErrorType.wsServerNotReachable)];
                        }
                        return [4 /*yield*/, this.synchronize()];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    NotesService.prototype.getNote = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._databaseService.getNote(id)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    NotesService.prototype.listNotes = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._databaseService.listNotes()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    NotesService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [database_service_1.DatabaseService,
            notes_web_service_manager_service_1.NotesWebServiceManagerService])
    ], NotesService);
    return NotesService;
}());
exports.NotesService = NotesService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90ZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5vdGVzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHNDQUEyQztBQUMzQyxnRUFBOEQ7QUFDOUQsa0dBQTZGO0FBQzdGLG1FQUFpRTtBQUdqRSxvRUFBaUU7QUFDakUsbURBQWtEO0FBQ2xELHVEQUFtRDtBQUVuRCw2Q0FBMEM7QUFHMUM7SUFDSSxzQkFBb0IsZ0JBQWlDLEVBQ2pDLGlCQUFnRDtRQURoRCxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWlCO1FBQ2pDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBK0I7SUFDcEUsQ0FBQztJQUVZLGtDQUFXLEdBQXhCOzs7Ozs0QkFDb0IscUJBQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxFQUFBOzt3QkFBN0MsT0FBTyxHQUFHLFNBQW1DO3dCQUNuRCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOzRCQUNuQixNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLHNCQUFTLENBQUMsb0JBQW9CLENBQUMsRUFBQzt3QkFDNUUsQ0FBQzt3QkFDcUIscUJBQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixFQUFFLEVBQUE7O3dCQUEvRCxhQUFhLEdBQUcsU0FBK0M7d0JBQ3JFLEVBQUUsQ0FBQSxDQUFDLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQzs0QkFBQyxNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLGFBQWEsQ0FBQyxTQUFTLENBQUMsRUFBQzt3QkFFMUQscUJBQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFBOzt3QkFBcEYseUJBQXlCLEdBQUcsU0FBd0Q7d0JBQzFGLEVBQUUsQ0FBQSxDQUFDLENBQUMseUJBQXlCLENBQUMsT0FBTyxDQUFDOzRCQUFDLE1BQU0sZ0JBQUMsSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUseUJBQXlCLENBQUMsU0FBUyxDQUFDLEVBQUM7d0JBRTlHLE9BQU8sR0FBRyxJQUFJLEtBQUssRUFBVSxDQUFDO3dCQUNwQyxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzs0QkFDdkIsR0FBRyxDQUFBLE9BQWlDLEVBQXBCLEtBQUEsYUFBYSxDQUFDLE1BQU0sRUFBcEIsY0FBb0IsRUFBcEIsSUFBb0I7Z0NBQTVCLElBQUk7Z0NBQ1IsT0FBTyxDQUFDLElBQUksQ0FBQyx1QkFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOzZCQUMzQzt3QkFDTCxDQUFDO3dCQUVrQixxQkFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSx5QkFBeUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBQTVHLFVBQVUsR0FBRyxTQUErRjt3QkFFbEgsRUFBRSxDQUFBLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBSSwyQkFBVyxDQUFDLEVBQUUsQ0FBQzs0QkFBQyxNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLHVCQUFVLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFDOzZCQUVqSSxDQUFBLFVBQVUsQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUEsRUFBNUMsd0JBQTRDO3dCQUN0QyxLQUFLLEdBQUcsSUFBSSxLQUFLLEVBQVEsQ0FBQzt3QkFDaEMsR0FBRyxDQUFDLE9BQXNDLEVBQXZCLEtBQUEsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQXZCLGNBQXVCLEVBQXZCLElBQXVCOzRCQUFqQyxNQUFNOzRCQUNYLEtBQUssQ0FBQyxJQUFJLENBQUMsdUJBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzt5QkFDN0M7d0JBQ3VCLHFCQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUE7O3dCQUE5RCxlQUFlLEdBQUcsU0FBNEM7d0JBQ3BFLEVBQUUsQ0FBQSxDQUFDLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQzs0QkFBQyxNQUFNLGdCQUFDLElBQUksa0NBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxTQUFTLENBQUMsRUFBQzs7OzZCQUdoRyxDQUFBLFVBQVUsQ0FBQyxNQUFNLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQSxFQUF2RCx3QkFBdUQ7d0JBQ3ZELHFCQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBOzt3QkFBNUYsU0FBNEYsQ0FBQzs7NEJBR2pHLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxJQUFJLENBQUMsRUFBQzs7OztLQUNwQztJQUVZLCtCQUFRLEdBQXJCLFVBQXNCLElBQVU7Ozs7Ozt3QkFDNUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxpQkFBTyxDQUFDLGlCQUFpQixFQUFFLENBQUM7d0JBQ25ELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO3dCQUNyQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzt3QkFDQSxxQkFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFBOzt3QkFBdkQsVUFBVSxHQUFHLFNBQTBDO3dCQUM3RCxFQUFFLENBQUEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUM7NEJBQUMsTUFBTSxnQkFBQyxVQUFVLEVBQUM7d0JBRTFCLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsRUFBQTs7d0JBQTdDLE9BQU8sR0FBRyxTQUFtQzt3QkFDbkQsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDbkIsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxNQUFNLEVBQUUsc0JBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFDO3dCQUN6RixDQUFDO3dCQUVrQixxQkFBTSxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUE7O3dCQUFyQyxVQUFVLEdBQUcsU0FBd0I7d0JBQzNDLHNCQUFPLElBQUksa0NBQWUsQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsWUFBWSxDQUFDLEVBQUE7Ozs7S0FDbkg7SUFFWSxxQ0FBYyxHQUEzQixVQUE0QixNQUFjOzs7Ozs0QkFDWCxxQkFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBdkUsa0JBQWtCLEdBQUcsU0FBa0Q7d0JBQzdFLEVBQUUsQ0FBQSxDQUFDLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDOzRCQUFDLE1BQU0sZ0JBQUMsa0JBQWtCLEVBQUM7d0JBRTFDLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsRUFBQTs7d0JBQTdDLE9BQU8sR0FBRyxTQUFtQzt3QkFDbkQsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDbkIsTUFBTSxnQkFBQyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxzQkFBUyxDQUFDLG9CQUFvQixDQUFDLEVBQUM7d0JBQzVFLENBQUM7d0JBRU0scUJBQU0sSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFBOzRCQUEvQixzQkFBTyxTQUF3QixFQUFDOzs7O0tBQ25DO0lBRVksOEJBQU8sR0FBcEIsVUFBcUIsRUFBVTs7Ozs0QkFDcEIscUJBQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBQTs0QkFBOUMsc0JBQU8sU0FBdUMsRUFBQzs7OztLQUNsRDtJQUVZLGdDQUFTLEdBQXRCOzs7OzRCQUNXLHFCQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsRUFBQTs0QkFBOUMsc0JBQU8sU0FBdUMsRUFBQzs7OztLQUNsRDtJQTdFUSxZQUFZO1FBRHhCLGlCQUFVLEVBQUU7eUNBRTZCLGtDQUFlO1lBQ2QsaUVBQTZCO09BRjNELFlBQVksQ0E4RXhCO0lBQUQsbUJBQUM7Q0FBQSxBQTlFRCxJQThFQztBQTlFWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBEYXRhYmFzZVNlcnZpY2UgfSBmcm9tIFwifi9zZXJ2aWNlcy9kYXRhYmFzZS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE5vdGVzV2ViU2VydmljZU1hbmFnZXJTZXJ2aWNlIH0gZnJvbSBcIn4vc2VydmljZXMvbm90ZXMtd2ViLXNlcnZpY2UtbWFuYWdlci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE9wZXJhdGlvblJlc3VsdCB9IGZyb20gXCJ+L21vZGVscy9jb3JlL29wZXJhdGlvbi1yZXN1bHRcIjtcclxuaW1wb3J0IHsgV3NOb3RlIH0gZnJvbSBcIn4vbW9kZWxzL3dlYi1zZXJ2aWNlL3dzLW5vdGVcIjtcclxuaW1wb3J0IHsgTm90ZSB9IGZyb20gXCJ+L21vZGVscy9jb3JlL25vdGVcIjtcclxuaW1wb3J0IHsgV3NFcnJvclR5cGUgfSBmcm9tIFwifi9tb2RlbHMvd2ViLXNlcnZpY2Uvd3MtZXJyb3ItdHlwZVwiO1xyXG5pbXBvcnQgeyBDb252ZXJ0ZXJzIH0gZnJvbSBcIn4vaGVscGVycy9jb252ZXJ0ZXJzXCI7XHJcbmltcG9ydCB7RXJyb3JUeXBlfSBmcm9tIFwifi9tb2RlbHMvY29yZS9lcnJvci10eXBlXCI7XHJcbmltcG9ydCB7Tm90ZURpc3BsYXl9IGZyb20gXCJ+L21vZGVscy9jb3JlL25vdGUtZGlzcGxheVwiO1xyXG5pbXBvcnQge0hlbHBlcnN9IGZyb20gXCJ+L2hlbHBlcnMvaGVscGVyc1wiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTm90ZXNTZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2RhdGFiYXNlU2VydmljZTogRGF0YWJhc2VTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfd3NNYW5hZ2VyU2VydmljZTogTm90ZXNXZWJTZXJ2aWNlTWFuYWdlclNlcnZpY2UpIHsgXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIHN5bmNocm9uaXplKCk6IFByb21pc2U8T3BlcmF0aW9uUmVzdWx0PG51bGw+PiB7XHJcbiAgICAgICAgY29uc3QgcGluZ1JlcyA9IGF3YWl0IHRoaXMuX3dzTWFuYWdlclNlcnZpY2UucGluZygpO1xyXG4gICAgICAgIGlmICghcGluZ1Jlcy5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBFcnJvclR5cGUud3NTZXJ2ZXJOb3RSZWFjaGFibGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBkYk5vdGVzUmVzdWx0ID0gYXdhaXQgdGhpcy5fZGF0YWJhc2VTZXJ2aWNlLmxpc3RNb2RpZmllZE5vdGVzKCk7XHJcbiAgICAgICAgaWYoIWRiTm90ZXNSZXN1bHQuc3VjY2VzcykgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIGRiTm90ZXNSZXN1bHQuZXJyb3JUeXBlKTtcclxuXHJcbiAgICAgICAgY29uc3QgZ2V0TGFzdFN5bmNEYXRlVGltZVJlc3VsdCA9IGF3YWl0IHRoaXMuX2RhdGFiYXNlU2VydmljZS5nZXRWYWx1ZShcIkxhc3RTeW5jRGF0ZVRpbWVcIik7XHJcbiAgICAgICAgaWYoIWdldExhc3RTeW5jRGF0ZVRpbWVSZXN1bHQuc3VjY2VzcykgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIGdldExhc3RTeW5jRGF0ZVRpbWVSZXN1bHQuZXJyb3JUeXBlKTtcclxuXHJcbiAgICAgICAgY29uc3Qgd3NOb3RlcyA9IG5ldyBBcnJheTxXc05vdGU+KCk7XHJcbiAgICAgICAgaWYgKGRiTm90ZXNSZXN1bHQucmVzdWx0KSB7XHJcbiAgICAgICAgICAgIGZvcihsZXQgbm90ZSBvZiBkYk5vdGVzUmVzdWx0LnJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgd3NOb3Rlcy5wdXNoKENvbnZlcnRlcnMudG9Xc05vdGUobm90ZSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzeW5jUmVzdWx0ID0gYXdhaXQgdGhpcy5fd3NNYW5hZ2VyU2VydmljZS5zeW5jaHJvbml6ZSh3c05vdGVzLCBnZXRMYXN0U3luY0RhdGVUaW1lUmVzdWx0LnJlc3VsdCkudG9Qcm9taXNlKCk7XHJcblxyXG4gICAgICAgIGlmKHN5bmNSZXN1bHQuZXJyb3JUeXBlICE9IFdzRXJyb3JUeXBlLm9rKSByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgbnVsbCwgQ29udmVydGVycy50b0NvcmVFcnJvclR5cGUoc3luY1Jlc3VsdC5lcnJvclR5cGUpKTtcclxuXHJcbiAgICAgICAgaWYgKHN5bmNSZXN1bHQucmVzdWx0ICYmIHN5bmNSZXN1bHQucmVzdWx0Lm5vdGVzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG5vdGVzID0gbmV3IEFycmF5PE5vdGU+KCk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IHdzTm90ZSBvZiBzeW5jUmVzdWx0LnJlc3VsdC5ub3Rlcykge1xyXG4gICAgICAgICAgICAgICAgbm90ZXMucHVzaChDb252ZXJ0ZXJzLnRvQ29yZU5vdGUod3NOb3RlKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3Qgc2F2ZU5vdGVzUmVzdWx0ID0gYXdhaXQgdGhpcy5fZGF0YWJhc2VTZXJ2aWNlLnNhdmVOb3Rlcyhub3Rlcyk7XHJcbiAgICAgICAgICAgIGlmKCFzYXZlTm90ZXNSZXN1bHQuc3VjY2VzcykgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQoZmFsc2UsIG51bGwsIHNhdmVOb3Rlc1Jlc3VsdC5lcnJvclR5cGUpO1xyXG4gICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc3luY1Jlc3VsdC5yZXN1bHQgJiYgc3luY1Jlc3VsdC5yZXN1bHQubGFzdFN5bmNEYXRlVGltZSkge1xyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLl9kYXRhYmFzZVNlcnZpY2Uuc2V0VmFsdWUoXCJMYXN0U3luY0RhdGVUaW1lXCIsIHN5bmNSZXN1bHQucmVzdWx0Lmxhc3RTeW5jRGF0ZVRpbWUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPcGVyYXRpb25SZXN1bHQodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFzeW5jIHNhdmVOb3RlKG5vdGU6IE5vdGUpOiBQcm9taXNlPE9wZXJhdGlvblJlc3VsdDxudW1iZXI+PiB7XHJcbiAgICAgICAgbm90ZS5kYXRlVGltZUNoYW5nZWQgPSBIZWxwZXJzLmdldERhdGVUaW1lU3RyaW5nKCk7XHJcbiAgICAgICAgbm90ZS5tb2RpZmllZCA9IHRydWU7XHJcbiAgICAgICAgbm90ZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIGNvbnN0IHNhdmVSZXN1bHQgPSBhd2FpdCB0aGlzLl9kYXRhYmFzZVNlcnZpY2Uuc2F2ZU5vdGUobm90ZSk7XHJcbiAgICAgICAgaWYoIXNhdmVSZXN1bHQuc3VjY2VzcykgcmV0dXJuIHNhdmVSZXN1bHQ7XHJcblxyXG4gICAgICAgIGNvbnN0IHBpbmdSZXMgPSBhd2FpdCB0aGlzLl93c01hbmFnZXJTZXJ2aWNlLnBpbmcoKTtcclxuICAgICAgICBpZiAoIXBpbmdSZXMuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE9wZXJhdGlvblJlc3VsdChmYWxzZSwgc2F2ZVJlc3VsdC5yZXN1bHQsIEVycm9yVHlwZS53c1NlcnZlck5vdFJlYWNoYWJsZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBzeW5jUmVzdWx0ID0gYXdhaXQgdGhpcy5zeW5jaHJvbml6ZSgpO1xyXG4gICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KHN5bmNSZXN1bHQuc3VjY2Vzcywgc2F2ZVJlc3VsdC5yZXN1bHQsIHN5bmNSZXN1bHQuZXJyb3JUeXBlLCBzeW5jUmVzdWx0LmVycm9yTWVzc2FnZSlcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgZGVhY3RpdmF0ZU5vdGUobm90ZUlkOiBudW1iZXIpOiBQcm9taXNlPE9wZXJhdGlvblJlc3VsdDxudWxsPj4ge1xyXG4gICAgICAgIGNvbnN0IGRlYWN0aXZhdGlvblJlc3VsdCA9IGF3YWl0IHRoaXMuX2RhdGFiYXNlU2VydmljZS5kZWFjdGl2YXRlTm90ZShub3RlSWQpO1xyXG4gICAgICAgIGlmKCFkZWFjdGl2YXRpb25SZXN1bHQuc3VjY2VzcykgcmV0dXJuIGRlYWN0aXZhdGlvblJlc3VsdDtcclxuXHJcbiAgICAgICAgY29uc3QgcGluZ1JlcyA9IGF3YWl0IHRoaXMuX3dzTWFuYWdlclNlcnZpY2UucGluZygpO1xyXG4gICAgICAgIGlmICghcGluZ1Jlcy5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT3BlcmF0aW9uUmVzdWx0KGZhbHNlLCBudWxsLCBFcnJvclR5cGUud3NTZXJ2ZXJOb3RSZWFjaGFibGUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGF3YWl0IHRoaXMuc3luY2hyb25pemUoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgZ2V0Tm90ZShpZDogbnVtYmVyKTogUHJvbWlzZTxPcGVyYXRpb25SZXN1bHQ8Tm90ZT4+IHtcclxuICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy5fZGF0YWJhc2VTZXJ2aWNlLmdldE5vdGUoaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBsaXN0Tm90ZXMoKTogUHJvbWlzZTxPcGVyYXRpb25SZXN1bHQ8QXJyYXk8Tm90ZURpc3BsYXk+Pj4ge1xyXG4gICAgICAgIHJldHVybiBhd2FpdCB0aGlzLl9kYXRhYmFzZVNlcnZpY2UubGlzdE5vdGVzKCk7XHJcbiAgICB9XHJcbn0iXX0=