﻿using System.Threading.Tasks;
using Xamarin.Essentials;
using XForms.Notes.Core.Models.IocInterfaces;

namespace XForms.Notes.Core.Services
{
    public class SecureStorageService : ISecureStorageService
    {
        private const string UserTokenKey = "USER_TOKEN_KEY";
        private const string UserKey = "USER_KEY";

        private string _cachedUserToken;
        private string _cachedUser;

        public async Task SaveUserToken(string userToken)
        {
            if (string.IsNullOrEmpty(userToken))
            {
                SecureStorage.Remove(UserTokenKey);
                _cachedUserToken = null;
                return;
            }
            await SecureStorage.SetAsync(UserTokenKey, userToken);
            _cachedUserToken = userToken;
        }

        public async Task SaveUser(string user)
        {
            if (string.IsNullOrEmpty(user))
            {
                SecureStorage.Remove(UserKey);
                _cachedUser = null;
                return;
            }
            await SecureStorage.SetAsync(UserKey, user);
            _cachedUser = user;
        }

        public async Task<string> GetUserToken()
        {
            return _cachedUserToken ?? await SecureStorage.GetAsync(UserTokenKey);
        }

        public async Task<string> GetUser()
        {
            return _cachedUser ?? await SecureStorage.GetAsync(UserKey);
        }
    }
}