import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'datetime'})
export class DatetimePipe implements PipeTransform {
    transform(value: string): string {

        let splDt = value.split("_");


        let date = new Date(Date.UTC(Number(splDt[0]),
            Number(splDt[1]) - 1,
            Number(splDt[2]),
            Number(splDt[3]),
            Number(splDt[4]),
            Number(splDt[5])));

        return date.getDate() + '.' +
            (date.getMonth() + 1) + '.' +
            date.getFullYear() + ' ' +
            date.getHours() + ':' +
            date.getMinutes() + ':' +
            date.getSeconds();
    }
}