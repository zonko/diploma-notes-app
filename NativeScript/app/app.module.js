"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_routing_1 = require("./app.routing");
var app_component_1 = require("./app.component");
var login_component_1 = require("~/components/login/login.component");
var user_service_1 = require("~/services/user.service");
var registration_component_1 = require("~/components/registration/registration.component");
var http_1 = require("@angular/common/http");
var notes_web_service_manager_service_1 = require("~/services/notes-web-service-manager.service");
var secure_storage_service_1 = require("~/services/secure-storage.service");
var forms_1 = require("nativescript-angular/forms");
var database_service_1 = require("~/services/database.service");
var notes_component_1 = require("~/components/notes/notes.component");
var note_edit_component_1 = require("~/components/note-edit/note-edit.component");
var notes_service_1 = require("~/services/notes.service");
var dialog_service_1 = require("~/services/dialog.service");
var startup_component_1 = require("~/components/startup/startup.component");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
var navigation_drawer_component_1 = require("~/components/navigation-drawer/navigation-drawer.component");
var datetime_pipe_1 = require("~/pipes/datetime.pipe");
var set_notification_component_1 = require("~/components/set-notification/set-notification.component");
var nativescript_angular_1 = require("nativescript-angular");
var notification_service_1 = require("~/services/notification.service");
// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";
// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpModule } from "nativescript-angular/http";
var AppModule = /** @class */ (function () {
    /*
    Pass your application module to the bootstrapModule function located in main.ts to start your app
    */
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                app_component_1.AppComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_1.AppRoutingModule,
                http_1.HttpClientModule,
                forms_1.NativeScriptFormsModule,
                angular_1.NativeScriptUISideDrawerModule
            ],
            declarations: [
                app_component_1.AppComponent,
                login_component_1.LoginComponent,
                registration_component_1.RegistrationComponent,
                notes_component_1.NotesComponent,
                note_edit_component_1.NoteEditComponent,
                startup_component_1.StartupComponent,
                navigation_drawer_component_1.NavigationDrawerComponent,
                datetime_pipe_1.DatetimePipe,
                set_notification_component_1.SetNotificationComponent
            ],
            entryComponents: [
                set_notification_component_1.SetNotificationComponent
            ],
            providers: [
                notes_web_service_manager_service_1.NotesWebServiceManagerService,
                user_service_1.UserService,
                secure_storage_service_1.SecureStorageService,
                database_service_1.DatabaseService,
                notes_service_1.NotesService,
                dialog_service_1.DialogService,
                nativescript_angular_1.ModalDialogService,
                notification_service_1.NotificationService
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
        /*
        Pass your application module to the bootstrapModule function located in main.ts to start your app
        */
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBQzlFLDZDQUFpRDtBQUNqRCxpREFBK0M7QUFFL0Msc0VBQW9FO0FBQ3BFLHdEQUFzRDtBQUN0RCwyRkFBeUY7QUFDekYsNkNBQXdEO0FBQ3hELGtHQUE2RjtBQUM3Riw0RUFBeUU7QUFDekUsb0RBQWtFO0FBQ2xFLGdFQUE4RDtBQUM5RCxzRUFBb0U7QUFDcEUsa0ZBQStFO0FBQy9FLDBEQUF3RDtBQUN4RCw0REFBMEQ7QUFDMUQsNEVBQTBFO0FBQzFFLDhEQUFrRjtBQUNsRiwwR0FBcUc7QUFDckcsdURBQW1EO0FBQ25ELHVHQUFrRztBQUNsRyw2REFBd0Q7QUFDeEQsd0VBQW9FO0FBRXBFLDJFQUEyRTtBQUMzRSx3RUFBd0U7QUFFeEUsNkVBQTZFO0FBQzdFLHNFQUFzRTtBQTRDdEU7SUFIQTs7TUFFRTtJQUNGO0lBQXlCLENBQUM7SUFBYixTQUFTO1FBMUNyQixlQUFRLENBQUM7WUFDTixTQUFTLEVBQUU7Z0JBQ1AsNEJBQVk7YUFDZjtZQUNELE9BQU8sRUFBRTtnQkFDTCx3Q0FBa0I7Z0JBQ2xCLDhCQUFnQjtnQkFDaEIsdUJBQWdCO2dCQUNoQiwrQkFBdUI7Z0JBQ3ZCLHdDQUE4QjthQUNqQztZQUNELFlBQVksRUFBRTtnQkFDViw0QkFBWTtnQkFDWixnQ0FBYztnQkFDZCw4Q0FBcUI7Z0JBQ3JCLGdDQUFjO2dCQUNkLHVDQUFpQjtnQkFDakIsb0NBQWdCO2dCQUNoQix1REFBeUI7Z0JBQ3pCLDRCQUFZO2dCQUNaLHFEQUF3QjthQUMzQjtZQUNELGVBQWUsRUFBRTtnQkFDYixxREFBd0I7YUFDM0I7WUFDRCxTQUFTLEVBQUU7Z0JBQ1AsaUVBQTZCO2dCQUM3QiwwQkFBVztnQkFDWCw2Q0FBb0I7Z0JBQ3BCLGtDQUFlO2dCQUNmLDRCQUFZO2dCQUNaLDhCQUFhO2dCQUNiLHlDQUFrQjtnQkFDbEIsMENBQW1CO2FBQ3RCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHVCQUFnQjthQUNuQjtTQUNKLENBQUM7UUFDRjs7VUFFRTtPQUNXLFNBQVMsQ0FBSTtJQUFELGdCQUFDO0NBQUEsQUFBMUIsSUFBMEI7QUFBYiw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL25hdGl2ZXNjcmlwdC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgQXBwUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL2FwcC5yb3V0aW5nXCI7XHJcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gXCIuL2FwcC5jb21wb25lbnRcIjtcclxuXHJcbmltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSBcIn4vY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwifi9zZXJ2aWNlcy91c2VyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgUmVnaXN0cmF0aW9uQ29tcG9uZW50IH0gZnJvbSBcIn4vY29tcG9uZW50cy9yZWdpc3RyYXRpb24vcmVnaXN0cmF0aW9uLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IE5vdGVzV2ViU2VydmljZU1hbmFnZXJTZXJ2aWNlIH0gZnJvbSBcIn4vc2VydmljZXMvbm90ZXMtd2ViLXNlcnZpY2UtbWFuYWdlci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFNlY3VyZVN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSBcIn4vc2VydmljZXMvc2VjdXJlLXN0b3JhZ2Uuc2VydmljZVwiO1xyXG5pbXBvcnQge05hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIlxyXG5pbXBvcnQgeyBEYXRhYmFzZVNlcnZpY2UgfSBmcm9tIFwifi9zZXJ2aWNlcy9kYXRhYmFzZS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE5vdGVzQ29tcG9uZW50IH0gZnJvbSBcIn4vY29tcG9uZW50cy9ub3Rlcy9ub3Rlcy5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgTm90ZUVkaXRDb21wb25lbnQgfSBmcm9tIFwifi9jb21wb25lbnRzL25vdGUtZWRpdC9ub3RlLWVkaXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IE5vdGVzU2VydmljZSB9IGZyb20gXCJ+L3NlcnZpY2VzL25vdGVzLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgRGlhbG9nU2VydmljZSB9IGZyb20gXCJ+L3NlcnZpY2VzL2RpYWxvZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFN0YXJ0dXBDb21wb25lbnQgfSBmcm9tIFwifi9jb21wb25lbnRzL3N0YXJ0dXAvc3RhcnR1cC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtOYXRpdmVTY3JpcHRVSVNpZGVEcmF3ZXJNb2R1bGV9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlci9hbmd1bGFyXCI7XHJcbmltcG9ydCB7TmF2aWdhdGlvbkRyYXdlckNvbXBvbmVudH0gZnJvbSBcIn4vY29tcG9uZW50cy9uYXZpZ2F0aW9uLWRyYXdlci9uYXZpZ2F0aW9uLWRyYXdlci5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtEYXRldGltZVBpcGV9IGZyb20gXCJ+L3BpcGVzL2RhdGV0aW1lLnBpcGVcIjtcclxuaW1wb3J0IHtTZXROb3RpZmljYXRpb25Db21wb25lbnR9IGZyb20gXCJ+L2NvbXBvbmVudHMvc2V0LW5vdGlmaWNhdGlvbi9zZXQtbm90aWZpY2F0aW9uLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge01vZGFsRGlhbG9nU2VydmljZX0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyXCI7XHJcbmltcG9ydCB7Tm90aWZpY2F0aW9uU2VydmljZX0gZnJvbSBcIn4vc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2VcIjtcclxuXHJcbi8vIFVuY29tbWVudCBhbmQgYWRkIHRvIE5nTW9kdWxlIGltcG9ydHMgaWYgeW91IG5lZWQgdG8gdXNlIHR3by13YXkgYmluZGluZ1xyXG4vLyBpbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xyXG5cclxuLy8gVW5jb21tZW50IGFuZCBhZGQgdG8gTmdNb2R1bGUgaW1wb3J0cyAgaWYgeW91IG5lZWQgdG8gdXNlIHRoZSBIVFRQIHdyYXBwZXJcclxuLy8gaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwXCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgYm9vdHN0cmFwOiBbXHJcbiAgICAgICAgQXBwQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcclxuICAgICAgICBBcHBSb3V0aW5nTW9kdWxlLFxyXG4gICAgICAgIEh0dHBDbGllbnRNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0VUlTaWRlRHJhd2VyTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgQXBwQ29tcG9uZW50LFxyXG4gICAgICAgIExvZ2luQ29tcG9uZW50LFxyXG4gICAgICAgIFJlZ2lzdHJhdGlvbkNvbXBvbmVudCxcclxuICAgICAgICBOb3Rlc0NvbXBvbmVudCxcclxuICAgICAgICBOb3RlRWRpdENvbXBvbmVudCxcclxuICAgICAgICBTdGFydHVwQ29tcG9uZW50LFxyXG4gICAgICAgIE5hdmlnYXRpb25EcmF3ZXJDb21wb25lbnQsXHJcbiAgICAgICAgRGF0ZXRpbWVQaXBlLFxyXG4gICAgICAgIFNldE5vdGlmaWNhdGlvbkNvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW1xyXG4gICAgICAgIFNldE5vdGlmaWNhdGlvbkNvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIE5vdGVzV2ViU2VydmljZU1hbmFnZXJTZXJ2aWNlLFxyXG4gICAgICAgIFVzZXJTZXJ2aWNlLFxyXG4gICAgICAgIFNlY3VyZVN0b3JhZ2VTZXJ2aWNlLFxyXG4gICAgICAgIERhdGFiYXNlU2VydmljZSxcclxuICAgICAgICBOb3Rlc1NlcnZpY2UsXHJcbiAgICAgICAgRGlhbG9nU2VydmljZSxcclxuICAgICAgICBNb2RhbERpYWxvZ1NlcnZpY2UsXHJcbiAgICAgICAgTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICAgXSxcclxuICAgIHNjaGVtYXM6IFtcclxuICAgICAgICBOT19FUlJPUlNfU0NIRU1BXHJcbiAgICBdXHJcbn0pXHJcbi8qXHJcblBhc3MgeW91ciBhcHBsaWNhdGlvbiBtb2R1bGUgdG8gdGhlIGJvb3RzdHJhcE1vZHVsZSBmdW5jdGlvbiBsb2NhdGVkIGluIG1haW4udHMgdG8gc3RhcnQgeW91ciBhcHBcclxuKi9cclxuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7IH1cclxuIl19