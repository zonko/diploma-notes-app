import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";

import { LoginComponent } from "~/components/login/login.component";
import { UserService } from "~/services/user.service";
import { RegistrationComponent } from "~/components/registration/registration.component";
import { HttpClientModule } from "@angular/common/http";
import { NotesWebServiceManagerService } from "~/services/notes-web-service-manager.service";
import { SecureStorageService } from "~/services/secure-storage.service";
import {NativeScriptFormsModule} from "nativescript-angular/forms"
import { DatabaseService } from "~/services/database.service";
import { NotesComponent } from "~/components/notes/notes.component";
import { NoteEditComponent } from "~/components/note-edit/note-edit.component";
import { NotesService } from "~/services/notes.service";
import { DialogService } from "~/services/dialog.service";
import { StartupComponent } from "~/components/startup/startup.component";
import {NativeScriptUISideDrawerModule} from "nativescript-ui-sidedrawer/angular";
import {NavigationDrawerComponent} from "~/components/navigation-drawer/navigation-drawer.component";
import {DatetimePipe} from "~/pipes/datetime.pipe";
import {SetNotificationComponent} from "~/components/set-notification/set-notification.component";
import {ModalDialogService} from "nativescript-angular";
import {NotificationService} from "~/services/notification.service";

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpModule } from "nativescript-angular/http";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        HttpClientModule,
        NativeScriptFormsModule,
        NativeScriptUISideDrawerModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        RegistrationComponent,
        NotesComponent,
        NoteEditComponent,
        StartupComponent,
        NavigationDrawerComponent,
        DatetimePipe,
        SetNotificationComponent
    ],
    entryComponents: [
        SetNotificationComponent
    ],
    providers: [
        NotesWebServiceManagerService,
        UserService,
        SecureStorageService,
        DatabaseService,
        NotesService,
        DialogService,
        ModalDialogService,
        NotificationService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
