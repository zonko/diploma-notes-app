﻿using SQLite;

namespace XForms.Notes.Core.Models.IocInterfaces
{
    public interface IPlatformFunctionsService
    {
        SQLiteConnection GetDatabaseConnection(string filename);
    }
}