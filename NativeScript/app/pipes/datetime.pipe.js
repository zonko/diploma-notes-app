"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DatetimePipe = /** @class */ (function () {
    function DatetimePipe() {
    }
    DatetimePipe.prototype.transform = function (value) {
        var splDt = value.split("_");
        var date = new Date(Date.UTC(Number(splDt[0]), Number(splDt[1]) - 1, Number(splDt[2]), Number(splDt[3]), Number(splDt[4]), Number(splDt[5])));
        return date.getDate() + '.' +
            (date.getMonth() + 1) + '.' +
            date.getFullYear() + ' ' +
            date.getHours() + ':' +
            date.getMinutes() + ':' +
            date.getSeconds();
    };
    DatetimePipe = __decorate([
        core_1.Pipe({ name: 'datetime' })
    ], DatetimePipe);
    return DatetimePipe;
}());
exports.DatetimePipe = DatetimePipe;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXRpbWUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRhdGV0aW1lLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxzQ0FBb0Q7QUFHcEQ7SUFBQTtJQW9CQSxDQUFDO0lBbkJHLGdDQUFTLEdBQVQsVUFBVSxLQUFhO1FBRW5CLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFHN0IsSUFBSSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQ3pDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQ3BCLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFDaEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUNoQixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQ2hCLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFdkIsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxHQUFHO1lBQ3ZCLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUc7WUFDM0IsSUFBSSxDQUFDLFdBQVcsRUFBRSxHQUFHLEdBQUc7WUFDeEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLEdBQUc7WUFDckIsSUFBSSxDQUFDLFVBQVUsRUFBRSxHQUFHLEdBQUc7WUFDdkIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFuQlEsWUFBWTtRQUR4QixXQUFJLENBQUMsRUFBQyxJQUFJLEVBQUUsVUFBVSxFQUFDLENBQUM7T0FDWixZQUFZLENBb0J4QjtJQUFELG1CQUFDO0NBQUEsQUFwQkQsSUFvQkM7QUFwQlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AUGlwZSh7bmFtZTogJ2RhdGV0aW1lJ30pXHJcbmV4cG9ydCBjbGFzcyBEYXRldGltZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nKTogc3RyaW5nIHtcclxuXHJcbiAgICAgICAgbGV0IHNwbER0ID0gdmFsdWUuc3BsaXQoXCJfXCIpO1xyXG5cclxuXHJcbiAgICAgICAgbGV0IGRhdGUgPSBuZXcgRGF0ZShEYXRlLlVUQyhOdW1iZXIoc3BsRHRbMF0pLFxyXG4gICAgICAgICAgICBOdW1iZXIoc3BsRHRbMV0pIC0gMSxcclxuICAgICAgICAgICAgTnVtYmVyKHNwbER0WzJdKSxcclxuICAgICAgICAgICAgTnVtYmVyKHNwbER0WzNdKSxcclxuICAgICAgICAgICAgTnVtYmVyKHNwbER0WzRdKSxcclxuICAgICAgICAgICAgTnVtYmVyKHNwbER0WzVdKSkpO1xyXG5cclxuICAgICAgICByZXR1cm4gZGF0ZS5nZXREYXRlKCkgKyAnLicgK1xyXG4gICAgICAgICAgICAoZGF0ZS5nZXRNb250aCgpICsgMSkgKyAnLicgK1xyXG4gICAgICAgICAgICBkYXRlLmdldEZ1bGxZZWFyKCkgKyAnICcgK1xyXG4gICAgICAgICAgICBkYXRlLmdldEhvdXJzKCkgKyAnOicgK1xyXG4gICAgICAgICAgICBkYXRlLmdldE1pbnV0ZXMoKSArICc6JyArXHJcbiAgICAgICAgICAgIGRhdGUuZ2V0U2Vjb25kcygpO1xyXG4gICAgfVxyXG59Il19