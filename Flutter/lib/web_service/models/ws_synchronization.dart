import 'package:ft_notes_app/web_service/models/ws_note.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ws_synchronization.g.dart';

@JsonSerializable()
class WsSynchronization {
  String lastSyncDateTime;
  List<WsNote> notes;

  WsSynchronization();

  factory WsSynchronization.fromJson(Map<String, dynamic> json) => _$WsSynchronizationFromJson(json);

  toJson() => _$WsSynchronizationToJson(this);
}