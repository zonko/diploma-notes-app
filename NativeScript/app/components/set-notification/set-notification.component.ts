import {Component, OnInit} from '@angular/core';
import {ModalDialogParams} from "nativescript-angular";
import {DatePicker} from "tns-core-modules/ui/date-picker";
import {TimePicker} from "tns-core-modules/ui/time-picker";

@Component({
    moduleId: module.id,
    selector: 'app-set-notification',
    templateUrl: './set-notification.component.html',
    styleUrls: ['./set-notification.component.css']
})
export class SetNotificationComponent implements OnInit {
    private _selectedDateTime = new Date(Date.now());

    constructor(private params: ModalDialogParams) {
    }

    ngOnInit() {

    }

    public onClose() {
        this.params.closeCallback(null);
    }

    public onConfirm() {
        this.params.closeCallback(this._selectedDateTime);
    }


    onDatePickerLoaded(args) {
        let datePicker = <DatePicker>args.object;
        datePicker.date = this._selectedDateTime;
    }

    onDateChanged(args) {
        this._selectedDateTime.setFullYear(args.value.getFullYear());
        this._selectedDateTime.setMonth(args.value.getMonth());
        this._selectedDateTime.setDate(args.value.getDate());
    }

    onTimePickerLoaded(args) {
        let timePicker = <TimePicker>args.object;
        timePicker.time = this._selectedDateTime;
    }

    onTimePickerTimeChanged(args) {
        this._selectedDateTime.setHours(args.value.getHours());
        this._selectedDateTime.setMinutes(args.value.getMinutes());
    }
}
