import 'dart:async';

import 'package:ft_notes_app/helpers/error_message_helper.dart';
import 'package:ft_notes_app/logic/notes_logic.dart';
import 'package:ft_notes_app/models/note_display.dart';

abstract class NotesScreenContract {
  onNotesLoaded(List<NoteDisplay> notes);
  Future<Null> onError(String errorMessage);
}

class NotesScreenPresenter {
  NotesScreenContract _view;
  NotesLogic _notesLogic =  new NotesLogic();

  NotesScreenPresenter(this._view);

  loadNotes() async{
    try{
      var notes = await _notesLogic.listNotes();
      _view.onNotesLoaded(notes);
    } catch (error) {
      _view.onError(error.toString());
    }
  }

  removeNote(NoteDisplay note) async {
    try {
      await _notesLogic.deactivateNote(note.id);
      try {
        await _notesLogic.synchronize();
      } catch (error) {
        await _view.onError("Zapisek je bil izbrisan v lokalni podatkovni bazi, sinhronizacija s strežnikom pa ni bila uspešna."  + ErrorMessageHelper.getErrorMessage(error));
      }
      await loadNotes();
    } catch (error) {
      _view.onError("Brisanje zapiska ni uspelo. "  + ErrorMessageHelper.getErrorMessage(error));
    }
  }
 
}